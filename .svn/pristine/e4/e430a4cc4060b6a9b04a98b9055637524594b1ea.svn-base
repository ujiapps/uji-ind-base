Ext.define('APA.controller.ControllerAplicacionesMapeos',
{
    extend : 'Ext.app.Controller',
    stores : [ 'Aplicaciones', 'AplicacionesMapeos' ],
    requires : [ 'Ext.ux.uji.form.LookupWindow' ],
    model : [ 'Aplicacion', 'AplicacionMapeo' ],

    refs : [
    {
        selector : 'comboaplicaciones',
        ref : 'comboAplicaciones'
    },
    {
        selector : 'gridaplicacionesmapeos',
        ref : 'gridAplicacionesMapeos'
    },
    {
        selector : 'formaplicacionesmapeos',
        ref : 'formAplicacionesMapeos'
    } ],

    init : function()
    {
        this.control(
        {
            'panelaplicacionesmapeos comboaplicaciones' :
            {
                change : this.onChangeAplicacion
            },

            'gridaplicacionesmapeos' :
            {
                beforeselect : this.onBeforeSelectRow,
                deselect : this.onDeselectRow,
                celldblclick : this.onClickRow
            },

            'formaplicacionesmapeos' :
            {
                afterrender : this.onRenderForm
            },

            'formaplicacionesmapeos textfield[name=personaInId]' :
            {
                change : this.onChangeTextFieldPersona
            },

            'formaplicacionesmapeos textfield[name=personaOutId]' :
            {
                change : this.onChangeTextFieldPersona
            },

            'formaplicacionesmapeos button[action=submit]' :
            {
                click : this.submitForm
            },

            'formaplicacionesmapeos lookupcombobox[name=personaInNombre]' :
            {
                LookoupWindowClickSeleccion : this.onSelectResponsableIn
            },

            'formaplicacionesmapeos lookupcombobox[name=personaOutNombre]' :
            {
                LookoupWindowClickSeleccion : this.onSelectResponsableOut
            }
        });
    },

    onChangeAplicacion : function(ref, newValue, oldValue, eOpts)
    {
        this.changeAplicacion(newValue);
    },

    changeAplicacion : function(newValue)
    {
        this.getGridAplicacionesMapeos().loadData(newValue);
        this.getFormAplicacionesMapeos().setUrl(newValue);

        if (newValue !== -1)
        {
            this.getGridAplicacionesMapeos().enable();
            this.getFormAplicacionesMapeos().enable();
        }
    },

    onRenderForm : function()
    {
        this.changeAplicacion(-1);
        this.getFormAplicacionesMapeos().setPersona(perId, "In");
    },

    onSelectResponsableIn : function(persona)
    {
        this.selectResponsable(persona, 'In');
    },

    onSelectResponsableOut : function(persona)
    {
        this.selectResponsable(persona, 'Out');
    },

    selectResponsable : function(persona, context)
    {
        this.getFormAplicacionesMapeos().setPersona(persona.getId(), context);
    },

    onClickRow : function(grid, td, cellIndex, record, tr, rowIndex, e)
    {
        this.getFormAplicacionesMapeos().setData(record.data.personaInId, record.data.personaOutId, record.data.motivo)
    },

    onBeforeSelectRow : function(rowModel, record, index)
    {
        var ref = this;

        Ext.MessageBox.prompt(
        {
            title : 'Motiu',
            msg : 'Motiu del mapeig:',
            width : 300,
            buttons : Ext.MessageBox.OKCANCEL,
            multiline : true,
            value : record.getData().motivo,
            fn : function(btn, txt)
            {
                if (btn === 'ok' && txt !== '')
                {
                    ref.requestSelection(record.getId(), txt, true, function(grid)
                    {
                        grid.reload();
                    });
                }
            }
        });

        return false;
    },

    onDeselectRow : function(rowModel, record, index)
    {
        this.requestSelection(record.getId(), record.data.motivo, false, null);
    },

    requestSelection : function(id, motivo, select, fn)
    {
        var grid = this.getGridAplicacionesMapeos();

        Ext.Ajax.request(
        {
            url : grid.getStore().getProxy().url + id,
            method : 'PUT',
            params :
            {
                activo : select,
                motivo : motivo
            },
            success : function()
            {
                if (fn)
                {
                    fn(grid);
                }
            },
            failure : function(result, request)
            {
                Ext.Msg.alert('Error', "S'ha produït una errada");
                grid.reload();
            }
        });
    },

    onChangeTextFieldPersona : function(textField, newValue, oldValue)
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/apa/rest/persona',
            method : 'POST',
            params :
            {
                query : newValue
            },
            success : function(r)
            {
                var responseJSON = Ext.JSON.decode(r.responseText);
                var msg = responseJSON.success ? responseJSON.msg : '';

                ref.getFormAplicacionesMapeos().setPersonaFromSearch(textField.context, msg);
            },
            failure : function(result, request)
            {
                Ext.Msg.alert('Error', "S'ha produït una errada");
            }
        });
    },

    submitForm : function()
    {
        var ref = this;
        var form = this.getFormAplicacionesMapeos();

        form.getForm().submit(
        {
            url : form.url,
            method : 'POST',
            success : function(form, action)
            {
                ref.getGridAplicacionesMapeos().reload();
            }
        });
    }
});