package es.uji.apps.ind.formula;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Variable;

public class VariableCheckerTest
{
    private VariableChecker variableChecker;
    private VariableExtractor variableExtractor;
    private List<String> variablesCode = new ArrayList<String>()
    {
        {
            add("A");
            add("B");
        }
    };
    private String formula = "a + b";

    @Before
    public void init()
    {
        variableExtractor = mock(VariableExtractor.class);
        variableChecker = new VariableChecker(variableExtractor);
    }

    @Test(expected = VariablesNoExistentesException.class)
    public void siAlgunaVariableNoEstaDefinidaDebeLanzarseUnaExcepcion()
            throws VariablesDiferentesTiposException, VariablesNoExistentesException
    {
        List<Variable> variables = new ArrayList<>();

        Variable variable = new Variable();
        variable.setNivelAgregacion("nivel1");
        variable.setComponenteTemporal(1);

        variables.add(variable);

        when(variableExtractor.extract("a + b")).thenReturn(variablesCode);

        variableChecker.checkFormula(formula, variables);
    }

    @Test(expected = VariablesDiferentesTiposException.class)
    public void siLasVariablesNoSonDelMismoTipoDebeLanzarseUnaExcepcion()
            throws VariablesDiferentesTiposException, VariablesNoExistentesException
    {
        List<Variable> variables = new ArrayList<>();

        Variable variable1 = new Variable();
        variable1.setNivelAgregacion("nivel1");
        variable1.setComponenteTemporal(1);

        Variable variable2 = new Variable();
        variable1.setNivelAgregacion("nivel2");
        variable1.setComponenteTemporal(2);

        variables.add(variable1);
        variables.add(variable2);

        when(variableExtractor.extract("a + b")).thenReturn(variablesCode);

        variableChecker.checkFormula(formula, variables);
    }
}
