package es.uji.apps.ind.formula;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.ParseException;
import bsh.TargetError;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;

@Component
public class FormulaEvaluator
{
    private Interpreter interpreter;

    public FormulaEvaluator()
    {
        interpreter = new Interpreter();
    }

    public Double eval(String formula) throws ErrorEvaluacionFormulaException
    {
        Double result = 0.0D;

        try
        {
            DecimalFormat df = new DecimalFormat("#"+ DecimalFormatSymbols.getInstance().getDecimalSeparator()+"####");
            Double val = df.parse(interpreter.eval(formula).toString()).doubleValue();
            result = Double.valueOf(df.format(val));
        }
        catch (TargetError e)
        {
            throw new ErrorEvaluacionFormulaException("Errada en la execucuó de la fòrmula: "
                    + e.getTarget());
        }
        catch (ParseException e)
        {
            throw new ErrorEvaluacionFormulaException("Errada sintáctica de la fòrmula: "
                    + e.getMessage());
        }
        catch (EvalError e)
        {
            throw new ErrorEvaluacionFormulaException();
        }
        catch (Exception e)
        {
            throw new ErrorEvaluacionFormulaException();
        }

        if (result == Float.NEGATIVE_INFINITY || result == Float.POSITIVE_INFINITY)
        {
            throw new ErrorEvaluacionFormulaException(
                    "Errada en la execucuó de la fòrmula: resultat infinit, possible divisió per 0");
        }

        return result;
    }
}
