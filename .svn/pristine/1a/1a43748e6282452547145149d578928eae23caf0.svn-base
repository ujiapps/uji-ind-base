package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.GrupoDAO;
import es.uji.apps.ind.model.Grupo;
import es.uji.apps.ind.model.Indice;

@Service
public class GrupoService
{
    private GrupoDAO grupoDAO;

    @Autowired
    public GrupoService(GrupoDAO grupoDAO)
    {
        this.grupoDAO = grupoDAO;
    }

    public Grupo getGrupo(Long id)
    {
        List<Grupo> grupos = grupoDAO.get(Grupo.class, id);

        if (grupos != null && !grupos.isEmpty())
        {
            return grupos.get(0);
        }

        return null;
    }

    public Grupo addGrupo(Grupo grupo)
    {
        return grupoDAO.insert(grupo);
    }

    public Grupo updateGrupo(Grupo grupo)
    {
        return grupoDAO.update(grupo);
    }

    public void deleteGrupo(Long id)
    {
        grupoDAO.delete(Grupo.class, id);
    }

    public List<Grupo> getAllGrupos()
    {
        return grupoDAO.get(Grupo.class);
    }
}
