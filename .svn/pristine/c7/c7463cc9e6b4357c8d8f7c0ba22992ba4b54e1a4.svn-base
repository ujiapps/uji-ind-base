package es.uji.apps.ind.formula;

import java.util.Collections;

import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.FormulaContieneCaracteresNoPermitidosException;
import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;

public class FormulaCheckerTest
{
    private FormulaChecker formulaChecker;
    private VariableChecker variableChecker;
    private VariableExtractor variableExtractor;
    private VariableReplacer variableReplacer;
    private FormulaEvaluator formulaEvaluator;

    @Before
    public void init()
    {
        variableChecker = mock(VariableChecker.class);

        variableExtractor = new VariableExtractor();
        variableReplacer = new VariableReplacer();
        formulaEvaluator = new FormulaEvaluator();

        formulaChecker = new FormulaChecker(variableChecker, formulaEvaluator, variableExtractor,
                variableReplacer);
    }

    @Test(expected = FormulaContieneCaracteresNoPermitidosException.class)
    public void siExistenCaracteresNoPermitidosEnUnaFormulaSeDebeLanzarUnaExcepcion() throws Exception
    {
        formulaChecker.check("abc,a", Collections.EMPTY_LIST);
    }

    @Test
    public void formulaConCaracteresPermitidos() throws Exception
    {
        formulaChecker.check("abc3 + 12.3 -(5 + ((n))) - a_b", Collections.EMPTY_LIST);
        formulaChecker.check("a", Collections.EMPTY_LIST);
    }
}
