package es.uji.apps.ind.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.FormulaException;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.formula.FormulaChecker;
import es.uji.apps.ind.formula.FormulaEvaluator;
import es.uji.apps.ind.formula.VariableChecker;
import es.uji.apps.ind.formula.VariableExtractor;
import es.uji.apps.ind.formula.VariableReplacer;

@Component
@Entity
@Table(name = "IND_INDICADORES")
public class Indicador
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String codigo;
    private String denominacion;
    private String responsable;
    private String formula;
    private String observaciones;

    @Column(name = "CONCEPTO_A_EVALUAR")
    private String conceptoAEvaluar;

    @Column(name = "VALOR_REFERENCIA")
    private String valorReferencia;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorUso> usos;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorAgrupacion> agrupaciones;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorMarca> marcas;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<VariableIndicador> variables;

    @Transient
    private VariableChecker variableChecker;

    @Transient
    private VariableExtractor variableExtractor;

    @Transient
    private VariableReplacer variableReplacer;

    @Transient
    private FormulaEvaluator formulaEvaluator;

    @Transient
    private FormulaChecker formulaChecker;

    @Autowired
    public void setVariableReplacer(VariableReplacer variableReplacer)
    {
        this.variableReplacer = variableReplacer;
    }

    @Autowired
    public void setFormulaEvaluator(FormulaEvaluator formulaEvaluator)
    {
        this.formulaEvaluator = formulaEvaluator;
    }

    @Autowired
    public void setFormulaChecker(FormulaChecker formulaChecker)
    {
        this.formulaChecker = formulaChecker;
    }

    @Autowired
    public void setVariableChecker(VariableChecker variableChecker)
    {
        this.variableChecker = variableChecker;
    }

    @Autowired
    public void setVariableExtractor(VariableExtractor variableExtractor)
    {
        this.variableExtractor = variableExtractor;
    }

    public Indicador()
    {
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getDenominacion()
    {
        return denominacion;
    }

    public void setDenominacion(String denominacion)
    {
        this.denominacion = denominacion;
    }

    public String getResponsable()
    {
        return responsable;
    }

    public void setResponsable(String responsable)
    {
        this.responsable = responsable;
    }

    public String getConceptoAEvaluar()
    {
        return conceptoAEvaluar;
    }

    public void setConceptoAEvaluar(String conceptoAEvaluar)
    {
        this.conceptoAEvaluar = conceptoAEvaluar;
    }

    public String getFormula()
    {
        return formula;
    }

    public void setFormula(String formula)
    {
        this.formula = formula;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public Set<IndicadorUso> getUsos()
    {
        return usos;
    }

    public void setUsos(Set<IndicadorUso> usos)
    {
        this.usos = usos;
    }

    public Set<IndicadorAgrupacion> getAgrupaciones()
    {
        return agrupaciones;
    }

    public void setAgrupaciones(Set<IndicadorAgrupacion> agrupaciones)
    {
        this.agrupaciones = agrupaciones;
    }

    public Set<IndicadorMarca> getMarcas()
    {
        return marcas;
    }

    public void setMarcas(Set<IndicadorMarca> marcas)
    {
        this.marcas = marcas;
    }

    public Set<VariableIndicador> getVariables()
    {
        return variables;
    }

    public void setVariables(Set<VariableIndicador> variables)
    {
        this.variables = variables;
    }

    public String getValorReferencia()
    {
        return valorReferencia;
    }

    public void setValorReferencia(String valorReferencia)
    {
        this.valorReferencia = valorReferencia;
    }

    private VariableExtractor getVariableExtractor()
    {
        if (variableExtractor == null)
        {
            variableExtractor = new VariableExtractor();
        }

        return variableExtractor;
    }

    private VariableReplacer getVariableReplacer()
    {
        if (variableReplacer == null)
        {
            variableReplacer = new VariableReplacer();
        }

        return variableReplacer;
    }

    private VariableChecker getVariableChecker()
    {
        if (variableChecker == null)
        {
            variableChecker = new VariableChecker(getVariableExtractor());
        }

        return variableChecker;
    }

    private FormulaEvaluator getFormulaEvaluator()
    {
        if (formulaEvaluator == null)
        {
            formulaEvaluator = new FormulaEvaluator();
        }

        return formulaEvaluator;
    }

    private FormulaChecker getFormulaChecker()
    {
        if (formulaChecker == null)
        {
            formulaChecker = new FormulaChecker(getVariableChecker(), getFormulaEvaluator(),
                    getVariableExtractor(), getVariableReplacer());
        }

        return formulaChecker;
    }

    public List<VariableValor> calcula(List<VariableValorForCalculus> valoresForCalculus)
    {
        List<VariableValor> resultados = new ArrayList<>();

        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            VariableValor valor = new VariableValor();

            valor.setAnyo(valorForCalculus.getAnyo());
            valor.setValorInternual(valorForCalculus.getValorInteranual());
            valor.setAgregacion(valorForCalculus.getAgregacion());

            try
            {
                String valorReemplazado = getVariableReplacer().replace(getFormula(),
                        valorForCalculus.getValores());
                Float resultadoEvaluacion = getFormulaEvaluator().eval(valorReemplazado);
                valor.setValor(resultadoEvaluacion.toString());
            }
            catch (Exception e)
            {
            }

            resultados.add(valor);
        }

        return resultados;
    }

    public List<String> getVariablesFormula()
    {
        return getVariableExtractor().extract(getFormula());
    }

    public void compruebaFormula(List<Variable> variables) throws FormulaException,
            VariableException
    {
        getFormulaChecker().check(getFormula(), variables);
    }

    public Indicador duplicar(Indicador indicador)
    {
        indicador.setId(null);
        indicador.setVariables(null);

        for (IndicadorUso indicadorUso : indicador.getUsos())
        {
            indicadorUso.setId(null);
            indicadorUso.setIndicador(indicador);
        }

        for (IndicadorAgrupacion indicadorAgrupacion : indicador.getAgrupaciones())
        {
            indicadorAgrupacion.setId(null);
            indicadorAgrupacion.setIndicador(indicador);
        }

        return indicador;
    }
}
