package es.uji.apps.ind.model;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import es.uji.apps.ind.formula.VariableValorForCalculusConversor;
import es.uji.apps.ind.model.enums.NivelAgregacion;

public class IndicadorTest
{
    Variable variable1, variable2;
    VariableValorForCalculusConversor variableValorForCalculusConversor = new VariableValorForCalculusConversor();
    List<VariableValorForCalculus> valoresForCalculus = new ArrayList<>();

    @Test
    public void debenCalcularseCorrectamenteLosResultados() throws Exception
    {
        variable1 = buildVariable("A1");
        variable2 = buildVariable("A2");

        addVariableValorToVariable(variable1, buildVariableValor("1", 1, 1, "1"));
        addVariableValorToVariable(variable1, buildVariableValor("1", 2, 3, "1"));

        addVariableValorToVariable(variable2, buildVariableValor("1", 1, 1, "1"));
        addVariableValorToVariable(variable2, buildVariableValor("1", 2, 3, "2"));
        addVariableValorToVariable(variable2, buildVariableValor("1", 3, 1, "1"));

        Indicador indicador = new Indicador();
        indicador.setFormula("A1 + A2");

        valoresForCalculus = variableValorForCalculusConversor.add(valoresForCalculus,
                new ArrayList<>(variable1.getValores()), variable1);

        valoresForCalculus = variableValorForCalculusConversor.add(valoresForCalculus,
                new ArrayList<>(variable2.getValores()), variable2);

        List<VariableValor> resultado = indicador.calcula(valoresForCalculus);

        assertTrue(resultado.size() == 3);
        assertTrue(get(resultado, "1", 1, 1).getValor().equals("2.0"));
        assertTrue(get(resultado, "1", 2, 3).getValor().equals("3.0"));
        assertTrue(get(resultado, "1", 3, 1).getValor() == null);
    }

    private VariableValor get(List<VariableValor> valores, String agregacion, Integer anyo,
            Integer mes)
    {
        for (VariableValor valor : valores)
        {
            if (valor.getAgregacion().equals(agregacion) && valor.getAnyo().equals(anyo)
                    && valor.getMes().equals(mes))
            {
                return valor;
            }
        }

        return null;
    }

    private Variable buildVariable(String codigo) throws Exception
    {
        Variable variable = new Variable();

        variable.setComponenteTemporal(1);
        variable.setNivelAgregacion(NivelAgregacion.CENTRE.toString());
        variable.setCodigo(codigo);

        return variable;
    }

    private VariableValor buildVariableValor(String agregacion, Integer anyo, Integer mes,
            String valor)
    {
        VariableValor variableValor = new VariableValor();

        variableValor.setAgregacion(agregacion);
        variableValor.setAnyo(anyo);
        variableValor.setMes(mes);
        variableValor.setValor(valor);

        return variableValor;
    }

    private void addVariableValorToVariable(Variable variable, VariableValor variableValor)
    {
        Set<VariableValor> valores = variable.getValores();

        if (valores == null)
        {
            valores = new HashSet<>();
        }

        valores.add(variableValor);
        variable.setValores(valores);
        variableValor.setVariable(variable);
    }
}
