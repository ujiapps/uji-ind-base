package es.uji.apps.ind.services.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.model.Agrupacion;
import es.uji.apps.ind.model.Uso;
import es.uji.apps.ind.services.AgrupacionService;
import es.uji.commons.rest.UIEntity;

@Path("agrupaciones")
public class AgrupacionResource
{
    @InjectParam
    private AgrupacionService agrupacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Agrupacion agrupacion : agrupacionService.getAllAgrupaciones())
        {
            entities.add(UIEntity.toUI(agrupacion));
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(agrupacionService.getAgrupacion(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        Agrupacion agrupacion = entity.toModel(Agrupacion.class);

        agrupacionService.addAgrupacion(agrupacion);

        return UIEntity.toUI(agrupacion);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(UIEntity entity)
    {
        Agrupacion agrupacion = entity.toModel(Agrupacion.class);

        agrupacionService.updateAgrupacion(agrupacion);

        return UIEntity.toUI(agrupacion);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        agrupacionService.deleteAgrupacion(id);

        return Response.noContent().build();
    }
}
