package es.uji.apps.ind.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.ArrayUtils;
import es.uji.apps.ind.dao.IndicadorDAO;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.FormulaException;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.VariableValorForCalculus;

@Service
public class IndicadorService
{
    private IndicadorDAO indicadorDAO;

    private VariableService variableService;

    @Autowired
    public IndicadorService(IndicadorDAO indicadorDAO, VariableService variableService)
    {
        this.indicadorDAO = indicadorDAO;
        this.variableService = variableService;
    }

    public Indicador getIndicador(Long id)
    {
        return indicadorDAO.getById(id);
    }

    public Indicador addOrUpdateIndicador(Indicador indicador) throws VariableException,
            FormulaException
    {
        Long id = indicador.getId();

        List<String> variablesCode = indicador.getVariablesFormula();
        List<Variable> variables = variableService.getAllVariablesByCodes(variablesCode);

        indicador.compruebaFormula(variables);

        if (id != null)
        {
            return indicadorDAO.update(indicador);
        }

        return indicadorDAO.insert(indicador);
    }

    public void deleteIndicador(Long id)
    {
        indicadorDAO.delete(Indicador.class, id);
    }

    public List<Indicador> getAllIndicadores()
    {
        return indicadorDAO.get(Indicador.class);
    }

    public void duplicarIndicador(Long id) throws VariableException, FormulaException
    {
        Indicador indicador = getIndicador(id);
        addOrUpdateIndicador(indicador.duplicar(indicador));
    }

    public String getNivelAgregacion(Indicador indicador) throws VariablesNoExistentesException
    {
        List<String> variableCodes = indicador.getVariablesFormula();

        if (variableCodes.size() > 0)
        {
            Variable variable = variableService.getVariableByCode(variableCodes.get(0));

            if (variable == null)
            {
                throw new VariablesNoExistentesException();
            }

            return variable.getNivelAgregacion();
        }

        return null;
    }

    public List<String> getDistinctComponentesTemporalesFromIndicador(Indicador indicador)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        List<Float> componentesTemporales = variableService
                .getDistinctComponentesTemporalesFromVariableList(indicador.getVariablesFormula());

        return ArrayUtils.floatArrayToString(componentesTemporales);
    }

    public List<VariableValor> calculaIndicador(Indicador indicador, String agregacion,
            Float componenteTemporal) throws ErrorEjecucionSQLException
    {
        List<VariableValorForCalculus> valoresForCalculus = new ArrayList<>();

        for (String variableCode : indicador.getVariablesFormula())
        {
            Variable variable = variableService.getVariableByCode(variableCode);

            List<VariableValor> valores = getValores(variable, agregacion, componenteTemporal);

            for (VariableValor valor : valores)
            {
                VariableValorForCalculus valorForCalculus = getIfExists(valoresForCalculus, valor);

                if (valorForCalculus == null)
                {
                    valorForCalculus = new VariableValorForCalculus(valor.getAgregacion(),
                            valor.getComponenteTemporal());

                    valoresForCalculus.add(valorForCalculus);
                }

                valorForCalculus.addValue(variable.getCodigo(), Float.parseFloat(valor.getValor()));
            }
        }

        return indicador.calcula(valoresForCalculus);

    }

    private List<VariableValor> getValores(Variable variable, String agregacion,
            Float componenteTemporal) throws ErrorEjecucionSQLException
    {
        return variableService.getFilteredVariablesValorFromVariableId(variable.getId(), agregacion,
                componenteTemporal);
    }

    private VariableValorForCalculus getIfExists(List<VariableValorForCalculus> valoresForCalculus,
            VariableValor valor)
    {
        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            if (valorForCalculus.getAgregacion().equals(valor.getAgregacion())
                    && valorForCalculus.getComponenteTemporal().equals(
                            valor.getComponenteTemporal()))
            {
                return valorForCalculus;
            }
        }

        return null;
    }
}
