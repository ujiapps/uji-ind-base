var NUM_MESES_ANYO = 12;

Ext.define('IND.controller.ControllerVariables',
{
    extend : 'Ext.app.Controller',

    stores : [ 'Variables', 'NivelesAgregaciones', 'TiposAnualidad', 'VariablesValores' ],

    refs : [
    {
        selector : 'gridVariables',
        ref : 'gridVariables'
    },
    {
        selector : 'gridVariablesValores',
        ref : 'gridVariablesValores'
    },
    {
        selector : 'windowVariables',
        ref : 'windowVariables'
    } ],

    init : function()
    {
        this.control(
        {
            'gridVariables button[action=add]' :
            {
                click : this.onButtonAddVariable
            },
            'gridVariables' :
            {
                itemdblclick : this.onItemDblCLick,
                select : this.onSelectGridVariable
            },
            'windowVariables checkbox[name=dinamica]' :
            {
                change : this.onChangeCheck
            },
            'windowVariables button[action=close]' :
            {
                click : this.onWindowClose
            },
            'windowVariables button[action=save]' :
            {
                click : this.onWindowSave
            },
            'windowVariables field[name=numMeses]' :
            {
                change : this.onChangeNumMeses
            },
            'gridVariablesValores button[action=view]' :
            {
                click : this.onButtonViewValores
            }
        });

        this.window = Ext.create('IND.view.variable.WindowVariable');
    },

    onButtonViewValores : function()
    {
        var grid = this.getGridVariablesValores();
        var variable = this.getGridVariables().getSelectedRow();

        grid.loadData(this.getGridVariables().getSelectedId(), variable.data.nivelAgregacion)
        grid.disableColumnsAndDefaultButtons(false);

        if (variable.data.dinamica)
        {
            grid.disableDefaultButtons(true);
            grid.allowEdit = false;
        }
    },

    onChangeNumMeses : function(ref, newValue, oldValue)
    {
        var tipoAnualidadField = this.getWindowVariables().down('field[name=tipoAnualidad]');

        if (newValue && (newValue % NUM_MESES_ANYO) === 0)
        {
            tipoAnualidadField.setDisabled(false);
            return;
        }

        tipoAnualidadField.setDisabled(true);
        tipoAnualidadField.setValue('');
    },

    onItemDblCLick : function(ref, record, item, index, e, eOpts)
    {
        this.window.load(record.data.id);
        this.window.show();
    },

    onSelectGridVariable : function(ref, record, index, eOpts)
    {
        var variableId = record.data.id;

        if (variableId)
        {
            var grid = this.getGridVariablesValores();

            grid.allowEdit = true;
            grid.setUrl(variableId);
            grid.clearStore();
            grid.disableColumnsAndDefaultButtons(true);
        }
    },

    onChangeCheck : function(cb, checked)
    {
        this.window.down('textarea').setDisabled(!checked)
    },

    onButtonAddVariable : function()
    {
        this.window.show();
    },

    onWindowClose : function()
    {
        this.window.clearAndHide();
    },

    onWindowSave : function()
    {
        var window = this.window;
        var ref = this;

        window.save(function()
        {
            window.clearAndHide.call(window);
            ref.getGridVariables().reloadData()
        });
    }
});