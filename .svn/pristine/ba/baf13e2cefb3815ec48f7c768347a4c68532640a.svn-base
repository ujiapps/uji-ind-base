package es.uji.apps.ind.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Component;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.model.QVariableValor;
import es.uji.apps.ind.model.VariableValor;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Component
public class VariableValorDAO extends BaseDAODatabaseImpl
{
    private QVariableValor variableValor = QVariableValor.variableValor;

    public List<VariableValor> getByVariableId(Long variableId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(variableValor).where(variableValor.variable.id.eq(variableId));

        return query.list(variableValor);
    }

    public List<VariableValor> executeSelect(String sql) throws ErrorEjecucionSQLException
    {
        List<VariableValor> valores = new ArrayList<>();
        List<Object[]> resultadoSelect;

        try
        {
            resultadoSelect = entityManager.createNativeQuery(sql).getResultList();
        }
        catch (JpaSystemException e)
        {
            throw new ErrorEjecucionSQLException(e.getCause().getCause().getCause().toString());
        }
        catch (Exception e)
        {
            throw new ErrorEjecucionSQLException();
        }

        for (Object[] filaSelect : resultadoSelect)
        {
            VariableValor variableValor = new VariableValor();

            try
            {
                variableValor.setAgregacion(String.valueOf(filaSelect[0]));
                variableValor.setComponenteTemporal(Float.valueOf(String.valueOf(filaSelect[1])));
                variableValor.setValor(String.valueOf(filaSelect[2]));
            }
            catch (Exception e)
            {
                throw new ErrorEjecucionSQLException(
                        "Errada extraent les dades dels resultats. El segon resultat ha de ser un número decimal. Tots són obligatoris");
            }

            valores.add(variableValor);
        }

        return valores;
    }

    public List<Float> getDistinctComponentesTemporales(List<String> variables)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(variableValor).where(
                variableValor.variable.codigo.in(variables).and(
                        variableValor.variable.dinamica.eq(0)));

        return query.distinct().list(variableValor.componenteTemporal);
    }
}
