package es.uji.apps.apa.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.apa.dao.AplicacionMapeoDAO;
import es.uji.apps.apa.exceptions.PersonaNoEncontradaException;
import es.uji.apps.apa.model.Aplicacion;
import es.uji.apps.apa.model.AplicacionMapeo;
import es.uji.apps.apa.model.Persona;

@Service
public class AplicacionMapeoService
{
    private PersonaService personaService;
    private AplicacionMapeoDAO aplicacionMapeoDAO;

    @Autowired
    public AplicacionMapeoService(AplicacionMapeoDAO aplicacionMapeoDAO,
            PersonaService personaService)
    {
        this.aplicacionMapeoDAO = aplicacionMapeoDAO;
        this.personaService = personaService;
    }

    public List<AplicacionMapeo> getLastByAplicacionId(Long aplicacionId)
    {
        return aplicacionMapeoDAO.getLastByAplicacionId(aplicacionId);
    }

    public List<AplicacionMapeo> getMapeosActivosInByPersonaId(Long personaId)
    {
        return aplicacionMapeoDAO.getMapeosInActivosByPersonaId(personaId);
    }

    public List<AplicacionMapeo> getMapeosActivosOutByPersonaId(Long personaId)
    {
        return aplicacionMapeoDAO.getMapeosOutActivosByPersonaId(personaId);
    }

    public AplicacionMapeo getById(Long aplicacionMapeoId)
    {
        return aplicacionMapeoDAO.get(AplicacionMapeo.class, aplicacionMapeoId).get(0);
    }

    public void checkAndInsert(String personaInId, String personaOutId, String motivo,
            Long aplicacionId) throws PersonaNoEncontradaException
    {
        Persona personaIn = personaService.searchBy(personaInId);
        Persona personaOut = personaService.searchBy(personaOutId);

        if (personaIn.getId() == null)
        {
            throw new PersonaNoEncontradaException(
                    "No s'ha pogut trobar cap persona amb aquest texte: " + personaInId);
        }

        if (personaOut.getId() == null)
        {
            throw new PersonaNoEncontradaException(
                    "No s'ha pogut trobar cap persona amb aquest texte: " + personaOutId);
        }

        insert(personaIn.getId(), personaOut.getId(), motivo, aplicacionId);
    }

    @Transactional
    public void insert(Long personaInId, Long personaOutId, String motivo, Long aplicacionId)
    {
        AplicacionMapeo aplicacionMapeo = new AplicacionMapeo();

        aplicacionMapeo.setFecha(new Date());
        aplicacionMapeo.setActivo(1);
        aplicacionMapeo.setMotivo(motivo);

        setRelationWithPersonaIn(personaInId, aplicacionMapeo);
        setRelationWithPersonaOut(personaOutId, aplicacionMapeo);
        setRelationWithAplicacion(aplicacionId, aplicacionMapeo);

        aplicacionMapeoDAO.setAllToNoActiveByAplicacionIdAndPersonaId(aplicacionId, personaInId);

        aplicacionMapeoDAO.insert(aplicacionMapeo);
    }

    public void update(Boolean activo, String motivo, Long aplicacionMapeoId)
    {
        AplicacionMapeo aplicacionMapeo = getById(aplicacionMapeoId);

        if (!activo)
        {
            aplicacionMapeo = getById(aplicacionMapeoId);

            aplicacionMapeo.setActivo(0);

            aplicacionMapeoDAO.update(aplicacionMapeo);

            return;
        }

        insert(aplicacionMapeo.getPersonaIn().getId(), aplicacionMapeo.getPersonaOut().getId(),
                motivo, aplicacionMapeo.getAplicacion().getId());
    }

    private void setRelationWithPersonaIn(Long personaId, AplicacionMapeo aplicacionMapeo)
    {
        Persona persona = new Persona(personaId);
        aplicacionMapeo.setPersonaIn(persona);
    }

    private void setRelationWithPersonaOut(Long personaId, AplicacionMapeo aplicacionMapeo)
    {
        Persona persona = new Persona(personaId);
        aplicacionMapeo.setPersonaOut(persona);
    }

    private void setRelationWithAplicacion(Long aplicacionId, AplicacionMapeo aplicacionMapeo)
    {
        Aplicacion aplicacion = new Aplicacion(aplicacionId);
        aplicacionMapeo.setAplicacion(aplicacion);
    }
}
