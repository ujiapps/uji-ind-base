package es.uji.apps.ind.formula;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;

public class FormulaEvaluatorTest
{
    FormulaEvaluator formulaEvaluator = new FormulaEvaluator();

    @Before
    public void init()
    {
    }

    @Test
    public void debeDePoderEvaluarseUnaFormulaSimple() throws ErrorEvaluacionFormulaException
    {
        String formula = "1 + 1";

        Assert.assertTrue(2D == formulaEvaluator.eval(formula));
    }

    @Test
    public void debeDePoderEvaluarseUnaFormulaCompleja() throws ErrorEvaluacionFormulaException
    {
        String formula = "(((4 + 1) * 2)/2) - 1";

        Assert.assertTrue(4D == formulaEvaluator.eval(formula));
    }

    @Test(expected = ErrorEvaluacionFormulaException.class)
    public void debeProducirseUnErrorSiLaFormulaNoEsSintacticamenteCorrecta()
            throws ErrorEvaluacionFormulaException
    {
        formulaEvaluator.eval("(1 + 1");
    }

    @Test(expected = ErrorEvaluacionFormulaException.class)
    public void debeProducirseUnErrorSiLaFormulaCausaUnaExcepcion()
            throws ErrorEvaluacionFormulaException
    {
        formulaEvaluator.eval("1 / 0");
    }

    @Test(expected = ErrorEvaluacionFormulaException.class)
    public void debeProducirseUnErrorSiLaFormulaProduceUnResultadoInfinito()
            throws ErrorEvaluacionFormulaException
    {
        formulaEvaluator.eval("1.0 / 0");
    }

    @Test(expected = ErrorEvaluacionFormulaException.class)
    public void debeProducirseUnErrorSiLaFormulaDevuelveAlgoQueNoEsUnNumero()
            throws ErrorEvaluacionFormulaException
    {
        formulaEvaluator.eval("a");
    }
}
