package es.uji.apps.ind.formula;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.VariablesSinValorException;

@Component
public class VariableReplacer
{
    private String expresion;
    private Map<String, Float> valores;

    private Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
    private Matcher matcher;
    private List<String> allMatches = new ArrayList<>();

    public VariableReplacer()
    {
    }

    public VariableReplacer(String expresion, Map<String, Float> valores)
    {
        super();
        this.expresion = expresion;
        this.valores = valores;
    }

    public String replace() throws VariablesSinValorException
    {
        matcher = pattern.matcher(expresion);
        StringBuilder builder = new StringBuilder();

        int i = 0;
        while (matcher.find())
        {
            Float replacement = valores.get(matcher.group());

            builder.append(expresion.substring(i, matcher.start()));

            if (replacement == null)
            {
                throw new VariablesSinValorException();
            }
            else
            {
                builder.append(replacement);
            }

            i = matcher.end();
        }
        builder.append(expresion.substring(i, expresion.length()));

        return builder.toString();
    }

    public String replace(String expresion, Map<String, Float> valores)
            throws VariablesSinValorException
    {
        this.expresion = expresion;
        this.valores = valores;

        return replace();
    }
}
