package es.uji.apps.ind.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.Agregacion;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorMarcaResultado;
import es.uji.apps.ind.model.IndicadorPorTipo;
import es.uji.apps.ind.model.ValorInteranual;
import es.uji.apps.ind.model.enums.TipoMarca;
import es.uji.apps.ind.services.AgregacionesService;
import es.uji.apps.ind.services.AgrupadorIndicadoresPorTipoService;
import es.uji.apps.ind.services.IndicadorAgrupacionService;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.apps.ind.services.IndicadorUsoService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("calculus")
public class CalculusResource
{
    @InjectParam
    private IndicadorService indicadorService;

    @InjectParam
    private IndicadorUsoService indicadorUsoService;

    @InjectParam
    private IndicadorAgrupacionService indicadorAgrupacionService;

    @InjectParam
    private AgregacionesService agregacionesService;

    @InjectParam
    private AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService;

    @POST
    @Path("calculated-data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCalculatedData(List<UIEntity> entities)
            throws ErrorEvaluacionFormulaException, VariablesSinValorException,
            ErrorEjecucionSQLException
    {
        List<UIEntity> completeCalculatedDataListEntity = new ArrayList<>();

        for (UIEntity entity : entities)
        {
            Long id = ParamUtils.parseLong(entity.get("id"));
            String agregacion = normalizeId(entity.get("agregacion"));
            Integer anyo = toInteger(normalizeId(entity.get("anyo")));
            Long valorInteranual = toLong(normalizeId(entity.get("valorInteranual").toString()));
            String cache = entity.get("cache").toString();

            List<UIEntity> calculatedDataListEntity = new ArrayList<>();
            List<IndicadorMarcaResultado> list = indicadorService
                    .calculaIndicador(indicadorService.getIndicador(id), agregacion, anyo, valorInteranual, cache);

            for (IndicadorMarcaResultado indicadorMarcaResultado : list)
            {
                UIEntity uiEntity = new UIEntity();
                uiEntity.put("id", indicadorMarcaResultado.getId());
                uiEntity.put("agregacion", indicadorMarcaResultado.getAgregacion());
                uiEntity.put("anyo", indicadorMarcaResultado.getAnyo());
                uiEntity.put("valorInteranual", indicadorMarcaResultado.getValorInteranual());
                uiEntity.put("valor", indicadorMarcaResultado.getValor());
                uiEntity.put("indicadorId", id);
                calculatedDataListEntity.add(uiEntity);
            }
            completeCalculatedDataListEntity.addAll(calculatedDataListEntity);
        }

        return completeCalculatedDataListEntity;
    }

    @POST
    @Path("save-calculated-data")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage saveCalculatedData(UIEntity bigEntity) throws ErrorEjecucionSQLException, VariablesSinValorException
    {
        UIEntity subUIEntity = bigEntity.getRelations().get("data").get(0); // Consultar esto a
        // Ricardo

        String marca = subUIEntity.get("marca");
        List<UIEntity> entities = subUIEntity.getRelations().get("list");

        ParamUtils.checkNotNull(marca);

        for (UIEntity entity : entities)
        {
            Long id = ParamUtils.parseLong(entity.get("id"));
            String agregacion = normalizeId(entity.get("agregacion"));
            Integer anyo = toInteger(normalizeId(entity.get("anyo")));
            Long valorInteranual = toLong(normalizeId(entity.get("valorInteranual").toString()));

            indicadorService.guardaResultadosCalculoIndicador(indicadorService.getIndicador(id),
                    agregacion, anyo, valorInteranual, marca, TipoMarca.MARCA.name());

        }

        return new ResponseMessage(true);
    }

    private Integer toInteger(String id)
    {
        return (id == null ? null : new Integer(id));
    }

    private Long toLong(String id)
    {
        return (id == null ? null : new Long(id));
    }

    private String normalizeId(String id)
    {
        return "-1".equals(id) ? null : id;
    }

    @GET
    @Path("values-combos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getValuesForCombos(@QueryParam("indicadorId") Long indicadorId,
                                             @QueryParam("usoId") Long usoId, @QueryParam("agrupacionId") Long agrupacionId, @QueryParam("indicadores") List<Long> indicadores)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        if (!indicadores.isEmpty())
        {
            return getValuesFromIndicadorIdList(indicadores);
        }
        if (indicadorId != null)
        {
            return getValuesFromIndicadorId(indicadorId);
        }

        if (usoId != null)
        {
            return getValuesFromUsoId(usoId);
        }

        if (agrupacionId != null)
        {
            return getValuesFromAgrupacionId(agrupacionId);
        }

        return new ArrayList<UIEntity>();
    }

    private List<UIEntity> getValuesFromUsoId(Long usoId) throws VariablesNoExistentesException,
            ErrorEjecucionSQLException, ParseException
    {
        List<UIEntity> entities = new ArrayList<>();

        List<IndicadorPorTipo> indicadoresPorTipos = agrupadorIndicadoresPorTipoService
                .agrupaPorUso(indicadorUsoService.getByUsoId(usoId));

        for (IndicadorPorTipo indicadorPorTipo : indicadoresPorTipos)
        {
            entities.add(getEntityFromIndicadorPorTipo(indicadorPorTipo));
        }

        return entities;
    }

    private List<UIEntity> getValuesFromAgrupacionId(Long agrupacionId)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        List<UIEntity> entities = new ArrayList<>();

        List<IndicadorPorTipo> indicadoresPorTipos = agrupadorIndicadoresPorTipoService
                .agrupaPorAgrupacion(indicadorAgrupacionService.getByAgrupacionId(agrupacionId));

        for (IndicadorPorTipo indicadorPorTipo : indicadoresPorTipos)
        {
            entities.add(getEntityFromIndicadorPorTipo(indicadorPorTipo));
        }

        return entities;
    }

    private List<UIEntity> getValuesFromIndicadorId(Long indicadorId)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        List<UIEntity> entities = new ArrayList<>();

        entities.add(getEntityFromIndicador(indicadorService.getIndicador(indicadorId)));

        return entities;
    }

    private List<UIEntity> getValuesFromIndicadorIdList(List<Long> indicadorList)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        List<UIEntity> entities = new ArrayList<>();
        for (Long id : indicadorList)
        {
            entities.add(getEntityFromIndicador(indicadorService.getIndicador(id)));
        }
        return entities;
    }

    private UIEntity getEntityFromIndicadorPorTipo(IndicadorPorTipo indicadorPorTipo)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        UIEntity entity = new UIEntity();
        List<UIEntity> listEntityIndicador = new ArrayList<>();
        String descripcion = "";
        String componenteTemporal = indicadorPorTipo.getComponenteTemporal();

        for (Indicador indicador : indicadorPorTipo.getIndicadores())
        {
            UIEntity entityIndicador = new UIEntity();

            entityIndicador.put("id", indicador.getId());
            entityIndicador.put("nombre", indicador.getNombre());
            entityIndicador.put("ultimaActualizacion",
                    indicadorService.getUltimaActulizacion(indicador.getId()));

            listEntityIndicador.add(entityIndicador);

            descripcion += indicador.getNombre() + ", ";
        }

        if (descripcion.length() > 1)
        {
            descripcion = descripcion.substring(0, descripcion.length() - 2);
        }

        List<String> anyos = indicadorService.getDistinctAnyosFromListIndicadoresCache(indicadorPorTipo
                .getIndicadores());
        List<Agregacion> agregaciones = indicadorService.getAgregacionesCache(indicadorPorTipo.getIndicadores().get(0), "ca");
        List<ValorInteranual> valoresInteranuales = indicadorService.getValoresInteranualesCache(indicadorPorTipo.getIndicadores());

        if (anyos.isEmpty() && agregaciones.isEmpty() && valoresInteranuales.isEmpty())
        {
            anyos = indicadorService.getDistinctAnyosFromListIndicadores(indicadorPorTipo.getIndicadores());
            agregaciones = indicadorService.getAgregaciones(indicadorPorTipo.getIndicadores().get(0));
            valoresInteranuales = indicadorService.getValoresInteranualesFromListIndicadores(indicadorPorTipo.getIndicadores());
        }

        entity.put("indicadores", listEntityIndicador);
        entity.put("descripcion", descripcion);
        entity.put("anyos", anyos);
        entity.put("nivelAgregacion", indicadorPorTipo.getNivelAgregacion());
        entity.put("componenteTemporal", componenteTemporal);

        entity.put("agregaciones", UIEntity.toUI(agregaciones));
        entity.put("valoresInteranuales", UIEntity.toUI(valoresInteranuales));

        return entity;
    }

    private UIEntity getEntityFromIndicador(Indicador indicador)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        UIEntity entity = new UIEntity();
        UIEntity entityIndicador = new UIEntity();
        String componenteTemporal = indicadorService.getComponenteTemporal(indicador);

        List<String> anyos = indicadorService.getDistinctAnyosFromCache(indicador);
        List<Agregacion> agregaciones = indicadorService.getAgregacionesCache(indicador, "ca");
        List<ValorInteranual> valoresInteranuales = indicadorService.getValoresInteranualesCache(Collections.singletonList(indicador));

        if (anyos.isEmpty() && agregaciones.isEmpty() && valoresInteranuales.isEmpty())
        {
            anyos = indicadorService.getDistinctAnyosFromIndicador(indicador);
            agregaciones = indicadorService.getAgregaciones(indicador);
            valoresInteranuales = indicadorService.getValoresInteranualesFromIndicador(indicador);
        }

        entityIndicador.put("id", indicador.getId());
        entityIndicador.put("nombre", indicador.getNombre());
        entityIndicador.put("ultimaActualizacion",
                indicadorService.getUltimaActulizacion(indicador.getId()));

        entity.put("indicadores", entityIndicador);
        entity.put("descripcion", indicador.getNombre());
        entity.put("anyos", anyos);
        entity.put("nivelAgregacion", indicadorService.getNivelAgregacion(indicador));
        entity.put("componenteTemporal", componenteTemporal);

        entity.put("agregaciones", UIEntity.toUI(agregaciones));
        entity.put("valoresInteranuales",
                UIEntity.toUI(valoresInteranuales));

        return entity;
    }
}
