package es.uji.apps.ind.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Agrupacion;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.IndicadorUso;
import es.uji.apps.ind.model.Uso;
import es.uji.apps.ind.services.AgrupacionService;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.apps.ind.services.UsoService;
import es.uji.apps.ind.ui.IndicadorUI;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

@Path("publicacion")
public class PublicacionResource
{
    @InjectParam
    private IndicadorService indicadorService;

    @InjectParam
    private UsoService usoService;

    @InjectParam
    private AgrupacionService agrupacionService;

    public IndicadorUI toUI(Indicador indicador) throws VariablesNoExistentesException
    {
        IndicadorUI indicadorUI = new IndicadorUI();
        indicadorUI.setId(indicador.getId());
        indicadorUI.setNombre(indicador.getNombre());
        indicadorUI.setCodigo(indicador.getCodigo());
        indicadorUI.setDenominacion(indicador.getDenominacion());
        indicadorUI.setResponsable(indicador.getResponsable());
        indicadorUI.setFormula(formulaToHtml(indicador.getFormula()));
        indicadorUI.setObservaciones(indicador.getObservaciones());
        indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluar());
        indicadorUI.setValorReferencia(indicador.getValorReferencia());
        indicadorUI.setUsos(joinUsos(indicador.getUsos()));
        indicadorUI.setAgrupaciones(joinAgrupaciones(indicador.getAgrupaciones()));
        indicadorUI.setAgregacion(indicadorService.getNivelAgregacion(indicador));
        indicadorUI.setPeriodo(indicadorService.getComponenteTemporal(indicador));
        return indicadorUI;
    }

    public String formulaToHtml(String formula)
    {
        Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcher = pattern.matcher(formula);
        StringBuilder builder = new StringBuilder();

        int i = 0;
        while (matcher.find())
        {
            String str = "<a href=\"/ind/rest/publicacion/variable/"+matcher.group()+"\">"+matcher.group()+"</a>";
            builder.append(formula.substring(i, matcher.start()));
            builder.append(str);
            i = matcher.end();
        }
        builder.append(formula.substring(i, formula.length()));

        return builder.toString();
    }

    public List<IndicadorUI> toUI(List<Indicador> indicadorList) throws VariablesNoExistentesException
    {
        List<IndicadorUI> list = new ArrayList<>();
        for (Indicador indicador : indicadorList)
        {
            list.add(toUI(indicador));
        }
        return list;
    }

    private static String joinUsos(Set<IndicadorUso> list)
    {

        StringBuilder sb = new StringBuilder();
        String delim = ", ";
        String loopDelim = "";

        for (IndicadorUso s : list)
        {

            sb.append(loopDelim);
            sb.append(s.getUso().getNombre());

            loopDelim = delim;
        }

        return sb.toString();
    }

    public static String joinAgrupaciones(Set<IndicadorAgrupacion> list)
    {

        StringBuilder sb = new StringBuilder();
        String delim = ", ";
        String loopDelim = "";

        for (IndicadorAgrupacion s : list)
        {

            sb.append(loopDelim);
            sb.append(s.getAgrupacion().getNombre());

            loopDelim = delim;
        }

        return sb.toString();
    }

    private Pagina getPaginaPublicacion(String idioma) throws ParseException
    {
        GrupoMenu grupo = new GrupoMenu("Indicadors");
        grupo.addItem(new ItemMenu("Buscador", "/ind/rest/publicacion"));

        Menu menu = new Menu();
        menu.addGrupo(grupo);

        Pagina pagina = new Pagina("", "", idioma, "IND");
        pagina.setTitulo("IND");
        pagina.setMenu(menu);

        return pagina;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarBuscador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @QueryParam("descripcion") String descripcion,
                                    @QueryParam("uso") Long uso,
                                    @QueryParam("agrupacion") Long agrupacion,
                                    @QueryParam("agregacion") String agregacion) throws VariablesNoExistentesException, ParseException
    {
        List<Uso> usoList = usoService.getAllUsos();
        List<Agrupacion> agrupacionList = agrupacionService.getAllAgrupaciones();

        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("pagina", getPaginaPublicacion(idioma));
        template.put("seccion", "ind/buscador");
        template.put("usos", usoList);
        template.put("agrupaciones", agrupacionList);

        if (ParamUtils.isNotNull(descripcion) ||
                ParamUtils.isNotNull(uso) ||
                ParamUtils.isNotNull(agrupacion) ||
                ParamUtils.isNotNull(agregacion))
        {
            List<Indicador> indicadorList = indicadorService.buscarIndicadores(descripcion, uso, agrupacion, agregacion);
            template.put("indicadores", toUI(indicadorList));
            template.put("descripcion", descripcion);
            template.put("uso", uso);
            template.put("agrupacion", agrupacion);
            template.put("agregacion", agregacion);
            return template;
        }
        return template;
    }

    @GET
    @Path("indicador/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarIndicador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @PathParam("id") Long id) throws VariablesNoExistentesException, ParseException
    {
        ParamUtils.checkNotNull(id);
        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("pagina", getPaginaPublicacion(idioma));
        template.put("seccion", "ind/indicador");
        Indicador indicador = indicadorService.getIndicador(id);

        template.put("indicador", toUI(indicador));
        return template;
    }
}
