package es.uji.apps.ind.model;

import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "IND_INDICADORES")
public class Indicador
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String codigo;
    private String denominacion;
    private String responsable;
    private String formula;
    private String observaciones;

    @Column(name = "CONCEPTO_A_EVALUAR")
    private String conceptoAEvaluar;

    @Column(name = "VALOR_REFERENCIA")
    private String valorReferencia;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorUso> usos;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorAgrupacion> agrupaciones;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getDenominacion()
    {
        return denominacion;
    }

    public void setDenominacion(String denominacion)
    {
        this.denominacion = denominacion;
    }

    public String getResponsable()
    {
        return responsable;
    }

    public void setResponsable(String responsable)
    {
        this.responsable = responsable;
    }

    public String getConceptoAEvaluar()
    {
        return conceptoAEvaluar;
    }

    public void setConceptoAEvaluar(String conceptoAEvaluar)
    {
        this.conceptoAEvaluar = conceptoAEvaluar;
    }

    public String getFormula()
    {
        return formula;
    }

    public void setFormula(String formula)
    {
        this.formula = formula;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public Set<IndicadorUso> getUsos()
    {
        return usos;
    }

    public void setUsos(Set<IndicadorUso> usos)
    {
        this.usos = usos;
    }

    public Set<IndicadorAgrupacion> getAgrupaciones()
    {
        return agrupaciones;
    }

    public void setAgrupaciones(Set<IndicadorAgrupacion> agrupaciones)
    {
        this.agrupaciones = agrupaciones;
    }

    public String getValorReferencia() {
        return valorReferencia;
    }

    public void setValorReferencia(String valorReferencia) {
        this.valorReferencia = valorReferencia;
    }
}
