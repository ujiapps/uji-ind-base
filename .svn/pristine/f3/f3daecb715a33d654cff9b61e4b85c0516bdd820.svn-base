package es.uji.apps.apa.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.apa.dao.PerfilDAO;
import es.uji.apps.apa.model.Perfil;
import es.uji.commons.db.BaseDAO;

@Service
public class PerfilService
{
    private BaseDAO baseDAO;
    private PerfilDAO perfilDAO;

    @Autowired
    public PerfilService(BaseDAO baseDAO, PerfilDAO perfilDAO)
    {
        this.baseDAO = baseDAO;
        this.perfilDAO = perfilDAO;
    }

    public List<Perfil> get()
    {
        return baseDAO.get(Perfil.class);
    }

    public Perfil getById(Long perfilId)
    {
        return baseDAO.get(Perfil.class, perfilId).get(0);
    }

    public List<Perfil> getByPersonaId(Long personaId)
    {
        return perfilDAO.getByPersonaId(personaId);
    }

    public Perfil insert(Perfil perfil)
    {
        return baseDAO.insert(perfil);
    }

    public Perfil update(Perfil perfil)
    {
        return baseDAO.update(perfil);
    }

    public void delete(Long perfilId)
    {
        baseDAO.delete(Perfil.class, perfilId);
    }

    public Integer getNumPersonasById(Long perfilId)
    {
        return perfilDAO.getNumPersonasById(perfilId);
    }
}
