Ext.define('IND.view.calculus.WindowCalculus',
{
    extend : 'Ext.window.Window',
    alias : 'widget.windowCalculus',

    title : 'Càlcul d\'indicador',
    width : 500,
    height : 400,
    frame : false,
    bodyStyle : 'padding:1em; background-color:white;',
    modal : true,
    closable : false,
    layout : 'fit',

    requires : [ 'IND.view.calculus.GridCalculusVariablesValores' ],

    items : [
    {
        xtype : 'panel',
        border : false,
        layout :
        {
            type : 'card'
        },
        items : [
        {
            xtype : 'form',
            border : false,
            autoScroll : true,
            items : []
        },
        {
            xtype : 'panel',
            border : false,
            layout :
            {
                type : 'vbox',
                align : 'stretch'
            },
            items : [
            {
                xtype : 'gridCalculusVariablesValores',
                autoScroll : true,
                title : '',
                flex : 1
            } ]
        } ]
    } ],

    buttons : [
    {
        text : 'Calcular Dades',
        action : 'calculate'
    },
    {
        text : 'Mostrar Dades',
        action : 'cache'
    },
    {
        text : 'Tornar',
        action : 'return',
        hidden : true
    },
    {
        text : 'Cancel·lar',
        action : 'close'
    } ],

    disableSaveComponents : function(disabled)
    {
        this.down('button[action=save-calculated]').setDisabled(disabled);
        this.down('textfield[name=marca]').setDisabled(disabled);
    },

    getIndicadorAndValuesForCombosFromUsoId : function(usoId)
    {
        this.getIndicadorAndValuesForCombos(null, usoId, null);
    },

    getIndicadorAndValuesForCombosFromAgrupacionId : function(agrupacionId)
    {
        this.getIndicadorAndValuesForCombos(null, null, agrupacionId);
    },

    getIndicadorAndValuesForCombosFromIndicadorId : function(indicadorId)
    {
        this.getIndicadorAndValuesForCombos(indicadorId, null, null);
    },

    getIndicadorAndValuesForCombos : function(indicadorId, usoId, agrupacionId)
    {
        var ref = this;

        Ext.Ajax.request(
        {
            url : '/ind/rest/calculus/values-combos',
            params :
            {
                indicadorId : indicadorId,
                usoId : usoId,
                agrupacionId : agrupacionId
            },
            method : 'GET',
            success : function(r)
            {
                var objects = Ext.JSON.decode(r.responseText).data;

                for ( var i = 0; i < objects.length; i++)
                {
                    var object = objects[i];
                    ref.addSetOfCombos(IND.Util.toList(object.indicadores), object.descripcion, object.nivelAgregacion, object.componenteTemporal, IND.Util.toList(object.valoresInteranuales), IND.Util
                            .toList(object.anyos), IND.Util.toList(object.agregaciones));
                }
            }
        });
    },

    addSetOfCombos : function(indicadores, descripcion, nivelAgregacion, componenteTemporal, valoresInteranuales, anyos, agregaciones)
    {
        var form = this.down("form");
        var fieldSet = Ext.create('Ext.form.FieldSet',
        {
            xtype : 'fieldset',
            title : descripcion,
            collapsible : false,
            border : true,
            indicadores : indicadores
        });

        this.addLabelToFieldSet(fieldSet, nivelAgregacion, componenteTemporal);
        this.addComboFromValues(fieldSet, "agregacion", "Agregació", agregaciones, 'IND.store.AgregacionesCombo');
        this.addComboFromListValues(fieldSet, "anyo", "Any",  anyos, 'IND.store.Anyos');
        this.addComboFromValues(fieldSet, "valorInteranual", "Valor interanual", valoresInteranuales, 'IND.store.ValoresInteranuales');

        form.add(fieldSet);
    },

    addComboFromValues : function(fieldSet, name, label, object, store)
    {
        fieldSet.add(
        {
            xtype : 'combobox',
            fieldLabel : label,
            labelWidth : 80,
            anchor: '95%',
            name : name,
            store : Ext.create(store,
            {
                data :
                {
                    data : object
                }
            }),
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            triggerAction : 'all',
            mode : 'local',
            editable : false,
            value : -1
        })
    },

    addComboFromListValues : function(fieldSet, name, label, list, store)
    {
        var data = [];

        for ( var i = 0; i <= list.length; i++)
        {
            data.push(
            {
                id : list[i],
                nombre : list[i]
            });
        }

        this.addComboFromValues(fieldSet, name, label, data, store);
    },

    addLabelToFieldSet : function(fieldSet, nivelAgregacion, componenteTemporal)
    {
        var text = '<strong>Nivell agregació:</strong> ' + nivelAgregacion.toLowerCase() + ' <strong>Component temporal:</strong> ' + componenteTemporal.toLowerCase();

        fieldSet.add(
        {
            xtype : 'label',
            html : text,
            style : 'text-align:center; display:block; padding-bottom: 5px;'
        })
    },

    clearForm : function()
    {
        this.down('form').removeAll();
    },

    clearGrid : function()
    {
        this.down('grid').clear();
        this.down('textfield[name=marca]').setValue('');
    },

    clearAndHide : function()
    {
        this.clearForm();
        this.clearGrid();
        this.disableSaveComponents(true);
        this.showForm();
        this.hide();
    },

    showResults : function(cache)
    {
        var grid = this.down('grid');
        var form = this.down('form');
        var fieldsSet = form.items.items;
        var ref = this;
        if (form.isValid() && fieldsSet.length > 0)
        {
            grid.load(this.buildObjectToSent(fieldsSet,cache), function()
            {
                ref.showGrid();
                ref.disableSaveComponents(false);
            });
        }
    },

    saveResults : function()
    {
        var grid = this.down('grid');
        var form = this.down('form');
        var fieldsSet = form.items.items;
        var marca = this.down('textfield[name=marca]');
        var ref = this;

        if (!marca.getValue())
        {
            marca.markInvalid("No es poden desar els càlculs sense una marca o etiqueta");
        }

        if (form.isValid() && marca.getValue())
        {
            grid.save(this.buildObjectToSent(fieldsSet), marca.getValue(), function()
            {
                ref.disableSaveComponents(true);
            })
        }
    },

    buildObjectToSent : function(sets,cache)
    {
        var list = [];
        var ref = this;

        for ( var i = 0; i < sets.length; i++)
        {
            var set = sets[i];

            for ( var j = 0; j < set.indicadores.length; j++)
            {
                var result =
                {
                    id : set.indicadores[j].id,
                    agregacion : set.down("combobox[name=agregacion]").getValue(),
                    anyo : set.down("combobox[name=anyo]").getValue(),
                    valorInteranual : set.down("combobox[name=valorInteranual]").getValue(),
                    cache: cache
                }

                list.push(result);
            }
        }

        return list;
    },

    getSetByIndicadorId : function(indicadorId)
    {
        var form = this.down('form');
        var sets = form.items.items;

        for ( var i = 0; i < sets.length; i++)
        {
            var set = sets[i];

            for ( var j = 0; j < set.indicadores.length; j++)
            {
                if (set.indicadores[j].id === indicadorId)
                {
                    return set;
                }
            }
        }
    },

    showGrid : function()
    {
        this.down("button[action=calculate]").hide();
        this.down("button[action=cache]").hide();
        this.down("button[action=return]").show();
        var cardLayout = this.down("panel").getLayout();
        cardLayout.setActiveItem(1);
    },

    showForm : function()
    {
        this.down("button[action=calculate]").show();
        this.down("button[action=cache]").show();
        this.down("button[action=return]").hide();
        var cardLayout = this.down("panel").getLayout();
        cardLayout.setActiveItem(0);
    },

    normalizeId : function(id)
    {
        return id !== -1 ? id : ''
    }
});