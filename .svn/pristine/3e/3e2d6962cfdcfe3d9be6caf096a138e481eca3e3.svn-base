package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QIndicadorAgrupacion;
import es.uji.apps.ind.model.QIndicadorUso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class IndicadorDAO extends BaseDAODatabaseImpl
{
    private QIndicador indicador = QIndicador.indicador;

    public Indicador getById(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.usos).fetch()
                .leftJoin(indicador.agrupaciones).fetch()
                .leftJoin(indicador.variables).fetch()
                .where(indicador.id.eq(id));

        return query.uniqueResult(indicador);
    }

    public List<Indicador> getIndicadoresByCode(String codigo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador)
                .where(indicador.formula.like("%" + codigo + "%"));

        return query.list(indicador);
    }

    public List<Indicador> buscarIndicadores(String descripcion,Long uso, Long agrupacion)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIndicadorUso qIndicadorUso= QIndicadorUso.indicadorUso;
        QIndicadorAgrupacion qIndicadorAgrupacion= QIndicadorAgrupacion.indicadorAgrupacion;

        query.from(indicador).leftJoin(indicador.usos, qIndicadorUso).fetch()
                .leftJoin(indicador.agrupaciones, qIndicadorAgrupacion).fetch()
                .leftJoin(indicador.variables).fetch();

        if(ParamUtils.isNotNull(descripcion))
        {
            query.where(indicador.nombre.like('%' + descripcion + '%').or(indicador.codigo.like('%'+descripcion+'%')));
        }
        if(ParamUtils.isNotNull(uso)){
            query.where(qIndicadorUso.uso.id.eq(uso));
        }
        if(ParamUtils.isNotNull(agrupacion)){
            query.where(qIndicadorAgrupacion.agrupacion.id.eq(agrupacion));
        }

        return query.distinct().list(indicador);
    }

    public List<Indicador> getAllIndicadores()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.variables).fetch();
        return query.distinct().list(indicador);
    }
}
