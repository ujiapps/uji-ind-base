package es.uji.apps.apa.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.apa.model.Perfil;
import es.uji.apps.apa.services.PerfilService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("perfil")
public class PerfilResource extends CoreBaseService
{
    @InjectParam
    private PerfilService perfilService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPerfiles()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Perfil perfil : perfilService.get())
        {
            UIEntity entity = UIEntity.toUI(perfil);

            addNumPersonasToUiEntity(perfil, entity);

            entities.add(entity);
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getPerfil(@PathParam("id") Long id)
    {
        return UIEntity.toUI(perfilService.getById(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addPerfil(UIEntity entity)
    {
        Perfil perfil = perfilService.insert(entity.toModel(Perfil.class));
        UIEntity newEntity = UIEntity.toUI(perfil);

        addNumPersonasToUiEntity(perfil, newEntity);

        return newEntity;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updatePerfil(@PathParam("id") Long id, UIEntity entity)
    {
        Perfil perfil = perfilService.update(entity.toModel(Perfil.class));
        UIEntity newEntity = UIEntity.toUI(perfil);

        addNumPersonasToUiEntity(perfil, newEntity);

        return newEntity;
    }

    private void addNumPersonasToUiEntity(Perfil perfil, UIEntity entity)
    {
        entity.put("numPersonas", perfilService.getNumPersonasById(perfil.getId()));
    }

    @DELETE
    @Path("{id}")
    public void deletePerfil(@PathParam("id") Long id)
    {
        perfilService.delete(id);
    }
}
