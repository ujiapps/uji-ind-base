package es.uji.apps.ind.services.rest;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.IndicadorMarca;
import es.uji.apps.ind.model.Marca;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import es.uji.apps.ind.services.MarcaService;
import es.uji.apps.ind.services.ValorInteranualService;
import es.uji.commons.rest.UIEntity;

@Path("marcas")
public class MarcasResource
{
    @InjectParam
    private MarcaService marcaService;

    @InjectParam
    private ValorInteranualService valorInteranualService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{marcaId}/indicador/{indicadorId}")
    public List<UIEntity> getMarcasByIndicador(
            @PathParam("marcaId") Long marcaId,@PathParam("indicadorId") Long indicadorId) throws ParseException
    {
        List<Marca> marcaList = marcaService.getMarcasByIndicador(marcaId, indicadorId);
        List<UIEntity> list = new ArrayList<>();
        for (Marca m : marcaList)
        {
            UIEntity uiEntity = UIEntity.toUI(m);
            List<Long> values = new ArrayList<>();
            values.add(m.getResultadoInteranual());
            uiEntity.put("nombreComponenteTemporal", valorInteranualService.getList(values, ComponenteTemporal.get(m.getCompotenteTemporal())).get(0).getNombre());
            list.add(uiEntity);
        }
        return list;
    }
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIndicadoresByMarca(
            @QueryParam("query") @DefaultValue("") String marca)
    {
        List<IndicadorMarca> marcaList = marcaService.getIndicadoresMarcaByMarca(marca);

        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (IndicadorMarca indicadorMarca : marcaService.getIndicadoresMarcaByMarca(marca))
        {
            entities.add(buildEntity(indicadorMarca));
        }

        return entities;
    }
    private UIEntity buildEntity(IndicadorMarca indicadorMarca)
    {
        UIEntity entity = new UIEntity();

        entity.put("id", indicadorMarca.getId());
        entity.put("fecha", indicadorMarca.getFecha());
        entity.put("marca", indicadorMarca.getMarca());
        entity.put("indicador", UIEntity.toUI(indicadorMarca.getIndicador()));

        return entity;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        marcaService.deleteMarca(id);

        return Response.noContent().build();
    }
}
