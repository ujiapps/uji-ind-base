package es.uji.apps.ind.formula;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Variable;

@Component
public class VariableChecker
{
    private VariableExtractor variableExtractor;

    @Autowired
    public VariableChecker(VariableExtractor variableExtractor)
    {
        this.variableExtractor = variableExtractor;
    }

    public void checkFormula(String formula, List<Variable> variables) throws VariablesDiferentesTiposException,
            VariablesNoExistentesException
    {
        List<String> variablesCode = variableExtractor.extract(formula);

        if (!allVariablesAreTheSameType(variables))
        {
            throw new VariablesDiferentesTiposException();
        }

        if (!existsAllVariables(variablesCode, variables))
        {
            throw new VariablesNoExistentesException();
        }
    }

    private boolean allVariablesAreTheSameType(List<Variable> variables)
    {
        if (variables.size() == 0)
        {
            return false;
        }

        Variable primeraVariable = variables.get(0);

        for (Variable variable : variables)
        {
            if (!primeraVariable.mismoTipo(variable))
            {
                return false;
            }
        }

        return true;
    }

    private boolean existsAllVariables(List<String> variablesCode, List<Variable> variables)
    {
        return (variablesCode.size() == variables.size());
    }
}
