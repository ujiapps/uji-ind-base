package es.uji.apps.ind.services.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.Agrupacion;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.IndicadorMarca;
import es.uji.apps.ind.model.IndicadorMarcaResultado;
import es.uji.apps.ind.model.IndicadorPorTipo;
import es.uji.apps.ind.model.IndicadorUso;
import es.uji.apps.ind.model.Uso;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.services.AgregacionesService;
import es.uji.apps.ind.services.AgrupacionService;
import es.uji.apps.ind.services.AgrupadorIndicadoresPorTipoService;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.apps.ind.services.UsoService;
import es.uji.apps.ind.services.ValorInteranualService;
import es.uji.apps.ind.services.VariableService;
import es.uji.apps.ind.ui.IndicadorCalculoUI;
import es.uji.apps.ind.ui.IndicadorResultadoUI;
import es.uji.apps.ind.ui.IndicadorUI;
import es.uji.apps.ind.ui.ResultadoUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

@Path("publicacion")
public class PublicacionResource extends CoreBaseService
{
    @InjectParam
    private IndicadorService indicadorService;

    @InjectParam
    private UsoService usoService;

    @InjectParam
    private AgrupacionService agrupacionService;

    @InjectParam
    private AgregacionesService agregacionesService;

    @InjectParam
    private ValorInteranualService valorInteranualService;

    @InjectParam
    private AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService;

    @InjectParam
    private VariableService variableService;

    public IndicadorUI toUI(Indicador indicador) throws VariablesNoExistentesException
    {
        IndicadorUI indicadorUI = new IndicadorUI();
        indicadorUI.setId(indicador.getId());
        indicadorUI.setNombre(indicador.getNombre());
        indicadorUI.setCodigo(indicador.getCodigo());
        indicadorUI.setDenominacion(indicador.getDenominacion());
        indicadorUI.setResponsable(indicador.getResponsable());
        indicadorUI.setFormula(indicador.getFormula());
        indicadorUI.setObservaciones(indicador.getObservaciones());
        indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluar());
        indicadorUI.setValorReferencia(indicador.getValorReferencia());
        indicadorUI.setUsos(joinUsos(indicador.getUsos()));
        indicadorUI.setAgrupaciones(joinAgrupaciones(indicador.getAgrupaciones()));
        indicadorUI.setAgregacion(indicadorService.getNivelAgregacion(indicador));
        indicadorUI.setPeriodo(indicadorService.getComponenteTemporal(indicador));
        indicadorUI.setUltimaActualizacion(indicadorService.getUltimaActulizacion(indicador.getId()));
        indicadorUI.setMarcas(marcasToUI(indicador.getMarcas()));
        return indicadorUI;
    }

    private Map<Long, String> marcasToUI(Set<IndicadorMarca> marcas)
    {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        Map<Long, String> treeMap = new TreeMap<Long, String>();
        for(IndicadorMarca indicadorMarca: marcas){
            treeMap.put(indicadorMarca.getId(),indicadorMarca.getMarca()+" ("+df.format(indicadorMarca.getFecha())+")");
        }
        return treeMap;
    }


    public List<IndicadorUI> toUI(List<Indicador> indicadorList) throws VariablesNoExistentesException
    {
        List<IndicadorUI> list = new ArrayList<>();
        for (Indicador indicador : indicadorList)
        {
            list.add(toUI(indicador));
        }
        return list;
    }

    private static String joinUsos(Set<IndicadorUso> list)
    {

        StringBuilder sb = new StringBuilder();
        String delim = ", ";
        String loopDelim = "";

        for (IndicadorUso s : list)
        {

            sb.append(loopDelim);
            sb.append(s.getUso().getNombre());

            loopDelim = delim;
        }

        return sb.toString();
    }

    public static String joinAgrupaciones(Set<IndicadorAgrupacion> list)
    {

        StringBuilder sb = new StringBuilder();
        String delim = ", ";
        String loopDelim = "";

        for (IndicadorAgrupacion s : list)
        {

            sb.append(loopDelim);
            sb.append(s.getAgrupacion().getNombre());

            loopDelim = delim;
        }

        return sb.toString();
    }

    private Pagina getPaginaPublicacion(String url,String idioma) throws ParseException
    {
        GrupoMenu grupo = null;
        if (idioma.equals("ca"))
        {
            grupo = new GrupoMenu("Indicadors");
            grupo.addItem(new ItemMenu("Buscador", "/ind/rest/publicacion"));

        } else if (idioma.equals("es"))
        {
             grupo = new GrupoMenu("Indicadores");
            grupo.addItem(new ItemMenu("Buscador", "/ind/rest/publicacion"));

        } else if (idioma.equals("en"))
        {
            grupo = new GrupoMenu("Indicators");
            grupo.addItem(new ItemMenu("Search", "/ind/rest/publicacion"));

        }

        Menu menu = new Menu();
        menu.addGrupo(grupo);

        Pagina pagina = new Pagina("ind", url, idioma, "IND");
        pagina.setTitulo("IND");
        pagina.setMenu(menu);

        return pagina;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarBuscador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @QueryParam("descripcion") String descripcion,
                                    @QueryParam("uso") Long uso,
                                    @QueryParam("agrupacion") Long agrupacion,
                                    @QueryParam("agregacion") String agregacion) throws VariablesNoExistentesException, ParseException
    {
        List<Uso> usoList = usoService.getAllUsos();
        List<Agrupacion> agrupacionList = agrupacionService.getAllAgrupaciones();

        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("pagina", getPaginaPublicacion("rest/publicacion",idioma));
        template.put("server", "http://ujiapps.uji.es");
        template.put("seccion", "ind/buscador");
        template.put("idioma", idioma);
        template.put("urlBase", "/ind/rest/publicacion");
        template.put("usos", usoList);
        template.put("agrupaciones", agrupacionList);
        if (ParamUtils.isNotNull(descripcion) ||
                ParamUtils.isNotNull(uso) ||
                ParamUtils.isNotNull(agrupacion) ||
                ParamUtils.isNotNull(agregacion))
        {
            List<Indicador> indicadorList = indicadorService.buscarIndicadores(descripcion, uso, agrupacion, agregacion);
            template.put("indicadores", toUI(indicadorList));
            template.put("descripcion", descripcion);
            template.put("uso", uso);
            template.put("agrupacion", agrupacion);
            template.put("agregacion", agregacion);
            return template;
        }
        return template;
    }

    @GET
    @Path("indicador/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarIndicador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @PathParam("id") Long id) throws VariablesNoExistentesException, ParseException
    {
        ParamUtils.checkNotNull(id);
        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("pagina", getPaginaPublicacion("rest/publicacion/indicador/"+id,idioma));
        template.put("server", "http://ujiapps.uji.es");
        template.put("seccion", "ind/indicador");
        template.put("idioma", idioma);
        template.put("urlBase", "/ind/rest/publicacion/indicador/"+id);
        Indicador indicador = indicadorService.getIndicador(id);

        template.put("indicador", toUI(indicador));
        return template;
    }

    @GET
    @Path("indicador/{id}/calculo/")
    @Produces(MediaType.TEXT_HTML)
    public Template calcularIndicador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                      @PathParam("id") Long id) throws VariablesNoExistentesException, ParseException, ErrorEjecucionSQLException
    {
        ParamUtils.checkNotNull(id);
        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("server", "http://ujiapps.uji.es");
        template.put("pagina", getPaginaPublicacion("rest/publicacion/"+id+"/calculo",idioma));
        template.put("seccion", "ind/calcular");
        template.put("idioma", idioma);
        template.put("urlBase", "/ind/rest/publicacion/"+id+"/calculo");
        Indicador indicador = indicadorService.getIndicador(id);
        String agregacion = indicadorService.getNivelAgregacion(indicador);

        template.put("indicador", toUI(indicador));
        template.put("anyos", indicadorService.getDistinctAnyosFromCache(indicador));
        template.put("nivelAgregacion", agregacion);
        template.put("componenteTemporal", indicadorService.getComponenteTemporal(indicador));
        template.put("agregaciones", indicadorService.getAgregacionesCache(Collections.singletonList(indicador), idioma));
        template.put("valoresInteranuales", indicadorService.getValoresInteranualesCache(Collections.singletonList(indicador)));
        return template;
    }

    @GET
    @Path("indicadores")
    @Produces(MediaType.TEXT_HTML)
    public Template calcularIndicadores(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("indicadores") List<Long> indiadores) throws VariablesNoExistentesException, ParseException, ErrorEjecucionSQLException
    {
        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("server", "http://ujiapps.uji.es");
        template.put("pagina", getPaginaPublicacion("/rest/publicacion/",idioma));
        template.put("seccion", "ind/indicadores");
        template.put("idioma", idioma);
        template.put("urlBase", "/ind/rest/publicacion");
        List<IndicadorCalculoUI> entities = new ArrayList<>();
        List<Indicador> indicadorList = new ArrayList<>();

        for (Long id : indiadores)
        {
            indicadorList.add(indicadorService.getIndicador(id));
        }
        for (IndicadorPorTipo indicadorPorTipo : agrupadorIndicadoresPorTipoService.agrupaIndicadores(indicadorList))
        {
            String descripcion = "";
            IndicadorCalculoUI indicadorCalculoUI = new IndicadorCalculoUI();
            List<Indicador> indicadores = indicadorPorTipo.getIndicadores();
            indicadorCalculoUI.setIndicadores(toUI(indicadores));
            indicadorCalculoUI.setDescripcion(descripcion);
            indicadorCalculoUI.setAnyos(indicadorService.getDistinctAnyosFromListIndicadoresCache(indicadores));
            indicadorCalculoUI.setNivelAgregacion(indicadorPorTipo.getNivelAgregacion());
            indicadorCalculoUI.setComponenteTemporal(indicadorPorTipo.getComponenteTemporal());
            indicadorCalculoUI.setAgregaciones(indicadorService.getAgregacionesCache(indicadores, idioma));
            indicadorCalculoUI.setValoresInteranuales(indicadorService
                    .getValoresInteranualesCache(indicadores));

            entities.add(indicadorCalculoUI);
        }

        template.put("indicadores", indiadores);
        template.put("indicadoresCalculo", entities);

        return template;
    }

    @POST
    @Path("indicador/{id}/calculo/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<IndicadorResultadoUI> mostrarDatos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                       @PathParam("id") Long id,
                                       @FormParam("agregacion") @DefaultValue("-1") Long agregacion,
                                       @FormParam("anyo") Integer anyo,
                                       @FormParam("valorInteranual") Long valorInteranual,
                                                   @FormParam("marca") List<Long> marca) throws ParseException, VariablesNoExistentesException, ErrorEjecucionSQLException, VariablesSinValorException
    {
        ParamUtils.checkNotNull(id);
        Indicador indicador = indicadorService.getIndicador(id);

        List<IndicadorMarcaResultado> list = indicadorService
                .calculaIndicador(null,indicadorService.getIndicador(id), agregacion, anyo, valorInteranual, "1", marca);

        return Collections.singletonList(toUI(null,indicador, idioma, list));
    }

    private IndicadorResultadoUI toUI(Long userId,Indicador indicador, @CookieParam("uji-lang") @DefaultValue("ca") String idioma, List<IndicadorMarcaResultado> list) throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        IndicadorResultadoUI indicadorResultadoUI = new IndicadorResultadoUI();
        indicadorResultadoUI.setIndicadorId(indicador.getId());
        indicadorResultadoUI.setTipoGrafico(indicador.getTipoGrafico());
        indicadorResultadoUI.setIndicadorNombre(indicador.getNombre());
        List<ResultadoUI> resultadosUI = new ArrayList<>();
        for (IndicadorMarcaResultado indicadorMarcaResultado : list)
        {
            ResultadoUI resultadoUI = new ResultadoUI();
            resultadoUI.setId(indicadorMarcaResultado.getId());
            resultadoUI.setAgregacion(indicadorMarcaResultado.getAgregacion());
            resultadoUI.setAgregacionNombre(indicadorService.getAgregacionNombreCache(indicador, idioma,indicadorMarcaResultado.getAgregacion()));
            resultadoUI.setAnyo(indicadorMarcaResultado.getAnyo());
            resultadoUI.setValorInteranual(indicadorMarcaResultado.getValorInteranual());
            resultadoUI.setValorInteranualtext(valorInteranualService.get(indicadorMarcaResultado.getValorInteranual(), indicadorService.getComponenteTemporal(indicador)));
            resultadoUI.setValor(indicadorMarcaResultado.getValor());
            resultadosUI.add(resultadoUI);
        }
        Comparator<ResultadoUI> comparator = (o1, o2) -> o1.getAgregacionNombre().compareTo(o2.getAgregacionNombre());
        comparator.thenComparing((o1, o2) -> o1.getAnyo().compareTo(o2.getAnyo()));
        resultadosUI.sort(comparator);
        indicadorResultadoUI.setResultados(resultadosUI);
        return indicadorResultadoUI;
    }

    @POST
    @Path("indicadores/calculo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<IndicadorResultadoUI> getCalculatedData(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, List<UIEntity> entities)
            throws ErrorEvaluacionFormulaException, VariablesSinValorException,
            ErrorEjecucionSQLException, VariablesNoExistentesException, ParseException
    {
        List<IndicadorResultadoUI> indicadorResultadoUIList = new ArrayList<>();
        for (UIEntity entity : entities)
        {
            Long id = ParamUtils.parseLong(entity.get("id"));
            Long agregacion = ParamUtils.parseLong(entity.get("agregacion"));
            Integer anyo = toInteger(normalizeId(entity.get("anyo")));
            Long valorInteranual = toLong(normalizeId(entity.get("valorInteranual").toString()));
            Indicador indicador = indicadorService.getIndicador(id);
            List<String> marcas= entity.getArray("marca");
            List<Long> marcaList = new ArrayList<>();
            for(String marca: marcas){
                marcaList.add(Long.parseLong(marca));
            }

            List<IndicadorMarcaResultado> list = indicadorService
                    .calculaIndicador(null,indicadorService.getIndicador(id), agregacion, anyo, valorInteranual, "1",marcaList);
            IndicadorResultadoUI indicadorResultadoUI = toUI(null,indicador,idioma,list);
            indicadorResultadoUIList.add(indicadorResultadoUI);

        }

        return indicadorResultadoUIList;
    }


    @GET
    @Path("variable/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarVariable(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @PathParam("id") String id) throws VariablesNoExistentesException, ParseException
    {
        ParamUtils.checkNotNull(id);
        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        template.put("server", "http://ujiapps.uji.es");
        template.put("pagina", getPaginaPublicacion("rest/publicacion/variable/"+id,idioma));
        template.put("seccion", "ind/variable");
        template.put("idioma", idioma);
        template.put("urlBase", "/ind/rest/publicacion/variable/"+id);
        Variable variable = variableService.getVariableByCode(id);

        template.put("variable", variable);
        return template;
    }

    private Integer toInteger(String id)
    {
        return (id == null ? null : new Integer(id));
    }

    private Long toLong(String id)
    {
        return (id == null ? null : new Long(id));
    }

    private String normalizeId(String id)
    {
        return "-1".equals(id) ? null : id;
    }
}
