package es.uji.apps.ind.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.VariableDAO;
import es.uji.apps.ind.model.Variable;

@Service
public class VariableService
{
    private VariableDAO variableDAO;

    @Autowired
    public VariableService(VariableDAO variableDAO)
    {
        this.variableDAO = variableDAO;
    }

    public Variable getVariable(Long id)
    {
        List<Variable> variables = variableDAO.get(Variable.class, id);

        if (variables != null && !variables.isEmpty())
        {
            return variables.get(0);
        }

        return null;
    }

    public Variable getVariableByCode(String variableCode)
    {
        List<Variable> variables = variableDAO.getByCode(variableCode);

        if (variables != null && !variables.isEmpty())
        {
            return variables.get(0);
        }

        return null;
    }

    public List<Variable> getAllVariablesByCodes(List<String> variablesCode)
    {
        List<Variable> variables = new ArrayList<>();

        for (String variableCode : variablesCode)
        {
            Variable variable = getVariableByCode(variableCode);

            if (variable != null)
            {
                variables.add(variable);
            }
        }

        return variables;
    }

    public Boolean existsAllVariables(List<String> variablesCode, List<Variable> variables)
    {
        if (variablesCode.size() == variables.size())
        {
            return true;
        }

        return false;
    }

    public Variable addOrUpdateVariable(Variable variable)
    {
        Long id = variable.getId();

        if (id != null)
        {
            return variableDAO.update(variable);
        }

        return variableDAO.insert(variable);
    }

    public void deleteVariable(Long id)
    {
        variableDAO.delete(Variable.class, id);
    }

    public List<Variable> getAllVariables()
    {
        return variableDAO.get(Variable.class);
    }

    public boolean allVariablesAreTheSameType(List<Variable> variables)
    {
        Boolean resultado = true;

        if (variables.size() == 0)
        {
            return resultado;
        }

        Variable primeraVariable = variables.get(0);

        for (Variable variable : variables)
        {
            resultado = resultado && sameType(primeraVariable, variable);
        }

        return resultado;
    }

    private boolean sameType(Variable variable1, Variable variable2)
    {
        if (variable1.getTipoAnualidad() == null && variable2.getTipoAnualidad() != null)
        {
            return false;
        }

        if (variable1.getTipoAnualidad() != null && variable2.getTipoAnualidad() == null)
        {
            return false;
        }

        if (variable1.getNumMeses().equals(variable2.getNumMeses())
                && (variable1.getTipoAnualidad() == null && variable2.getTipoAnualidad() == null)
                && variable1.getNivelAgregacion().equals(variable2.getNivelAgregacion()))
        {
            return true;
        }

        if (variable1.getNumMeses().equals(variable2.getNumMeses())
                && (variable1.getTipoAnualidad().equals(variable2.getTipoAnualidad()))
                && variable1.getNivelAgregacion().equals(variable2.getNivelAgregacion()))
        {
            return true;
        }

        return false;
    }
}
