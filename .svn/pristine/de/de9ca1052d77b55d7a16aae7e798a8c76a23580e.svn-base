package es.uji.apps.ind.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.IndicadorDAO;
import es.uji.apps.ind.dao.VariableDAO;
import es.uji.apps.ind.dao.VariableValorDAO;
import es.uji.apps.ind.exceptions.CodigoVariableEnUsoException;
import es.uji.apps.ind.exceptions.CodigoVariableRepetidoException;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.commons.rest.ParamUtils;

@Service
public class VariableService
{
    private VariableDAO variableDAO;
    private IndicadorDAO indicadorDAO;
    private VariableValorDAO variableValorDAO;

    @Autowired
    public VariableService(VariableDAO variableDAO, IndicadorDAO indicadorDAO,
            VariableValorDAO variableValorDAO)
    {
        this.variableDAO = variableDAO;
        this.indicadorDAO = indicadorDAO;
        this.variableValorDAO = variableValorDAO;
    }

    public Variable getVariable(Long id)
    {
        return variableDAO.getById(id);
    }

    public Variable getVariableByCode(String variableCode)
    {
        return variableDAO.getByCode(variableCode);
    }

    public VariableValor getVariableValor(Long id)
    {
        return variableValorDAO.get(VariableValor.class, id).get(0);
    }

    public List<Variable> getVariables(String filter)
    {
        if(ParamUtils.isNotNull(filter)){
            return variableDAO.getVariableByName(filter);
        } else{
            return variableDAO.get(Variable.class);
        }
    }

    public void delete(Long id)
    {
        variableValorDAO.delete(VariableValor.class, id);
    }

    public VariableValor insert(VariableValor variableValor)
    {
        return variableValorDAO.insert(variableValor);
    }

    public VariableValor update(VariableValor variableValor)
    {
        return variableValorDAO.update(variableValor);
    }

    public List<VariableValor> getVariablesValorByVariableId(Long userId,Long variableId)
            throws ErrorEjecucionSQLException
    {
        Variable variable = getVariable(variableId);

        if (variable.esDinamica())
        {
            return variableValorDAO.executeSelect(userId,variable.getSql());
        }

        return variableValorDAO.getVariablesValorByVariableId(variableId);
    }

    public List<VariableValor> getFilteredVariablesValorFromVariableId(Long userId,Long variableId,
            String agregacion, Integer anyo, Long valorInteranual)
            throws ErrorEjecucionSQLException
    {
        Variable variable = getVariable(variableId);

        if (variable.esDinamica())
        {
            return variableValorDAO.executeSelectAndFilterResults(userId,variable.getSql(), agregacion,
                    anyo, valorInteranual);
        }

        return variableValorDAO.getFilteredVariablesValorByVariableId(variableId, agregacion, anyo,
                valorInteranual);
    }

    public List<Variable> getAllVariablesByCodes(List<String> variablesCode)
    {
        List<Variable> variables = new ArrayList<>();

        for (String variableCode : variablesCode)
        {
            Variable variable = getVariableByCode(variableCode);

            if (variable != null)
            {
                variables.add(variable);
            }
        }

        return variables;
    }

    public Variable add(Variable variable) throws CodigoVariableRepetidoException
    {
        try
        {
            return variableDAO.insert(variable);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new CodigoVariableRepetidoException();
        }
    }

    public Variable update(Variable variable, String codigoAnterior,
            Integer componenteTemporalAnterior, String nivelAgregacionAnterior)
            throws VariableFormatoIncorrectoException, CodigoVariableRepetidoException,
            CodigoVariableEnUsoException
    {
        if (!variable.getCodigo().equals(codigoAnterior)
                || !variable.getComponenteTemporal().equals(componenteTemporalAnterior)
                || !variable.getNivelAgregacion().equals(nivelAgregacionAnterior))
        {
            checkCodigoInIndicadores(codigoAnterior);
        }

        return actualizaValoresVariable(variable);
    }

    private Variable actualizaValoresVariable(Variable variable)
            throws CodigoVariableRepetidoException
    {
        try
        {
            return variableDAO.update(variable);
        }
        catch (DataIntegrityViolationException e)
        {
            throw new CodigoVariableRepetidoException();
        }
    }

    public void deleteVariable(Long id) throws CodigoVariableEnUsoException
    {
        checkCodigoInIndicadores(getVariable(id).getCodigo());

        variableDAO.delete(Variable.class, id);
    }

    private void checkCodigoInIndicadores(String codigo) throws CodigoVariableEnUsoException
    {
        if (codigo == null || codigo.isEmpty())
        {
            return;
        }

        List<String> indicadores = getIndicadoresByVariable(codigo);

        if (!indicadores.isEmpty())
        {
            throw new CodigoVariableEnUsoException(indicadores);
        }
    }

    private List<String> getIndicadoresByVariable(String codigo)
    {
        List<String> indicadoresUsandoVariable = new ArrayList<>();

        if (codigo != null && !codigo.isEmpty())
        {
            List<Indicador> indicadores = indicadorDAO.getIndicadoresByCode(codigo);

            for (Indicador indicador : indicadores)
            {
                if (variableEnFormula(indicador, codigo))
                {
                    indicadoresUsandoVariable.add(indicador.getCodigo());
                }
            }
        }

        return indicadoresUsandoVariable;
    }

    private Boolean variableEnFormula(Indicador indicador, String codigo)
    {
        for (String variable : indicador.getVariablesFormula())
        {
            if (variable.equals(codigo))
            {
                return true;
            }
        }

        return false;
    }

    public List<Variable> getAllVariables()
    {
        return variableDAO.get(Variable.class);
    }

    public void duplicarVariable(Long id) throws CodigoVariableRepetidoException,
            VariableFormatoIncorrectoException, CodigoVariableEnUsoException
    {
        Variable variable = getVariable(id);

        actualizaValoresVariable(variable.duplicarVariable(variable));
    }

    public List<String> getDistinctAnyosFromVariableList(Long userId,List<String> variableCodes)
            throws ErrorEjecucionSQLException
    {
        List<Variable> variables = variableDAO.getVariablesByCodes(variableCodes);
        return variableValorDAO.getDistinctAnyos(userId,getStaticVariables(variables),
                getDynamicVariables(variables));
    }

    public List<Long> getDistinctValoresInteranualesFromVariableList(Long userId,List<String> variableCodes)
            throws ErrorEjecucionSQLException
    {
        List<Variable> variables = variableDAO.getVariablesByCodes(variableCodes);
        return variableValorDAO.getDistinctValoresInteranuales(userId,getStaticVariables(variables),
                getDynamicVariables(variables));
    }

    public List<String> getDistinctAgregacionesIdFromVariableList(Long userId,List<String> variableCodes)
            throws ErrorEjecucionSQLException
    {
        List<Variable> variables = variableDAO.getVariablesByCodes(variableCodes);
        return variableValorDAO.getDistinctAgregacionesId(userId,getStaticVariables(variables),
                getDynamicVariables(variables));
    }

    private List<Variable> getStaticVariables(List<Variable> variables)
    {
        List<Variable> variablesEstaticas = new ArrayList<>();

        for (Variable variable : variables)
        {
            if (!variable.esDinamica())
            {
                variablesEstaticas.add(variable);
            }
        }

        return variablesEstaticas;
    }

    private List<Variable> getDynamicVariables(List<Variable> variables)
    {
        List<Variable> variablesDinamicas = new ArrayList<>();

        for (Variable variable : variables)
        {
            if (variable.esDinamica())
            {
                variablesDinamicas.add(variable);
            }
        }

        return variablesDinamicas;
    }
}