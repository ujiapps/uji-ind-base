package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.IndicadorDAO;

@Service
public class IndicadorService
{
    private IndicadorDAO indicadorDAO;

    @Autowired
    public IndicadorService(IndicadorDAO indicadorDAO)
    {
        this.indicadorDAO = indicadorDAO;
    }

    public Indicador getIndicador(Long id)
    {
        List<Indicador> indicadores = indicadorDAO.get(Indicador.class, id);

        if (indicadores != null && !indicadores.isEmpty())
        {
            return indicadores.get(0);
        }

        return null;
    }

    public Indicador addIndicador(Indicador indicador)
    {
        return indicadorDAO.insert(indicador);
    }

    public Indicador updateIndicador(Indicador indicador)
    {
        return indicadorDAO.update(indicador);
    }

    public void deleteIndicador(Long id)
    {
        indicadorDAO.delete(Indicador.class, id);
    }

    public List<Indicador> getAllIndicadores()
    {
        return indicadorDAO.get(Indicador.class);
    }
}
