package es.uji.apps.ind.formula;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.uji.apps.ind.dao.VariableDAO;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.enums.Agregacion;
import es.uji.apps.ind.model.enums.TipoAnualidad;
import es.uji.apps.ind.services.VariableService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class VariableServiceTest
{
    private VariableService variableService;
    private VariableDAO variableDAO;
    private CodigoVariableChecker codigoVariableChecker;

    private List<Variable> variables = new ArrayList<>();

    @Before
    public void init()
    {
        variableDAO = mock(VariableDAO.class);
        codigoVariableChecker = mock(CodigoVariableChecker.class);
        variableService = new VariableService(variableDAO, codigoVariableChecker);

        variables = new ArrayList<>();
    }

    @Test
    public void verdaderoSiLasVariablesSonDelMismoTipo()
    {
        variables.add(buildVariableTipo1());
        variables.add(buildVariableTipo1());
        variables.add(buildVariableTipo1());

        Assert.assertTrue(variableService.allVariablesAreTheSameType(variables));
    }

    @Test
    public void falsoSiLasVariablesSonDeDiferentesTipos()
    {
        variables.add(buildVariableTipo1());
        variables.add(buildVariableTipo2());

        Assert.assertFalse(variableService.allVariablesAreTheSameType(variables));

        variables = new ArrayList<>();

        variables.add(buildVariableTipo2());
        variables.add(buildVariableTipo3());

        Assert.assertFalse(variableService.allVariablesAreTheSameType(variables));

        variables = new ArrayList<>();

        variables.add(buildVariableTipo3());
        variables.add(buildVariableTipo4());

        Assert.assertFalse(variableService.allVariablesAreTheSameType(variables));
    }

    private Variable buildVariableTipo1()
    {
        Variable variable = new Variable();

        variable.setNumMeses(12F);
        variable.setTipoAnualidad(TipoAnualidad.AÑO.toString());
        variable.setNivelAgregacion(Agregacion.CENTRO.toString());

        return variable;
    }

    private Variable buildVariableTipo2()
    {
        Variable variable = new Variable();

        variable.setNumMeses(13F);
        variable.setTipoAnualidad(null);
        variable.setNivelAgregacion(Agregacion.CENTRO.toString());

        return variable;
    }

    private Variable buildVariableTipo3()
    {
        Variable variable = new Variable();

        variable.setNumMeses(12F);
        variable.setTipoAnualidad(TipoAnualidad.CURSO.toString());
        variable.setNivelAgregacion(Agregacion.CENTRO.toString());

        return variable;
    }

    private Variable buildVariableTipo4()
    {
        Variable variable = new Variable();

        variable.setNumMeses(12F);
        variable.setTipoAnualidad(TipoAnualidad.CURSO.toString());
        variable.setNivelAgregacion(Agregacion.DEPARTAMENTO.toString());

        return variable;
    }
}
