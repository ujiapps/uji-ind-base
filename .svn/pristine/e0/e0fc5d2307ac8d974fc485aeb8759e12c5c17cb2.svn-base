package es.uji.apps.apa.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.apa.model.Aplicacion;
import es.uji.apps.apa.services.AplicacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("aplicacion")
public class AplicacionResource extends CoreBaseService
{
    @InjectParam
    private AplicacionService aplicacionService;

    @Path("{id}/item")
    public AplicacionItemResource getPlatformItem(
            @InjectParam AplicacionItemResource aplicacionItemResource)
    {
        return aplicacionItemResource;
    }

    @Path("{id}/responsable")
    public AplicacionResponsableResource getPlatformItem(
            @InjectParam AplicacionResponsableResource aplicacionResponsableResource)
    {
        return aplicacionResponsableResource;
    }

    @Path("{id}/mapeo")
    public AplicacionMapeoResource getPlatformItem(
            @InjectParam AplicacionMapeoResource aplicacionMapeoResource)
    {
        return aplicacionMapeoResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAplicaciones()
    {
        return UIEntity.toUI(aplicacionService.get());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getAplicacion(@PathParam("id") Long id)
    {
        return UIEntity.toUI(aplicacionService.getById(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        return UIEntity.toUI(aplicacionService.insert(entity.toModel(Aplicacion.class)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateAplicacion(UIEntity entity)
    {
        return UIEntity.toUI(aplicacionService.update(entity.toModel(Aplicacion.class)));
    }

    @DELETE
    @Path("{id}")
    public void deleteAplicacion(@PathParam("id") Long id)
    {
        aplicacionService.delete(id);
    }
}
