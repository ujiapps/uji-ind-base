package es.uji.apps.ind.formula;

import java.util.List;

import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.VariableValorForCalculus;

@Component
public class VariableValorForCalculusConversor
{
    public List<VariableValorForCalculus> add(List<VariableValorForCalculus> valoresForCalculus,
            List<VariableValor> valores, Variable variable) throws VariablesSinValorException
    {
        for (VariableValor valor : valores)
        {
            try
            {
                VariableValorForCalculus valorForCalculus = getIfExists(valoresForCalculus, valor);

                if (valorForCalculus == null)
                {
                    valorForCalculus = new VariableValorForCalculus(valor.getAgregacion(),
                            valor.getAnyo(), valor.getValorInteranual());

                    valoresForCalculus.add(valorForCalculus);
                }

                valorForCalculus.addValue(variable.getCodigo(), Float.parseFloat(valor.getValor()));
            }
            catch (NullPointerException e)
            {
                throw new VariablesSinValorException("La variable " + variable.getCodigo() + " te valors incorrectes.");
            }
        }

        return valoresForCalculus;
    }

    private VariableValorForCalculus getIfExists(List<VariableValorForCalculus> valoresForCalculus,
            VariableValor valor)
    {
        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            if (valorForCalculus.getAgregacion().equals(valor.getAgregacion())
                    && valorForCalculus.getAnyo().equals(valor.getAnyo())
                    && valorForCalculus.getValorInteranual().equals(valor.getValorInteranual()))
            {
                return valorForCalculus;
            }
        }

        return null;
    }
}
