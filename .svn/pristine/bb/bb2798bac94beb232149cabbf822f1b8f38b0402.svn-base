<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>es.uji.apps.ind</groupId>
  <artifactId>uji-ind-base</artifactId>
  <packaging>war</packaging>
  <version>0.0.1-SNAPSHOT</version>
  <name>uji-ind-base</name>

  <dependencies>
    <dependency>
      <groupId>es.uji.commons</groupId>
      <artifactId>uji-commons-testing</artifactId>
      <version>0.1.6-SNAPSHOT</version>
    </dependency>

    <!-- Logging -->

    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-api</artifactId>
      <version>1.6.1</version>
    </dependency>

    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-log4j12</artifactId>
      <version>1.6.1</version>
    </dependency>
  </dependencies>

  <scm>
    <connection>scm:svn:http://jira.uji.es/repos/uji/IND/uji-ind-base/trunk</connection>
    <url>http://jira.uji.es/repos/uji/IND/uji-ind-base/trunk</url>
  </scm>

  <distributionManagement>
    <repository>
      <id>nexus</id>
      <url>http://devel01.uji.es:8080/nexus/content/repositories/releases</url>
    </repository>
    <snapshotRepository>
      <id>nexus</id>
      <url>http://devel01.uji.es:8080/nexus/content/repositories/snapshots</url>
    </snapshotRepository>
  </distributionManagement>


  <build>
    <finalName>ind</finalName>

    <!-- Sustitución de propiedades -->

    <resources>
      <resource>
        <directory>${basedir}/src/main/resources</directory>
      </resource>
      <resource>
        <directory>${basedir}/src/main/webapp/WEB-INF</directory>
        <includes>
          <include>web.xml</include>
        </includes>
        <filtering>true</filtering>
        <targetPath>..</targetPath>
      </resource>
    </resources>

    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.1</version>
        <configuration>
          <source>1.7</source>
          <target>1.7</target>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.mortbay.jetty</groupId>
        <artifactId>maven-jetty-plugin</artifactId>
        <version>6.1.22</version>
        <configuration>
          <connectors>
            <connector implementation="org.mortbay.jetty.nio.SelectChannelConnector">
              <port>9005</port>
            </connector>
          </connectors>
          <contextPath>/ind</contextPath>
          <webXml>target/web.xml</webXml>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>aspectj-maven-plugin</artifactId>
        <version>1.4</version>
        <dependencies>
          <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjrt</artifactId>
            <version>${aspectj.version}</version>
          </dependency>
          <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjtools</artifactId>
            <version>${aspectj.version}</version>
          </dependency>
        </dependencies>
        <executions>
          <execution>
            <goals>
              <goal>compile</goal>
              <goal>test-compile</goal>
            </goals>
          </execution>
        </executions>
        <configuration>
          <source>1.7</source>
          <target>1.7</target>
          <weaveDependencies>
            <weaveDependency>
              <groupId>es.uji.commons</groupId>
              <artifactId>uji-commons-rest</artifactId>
            </weaveDependency>
          </weaveDependencies>
          <outxml>true</outxml>
          <aspectLibraries>
            <aspectLibrary>
              <groupId>org.springframework</groupId>
              <artifactId>spring-aspects</artifactId>
            </aspectLibrary>
          </aspectLibraries>
        </configuration>
      </plugin>

      <!-- Sustitución de propiedades en el web.xml -->

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <version>2.3</version>
        <configuration>
          <webXml>target/web.xml</webXml>
        </configuration>
      </plugin>

      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>properties-maven-plugin</artifactId>
        <version>1.0-alpha-2</version>
        <executions>
          <execution>
            <phase>initialize</phase>
            <goals>
              <goal>read-project-properties</goal>
            </goals>
            <configuration>
              <files>
                <file>/etc/uji/apa/app.properties</file>
              </files>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <!-- QueryDSL -->

      <plugin>
        <groupId>com.mysema.maven</groupId>
        <artifactId>maven-apt-plugin</artifactId>
        <version>0.3.2</version>
        <executions>
          <execution>
            <goals>
              <goal>process</goal>
            </goals>
            <configuration>
              <outputDirectory>target/generated-sources/java</outputDirectory>
              <processor>com.mysema.query.apt.jpa.JPAAnnotationProcessor</processor>
            </configuration>
          </execution>
        </executions>
      </plugin>

      <!-- Proceso de release -->

      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-release-plugin</artifactId>
        <version>2.2.1</version>
        <configuration>
          <tagNameFormat>${project.name}_@{project.version}</tagNameFormat>
        </configuration>
      </plugin>
    </plugins>
  </build>
</project>
