package es.uji.apps.ind.services.rest;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.IndicadorMarca;
import es.uji.apps.ind.model.Marca;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import es.uji.apps.ind.services.MarcaService;
import es.uji.apps.ind.services.ValorInteranualService;
import es.uji.apps.ind.utils.CSV;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

@Path("marcas")
public class MarcasResource
{
    @InjectParam
    private MarcaService marcaService;

    @InjectParam
    private ValorInteranualService valorInteranualService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{marcaId}/indicador/{indicadorId}")
    public List<UIEntity> getMarcasByIndicador(
            @PathParam("marcaId") Long marcaId, @PathParam("indicadorId") Long indicadorId) throws ParseException
    {
        List<Marca> marcaList = marcaService.getMarcasByIndicador(marcaId, indicadorId);
        return toUI(marcaList);
    }

    private List<UIEntity> toUI(List<Marca> marcaList) throws ParseException
    {
        List<UIEntity> list = new ArrayList<>();
        for (Marca m : marcaList)
        {
            UIEntity uiEntity = UIEntity.toUI(m);
            List<Long> values = new ArrayList<>();
            values.add(m.getResultadoInteranual());
            uiEntity.put("nombreComponenteTemporal", valorInteranualService.getList(values, ComponenteTemporal.get(m.getCompotenteTemporal())).get(0).getNombre());
            list.add(uiEntity);
        }
        return list;
    }

    @GET
    @Path("{marcaId}/indicador/{indicadorId}/csv")
    public Response getMarcasCSV(
            @PathParam("marcaId") Long marcaId, @PathParam("indicadorId") Long indicadorId) throws ParseException, IOException
    {
        List<Marca> marcaList = marcaService.getMarcasByIndicador(marcaId, indicadorId);


        return Response.ok(toCSV(marcaList), "text/csv")
                .header("Content-Disposition", "attachment; filename = Snapshot.csv")
                .build();

    }

    private byte[] toCSV(List<Marca> marcaList) throws IOException, ParseException
    {
        StringWriter sw = new StringWriter();

        CSV.writeLine(sw, Arrays.asList("Indicador", "Codi", "Docència en altres llengües", "Any", "Interanual", "Valor", "Expresió"));
        for (Marca m : marcaList)
        {
            List<String> list = new ArrayList<>();
            list.add(m.getIndicadorId().toString());
            list.add(m.getIndicadorCodigo());
            list.add(m.getNombreAgregacion());
            list.add(m.getResultadoAnyo().toString());
            List<Long> values = new ArrayList<>();
            values.add(m.getResultadoInteranual());
            list.add(valorInteranualService.getList(values, ComponenteTemporal.get(m.getCompotenteTemporal())).get(0).getNombre());
            if (ParamUtils.isNotNull(m.getResultadoValor()))
            {
                list.add(m.getResultadoValor().replace('.',','));
            }
            else
            {
                list.add("");
            }
            list.add(m.getExpresion().replace('.',','));
            list.add(m.getCodeError());
            CSV.writeLine(sw, list);

        }
        sw.flush();
        return sw.toString().getBytes("UTF-8");
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIndicadoresByMarca(
            @QueryParam("query") @DefaultValue("") String marca)
    {
        List<IndicadorMarca> marcaList = marcaService.getIndicadoresMarcaByMarca(marca);

        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (IndicadorMarca indicadorMarca : marcaService.getIndicadoresMarcaByMarca(marca))
        {
            entities.add(buildEntity(indicadorMarca));
        }

        return entities;
    }

    private UIEntity buildEntity(IndicadorMarca indicadorMarca)
    {
        UIEntity entity = new UIEntity();

        entity.put("id", indicadorMarca.getId());
        entity.put("fecha", indicadorMarca.getFecha());
        entity.put("marca", indicadorMarca.getMarca());
        entity.put("indicador", UIEntity.toUI(indicadorMarca.getIndicador()));

        return entity;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        marcaService.deleteMarca(id);

        return Response.noContent().build();
    }
}
