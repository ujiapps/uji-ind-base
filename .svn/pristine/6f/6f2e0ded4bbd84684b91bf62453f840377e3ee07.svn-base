package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.CodigoVariableEnUsoException;
import es.uji.apps.ind.exceptions.CodigoVariableRepetidoException;
import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;
import es.uji.apps.ind.exceptions.VariableSQLIncorrectoException;
import es.uji.apps.ind.model.AgregacionServicio;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.enums.NivelAgregacion;
import es.uji.apps.ind.services.VariableService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("variables")
public class VariableResource
{
    @InjectParam
    private VariableService variableService;

    @Path("{id}/usos")
    public IndicadorUsoResource getPlatformItem(
            @InjectParam IndicadorUsoResource indicadorUsosResource)
    {
        return indicadorUsosResource;
    }

    @Path("{id}/agrupaciones")
    public IndicadorAgrupacionResource getPlatformItem(
            @InjectParam IndicadorAgrupacionResource indicadorAgrupacionResource)
    {
        return indicadorAgrupacionResource;
    }

    @Path("{id}/valores")
    public VariableValorResource getPlatformItem(
            @InjectParam VariableValorResource variableValorResource)
    {
        return variableValorResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariables(@QueryParam("query") @DefaultValue("") String filter)
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Variable variable :variableService.getVariables(filter))
        {
            UIEntity entity = UIEntity.toUI(variable);

            entities.add(entity);
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(variableService.getVariable(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage add(@FormParam("id") Long id, @FormParam("nombre") String nombre,
            @FormParam("codigo") String codigo, @FormParam("dinamica") Integer dinamica,
            @FormParam("sql") String sql, @FormParam("nivelAgregacion") String nivelAgregacion,
            @FormParam("componenteTemporal") Integer componenteTemporal, @FormParam("servicioId") Long servicioId, @FormParam("tipoAnualidad") String tipoAnualidad,@FormParam("observaciones") String observaciones)
            throws VariableFormatoIncorrectoException, CodigoVariableRepetidoException,
            CodigoVariableEnUsoException, VariableSQLIncorrectoException
    {
        ParamUtils.checkNotNull(nombre, codigo, dinamica, nivelAgregacion, componenteTemporal,servicioId);

        Variable variable = new Variable();
        String codigoAnterior = null;
        Integer componenteTemporalAnterior = null;
        String nivelAgregacionAnterior = null;

        if (id != null)
        {
            variable = variableService.getVariable(id);
            codigoAnterior = variable.getCodigo();
            componenteTemporalAnterior = variable.getComponenteTemporal();
            nivelAgregacionAnterior = variable.getNivelAgregacion();
        }

        variable.setCodigo(codigo);
        variable.setNombre(nombre);
        variable.setDinamica(dinamica);
        variable.setNivelAgregacion(NivelAgregacion.valueOf(nivelAgregacion.toUpperCase())
                .toString());
        variable.setComponenteTemporal(componenteTemporal);
        variable.setObservaciones(observaciones);

        AgregacionServicio agregacionServicio = new AgregacionServicio();
        agregacionServicio.setId(servicioId);
        variable.setServicio(agregacionServicio);


        if (variable.getDinamica().equals(1))
        {
            variable.setSql(sql);
        }

        if (variable.getId() == null)
        {
            variableService.add(variable);
        }
        else
        {
            variableService.update(variable, codigoAnterior, componenteTemporalAnterior, nivelAgregacionAnterior);
        }

        return new ResponseMessage(true);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) throws CodigoVariableEnUsoException
    {
        variableService.deleteVariable(id);

        return Response.noContent().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/duplicar")
    public ResponseMessage duplicate(@PathParam("id") Long id)
            throws CodigoVariableRepetidoException, VariableFormatoIncorrectoException,
            CodigoVariableEnUsoException
    {
        variableService.duplicarVariable(id);

        return new ResponseMessage(true);
    }
}
