package es.uji.apps.ind.formula;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import es.uji.apps.ind.exceptions.VariablesSinValorException;

public class VariableReplacerTest
{
    VariableReplacer variableReplacer = new VariableReplacer();

    @Test
    public void elExtractorDeVariablesDeberiaExtraerTodasLasVariablesDeLaExpresion()
            throws VariablesSinValorException
    {
        String expresion = "e1 + ae12 * (7 / base2_2) + e1";

        Map<String, Float> valores = new HashMap<>();
        valores.put("e1", 1F);
        valores.put("ae12", 2F);
        valores.put("base2_2", (float) 3.3);

        Assert.assertEquals("1.0 + 2.0 * (7 / 3.3) + 1.0",
                variableReplacer.replace(expresion, valores));
    }

    @Test(expected = VariablesSinValorException.class)
    public void debeDevolverUnErrorSiUnaVariableNoTieneValor() throws VariablesSinValorException
    {
        String expresion = "e1 + ae12 * (7 / base2_2) + e1";

        Map<String, Float> valores = new HashMap<>();
        valores.put("e1", 1F);
        valores.put("ae12", 2F);

        variableReplacer.replace(expresion, valores);
    }

    @Test(expected = VariablesSinValorException.class)
    public void debeDevolverUnErrorSiUnaVariableTieneValorANulo() throws VariablesSinValorException
    {
        String expresion = "e1 + ae12 * (7 / base2_2) + e1";

        Map<String, Float> valores = new HashMap<>();
        valores.put("e1", 1F);
        valores.put("ae12", 2F);
        valores.put("base2_2", null);

        variableReplacer.replace(expresion, valores);
    }
}
