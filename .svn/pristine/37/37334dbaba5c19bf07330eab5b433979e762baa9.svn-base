package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.commons.rest.UIEntity;

@Path("indicadores")
public class IndicadorResource
{
    @InjectParam
    private IndicadorService indicadorService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Indicador indicador : indicadorService.getAllIndicadores())
        {
            entities.add(UIEntity.toUI(indicador));
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(indicadorService.getIndicador(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        Indicador indicador = entity.toModel(Indicador.class);

        indicadorService.addIndicador(indicador);

        return UIEntity.toUI(indicador);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(UIEntity entity)
    {
        Indicador indicador = entity.toModel(Indicador.class);

        indicadorService.updateIndicador(indicador);

        return UIEntity.toUI(indicador);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        indicadorService.deleteIndicador(id);

        return Response.noContent().build();
    }
}