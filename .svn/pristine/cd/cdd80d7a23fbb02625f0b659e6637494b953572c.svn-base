Ext.define('IND.controller.ControllerIndicadores',
{
    extend : 'Ext.app.Controller',

    stores : [ 'Indicadores', 'IndicadoresUsos', 'IndicadoresAgrupaciones', 'CalculusVariablesValores', 'NivelesAgregaciones' ],
    model : [ 'Indicador', 'IndicadorUso', 'IndicadorAgrupacion', 'VariableValor' ],

    refs : [
    {
        selector : 'gridIndicadores',
        ref : 'gridIndicadores'
    },
    {
        selector : 'gridIndicadoresUsos',
        ref : 'gridIndicadoresUsos'
    },
    {
        selector : 'gridIndicadoresAgrupaciones',
        ref : 'gridIndicadoresAgrupaciones'
    } ],

    init : function()
    {
        this.control(
        {
            'gridIndicadores button[action=add]' :
            {
                click : this.onButtonAddIndicador
            },

            'gridIndicadores' :
            {
                itemdblclick : this.onItemDblCLick,
                select : this.onSelectRow,
                beforerender : this.clearDetailStores
            },

            'windowIndicadores button[action=close]' :
            {
                click : this.onWindowClose
            },

            'windowIndicadores button[action=save]' :
            {
                click : this.onWindowSave
            },

            'gridIndicadores button[action=duplicate]' :
            {
                click : this.onDuplicateIndicador
            },

            'gridIndicadores button[action=calculate]' :
            {
                click : this.onShowWindowCalculus
            }
        });

        this.window = Ext.create('IND.view.indicador.WindowIndicador');
        this.windowCalculus = Ext.create('IND.view.calculus.WindowCalculus');
    },

    onSelectRow : function(ref, record, index, eOpts)
    {
        var indicadorId = record.data.id;

        this.getGridIndicadoresUsos().loadData(indicadorId);
        this.getGridIndicadoresAgrupaciones().loadData(indicadorId);
    },

    onItemDblCLick : function(ref, record, item, index, e, eOpts)
    {
        this.window.load(record.data.id);
        this.window.show();
    },

    onDuplicateIndicador : function()
    {
        this.getGridIndicadores().duplicateRecord();
    },

    onButtonAddIndicador : function()
    {
        this.window.show();
    },

    onWindowClose : function()
    {
        this.window.clearAndHide();
    },

    onWindowSave : function()
    {
        var window = this.window;
        var ref = this;

        window.save(function()
        {
            window.clearAndHide.call(window);
            ref.getGridIndicadores().reloadData()
        });
    },

    clearDetailStores : function()
    {
        this.getGridIndicadoresUsos().clearStore();
        this.getGridIndicadoresAgrupaciones().clearStore();
    },

    onShowWindowCalculus : function()
    {
        var indicadorId = this.getGridIndicadores().getSelectedId();

        if (indicadorId)
        {
            this.windowCalculus.getIndicadorAndValuesForCombosFromIndicadorId(indicadorId);
            this.windowCalculus.show();
        }
    }
});
