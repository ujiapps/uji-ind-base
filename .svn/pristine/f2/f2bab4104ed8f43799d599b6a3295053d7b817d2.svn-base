package es.uji.apps.ind.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.services.VariableService;
import es.uji.apps.ind.services.VariableValorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class VariableValorResource extends CoreBaseService
{
    @PathParam("id")
    private Long variableId;

    @InjectParam
    private VariableService variableService;

    @InjectParam
    private VariableValorService variableValorService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariablesValores() throws ErrorEjecucionSQLException
    {
        Variable variable = variableService.getVariable(variableId);

        if (variable.getDinamica() == 1)
        {
            return getDynamicValues(variable);
        }

        return getStaticValues();
    }

    private List<UIEntity> getDynamicValues(Variable variable) throws ErrorEjecucionSQLException
    {
        return UIEntity.toUI(variableValorService.executeSelect(variable.getSql()));
    }

    private List<UIEntity> getStaticValues()
    {
        return UIEntity.toUI(variableValorService.getByVariableId(variableId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addVariableValor(UIEntity entity)
    {
        String agregacion = entity.get("agregacion");
        String componenteTemporal = entity.get("componenteTemporal");
        String valor = entity.get("valor");

        if (agregacion == null)
        {
            return new UIEntity();
        }

        ParamUtils.checkNotNull(agregacion, componenteTemporal, valor);

        VariableValor variableValor = new VariableValor();
        variableValor.setVariable(variableService.getVariable(variableId));
        variableValor.setAgregacion(agregacion);
        variableValor.setComponenteTemporal(Float.parseFloat(componenteTemporal));
        variableValor.setValor(valor);

        return UIEntity.toUI(variableValorService.insert(variableValor));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateVariableValor(@PathParam("id") Long id, UIEntity entity)
    {
        String agregacion = entity.get("agregacion");
        String componenteTemporal = entity.get("componenteTemporal");
        String valor = entity.get("valor");

        if (agregacion == null)
        {
            return new UIEntity();
        }

        ParamUtils.checkNotNull(agregacion, componenteTemporal, valor);

        VariableValor variableValor = variableValorService.get(id);
        variableValor.setAgregacion(agregacion);
        variableValor.setComponenteTemporal(Float.parseFloat(componenteTemporal));
        variableValor.setValor(valor);

        return UIEntity.toUI(variableValorService.update(variableValor));
    }

    @DELETE
    @Path("{id}")
    public void deleteVariableValor(@PathParam("id") Long id)
    {
        variableValorService.delete(id);
    }
}
