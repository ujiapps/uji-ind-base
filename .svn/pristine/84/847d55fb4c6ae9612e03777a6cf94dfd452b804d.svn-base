package es.uji.apps.ind.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.IndicadorPorTipo;
import es.uji.apps.ind.model.IndicadorUso;

@Service
public class AgrupadorIndicadoresPorTipoService
{
    @Autowired
    IndicadorService indicadorService;

    public List<IndicadorPorTipo> agrupaPorUso(List<IndicadorUso> indicadoresUsos)
            throws VariablesNoExistentesException
    {
        List<IndicadorPorTipo> indicadoresPorTipo = new ArrayList<>();

        for (IndicadorUso indicadorUso : indicadoresUsos)
        {
            agrupa(indicadoresPorTipo, indicadorUso.getIndicador());
        }

        return indicadoresPorTipo;
    }

    public List<IndicadorPorTipo> agrupaPorAgrupacion(
            List<IndicadorAgrupacion> indicadoresAgrupaciones)
            throws VariablesNoExistentesException
    {
        List<IndicadorPorTipo> indicadoresPorTipo = new ArrayList<>();

        for (IndicadorAgrupacion indicadorAgrupacion : indicadoresAgrupaciones)
        {
            agrupa(indicadoresPorTipo, indicadorAgrupacion.getIndicador());
        }

        return indicadoresPorTipo;
    }

    private void agrupa(List<IndicadorPorTipo> indicadoresPorTipo, Indicador indicador)
            throws VariablesNoExistentesException
    {
        IndicadorPorTipo indicadorPorTipo = getTipoIndicador(indicador, indicadoresPorTipo);
        indicadorPorTipo.addIndicador(indicador);
    }

    private IndicadorPorTipo getTipoIndicador(Indicador indicador,
            List<IndicadorPorTipo> indicadoresPorTipo) throws VariablesNoExistentesException
    {
        for (IndicadorPorTipo indicadorPorTipo : indicadoresPorTipo)
        {
            if (existsIndicadorPorTipo(indicador, indicadorPorTipo))
            {
                return indicadorPorTipo;
            }
        }

        IndicadorPorTipo indicadorPorTipo = new IndicadorPorTipo(
                indicadorService.getNivelAgregacion(indicador),
                indicadorService.getComponenteTemporal(indicador));

        indicadoresPorTipo.add(indicadorPorTipo);

        return indicadorPorTipo;
    }

    private boolean existsIndicadorPorTipo(Indicador indicador, IndicadorPorTipo indicadorPorTipo)
            throws VariablesNoExistentesException
    {
        return indicadorPorTipo.getComponenteTemporal().equals(
                indicadorService.getComponenteTemporal(indicador))
                && indicadorPorTipo.getNivelAgregacion().equals(
                        indicadorService.getNivelAgregacion(indicador));
    }
}
