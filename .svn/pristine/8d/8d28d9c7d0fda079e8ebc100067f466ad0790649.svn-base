package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.IndicadorDAO;
import es.uji.apps.ind.exceptions.FormulaContieneCaracteresNoPermitidosException;
import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.formula.FormulaChecker;
import es.uji.apps.ind.formula.VariableExtractor;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.IndicadorUso;

@Service
public class IndicadorService
{
    private IndicadorDAO indicadorDAO;
    private FormulaChecker formulaChecker;
    private VariableExtractor variableExtractor;

    @Autowired
    public IndicadorService(IndicadorDAO indicadorDAO, FormulaChecker formulaChecker,
            VariableExtractor variableExtractor)
    {
        this.indicadorDAO = indicadorDAO;
        this.formulaChecker = formulaChecker;
        this.variableExtractor = variableExtractor;
    }

    public Indicador getIndicador(Long id)
    {
        List<Indicador> indicadores = indicadorDAO.getById(id);

        if (indicadores != null && !indicadores.isEmpty())
        {
            return indicadores.get(0);
        }

        return null;
    }

    public Indicador addOrUpdateIndicador(Indicador indicador)
            throws VariablesDiferentesTiposException, VariablesNoExistentesException,
            FormulaContieneCaracteresNoPermitidosException
    {
        Long id = indicador.getId();

        checkFormula(indicador.getFormula());

        if (id != null)
        {
            return indicadorDAO.update(indicador);
        }

        return indicadorDAO.insert(indicador);
    }

    private void checkFormula(String formula) throws VariablesDiferentesTiposException,
            VariablesNoExistentesException, FormulaContieneCaracteresNoPermitidosException
    {
        if (formula != null && !formula.isEmpty())
        {
            formulaChecker.check(formula);
        }
    }

    public void deleteIndicador(Long id)
    {
        indicadorDAO.delete(Indicador.class, id);
    }

    public List<Indicador> getAllIndicadores()
    {
        return indicadorDAO.get(Indicador.class);
    }

    public void duplicarIndicador(Long id) throws VariablesDiferentesTiposException,
            VariablesNoExistentesException, FormulaContieneCaracteresNoPermitidosException
    {
        Indicador indicador = getIndicador(id);

        indicador.setId(null);

        for (IndicadorUso indicadorUso : indicador.getUsos())
        {
            indicadorUso.setId(null);
            indicadorUso.setIndicador(indicador);
        }

        for (IndicadorAgrupacion indicadorAgrupacion : indicador.getAgrupaciones())
        {
            indicadorAgrupacion.setId(null);
            indicadorAgrupacion.setIndicador(indicador);
        }

        addOrUpdateIndicador(indicador);
    }
}
