package es.uji.apps.ind.formula;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.services.VariableService;

@Component
public class VariableChecker
{
    private VariableService variableService;
    private VariableExtractor variableExtractor;

    public VariableChecker()
    {
    }

    @Autowired
    public VariableChecker(VariableService variableService, VariableExtractor variableExtractor)
    {
        this.variableService = variableService;
        this.variableExtractor = variableExtractor;
    }

    public void check(String formula) throws VariablesDiferentesTiposException,
            VariablesNoExistentesException
    {
        List<String> variablesCode = variableExtractor.extract(formula);
        List<Variable> variables = variableService.getAllVariablesByCodes(variablesCode);

        if (!variableService.allVariablesAreTheSameType(variables))
        {
            throw new VariablesDiferentesTiposException();
        }

        if (!variableService.existsAllVariables(variablesCode, variables))
        {
            throw new VariablesNoExistentesException();
        }
    }
}
