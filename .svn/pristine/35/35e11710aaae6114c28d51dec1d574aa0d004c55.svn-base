<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>
    <groupId>es.uji.apps.ind</groupId>
    <artifactId>uji-ind-base</artifactId>
    <packaging>war</packaging>
    <version>0.0.4-SNAPSHOT</version>
    <name>uji-ind-base</name>

    <properties>
        <aspectj.version>1.7.4</aspectj.version>
        <jersey.version>1.17</jersey.version>
        <project.build.sourceEncoding>utf-8</project.build.sourceEncoding>
        <spring.version>3.2.8.RELEASE</spring.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>3.1.0</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>es.uji.commons</groupId>
            <artifactId>uji-commons-testing</artifactId>
            <version>0.1.9</version>
        </dependency>

        <!-- Apache POI (Excel) -->

        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi</artifactId>
            <version>3.9</version>
        </dependency>

        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>3.7</version>
        </dependency>

        <!-- Bean Shell -->

        <dependency>
            <groupId>org.beanshell</groupId>
            <artifactId>bsh-core</artifactId>
            <version>2.0b4</version>
        </dependency>

        <!-- Testing -->

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <version>${spring.version}</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.8.1</version>
            <scope>test</scope>
        </dependency>

        <!-- Jersey Test Framework -->

        <dependency>
            <groupId>com.sun.jersey.jersey-test-framework</groupId>
            <artifactId>jersey-test-framework-core</artifactId>
            <version>${jersey.version}</version>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.glassfish</groupId>
                    <artifactId>javax.servlet</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>com.sun.jersey.jersey-test-framework</groupId>
            <artifactId>jersey-test-framework-grizzly</artifactId>
            <version>${jersey.version}</version>
            <scope>test</scope>
        </dependency>

    </dependencies>

    <scm>
        <connection>scm:svn:http://jira.uji.es/repos/uji/IND/uji-ind-base/trunk</connection>
        <url>http://jira.uji.es/repos/uji/IND/uji-ind-base/trunk</url>
    </scm>

    <distributionManagement>
        <repository>
            <id>nexus</id>
            <url>http://devel01.uji.es:8080/nexus/content/repositories/releases</url>
        </repository>
        <snapshotRepository>
            <id>nexus</id>
            <url>http://devel01.uji.es:8080/nexus/content/repositories/snapshots</url>
        </snapshotRepository>
    </distributionManagement>


    <build>
        <finalName>ind</finalName>

        <!-- Sustitución de propiedades -->

        <resources>
            <resource>
                <directory>${basedir}/src/main/resources</directory>
            </resource>
            <resource>
                <directory>${basedir}/src/main/webapp/WEB-INF</directory>
                <includes>
                    <include>web.xml</include>
                </includes>
                <filtering>true</filtering>
                <targetPath>..</targetPath>
            </resource>
        </resources>

        <plugins>

            <!-- Compilación del código -->

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                </configuration>
            </plugin>

            <!-- Servidor de aplicaciones Jetty -->

            <plugin>
                <groupId>org.eclipse.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
                <version>9.1.2.v20140210</version>
                <configuration>
                    <stopKey>ujiapps</stopKey>
                    <stopPort>9905</stopPort>
                    <systemProperties>
                        <systemProperty>
                            <name>jetty.port</name>
                            <value>9006</value>
                        </systemProperty>
                    </systemProperties>
                    <webApp>
                        <contextPath>/ind</contextPath>
                        <descriptor>target/web.xml</descriptor>
                    </webApp>
                </configuration>
            </plugin>

            <!-- Weaving de dependencias -->

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>aspectj-maven-plugin</artifactId>
                <version>1.6</version>
                <dependencies>
                    <dependency>
                        <groupId>org.aspectj</groupId>
                        <artifactId>aspectjrt</artifactId>
                        <version>${aspectj.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.aspectj</groupId>
                        <artifactId>aspectjtools</artifactId>
                        <version>${aspectj.version}</version>
                    </dependency>
                </dependencies>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <showWeaveInfo>false</showWeaveInfo>
                    <verbose>false</verbose>
                    <source>1.7</source>
                    <target>1.7</target>
                    <complianceLevel>1.7</complianceLevel>
                    <weaveDependencies>
                        <weaveDependency>
                            <groupId>es.uji.commons</groupId>
                            <artifactId>uji-commons-db</artifactId>
                        </weaveDependency>
                        <weaveDependency>
                            <groupId>es.uji.commons</groupId>
                            <artifactId>uji-commons-sso</artifactId>
                        </weaveDependency>
                        <weaveDependency>
                            <groupId>es.uji.commons</groupId>
                            <artifactId>uji-commons-rest</artifactId>
                        </weaveDependency>
                    </weaveDependencies>
                    <outxml>true</outxml>
                    <aspectLibraries>
                        <aspectLibrary>
                            <groupId>org.springframework</groupId>
                            <artifactId>spring-aspects</artifactId>
                        </aspectLibrary>
                    </aspectLibraries>
                </configuration>
            </plugin>

            <!-- Sustitución de propiedades en el web.xml -->

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>2.4</version>
                <configuration>
                    <webXml>target/web.xml</webXml>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>properties-maven-plugin</artifactId>
                <version>1.0-alpha-2</version>
                <executions>
                    <execution>
                        <phase>initialize</phase>
                        <goals>
                            <goal>read-project-properties</goal>
                        </goals>
                        <configuration>
                            <files>
                                <file>/etc/uji/ind/app.properties</file>
                            </files>
                        </configuration>
                    </execution>
                </executions>
            </plugin>


            <!-- QueryDSL -->

            <plugin>
                <groupId>com.mysema.maven</groupId>
                <artifactId>maven-apt-plugin</artifactId>
                <version>1.0.4</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>process</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>target/generated-sources/java</outputDirectory>
                            <processor>com.mysema.query.apt.jpa.JPAAnnotationProcessor</processor>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Proceso de release -->

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.4.2</version>
                <configuration>
                    <tagNameFormat>${project.name}_@{project.version}</tagNameFormat>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
