package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.FormulaContieneCaracteresNoPermitidosException;
import es.uji.apps.ind.exceptions.FormulaException;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("indicadores")
public class IndicadorResource
{
    @InjectParam
    private IndicadorService indicadorService;

    @Path("{id}/usos")
    public IndicadorUsoResource getPlatformItem(
            @InjectParam IndicadorUsoResource indicadorUsosResource)
    {
        return indicadorUsosResource;
    }

    @Path("{id}/agrupaciones")
    public IndicadorAgrupacionResource getPlatformItem(
            @InjectParam IndicadorAgrupacionResource indicadorAgrupacionResource)
    {
        return indicadorAgrupacionResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll() throws VariablesNoExistentesException
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Indicador indicador : indicadorService.getAllIndicadores())
        {
            UIEntity entity = UIEntity.toUI(indicador);

            entity.put("nivelAgregacion", indicadorService.getNivelAgregacion(indicador));
            entity.put("componenteTemporal", indicadorService.getComponenteTemporal(indicador));

            entities.add(entity);
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(indicadorService.getIndicador(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/save-calculated-data")
    public ResponseMessage add(@PathParam("id") Long id, @FormParam("anyo") Integer anyo,
            @FormParam("mes") Integer mes, @FormParam("agregacion") String agregacion,
            @FormParam("marca") String marca) throws ErrorEjecucionSQLException
    {
        ParamUtils.checkNotNull(marca);

        indicadorService.guardaResultadosCalculoIndicador(indicadorService.getIndicador(id),
                agregacion, anyo, mes, marca);

        return new ResponseMessage(true);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage add(@FormParam("id") Long id, @FormParam("nombre") String nombre,
            @FormParam("codigo") String codigo, @FormParam("denominacion") String denominacion,
            @FormParam("responsable") String responsable,
            @FormParam("conceptoAEvaluar") String conceptoAEvaluar,
            @FormParam("formula") String formula, @FormParam("observaciones") String observaciones,
            @FormParam("valorReferencia") String valorReferencia) throws VariableException,
            FormulaException
    {
        ParamUtils.checkNotNull(nombre, codigo, denominacion, responsable, conceptoAEvaluar,
                formula);

        Indicador indicador = new Indicador();

        if (id != null)
        {
            indicador = indicadorService.getIndicador(id);
        }

        indicador.setCodigo(codigo);
        indicador.setNombre(nombre);
        indicador.setDenominacion(denominacion);
        indicador.setResponsable(responsable);
        indicador.setConceptoAEvaluar(conceptoAEvaluar);
        indicador.setFormula(formula);
        indicador.setObservaciones(observaciones);
        indicador.setValorReferencia(valorReferencia);

        indicadorService.addOrUpdateIndicador(indicador);

        return new ResponseMessage(true);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        indicadorService.deleteIndicador(id);

        return Response.noContent().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/duplicar")
    public ResponseMessage duplicate(@PathParam("id") Long id) throws VariableException,
            FormulaException
    {
        indicadorService.duplicarIndicador(id);

        return new ResponseMessage(true);
    }
}
