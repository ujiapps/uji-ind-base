package es.uji.apps.ind.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;
import es.uji.apps.ind.exceptions.VariableSQLIncorrectoException;
import es.uji.apps.ind.formula.CodigoVariableChecker;

@Component
@Entity
@Table(name = "IND_VARIABLES")
public class Variable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String codigo;
    private String nombre;
    private Integer dinamica;
    private String sql;
    private String observaciones;

    @Column(name = "NIVEL_AGREGACION")
    private String nivelAgregacion;

    @Column(name = "COMPONENTE_TEMPORAL")
    private Integer componenteTemporal;

    @OneToMany(mappedBy = "variable")
    private Set<VariableIndicador> indicadores;

    @OneToMany(mappedBy = "variable", cascade = CascadeType.ALL)
    private Set<VariableValor> valores;

    @Transient
    private CodigoVariableChecker codigoVariableChecker;

    public Variable()
    {
    }

    @Autowired
    public void setCodigoVariableChecker(CodigoVariableChecker codigoVariableChecker)
    {
        this.codigoVariableChecker = codigoVariableChecker;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo) throws VariableFormatoIncorrectoException
    {
        getCodigoVariableChecker().check(codigo);

        this.codigo = codigo;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Integer getDinamica()
    {
        return dinamica;
    }

    public void setDinamica(Integer dinamica)
    {
        this.dinamica = dinamica;
    }

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)  throws VariableSQLIncorrectoException
    {
        if(sql.toUpperCase().contains("ORDER BY")){
            throw new VariableSQLIncorrectoException("La consulta SQL no pot contenir expressions ORDER BY");
        }
        this.sql = sql;
    }

    public String getNivelAgregacion()
    {
        if (this.nivelAgregacion != null)
        {
            this.nivelAgregacion = this.nivelAgregacion.toUpperCase();
        }

        return nivelAgregacion;
    }

    public void setNivelAgregacion(String nivelAgregacion)
    {
        this.nivelAgregacion = nivelAgregacion;
    }

    public Set<VariableIndicador> getIndicadores()
    {
        return indicadores;
    }

    public void setIndicadores(Set<VariableIndicador> indicadores)
    {
        this.indicadores = indicadores;
    }

    public Integer getComponenteTemporal()
    {
        return componenteTemporal;
    }

    public void setComponenteTemporal(Integer componenteTemporal)
    {
        this.componenteTemporal = componenteTemporal;
    }

    public Set<VariableValor> getValores()
    {
        return valores;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public void setValores(Set<VariableValor> valores)
    {
        this.valores = valores;
    }

    private CodigoVariableChecker getCodigoVariableChecker()
    {
        if (codigoVariableChecker == null)
        {
            codigoVariableChecker = new CodigoVariableChecker();
        }

        return codigoVariableChecker;
    }

    public boolean mismoTipo(Variable variable)
    {
        if (getComponenteTemporal().equals(variable.getComponenteTemporal())
                && getNivelAgregacion().equals(variable.getNivelAgregacion()))
        {
            return true;
        }

        return false;
    }

    public boolean esDinamica()
    {
        return getDinamica() == 1;
    }

    public Variable duplicarVariable(Variable variable) throws VariableFormatoIncorrectoException
    {
        variable.setId(null);
        variable.setCodigo(variable.getCodigo() + "_nova");

        for (VariableValor variableValor : variable.getValores())
        {
            variableValor.setId(null);
            variableValor.setVariable(variable);
        }

        return variable;
    }
}
