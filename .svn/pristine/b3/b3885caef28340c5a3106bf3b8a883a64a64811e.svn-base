package es.uji.apps.ind.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.stereotype.Component;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.model.ConsultasLog;
import es.uji.apps.ind.model.QVariableValor;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Component
public class VariableValorDAO extends BaseDAODatabaseImpl
{
    private QVariableValor variableValor = QVariableValor.variableValor;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

    public List<VariableValor> getFilteredVariablesValorByVariableId(Long variableId,
                                                                     String agregacion, Integer anyo, Long valorInteranual)
    {
        List<VariableValor> valores = getVariablesValorByVariableId(variableId);

        return filter(valores, agregacion, anyo, valorInteranual);
    }

    public List<VariableValor> getVariablesValorByVariableId(Long variableId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(variableValor).where(variableValor.variable.id.eq(variableId));

        return query.list(variableValor);
    }

    private List<VariableValor> filter(List<VariableValor> valores, String agregacion,
                                       Integer anyo, Long valorInteranual)
    {
        return filterByValorInteranual(filterByAnyo(filterByAgregacion(valores, agregacion), anyo),
                valorInteranual);
    }

    private List<VariableValor> filterByAgregacion(List<VariableValor> valores, String agregacion)
    {
        List<VariableValor> filteredValores = new ArrayList<>();

        for (VariableValor valor : valores)
        {
            if (agregacion != null && !agregacion.isEmpty()
                    && valor.getAgregacion().equals(agregacion))
            {
                filteredValores.add(valor);
            }

            if (agregacion == null || agregacion.isEmpty())
            {
                filteredValores.add(valor);
            }
        }

        return filteredValores;
    }

    private List<VariableValor> filterByAnyo(List<VariableValor> valores, Integer anyo)
    {
        List<VariableValor> filteredValores = new ArrayList<>();

        for (VariableValor valor : valores)
        {
            if (anyo != null && valor.getAnyo().equals(anyo))
            {
                filteredValores.add(valor);
            }

            if (anyo == null)
            {
                filteredValores.add(valor);
            }
        }

        return filteredValores;
    }

    private List<VariableValor> filterByValorInteranual(List<VariableValor> valores,
                                                        Long valorInteranual)
    {
        List<VariableValor> filteredValores = new ArrayList<>();

        for (VariableValor valor : valores)
        {
            if (valorInteranual != null && valor.getValorInteranual().equals(valorInteranual))
            {
                filteredValores.add(valor);
            }

            if (valorInteranual == null)
            {
                filteredValores.add(valor);
            }
        }

        return filteredValores;
    }

    public List<VariableValor> executeSelectAndFilterResults(Long userId, String sql, String agregacion,
                                                             Integer anyo, Long valorInteranual) throws ErrorEjecucionSQLException
    {
        List<VariableValor> valores = executeSelect(userId,sql);

        return filter(valores, agregacion, anyo, valorInteranual);
    }

    public List<VariableValor> executeSelect(Long userId, String sql) throws ErrorEjecucionSQLException
    {
        List<VariableValor> valores = new ArrayList<>();
        List<Object[]> resultadoSelect;

        try
        {
            if(ParamUtils.isNotNull(userId))
            {
                insert(new ConsultasLog(userId, sql));
            }
            resultadoSelect = entityManager.createNativeQuery(sql).getResultList();
        }
        catch (JpaSystemException e)
        {
            throw new ErrorEjecucionSQLException(e.getCause().getCause().getCause().toString());
        }
        catch (Exception e)
        {
            throw new ErrorEjecucionSQLException();
        }

        for (Object[] filaSelect : resultadoSelect)
        {
            VariableValor variableValor = new VariableValor();

            try
            {
                variableValor.setAgregacion(String.valueOf(filaSelect[0]));
                variableValor.setAnyo(Integer.valueOf(String.valueOf(filaSelect[1])));
                if (filaSelect[2].getClass().getCanonicalName().equals("java.sql.Timestamp"))
                {
                    Date date = toDate((Timestamp) filaSelect[2]);
                    variableValor.setValorInternual(Long.valueOf(simpleDateFormat.format(date)));
                }
                else
                {
                    variableValor.setValorInternual(Long.valueOf(String.valueOf(filaSelect[2])));
                }
                variableValor.setValor(String.valueOf(filaSelect[3]).replace(",", "."));
            }
            catch (Exception e)
            {
                throw new ErrorEjecucionSQLException(
                        "Errada extraent les dades dels resultats. El segon i el tercer resultat han de ser nombres enters. Tots són obligatoris");
            }

            valores.add(variableValor);
        }

        return valores;
    }

    public static Date toDate(Timestamp timestamp)
    {
        long milliseconds = timestamp.getTime() + (timestamp.getNanos() / 1000000);
        return new Date(milliseconds);
    }

    public List<VariableValor> getFilteredVariablesValoresFromVariablesList(Long userId,
            List<Variable> staticVariables, List<Variable> dynamicVariables, String agregacion,
            Integer anyo, Long valorInteranual) throws ErrorEjecucionSQLException
    {
        String sql = getSelectValoresFromStaticVariables(staticVariables, checkVariableTypeDiaria(staticVariables, dynamicVariables));
        String dynamicSql = getSelectValoresFromDynamicVariables(dynamicVariables);

        if (dynamicSql.length() > 0)
        {
            sql += " union " + dynamicSql;
        }

        return filterByValorInteranual(
                filterByAnyo(filterByAgregacion(executeSelect(userId,sql), agregacion), anyo),
                valorInteranual);
    }

    private boolean checkVariableTypeDiaria(List<Variable> staticVariables,
                                            List<Variable> dynamicVariables)
    {
        for (Variable variable : staticVariables)
        {
            if (variable.getComponenteTemporal().equals(ComponenteTemporal.DIARI.getValue()))
            {
                return true;
            }
        }
        for (Variable variable : dynamicVariables)
        {
            if (variable.getComponenteTemporal().equals(ComponenteTemporal.DIARI.getValue()))
            {
                return true;
            }
        }
        return false;
    }

    private String getSelectValoresFromStaticVariables(List<Variable> staticVariables, Boolean tipoDiaria)
    {
        String sql = "";
        if (tipoDiaria)
        {
            sql = "select agregacion, anyo,  to_date(valor_interanual,'YYYYMMDDHH24MISS') valor_interanual, valor from ind_variables_valores val, ind_variables var where var.id = val.variable_id"
                    + " and dinamica = 0 and upper(var.codigo) in (";
        }
        else
        {
            sql = "select agregacion, anyo, valor_interanual, valor from ind_variables_valores val, ind_variables var where var.id = val.variable_id"
                    + " and dinamica = 0 and upper(var.codigo) in (";
        }
        String stringCodes = "";
        for (Variable variable : staticVariables)
        {
            stringCodes += "'" + variable.getCodigo().toUpperCase() + "',";
        }

        if (staticVariables.size() > 0)
        {
            stringCodes = stringCodes.substring(0, stringCodes.length() - 1);
        }
        else
        {
            stringCodes = "''";
        }

        sql += stringCodes;

        sql += ")";
        return sql;
    }

    private String getSelectValoresFromDynamicVariables(List<Variable> dynamicVariables)
    {
        String sql = "";

        for (int i = 0; i < dynamicVariables.size(); i++)
        {
            Variable variable = dynamicVariables.get(i);

            if (variable.getDinamica() == 1)
            {
                sql += variable.getSql();
            }

            if (i < dynamicVariables.size() - 1)
            {
                sql += " union ";
            }
        }

        return sql;
    }

    public List<String> getDistinctAnyos(Long userId, List<Variable> staticVariables,
                                         List<Variable> dynamicVariables) throws ErrorEjecucionSQLException
    {
        try
        {
            String sql = getSql("anyo", staticVariables, dynamicVariables);
            if(ParamUtils.isNotNull(userId))
            {
                insert(new ConsultasLog(userId, sql));
            }

            return bigDecimalToString(entityManager.createNativeQuery(
                    sql).getResultList());
        }
        catch (JpaSystemException e)
        {
            throw new ErrorEjecucionSQLException(e.getCause().getCause().getCause().toString());
        }
        catch (Exception e)
        {
            throw new ErrorEjecucionSQLException();
        }
    }

    private String getSql(String columna, List<Variable> staticVariables,
                          List<Variable> dynamicVariables)
    {
        String sql = "select distinct " + columna + " from ("
                + getSelectValoresFromStaticVariables(staticVariables, checkVariableTypeDiaria(staticVariables, dynamicVariables));

        String dynamicSql = getSelectValoresFromDynamicVariables(dynamicVariables);

        if (dynamicSql.length() > 0)
        {
            sql += " union " + dynamicSql;
        }

        sql += ") order by 1";

        return sql;
    }

    private List<String> bigDecimalToString(List<BigDecimal> valoresDecimales)
    {
        List<String> valoresString = new ArrayList<>();

        for (BigDecimal decimal : valoresDecimales)
        {
            valoresString.add(decimal.toString());
        }

        return valoresString;
    }

    public List<Long> getDistinctValoresInteranuales(Long userId, List<Variable> staticVariables,
                                                     List<Variable> dynamicVariables) throws ErrorEjecucionSQLException
    {
        try
        {
            String sql = getSql("valor_interanual", staticVariables, dynamicVariables);
            if(ParamUtils.isNotNull(userId))
            {
                insert(new ConsultasLog(userId, sql));
            }
            if (checkVariableTypeDiaria(staticVariables, dynamicVariables))
            {
                return timeStampToLong(entityManager.createNativeQuery(
                        sql).getResultList());
            }
            else
            {
                return bigDecimalToLong(entityManager.createNativeQuery(
                        sql).getResultList());
            }
        }
        catch (JpaSystemException e)
        {
            throw new ErrorEjecucionSQLException(e.getCause().getCause().getCause().toString());
        }
        catch (Exception e)
        {
            throw new ErrorEjecucionSQLException();
        }
    }

    private List<Long> bigDecimalToLong(List<BigDecimal> valoresDecimales)
    {
        List<Long> valoresInteger = new ArrayList<>();

        for (BigDecimal decimal : valoresDecimales)
        {
            valoresInteger.add(decimal.longValue());
        }

        return valoresInteger;
    }

    private List<Long> timeStampToLong(List<Timestamp> valoresTimestamp)
    {
        List<Long> valoresLong = new ArrayList<>();

        for (Timestamp timestamp : valoresTimestamp)
        {
            Date date = toDate(timestamp);
            valoresLong.add(Long.valueOf(simpleDateFormat.format(date)));
        }

        return valoresLong;
    }

    public List<String> getDistinctAgregacionesId(Long userId, List<Variable> staticVariables,
                                                  List<Variable> dynamicVariables) throws ErrorEjecucionSQLException
    {
        try
        {
            String sql = getSql("agregacion", staticVariables, dynamicVariables);
            if(ParamUtils.isNotNull(userId))
            {
                insert(new ConsultasLog(userId, sql));
            }
            return entityManager.createNativeQuery(sql).getResultList();
        }
        catch (JpaSystemException e)
        {
            throw new ErrorEjecucionSQLException(e.getCause().getCause().getCause().toString());
        }
        catch (Exception e)
        {
            throw new ErrorEjecucionSQLException();
        }
    }

    private List<Integer> bigDecimalToInteger(List<BigDecimal> valoresDecimales)
    {
        List<Integer> valoresInteger = new ArrayList<>();

        for (BigDecimal decimal : valoresDecimales)
        {
            valoresInteger.add(decimal.intValue());
        }

        return valoresInteger;
    }

//    public List<VariableValor> getVariableValorsFromDynamicVariables(List<Variable> variables)
//            throws ErrorEjecucionSQLException
//    {
//        String sqlForDinamicVariables = "";
//
//        for (int i = 0; i < variables.size(); i++)
//        {
//            Variable variable = variables.get(i);
//
//            if (variable.getDinamica() == 1)
//            {
//                sqlForDinamicVariables += variable.getSql();
//            }
//
//            if (i < variables.size() - 1)
//            {
//                sqlForDinamicVariables += " union ";
//            }
//        }
//
//        if (sqlForDinamicVariables.isEmpty())
//        {
//            return new ArrayList<>();
//        }
//
//        return executeSelect(userId,sqlForDinamicVariables);
//    }
}