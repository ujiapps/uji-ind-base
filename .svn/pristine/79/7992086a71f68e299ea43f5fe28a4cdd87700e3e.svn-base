package es.uji.apps.ind.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.IndicadorDAO;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.FormulaContieneCaracteresNoPermitidosException;
import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.formula.FormulaChecker;
import es.uji.apps.ind.formula.FormulaEvaluator;
import es.uji.apps.ind.formula.VariableExtractor;
import es.uji.apps.ind.formula.VariableReplacer;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.IndicadorUso;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.VariableValorForCalculus;

@Service
public class IndicadorService
{
    private IndicadorDAO indicadorDAO;
    private FormulaChecker formulaChecker;
    private VariableExtractor variableExtractor;
    private VariableService variableService;
    private VariableValorService variableValorService;
    private VariableReplacer variableReplacer;
    private FormulaEvaluator formulaEvaluator;

    @Autowired
    public IndicadorService(IndicadorDAO indicadorDAO, FormulaChecker formulaChecker,
            VariableExtractor variableExtractor, VariableService variableService,
            VariableValorService variableValorService, VariableReplacer variableReplacer,
            FormulaEvaluator formulaEvaluator)
    {
        this.indicadorDAO = indicadorDAO;
        this.formulaChecker = formulaChecker;
        this.variableExtractor = variableExtractor;
        this.variableService = variableService;
        this.variableValorService = variableValorService;
        this.variableReplacer = variableReplacer;
        this.formulaEvaluator = formulaEvaluator;
    }

    public Indicador getIndicador(Long id)
    {
        List<Indicador> indicadores = indicadorDAO.getById(id);

        if (indicadores != null && !indicadores.isEmpty())
        {
            return indicadores.get(0);
        }

        return null;
    }

    public Indicador addOrUpdateIndicador(Indicador indicador)
            throws VariablesDiferentesTiposException, VariablesNoExistentesException,
            FormulaContieneCaracteresNoPermitidosException, ErrorEvaluacionFormulaException,
            VariablesSinValorException
    {
        Long id = indicador.getId();

        checkFormula(indicador.getFormula());

        if (id != null)
        {
            return indicadorDAO.update(indicador);
        }

        return indicadorDAO.insert(indicador);
    }

    private void checkFormula(String formula) throws VariablesDiferentesTiposException,
            VariablesNoExistentesException, FormulaContieneCaracteresNoPermitidosException,
            ErrorEvaluacionFormulaException, VariablesSinValorException
    {
        if (formula != null && !formula.isEmpty())
        {
            formulaChecker.check(formula);
        }
    }

    public void deleteIndicador(Long id)
    {
        indicadorDAO.delete(Indicador.class, id);
    }

    public List<Indicador> getAllIndicadores()
    {
        return indicadorDAO.get(Indicador.class);
    }

    public void duplicarIndicador(Long id) throws VariablesDiferentesTiposException,
            VariablesNoExistentesException, FormulaContieneCaracteresNoPermitidosException,
            ErrorEvaluacionFormulaException, VariablesSinValorException
    {
        Indicador indicador = getIndicador(id);

        indicador.setId(null);

        for (IndicadorUso indicadorUso : indicador.getUsos())
        {
            indicadorUso.setId(null);
            indicadorUso.setIndicador(indicador);
        }

        for (IndicadorAgrupacion indicadorAgrupacion : indicador.getAgrupaciones())
        {
            indicadorAgrupacion.setId(null);
            indicadorAgrupacion.setIndicador(indicador);
        }

        addOrUpdateIndicador(indicador);
    }

    public String getNivelAgregacion(Indicador indicador) throws VariablesNoExistentesException
    {
        List<String> variableCodes = variableExtractor.extract(indicador.getFormula());

        if (variableCodes.size() > 0)
        {
            Variable variable = variableService.getVariableByCode(variableCodes.get(0));

            if (variable == null)
            {
                throw new VariablesNoExistentesException();
            }

            return variable.getNivelAgregacion().toUpperCase();
        }

        return null;
    }

    public List<String> getDistinctComponentesTemporalesFromIndicador(Indicador indicador)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        return floatToString(variableValorService
                .getDistinctComponentesTemporalesFromVariableList(variableExtractor
                        .extract(indicador.getFormula())));
    }

    private List<String> floatToString(List<Float> componentesTemporales)
    {
        List<String> componenetesTemporalesParseadas = new ArrayList<>();

        for (Float componenteTemporal : componentesTemporales)
        {
            componenetesTemporalesParseadas.add(floatToString(componenteTemporal));
        }

        return componenetesTemporalesParseadas;
    }

    private String floatToString(Float componenteTemporal)
    {
        String componenteTemporalString = componenteTemporal.toString();

        if (componenteTemporalString.endsWith(".0"))
        {
            componenteTemporalString = componenteTemporalString.substring(0,
                    componenteTemporalString.indexOf(".0"));
        }

        return componenteTemporalString;
    }

    public List<VariableValor> getCalculatedData(Indicador indicador, String agregacion,
            Float componenteTemporal) throws VariablesSinValorException,
            ErrorEvaluacionFormulaException, ErrorEjecucionSQLException
    {
        List<String> variableCodes = variableExtractor.extract(indicador.getFormula());

        return buildCalculatedResults(indicador,
                buildVariablesValoresForCalculus(variableCodes, agregacion, componenteTemporal));
    }

    private List<VariableValor> buildCalculatedResults(Indicador indicador,
            List<VariableValorForCalculus> valoresForCalculus)
    {
        List<VariableValor> resultados = new ArrayList<>();

        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            VariableValor valor = new VariableValor();

            valor.setComponenteTemporal(valorForCalculus.getComponenteTemporal());
            valor.setAgregacion(valorForCalculus.getAgregacion());

            try
            {
                valor.setValor(formulaEvaluator.eval(
                        variableReplacer.replace(indicador.getFormula(),
                                valorForCalculus.getValores())).toString());
            }
            catch (Exception e)
            {
            }

            resultados.add(valor);
        }

        return resultados;
    }

    private List<VariableValorForCalculus> buildVariablesValoresForCalculus(
            List<String> variableCodes, String agregacion, Float componenteTemporal)
            throws ErrorEjecucionSQLException
    {
        List<VariableValorForCalculus> valoresForCalculus = new ArrayList<>();

        for (String variableCode : variableCodes)
        {
            Variable variable = variableService.getVariableByCode(variableCode);

            List<VariableValor> valores = getValores(variable, agregacion, componenteTemporal);

            for (VariableValor valor : valores)
            {
                VariableValorForCalculus valorForCalculus = getIfExists(valoresForCalculus, valor);

                if (valorForCalculus == null)
                {
                    valorForCalculus = new VariableValorForCalculus(valor.getAgregacion(),
                            valor.getComponenteTemporal());

                    valoresForCalculus.add(valorForCalculus);
                }

                valorForCalculus.addValue(variable.getCodigo(), Float.parseFloat(valor.getValor()));
            }
        }

        return valoresForCalculus;
    }

    private List<VariableValor> getValores(Variable variable, String agregacion,
            Float componenteTemporal) throws ErrorEjecucionSQLException
    {
        if (variable.getDinamica() == 1)
        {
            return filterValores(variableValorService.executeSelect(variable.getSql()), agregacion,
                    componenteTemporal);
        }

        return filterValores(variableValorService.getByVariableId(variable.getId()), agregacion,
                componenteTemporal);
    }

    private List<VariableValor> filterValores(List<VariableValor> valores, String agregacion,
            Float componenteTemporal)
    {
        return filterByComponenteTemporal(filterByAgregacion(valores, agregacion),
                componenteTemporal);
    }

    private List<VariableValor> filterByAgregacion(List<VariableValor> valores, String agregacion)
    {
        List<VariableValor> filteredValores = new ArrayList<>();

        for (VariableValor valor : valores)
        {
            if (agregacion != null && !agregacion.isEmpty()
                    && valor.getAgregacion().equals(agregacion))
            {
                filteredValores.add(valor);
            }

            if (agregacion == null || agregacion.isEmpty())
            {
                filteredValores.add(valor);
            }
        }

        return filteredValores;
    }

    private List<VariableValor> filterByComponenteTemporal(List<VariableValor> valores,
            Float componenteTemporal)
    {
        List<VariableValor> filteredValores = new ArrayList<>();

        for (VariableValor valor : valores)
        {
            if (componenteTemporal != null
                    && valor.getComponenteTemporal().equals(componenteTemporal))
            {
                filteredValores.add(valor);
            }

            if (componenteTemporal == null)
            {
                filteredValores.add(valor);
            }
        }

        return filteredValores;
    }

    private VariableValorForCalculus getIfExists(List<VariableValorForCalculus> valoresForCalculus,
            VariableValor valor)
    {
        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            if (valorForCalculus.getAgregacion().equals(valor.getAgregacion())
                    && valorForCalculus.getComponenteTemporal().equals(
                            valor.getComponenteTemporal()))
            {
                return valorForCalculus;
            }
        }

        return null;
    }
}
