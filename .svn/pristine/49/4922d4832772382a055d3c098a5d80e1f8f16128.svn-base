package es.uji.apps.ind.services.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.SoloSeAdmitenFicherosExcelException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.services.AgregacionesService;
import es.uji.apps.ind.services.ImportDatosVariablesService;
import es.uji.apps.ind.services.VariableService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

public class VariableValorResource extends CoreBaseService
{
    @PathParam("id")
    private Long variableId;

    @InjectParam
    private VariableService variableService;

    @InjectParam
    private ImportDatosVariablesService importDatosVariablesService;

    @InjectParam
    private AgregacionesService agregacionesService;

    @Context
    ServletContext servletContext;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariablesValores() throws ErrorEjecucionSQLException
    {
        Variable variable = variableService.getVariable(variableId);

        if (variable.getDinamica() == 1)
        {
            return getDynamicValues(variable);
        }

        return getStaticValues(variable);
    }

    private List<UIEntity> getDynamicValues(Variable variable) throws ErrorEjecucionSQLException
    {
        return UIEntity.toUI(variableService.executeSelect(variable.getSql()));
    }

    private List<UIEntity> getStaticValues(Variable variable)
    {
        return UIEntity.toUI(variableService.getByVariableId(variableId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addVariableValor(UIEntity entity)
    {
        String agregacion = entity.get("agregacion");
        String componenteTemporal = entity.get("componenteTemporal");
        String valor = entity.get("valor");

        if (agregacion == null)
        {
            return new UIEntity();
        }

        ParamUtils.checkNotNull(agregacion, componenteTemporal, valor);

        VariableValor variableValor = new VariableValor();
        variableValor.setVariable(variableService.getVariable(variableId));
        variableValor.setAgregacion(agregacion);
        variableValor.setComponenteTemporal(Float.parseFloat(componenteTemporal));
        variableValor.setValor(valor);

        return UIEntity.toUI(variableService.insert(variableValor));
    }

    @POST
    @Path("excel")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage addFromExcelFile(@FormDataParam("fileExcel") byte[] file,
                                            @FormDataParam("fileExcel") FormDataContentDisposition fileDetail)
            throws SoloSeAdmitenFicherosExcelException, IOException
    {
        ParamUtils.checkNotNull(fileDetail, file);

        String nombre = fileDetail.getFileName();
        String mimeType = servletContext.getMimeType(nombre);
        Variable variable = variableService.getVariable(variableId);

        if (file.length > 0 && nombre != null)
        {
            importDatosVariablesService.insertFromExcel(new ByteArrayInputStream(file), variable, mimeType);
        }

        return new ResponseMessage(true);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateVariableValor(@PathParam("id") Long id, UIEntity entity)
    {
        String agregacion = entity.get("agregacion");
        String componenteTemporal = entity.get("componenteTemporal");
        String valor = entity.get("valor");

        if (agregacion == null)
        {
            return new UIEntity();
        }

        ParamUtils.checkNotNull(componenteTemporal, valor);

        VariableValor variableValor = variableService.getVariableValor(id);
        variableValor.setAgregacion(agregacion);
        variableValor.setComponenteTemporal(Float.parseFloat(componenteTemporal));
        variableValor.setValor(valor);

        return UIEntity.toUI(variableService.update(variableValor));
    }

    @DELETE
    @Path("{id}")
    public void deleteVariableValor(@PathParam("id") Long id)
    {
        variableService.delete(id);
    }
}
