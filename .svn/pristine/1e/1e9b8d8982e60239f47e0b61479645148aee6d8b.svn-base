package es.uji.apps.apa.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.apa.model.AplicacionResponsable;
import es.uji.apps.apa.model.Persona;
import es.uji.apps.apa.services.AplicacionResponsableService;
import es.uji.apps.apa.services.PersonaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class AplicacionResponsableResource extends CoreBaseService
{
    @PathParam("id")
    private Long aplicacionId;

    @InjectParam
    private AplicacionResponsableService aplicacionResponsableService;

    @InjectParam
    private PersonaService personaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAplicacionesResponsables()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (AplicacionResponsable aplicacionResponsable : aplicacionResponsableService
                .getByAplicacionId(aplicacionId))
        {
            UIEntity entity = UIEntity.toUI(aplicacionResponsable);

            entities.add(addPersonData(entity, aplicacionResponsable.getPersona()));
        }

        return entities;
    }

    @DELETE
    @Path("{aplicacionResponsableId}")
    public void deleteAplicacionResponsable(
            @PathParam("aplicacionResponsableId") Long aplicacionResponsableId)
    {
        aplicacionResponsableService.delete(aplicacionResponsableId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addAplicacionResponsable(UIEntity entity)
    {
        Long personaId = ParamUtils.parseLong(entity.get("personaId"));

        ParamUtils.checkNotNull(personaId, aplicacionId);

        return addPersonData(
                UIEntity.toUI(aplicacionResponsableService.insert(personaId, aplicacionId)),
                personaService.getById(personaId));
    }

    private UIEntity addPersonData(UIEntity entity, Persona persona)
    {
        entity.put("personaId", persona.getId());
        entity.put("personaNombre", persona.getNombreCompleto());
        entity.put("personaCuenta", persona.getCuenta());

        return entity;
    }
}
