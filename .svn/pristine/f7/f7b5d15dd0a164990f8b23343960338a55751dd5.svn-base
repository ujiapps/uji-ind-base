package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorMarcaResultado;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QIndicadorAgrupacion;
import es.uji.apps.ind.model.QIndicadorMarca;
import es.uji.apps.ind.model.QIndicadorMarcaResultado;
import es.uji.apps.ind.model.QIndicadorUso;
import es.uji.apps.ind.model.enums.TipoMarca;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;

@Repository
public class IndicadorDAO extends BaseDAODatabaseImpl
{
    private QIndicador indicador = QIndicador.indicador;

    public Indicador getById(Long id)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.usos).fetch()
                .leftJoin(indicador.agrupaciones).fetch()
                .leftJoin(indicador.variables).fetch()
                .where(indicador.id.eq(id));

        return query.uniqueResult(indicador);
    }

    public List<Indicador> getIndicadoresByCode(String codigo)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador)
                .where(indicador.formula.like("%" + codigo + "%"));

        return query.list(indicador);
    }

    public List<Indicador> buscarIndicadores(String descripcion,Long uso, Long agrupacion)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QIndicadorUso qIndicadorUso= QIndicadorUso.indicadorUso;
        QIndicadorAgrupacion qIndicadorAgrupacion= QIndicadorAgrupacion.indicadorAgrupacion;

        query.from(indicador).leftJoin(indicador.usos, qIndicadorUso).fetch()
                .leftJoin(indicador.agrupaciones, qIndicadorAgrupacion).fetch()
                .leftJoin(indicador.variables).fetch();

        if(ParamUtils.isNotNull(descripcion))
        {
            String s = StringUtils.limpiaAcentos(descripcion).toUpperCase();
            query.where(indicador.nombre.upper().like('%' + s + '%').or(indicador.codigo.upper().like('%'+s+'%')));
        }
        if(ParamUtils.isNotNull(uso)){
            query.where(qIndicadorUso.uso.id.eq(uso));
        }
        if(ParamUtils.isNotNull(agrupacion)){
            query.where(qIndicadorAgrupacion.agrupacion.id.eq(agrupacion));
        }

        return query.distinct().list(indicador);
    }

    public List<Indicador> getAllIndicadores()
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.variables).fetch();
        return query.distinct().list(indicador);
    }

    public void borraCache(Long id)
    {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager,qIndicadorMarca);

        deleteClause.where(qIndicadorMarca.indicador.id.eq(id).and(qIndicadorMarca.tipo.eq(TipoMarca.CACHE))).execute();
    }

    public List<IndicadorMarcaResultado> getResultadosCache(Indicador indicador, String agregacion, Integer anyo, Long valorInteranual)
    {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca,qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.id.eq(indicador.getId())));
        if(ParamUtils.isNotNull(agregacion)){
           query.where(qIndicadorMarcaResultado.agregacion.eq(agregacion));
        }

        if(ParamUtils.isNotNull(anyo)){
            query.where(qIndicadorMarcaResultado.anyo.eq(anyo));
        }
        if(ParamUtils.isNotNull(valorInteranual)){
            query.where(qIndicadorMarcaResultado.valorInteranual.eq(valorInteranual));
        }

        return query.list(qIndicadorMarcaResultado);
    }

    public List<Integer> getDistinctAnyosFromIndicadorCache(List<Indicador> indicador)
    {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca,qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.in(indicador)));


        return query.distinct().list(qIndicadorMarcaResultado.anyo);

    }

    public List<String> getDistinctAgregacionesIdCache(Indicador indicador)
    {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca,qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.id.eq(indicador.getId())));


        return query.distinct().list(qIndicadorMarcaResultado.agregacion);
    }

    public List<Long> getDistinctValoresInteranualesCache(List<Indicador> indicadores)
    {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca,qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.in(indicadores)));


        return query.distinct().list(qIndicadorMarcaResultado.valorInteranual);
    }
}
