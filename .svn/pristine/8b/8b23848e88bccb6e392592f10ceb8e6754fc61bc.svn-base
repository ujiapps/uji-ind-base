package es.uji.apps.ind.services.rest;

import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.enums.Agregacion;
import es.uji.apps.ind.model.enums.TipoAnualidad;
import es.uji.apps.ind.services.VariableService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("variables")
public class VariableResource
{
    public static final int NUM_MESES_ANYO = 12;
    @InjectParam
    private VariableService variableService;

    @Path("{id}/usos")
    public IndicadorUsoResource getPlatformItem(
            @InjectParam IndicadorUsoResource indicadorUsosResource)
    {
        return indicadorUsosResource;
    }

    @Path("{id}/agrupaciones")
    public IndicadorAgrupacionResource getPlatformItem(
            @InjectParam IndicadorAgrupacionResource indicadorAgrupacionResource)
    {
        return indicadorAgrupacionResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Variable variable : variableService.getAllVariables())
        {
            UIEntity entity = UIEntity.toUI(variable);

            entities.add(entity);
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(variableService.getVariable(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage add(@FormParam("id") Long id, @FormParam("nombre") String nombre,
            @FormParam("codigo") String codigo, @FormParam("dinamica") Integer dinamica,
            @FormParam("sql") String sql, @FormParam("nivelAgregacion") String nivelAgregacion,
            @FormParam("numMeses") Float numMeses, @FormParam("tipoAnualidad") String tipoAnualidad)
            throws VariableFormatoIncorrectoException
    {
        ParamUtils.checkNotNull(nombre, codigo, dinamica, nivelAgregacion, numMeses);

        Variable variable = new Variable();

        if (id != null)
        {
            variable = variableService.getVariable(id);
        }

        variable.setCodigo(codigo);
        variable.setNombre(nombre);
        variable.setDinamica(dinamica);
        variable.setNivelAgregacion(Agregacion.valueOf(nivelAgregacion.toUpperCase()).toString());
        variable.setNumMeses(numMeses);

        if (variable.getDinamica().equals(1))
        {
            variable.setSql(sql);
        }

        variable.setTipoAnualidad(null);
        if (numMeses != null && isDivisiblePorNumMesesAnyo(numMeses))
        {
            variable.setTipoAnualidad(TipoAnualidad.valueOf(tipoAnualidad.toUpperCase()).toString());
        }

        variableService.addOrUpdateVariable(variable);

        return new ResponseMessage(true);
    }

    private boolean isDivisiblePorNumMesesAnyo(Float numMeses)
    {
        return (numMeses % NUM_MESES_ANYO) == 0;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        variableService.deleteVariable(id);

        return Response.noContent().build();
    }
}
