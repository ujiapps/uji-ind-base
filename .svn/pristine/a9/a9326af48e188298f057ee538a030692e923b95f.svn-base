package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.Grupo;
import es.uji.apps.ind.services.GrupoService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

@Path("grupos")
public class GrupoResource
{
    @InjectParam
    private GrupoService grupoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Grupo grupo : grupoService.getAllGrupos())
        {
            entities.add(UIEntity.toUI(grupo));
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(grupoService.getGrupo(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        Grupo grupo = entity.toModel(Grupo.class);

        grupoService.addGrupo(grupo);

        return UIEntity.toUI(grupo);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(UIEntity entity)
    {
        Grupo grupo = entity.toModel(Grupo.class);

        grupoService.updateGrupo(grupo);

        return UIEntity.toUI(grupo);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        grupoService.deleteGrupo(id);

        return Response.noContent().build();
    }

    @GET
    @Path("test")
    public ResponseMessage test()
    {
        UIEntity entity = new UIEntity();
        entity.put("a", 1);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setTotalCount(100);
        responseMessage.setData(Collections.singletonList(entity));

        return responseMessage;
    }

    @GET
    @Path("test2")
    public ResponseMessage test2()
    {
        UIEntity entity1 = new UIEntity();
        entity1.put("a", 1);

        UIEntity entity2 = new UIEntity();
        entity2.put("a", 2);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(Arrays.asList(entity1, entity2));

        return responseMessage;
    }

    @GET
    @Path("test3")
    public UIEntity test3()
    {
        UIEntity entity0 = new UIEntity();
        entity0.put("e", 1);

        UIEntity entity1 = new UIEntity();
        entity1.put("c", 1);
        entity1.put("d", entity0);

        UIEntity entity2 = new UIEntity();
        entity2.put("a", 2);
        entity2.put("b", entity1);

        return entity2;
    }

    @GET
    @Path("test4")
    public ResponseMessage test4()
    {
        UIEntity entity0 = new UIEntity();
        entity0.put("e", 1);

        UIEntity entity1 = new UIEntity();
        entity1.put("c", 1);
        entity1.put("d", entity0);

        UIEntity entity2 = new UIEntity();
        entity2.put("a", 2);
        entity2.put("b", entity1);

        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(Collections.singletonList(entity2));

        return responseMessage;
    }
}