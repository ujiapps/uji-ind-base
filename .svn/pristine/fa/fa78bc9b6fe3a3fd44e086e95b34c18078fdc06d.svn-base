package es.uji.apps.ind.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.ind.dao.IndicadorDAO;
import es.uji.apps.ind.dao.VariableIndicadorDAO;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.FormulaException;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.formula.VariableValorForCalculusConversor;
import es.uji.apps.ind.model.Agregacion;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorMarca;
import es.uji.apps.ind.model.IndicadorMarcaResultado;
import es.uji.apps.ind.model.ValorInteranual;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableIndicador;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.VariableValorForCalculus;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import es.uji.apps.ind.model.enums.TipoMarca;
import es.uji.commons.rest.ParamUtils;

@Service
public class IndicadorService
{
    private IndicadorDAO indicadorDAO;
    private VariableIndicadorDAO variableIndicadorDAO;
    private VariableService variableService;
    private VariableValorForCalculusConversor variableValorForCalculusConversor;
    private ValorInteranualService valorInteranualService;
    private AgregacionesService agregacionesService;

    @Autowired
    public IndicadorService(IndicadorDAO indicadorDAO, VariableService variableService,
                            VariableValorForCalculusConversor variableValorForCalculusConversor,
                            ValorInteranualService valorInteranualService, AgregacionesService agregacionesService,
                            VariableIndicadorDAO variableIndicadorDAO)
    {
        this.indicadorDAO = indicadorDAO;
        this.variableValorForCalculusConversor = variableValorForCalculusConversor;
        this.variableService = variableService;
        this.valorInteranualService = valorInteranualService;
        this.agregacionesService = agregacionesService;
        this.variableIndicadorDAO = variableIndicadorDAO;
    }

    public Indicador getIndicador(Long id)
    {
        return indicadorDAO.getById(id);
    }

    @Transactional
    public Indicador addOrUpdateIndicador(Indicador indicador) throws VariableException,
            FormulaException
    {
        Long id = indicador.getId();
        Indicador result = new Indicador();
        List<String> variablesCode = indicador.getVariablesFormula();
        List<Variable> variables = variableService.getAllVariablesByCodes(variablesCode);

        indicador.compruebaFormula(variables);

        if (id != null)
        {
            result = indicadorDAO.update(indicador);
            variableIndicadorDAO.deleteByIndicadorId(id);
        } else
        {
            result = indicadorDAO.insert(indicador);
        }

        for (Variable variable : variables)
        {
            VariableIndicador variableIndicador = new VariableIndicador();
            variableIndicador.setIndicador(result);
            variableIndicador.setVariable(variable);
            variableIndicadorDAO.insert(variableIndicador);
        }
        return result;
    }

    public void deleteIndicador(Long id)
    {
        indicadorDAO.delete(Indicador.class, id);
    }

    public List<Indicador> getAllIndicadores()
    {
        return indicadorDAO.getAllIndicadores();
    }

    public void duplicarIndicador(Long id) throws VariableException, FormulaException
    {
        Indicador indicador = getIndicador(id);
        addOrUpdateIndicador(indicador.duplicar(indicador));
    }

    public String getNivelAgregacion(Indicador indicador) throws VariablesNoExistentesException
    {
        Variable firstVariable = getFirstVariable(indicador);

        if (firstVariable != null)
        {
            return firstVariable.getNivelAgregacion();
        }

        return null;
    }

    public String getComponenteTemporal(Indicador indicador) throws VariablesNoExistentesException
    {
        Variable firstVariable = getFirstVariable(indicador);

        if (firstVariable != null)
        {
            return ComponenteTemporal.get(firstVariable.getComponenteTemporal());
        }

        return null;
    }

    private Variable getFirstVariable(Indicador indicador) throws VariablesNoExistentesException
    {
        for (VariableIndicador variable : indicador.getVariables())
        {
            return variable.getVariable();
        }
        return null;
    }

    public List<Agregacion> getAgregaciones(Long userId,Indicador indicador)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        return getAgregaciones(userId,indicador, "ca");
    }

    public List<Agregacion> getAgregaciones(Long userId,Indicador indicador, String idioma)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        return agregacionesService.getFilteredList(getNivelAgregacion(indicador).toString(),
                variableService.getDistinctAgregacionesIdFromVariableList(userId,indicador
                        .getVariablesFormula()), idioma);
    }

    public Agregacion getAgregacion(Long userId,Indicador indicador, String idioma,Long agregacion)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        return agregacionesService.getFilteredList(getNivelAgregacion(indicador).toString(),
                variableService.getDistinctAgregacionesIdFromVariableList(userId,indicador
                        .getVariablesFormula()), idioma, agregacion);
    }

    public List<String> getDistinctAnyosFromIndicador(Long userId,Indicador indicador)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        return variableService.getDistinctAnyosFromVariableList(userId,indicador.getVariablesFormula());
    }

    public List<String> getDistinctAnyosFromListIndicadores(Long userId,List<Indicador> indicadores)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        List<String> variables = new ArrayList<>();

        for (Indicador indicador : indicadores)
        {
            variables.addAll(indicador.getVariablesFormula());
        }

        return variableService.getDistinctAnyosFromVariableList(userId,variables);
    }

    public List<ValorInteranual> getValoresInteranualesFromIndicador(Long userId,Indicador indicador)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException
    {
        List<Long> values = variableService
                .getDistinctValoresInteranualesFromVariableList(userId,indicador.getVariablesFormula());
        String componenteTemporal = getComponenteTemporal(indicador);
        return valorInteranualService.getList(values, componenteTemporal);
    }

    public List<ValorInteranual> getValoresInteranualesFromListIndicadores(Long userId,
            List<Indicador> indicadores) throws VariablesNoExistentesException,
            ErrorEjecucionSQLException, ParseException
    {
        List<String> variables = new ArrayList<>();

        for (Indicador indicador : indicadores)
        {
            variables.addAll(indicador.getVariablesFormula());
        }

        List<Long> resultList = variableService
                .getDistinctValoresInteranualesFromVariableList(userId,variables);

        String componenteTemporal = getComponenteTemporal(indicadores.get(0));

        return valorInteranualService.getList(resultList, componenteTemporal);
    }

    public List<IndicadorMarcaResultado> calculaIndicador(Long userId,Indicador indicador, Long agregacion,
                                                          Integer anyo, Long valorInteranual, String cache) throws ErrorEjecucionSQLException, VariablesSinValorException
    {
        if (cache == "0")
        {
            guardaResultadosCalculoIndicador(userId,indicador, null, null, null, null, TipoMarca.CACHE.toString());
        }
        return getResultadosCache(indicador, agregacion, anyo, valorInteranual);
    }

    public List<IndicadorMarcaResultado> getResultadosCache(Indicador indicador, Long agregacion,
                                                            Integer anyo, Long valorInteranual)
    {
        return indicadorDAO.getResultadosCache(indicador, agregacion, anyo, valorInteranual);
    }

    @Transactional
    public void guardaResultadosCalculoIndicador(Long userId,Indicador indicador, Long agregacion,
                                                 Integer anyo, Long valorInteranual, String marca, String tipoMarca) throws ErrorEjecucionSQLException, VariablesSinValorException
    {
        List<VariableValor> resultados = getResultados(userId,indicador, agregacion, anyo, valorInteranual);

        if (resultados.size() == 0)
        {
            return;
        }

        IndicadorMarca indicadorMarca = new IndicadorMarca();
        indicadorMarca.setIndicador(indicador);
        if (tipoMarca.equals(TipoMarca.CACHE.toString()))
        {
            indicadorDAO.borraCache(indicador.getId());
            indicadorMarca.setMarca("Cache");
            indicadorMarca.setTipo(TipoMarca.CACHE);
        } else
        {
            indicadorMarca.setMarca(marca);
            indicadorMarca.setTipo(TipoMarca.MARCA);
        }

        Set<IndicadorMarcaResultado> resultadosParaGuardar = new HashSet<>();

        for (VariableValor valor : resultados)
        {
            IndicadorMarcaResultado marcaResultado = new IndicadorMarcaResultado();

            marcaResultado.setMarca(indicadorMarca);
            marcaResultado.setAnyo(valor.getAnyo());
            marcaResultado.setValorInteranual(valor.getValorInteranual());
            marcaResultado.setAgregacion(valor.getAgregacion());
            marcaResultado.setValor(valor.getValor());

            resultadosParaGuardar.add(marcaResultado);
        }

        indicadorMarca.setResultados(resultadosParaGuardar);

        indicadorDAO.insert(indicadorMarca);
    }

    private List<VariableValor> getResultados(Long userId,Indicador indicador, Long agregacion, Integer anyo,
                                              Long valorInteranual) throws ErrorEjecucionSQLException, VariablesSinValorException
    {
        List<VariableValorForCalculus> valoresForCalculus = new ArrayList<>();

        for (VariableIndicador variableIndicador : indicador.getVariables())
        {
            List<VariableValor> valores = getValores(userId,variableIndicador.getVariable(), agregacion,
                    anyo, valorInteranual);

            valoresForCalculus = variableValorForCalculusConversor.add(valoresForCalculus, valores,
                    variableIndicador.getVariable());
        }

        return indicador.calcula(valoresForCalculus);
    }

    private List<VariableValor> getValores(Long userId,Variable variable, Long agregacion, Integer anyo,
                                           Long valorInteranual) throws ErrorEjecucionSQLException
    {
        return variableService.getFilteredVariablesValorFromVariableId(userId,variable.getId(),
                agregacion, anyo, valorInteranual);
    }

    public List<Indicador> buscarIndicadores(String descripcion, Long uso, Long agrupacion,
                                             String agregacion) throws VariablesNoExistentesException
    {
        List<Indicador> list = indicadorDAO.buscarIndicadores(descripcion, uso, agrupacion);
        List<Indicador> result = new ArrayList<>();
        if (ParamUtils.isNotNull(agregacion))
        {
            for (Indicador indicador : list)
            {
                if (getNivelAgregacion(indicador).equals(agregacion.toUpperCase()))
                {
                    result.add(indicador);
                }
            }
            return result;
        }
        return list;
    }

    public List<String> getDistinctAnyosFromCache(Indicador indicador)
    {
        List<String> res = new ArrayList<>();
        for(Integer any: indicadorDAO.getDistinctAnyosFromIndicadorCache(Collections.singletonList(indicador))){
         res.add(any.toString());
        }
        return res;
    }

    public List<String> getDistinctAnyosFromListIndicadoresCache(List<Indicador> indicadores)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException
    {
        List<String> res = new ArrayList<>();
        for(Integer any: indicadorDAO.getDistinctAnyosFromIndicadorCache(indicadores)){
            res.add(any.toString());
        }
        return res;
    }

//    public List<Agregacion> getAgregacionesCache(Indicador indicador, String idioma) throws VariablesNoExistentesException
//    {
//            return agregacionesService.getFilteredList(getNivelAgregacion(indicador).toString(),
//                    indicadorDAO.getDistinctAgregacionesIdCache(indicador), idioma);
//    }
    public List<Agregacion> getAgregacionesCache(List<Indicador> indicadores, String idioma) throws VariablesNoExistentesException
    {
        return agregacionesService.getFilteredList(getNivelAgregacion(indicadores.get(0)).toString(),
                indicadorDAO.getDistinctAgregacionesIdCache(indicadores), idioma);
    }

    public List<ValorInteranual> getValoresInteranualesCache(
            List<Indicador> indicadores) throws VariablesNoExistentesException,
            ErrorEjecucionSQLException, ParseException
    {
        List<Long> resultList = indicadorDAO.getDistinctValoresInteranualesCache(indicadores);
        String componenteTemporal = getComponenteTemporal(indicadores.get(0));

        return valorInteranualService.getList(resultList, componenteTemporal);
    }

    public Date getUltimaActulizacion(Long id)
    {
        return indicadorDAO.getUltimaActulizacion(id);

    }
}
