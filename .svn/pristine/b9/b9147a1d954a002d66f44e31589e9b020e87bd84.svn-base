package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.IndicadorUso;
import es.uji.apps.ind.model.Uso;
import es.uji.apps.ind.services.UsoService;
import es.uji.commons.rest.UIEntity;

@Path("usos")
public class UsoResource
{
    @InjectParam
    private UsoService usoService;

    @Path("{id}/indicadores")
    public UsoIndicadorResource getPlatformItem(
            @InjectParam UsoIndicadorResource usoIndicadorResource)
    {
        return usoIndicadorResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (Uso uso : usoService.getAllUsos())
        {
            entities.add(UIEntity.toUI(uso));
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id)
    {
        return UIEntity.toUI(usoService.getUso(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        Uso uso = entity.toModel(Uso.class);

        usoService.addUso(uso);

        return UIEntity.toUI(uso);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(UIEntity entity)
    {
        Uso uso = entity.toModel(Uso.class);

        usoService.updateUso(uso);

        return UIEntity.toUI(uso);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        usoService.deleteUso(id);

        return Response.noContent().build();
    }
}
