package es.uji.apps.apa.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.apa.model.Rol;
import es.uji.apps.apa.services.RolService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("rol")
public class RolResource extends CoreBaseService
{
    @InjectParam
    private RolService rolService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getRoles()
    {
        return UIEntity.toUI(rolService.get());
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getRol(@PathParam("id") Long id)
    {
        return UIEntity.toUI(rolService.getById(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addRol(UIEntity entity)
    {
        return UIEntity.toUI(rolService.insert(entity.toModel(Rol.class)));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateRol(UIEntity entity)
    {
        return UIEntity.toUI(rolService.update(entity.toModel(Rol.class)));
    }

    @DELETE
    @Path("{id}")
    public void deleteRol(@PathParam("id") Long id)
    {
        rolService.delete(id);
    }
}
