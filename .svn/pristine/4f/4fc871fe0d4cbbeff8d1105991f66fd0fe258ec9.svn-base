package es.uji.apps.apa.dao;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.apa.model.AplicacionItem;
import es.uji.apps.apa.model.QAplicacionItem;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Component
public class AplicacionItemDAO extends BaseDAODatabaseImpl
{
    private QAplicacionItem qAplicacionItem = QAplicacionItem.aplicacionItem;

    public List<AplicacionItem> getByAplicacionId(Long aplicacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qAplicacionItem).where(qAplicacionItem.aplicacion.id.eq(aplicacionId));

        return query.orderBy(qAplicacionItem.orden.asc()).orderBy(qAplicacionItem.id.desc())
                .list(qAplicacionItem);
    }

    @Transactional
    public void actualizarOrdenAplicacionesItems(Long aplicacionId,
            List<AplicacionItem> aplicacionesItemsAActualizar, Integer orden)
    {
        if (aplicacionesItemsAActualizar == null || aplicacionesItemsAActualizar.isEmpty())
        {
            return;
        }

        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager, qAplicacionItem);

        updateClause
                .where(qAplicacionItem.aplicacion.id.eq(aplicacionId).and(
                        qAplicacionItem.in(aplicacionesItemsAActualizar)))
                .set(qAplicacionItem.orden, qAplicacionItem.orden.add(orden)).execute();
    }
}
