package es.uji.apps.ind.formula;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

@Component
public class VariableExtractor
{
    private Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");

    private Matcher matcher;

    public List<String> extract(String expresion)
    {
        matcher = pattern.matcher(expresion);

        List<String> allMatches = new ArrayList<>();

        while (matcher.find())
        {
            allMatches.add(matcher.group());
        }

        return allMatches;
    }
}
