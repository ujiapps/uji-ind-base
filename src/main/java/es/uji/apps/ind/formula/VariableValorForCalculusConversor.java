package es.uji.apps.ind.formula;

import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.VariableValorForCalculus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VariableValorForCalculusConversor
{
    public final static Logger log = LoggerFactory.getLogger(VariableValorForCalculusConversor.class);

    public List<VariableValorForCalculus> add(List<VariableValorForCalculus> valoresForCalculus,
                                              List<VariableValor> valores, Variable variable) throws VariablesSinValorException
    {
        for (VariableValor valor : valores)
        {
            try
            {
                VariableValorForCalculus valorForCalculus = getIfExists(valoresForCalculus, valor);

                if (valorForCalculus == null)
                {
                    valorForCalculus = new VariableValorForCalculus(valor.getAgregacion(),
                            valor.getAnyo(), valor.getValorInteranual());

                    valoresForCalculus.add(valorForCalculus);
                }
//                Double val = null;
//                try{
//                   val = Double.parseDouble(valor.getValor());
//                }catch (NumberFormatException e){
//                    val = Double.parseDouble(valor.getValor().replace(',','.'));
//                }

                valorForCalculus.addValue(variable.getCodigo(), valor.getValor());

            } catch (NullPointerException | NumberFormatException e)
            {
                log.error("Error en valor de variable " + variable.getCodigo(), e);
                throw new VariablesSinValorException("La variable " + variable.getCodigo() + " te valors incorrectes.");
            }
        }

        return valoresForCalculus;
    }

    private VariableValorForCalculus getIfExists(List<VariableValorForCalculus> valoresForCalculus,
                                                 VariableValor valor)
    {
        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            if (valorForCalculus.getAgregacion().equals(valor.getAgregacion())
                    && valorForCalculus.getAnyo().equals(valor.getAnyo())
                    && valorForCalculus.getValorInteranual().equals(valor.getValorInteranual()))
            {
                return valorForCalculus;
            }
        }

        return null;
    }
}
