package es.uji.apps.ind.formula;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.FormulaContieneCaracteresNoPermitidosException;
import es.uji.apps.ind.exceptions.VariablesDiferentesTiposException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.Variable;

@Component
public class FormulaChecker
{
    public static final BigDecimal VARIABLE_PRESENTE = new BigDecimal("1.0");

    private String pattern = "[a-zA-Z0-9\\._\\*\\+\\-/\\(\\)\\ ]*";

    private VariableChecker variableChecker;
    private FormulaEvaluator formulaEvaluator;
    private VariableExtractor variableExtractor;
    private VariableReplacer variableReplacer;

    @Autowired
    public FormulaChecker(VariableChecker variableChecker, FormulaEvaluator formulaEvaluator,
                          VariableExtractor variableExtractor, VariableReplacer variableReplacer)
    {
        this.variableChecker = variableChecker;
        this.variableExtractor = variableExtractor;
        this.formulaEvaluator = formulaEvaluator;
        this.variableReplacer = variableReplacer;
    }

    public void check(String formula, List<Variable> variables) throws FormulaContieneCaracteresNoPermitidosException,
            VariablesDiferentesTiposException, VariablesNoExistentesException, VariablesSinValorException, ErrorEvaluacionFormulaException
    {
        if (formula == null || formula.isEmpty())
        {
            return;
        }

        if (!formula.matches(pattern))
        {
            throw new FormulaContieneCaracteresNoPermitidosException();
        }

        variableChecker.checkFormula(formula, variables);

        List<String> variablesCode = variableExtractor.extract(formula);
        Map<String, BigDecimal> valores = new HashMap<>();

        for (String variablecode : variablesCode)
        {
            valores.put(variablecode, VARIABLE_PRESENTE);
        }

        formulaEvaluator.eval(variableReplacer.replace(formula, valores));
    }
}
