package es.uji.apps.ind.formula;

import bsh.EvalError;
import bsh.Interpreter;
import bsh.ParseException;
import bsh.TargetError;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;

@Component
public class FormulaEvaluator
{
    private Interpreter interpreter;

    public FormulaEvaluator()
    {
        interpreter = new Interpreter();
    }

    public BigDecimal eval(String formula) throws ErrorEvaluacionFormulaException
    {
        BigDecimal result = new BigDecimal("0.0");

        try
        {
            Object o = interpreter.eval(formula);
            if (o == null)
            {
                throw new ErrorEvaluacionFormulaException();
            }
            else
            {
                result = new BigDecimal(o.toString());

            }
        }
        catch (TargetError e)
        {
            throw new ErrorEvaluacionFormulaException("Errada en la execucuó de la fòrmula: "
                    + e.getTarget());
        }
        catch (ParseException e)
        {
            throw new ErrorEvaluacionFormulaException("Errada sintáctica de la fòrmula: "
                    + e.getMessage());
        }
        catch (EvalError e)
        {
            throw new ErrorEvaluacionFormulaException();
        }

//        if (result == BigDecimal.NEGATIVE_INFINITY || result == Float.POSITIVE_INFINITY)
//        {
//            throw new ErrorEvaluacionFormulaException(
//                    "Errada en la execucuó de la fòrmula: resultat infinit, possible divisió per 0");
//        }

        return result;
    }
}
