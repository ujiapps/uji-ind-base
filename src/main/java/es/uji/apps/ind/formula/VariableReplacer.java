package es.uji.apps.ind.formula;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import es.uji.apps.ind.exceptions.VariablesSinValorException;

@Component
public class VariableReplacer
{
    private String expresion;
    private Map<String, BigDecimal> valores;

    private Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
    private Matcher matcher;
    private List<String> allMatches = new ArrayList<>();

    public VariableReplacer()
    {
    }

    public VariableReplacer(String expresion, Map<String, BigDecimal> valores)
    {
        super();
        this.expresion = expresion;
        this.valores = valores;
    }

    public String replace() throws VariablesSinValorException
    {
        matcher = pattern.matcher(expresion);
        StringBuilder builder = new StringBuilder();
        Boolean variableSinValor= false;

        int i = 0;
        while (matcher.find())
        {
            BigDecimal replacement = valores.get(matcher.group());

            builder.append(expresion.substring(i, matcher.start()));

            if (replacement == null)
            {
                builder.append(matcher.group());
                variableSinValor=true;
            }
            else
            {
                builder.append(replacement);
            }

            i = matcher.end();
        }

        builder.append(expresion.substring(i, expresion.length()));
        if(variableSinValor){
            throw new VariablesSinValorException(builder.toString());
        }

        return builder.toString();
    }

    public String replace(String expresion, Map<String, BigDecimal> valores) throws VariablesSinValorException

    {
        this.expresion = expresion;
        this.valores = valores;

        return replace();
    }
}
