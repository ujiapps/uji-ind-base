package es.uji.apps.ind.utils;

import com.mysema.query.support.Expressions;
import com.mysema.query.types.expr.StringExpression;
import com.mysema.query.types.path.StringPath;
import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.IndicadorMarca;
import es.uji.apps.ind.model.IndicadorUso;
import es.uji.commons.web.template.HTMLTemplate;
import es.uji.commons.web.template.Template;
import es.uji.commons.web.template.model.GrupoMenu;
import es.uji.commons.web.template.model.ItemMenu;
import es.uji.commons.web.template.model.Menu;
import es.uji.commons.web.template.model.Pagina;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class AppUtils
{
    private static Pagina getPaginaPublicacion(String url, String idioma, Boolean publico, Boolean showMenu) throws ParseException
    {
        String baseUrl = "/ind/rest/publicacion";
        if (publico)
        {
            baseUrl = "/ind/rest/public";
        }
        Menu menu = new Menu();
        if(showMenu)
        {
            GrupoMenu grupo = null;
            if (idioma.equals("ca"))
            {
                grupo = new GrupoMenu("Indicadors");
                grupo.addItem(new ItemMenu("Buscador", baseUrl));

            }
            else if (idioma.equals("es"))
            {
                grupo = new GrupoMenu("Indicadores");
                grupo.addItem(new ItemMenu("Buscador", baseUrl));

            }
            else if (idioma.equals("en"))
            {
                grupo = new GrupoMenu("Indicators");
                grupo.addItem(new ItemMenu("Search", baseUrl));

            }

            menu.addGrupo(grupo);
        }
        Pagina pagina = new Pagina("ind", url, idioma, "IND");
        pagina.setTitulo("IND");
        pagina.setMenu(menu);

        return pagina;
    }

    public static Template buildPagina(String url, String idioma, Boolean publico) throws ParseException
    {
        return buildPagina(url, idioma, publico, true);

    }

    public static Template buildPagina(String url, String idioma, Boolean publico, Boolean showMenu) throws ParseException
    {
        Template template = new HTMLTemplate("ind/base", new Locale(idioma), "ind");
        String base = "/ind/rest/publicacion";
        if (publico)
        {
            base = "/ind/rest/public";
        }
        template.put("pagina", getPaginaPublicacion(url, idioma, publico, showMenu));
        template.put("server", "https://ujiapps.uji.es");
        template.put("idioma", idioma);
        template.put("publico", publico);
        template.put("base", base);
        template.put("urlBase", "/ind/" + url);

        return template;
    }

    public static Integer toInteger(String id)
    {
        return (id == null ? null : new Integer(id));
    }

    public static Long toLong(String id)
    {
        return (id == null ? null : new Long(id));
    }

    public static String normalizeId(String id)
    {
        return "-1".equals(id) ? null : id;
    }

    public static String joinUsos(Set<IndicadorUso> list)
    {

        StringBuilder sb = new StringBuilder();
        String delim = ", ";
        String loopDelim = "";

        for (IndicadorUso s : list)
        {

            sb.append(loopDelim);
            sb.append(s.getUso().getNombre());

            loopDelim = delim;
        }

        return sb.toString();
    }

    public static String joinAgrupaciones(Set<IndicadorAgrupacion> list)
    {

        StringBuilder sb = new StringBuilder();
        String delim = ", ";
        String loopDelim = "";

        for (IndicadorAgrupacion s : list)
        {

            sb.append(loopDelim);
            sb.append(s.getAgrupacion().getNombre());

            loopDelim = delim;
        }

        return sb.toString();
    }

    public static Map<Long, String> marcasToUI(Set<IndicadorMarca> marcas)
    {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        Map<Long, String> treeMap = new TreeMap<Long, String>();
        for (IndicadorMarca indicadorMarca : marcas)
        {
            treeMap.put(indicadorMarca.getId(), indicadorMarca.getMarca() + " (" + df.format(indicadorMarca.getFecha()) + ")");
        }
        return treeMap;
    }

    public static StringExpression limpiaAcentos(StringPath nombre)
    {
        return Expressions.stringTemplate("upper(translate({0}, 'âàãáÁÂÀÃéêÉÊíÍóôõÓÔÕüúÜÚÇç', 'aaaaaaaaeeeeiioooooouuuucc'))", nombre);
    }


}
