package es.uji.apps.ind.excelReader;

import org.springframework.stereotype.Component;

import es.uji.apps.ind.model.enums.TipoFicheroExcel;

@Component
public class ValorExtractorFactory
{
    public ValorExtractor getExtractor(TipoFicheroExcel tipoFicheroExcel)
    {
        if (tipoFicheroExcel.equals(TipoFicheroExcel.XLS))
        {
            return new ValorExtractorFromXLS();
        }

        return new ValorExtractorFromXLSX();
    }
}
