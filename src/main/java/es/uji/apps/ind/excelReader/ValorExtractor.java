package es.uji.apps.ind.excelReader;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import es.uji.apps.ind.model.VariableValor;

public abstract class ValorExtractor
{
    public abstract List<VariableValor> extract(InputStream input) throws IOException;

    protected Long castToAgregacionValue(Cell cell)
    {
        if (isNumeric(cell))
        {
            Double value = cell.getNumericCellValue();
            return value.longValue();
        }
        else
        {
            String value =  cell.getStringCellValue();
            return Long.valueOf(value);
        }
    }

    protected BigDecimal castToValorValue(Cell cell)
    {
        if (isNumeric(cell))
        {
            Double value = cell.getNumericCellValue();
            return new BigDecimal(value.toString());
        }
        else
        {
            return new BigDecimal(cell.getStringCellValue());
        }
    }

    protected boolean isValid(Row row)
    {
        return row.getCell(1) != null && isNumeric(row.getCell(1));
    }

    private boolean isNumeric(Cell cell)
    {
        return CellType.NUMERIC == cell.getCellType();
    }
}
