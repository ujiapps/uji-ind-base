package es.uji.apps.ind.excelReader;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import es.uji.apps.ind.model.VariableValor;

public class ValorExtractorFromXLSX extends ValorExtractor
{
    public List<VariableValor> extract(InputStream input) throws IOException
    {
        List<VariableValor> valores = new ArrayList<>();

        XSSFWorkbook wb = new XSSFWorkbook(input);
        XSSFSheet sheet = wb.getSheetAt(0);

        Iterator rows = sheet.rowIterator();

        while (rows.hasNext())
        {
            XSSFRow row = (XSSFRow) rows.next();

            VariableValor valor = new VariableValor();

            if (isValid(row))
            {
                valor.setAgregacion(castToAgregacionValue(row.getCell(0)));
                valor.setAnyo((int) row.getCell(1).getNumericCellValue());
                if(DateUtil.isCellDateFormatted(row.getCell(2)))
                {
                    Date date = row.getCell(2).getDateCellValue();
                    SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyyMMddHHmmss");
                    valor.setValorInteranual(Long.parseLong(formateadorFecha.format(date)));
                }else{
                    valor.setValorInteranual((long) row.getCell(2).getNumericCellValue());

                }
                valor.setValor(castToValorValue(row.getCell(3)));

                valores.add(valor);
            }
        }

        return valores;
    }
}
