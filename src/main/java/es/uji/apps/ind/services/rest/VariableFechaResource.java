package es.uji.apps.ind.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.exceptions.VariableFechaIncorrectaException;
import es.uji.apps.ind.model.VariableFecha;
import es.uji.apps.ind.model.VariableFechaPK;
import es.uji.apps.ind.services.VariableFechaService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class VariableFechaResource extends CoreBaseService {

    @PathParam("id")
    private Long variableId;

    @InjectParam
    private VariableFechaService variableFechaService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getVariableFechas() {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            List<UIEntity> fechas = variableFechaService.getVariableFechas(variableId);
            responseMessage.setData(fechas);
        } catch (Exception e) {
            responseMessage.setSuccess(false);
            e.printStackTrace();
        }

        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addVariableFecha(UIEntity entity) throws VariableFechaIncorrectaException {
        VariableFecha variableFecha = new VariableFecha();
        VariableFechaPK variableFechaPK = new VariableFechaPK();

        try {
            variableFechaPK.setVariableId(variableId);
            if (entity.get("fechaInicio") != null)
                variableFechaPK.setFechaInicio(new SimpleDateFormat("dd/MM/yyyy").parse(entity.get("fechaInicio")));

            if (entity.get("fechaFin") != null)
                variableFecha.setFechaFin(new SimpleDateFormat("dd/MM/yyyy").parse(entity.get("fechaFin")));
            variableFecha.setId(variableFechaPK);

            variableFechaService.addVariableFecha(variableFecha);
            return entity;
        } catch (ParseException e) {
            throw new VariableFechaIncorrectaException("Error al insertar el registre. Per favor, revisa les dates i prova de nou.");
        }

    }

    @DELETE
    @Path("{id}")
    public Response deleteVariableFecha(@PathParam("id") String id, UIEntity entity) throws Exception {
        try {
            variableFechaService.deleteVariableFecha(entity);
            return Response.ok().build();
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception("Error al eliminar el registre");
        }

    }
}
