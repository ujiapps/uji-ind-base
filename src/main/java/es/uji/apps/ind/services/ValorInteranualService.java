package es.uji.apps.ind.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import es.uji.apps.ind.model.ValorInteranual;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import es.uji.apps.ind.model.enums.ValorInteranualAnual;
import es.uji.apps.ind.model.enums.ValorInteranualCuatrimestral;
import es.uji.apps.ind.model.enums.ValorInteranualCurso;
import es.uji.apps.ind.model.enums.ValorInteranualMensual;
import es.uji.apps.ind.model.enums.ValorInteranualSemestral;
import es.uji.apps.ind.model.enums.ValorInteranualTrimestral;

@Service
public class ValorInteranualService
{
    public List<ValorInteranual> getList(List<Long> values, String componenteTemporal) throws ParseException
    {
        List<ValorInteranual> result = Collections.EMPTY_LIST;

        if (componenteTemporal.equals(ComponenteTemporal.MENSUAL.toString()))
        {
            result = ValorInteranualMensual.getValues(values);
        }

        if (componenteTemporal.equals(ComponenteTemporal.TRIMESTRE.toString()))
        {
            result = ValorInteranualTrimestral.getValues(values);
        }

        if (componenteTemporal.equals(ComponenteTemporal.QUADRIMESTRE.toString()))
        {
            result = ValorInteranualCuatrimestral.getValues(values);
        }

        if (componenteTemporal.equals(ComponenteTemporal.SEMESTRE.toString()))
        {
            result = ValorInteranualSemestral.getValues(values);
        }

        if (componenteTemporal.equals(ComponenteTemporal.ANY.toString()))
        {
            result = ValorInteranualAnual.getValues(values);
        }

        if (componenteTemporal.equals(ComponenteTemporal.CURS.toString()))
        {
            result = ValorInteranualCurso.getValues(values);
        }

        if (componenteTemporal.equals(ComponenteTemporal.DIARI.toString()))
        {
            List<ValorInteranual> valorInteranualList = new ArrayList<>();
            for(Long value : values){
                SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyyMMddHHmmss");
                Date fecha =  formateadorFecha.parse(value.toString());
                SimpleDateFormat formatFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                ValorInteranual valorInteranual = new ValorInteranual(value,formatFecha.format(fecha));
                valorInteranualList.add(valorInteranual);
            }
            result=valorInteranualList;
        }

        return result;
    }
    public String get(Long value, String componenteTemporal) throws ParseException
    {
        String result = null;

        if (componenteTemporal.equals(ComponenteTemporal.MENSUAL.toString()))
        {
            result = ValorInteranualMensual.get(value);
        }

        if (componenteTemporal.equals(ComponenteTemporal.TRIMESTRE.toString()))
        {
            result = ValorInteranualTrimestral.get(value);
        }

        if (componenteTemporal.equals(ComponenteTemporal.QUADRIMESTRE.toString()))
        {
            result = ValorInteranualCuatrimestral.get(value);
        }

        if (componenteTemporal.equals(ComponenteTemporal.SEMESTRE.toString()))
        {
            result = ValorInteranualSemestral.get(value);
        }

        if (componenteTemporal.equals(ComponenteTemporal.ANY.toString()))
        {
            result = ValorInteranualAnual.get(value);
        }

        if (componenteTemporal.equals(ComponenteTemporal.CURS.toString()))
        {
            result = ValorInteranualCurso.get(value);
        }
        if (componenteTemporal.equals(ComponenteTemporal.CURS.name().concat("/").concat(ComponenteTemporal.ANY.name())))
        {
            result = ComponenteTemporal.CURS.name().concat("/").concat(ComponenteTemporal.ANY.name());
        }

        if (componenteTemporal.equals(ComponenteTemporal.DIARI.toString()))
        {
                SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyyMMddHHmmss");
                Date fecha =  formateadorFecha.parse(value.toString());
                SimpleDateFormat formatFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                result = formatFecha.format(fecha);
        }

        return result;
    }
}
