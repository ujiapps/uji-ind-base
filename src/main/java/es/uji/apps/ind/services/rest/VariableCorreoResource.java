package es.uji.apps.ind.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.model.VariableCorreo;
import es.uji.apps.ind.services.VariableCorreoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import oracle.jdbc.proxy.annotation.Post;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class VariableCorreoResource extends CoreBaseService {

    @PathParam("id")
    private Long variableId;

    @InjectParam
    private VariableCorreoService variableCorreoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getVariablesCorreos() throws Exception {
        ResponseMessage responseMessage = new ResponseMessage(true);
        try {
            List<UIEntity> correos = variableCorreoService.getVariablesCorreos(variableId);
            responseMessage.setData(correos);

        } catch (Exception e) {
            responseMessage.setSuccess(false);
            e.printStackTrace();
        }

        return responseMessage;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addVariableCorreo(UIEntity entity){
        variableCorreoService.addVariableCorreo(variableId, entity);
        return entity;
    }

    @DELETE
    @Path("{id}")
    public Response deleteVariableCorreo(@PathParam("id") String id, UIEntity entity){
        variableCorreoService.deleteVariableCorreo(variableId, entity.get("correo"));
        return Response.ok().build();
    }
}
