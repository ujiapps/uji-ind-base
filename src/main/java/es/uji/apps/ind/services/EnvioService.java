package es.uji.apps.ind.services;

import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.MediaType;

@Service
public class EnvioService {

    public void enviaCorreoDenegacion(String responsable, Long revisionId, String motivo, String variableCodigo) throws MessageNotSentException {

        MailMessage mensaje = new MailMessage("IND");

        mensaje.setTitle("Notificació (informativa) al procés de validació de variables/dades dels indicadors");
        mensaje.setContentType(MediaType.TEXT_HTML);

        mensaje.setContent("Benvolgut usuari, Benvolguda usuària,<br><br>Et comuniquem que les variables/dades del procés de validació amb codi: " + revisionId + " sobre la variable " + variableCodigo + "<br>han sigut rebutjades amb el següent comentari: " + motivo + "<br><br>Salutacions,");

        String responder = "noreply@uji.es";
        mensaje.setReplyTo(responder);
        mensaje.setSender(responder + " <" + responder + ">");

        mensaje.addToRecipient(responsable);

        MessagingClient cliente = new MessagingClient();
        cliente.send(mensaje);
    }

    public void enviaCorreoRevalidacion(String responsable, Long variableId, Long revisionAnteriorId, String variableCodigo) throws MessageNotSentException {
        MailMessage mensaje = new MailMessage("IND");

        mensaje.setTitle("Notificació (d'acció) al procés de validació de variables/dades dels indicadors");
        mensaje.setContentType(MediaType.TEXT_HTML);

        mensaje.setContent("Benvolgut usuari, Benvolguda usuària,<br><br>Et comuniquem que s'inicia la reobertura de validació de les variables/dades del procés de validació amb codi: " + variableId + " sobre la variable " + variableCodigo + " creades a partir de la revisió amb codi: " +  revisionAnteriorId + " .<br><br> Pots accedir polsant <a href='https://ujiapps.uji.es/ind/#validacion-usuario'>ací</a>.<br><br>Salutacions,");
        String responder = "noreply@uji.es";
        mensaje.setReplyTo(responder);
        mensaje.setSender(responder + " <" + responder + ">");

        mensaje.addToRecipient(responsable);

        MessagingClient cliente = new MessagingClient();
        cliente.send(mensaje);
    }

}
