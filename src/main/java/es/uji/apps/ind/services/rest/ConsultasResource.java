package es.uji.apps.ind.services.rest;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.model.ConsultaSQL;
import es.uji.apps.ind.services.ConsultasService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.sso.AccessManager;

@Path("consultas")
public class ConsultasResource extends CoreBaseService
{
    @InjectParam
    private ConsultasService consultasService;


    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ConsultaSQL ejecutaSQl(@FormParam("sql") String sql) throws SQLException, ErrorEjecucionSQLException

    {
        ParamUtils.checkNotNull(sql);
        Long userId = AccessManager.getConnectedUserId(request);
        ConsultaSQL consultaSQL = consultasService.ejecutaSQl(userId,sql);
        return consultaSQL;

    }
}
