package es.uji.apps.ind.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

public class VariableIndicadorResource extends CoreBaseService {

    @PathParam("id")
    private Long variableId;

    @InjectParam
    private IndicadorService indicadorService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariablesIndicadores()
    {
        return UIEntity.toUI(indicadorService.getIndicadoresByVariableId(variableId));
    }
}
