package es.uji.apps.ind.services;

import es.uji.apps.ind.dao.VariableCorreoDAO;
import es.uji.apps.ind.model.VariableCorreo;
import es.uji.apps.ind.model.VariableCorreoPK;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VariableCorreoService {

    private VariableCorreoDAO variableCorreoDAO;

    @Autowired
    public VariableCorreoService(VariableCorreoDAO variableCorreoDAO){
        this.variableCorreoDAO = variableCorreoDAO;
    }
    public List<UIEntity> getVariablesCorreos(Long variableId) {
        List<VariableCorreo> correos = variableCorreoDAO.getVariablesCorreos(variableId);
        List<UIEntity> resultList = new ArrayList<>();
        correos.forEach(variableCorreo -> resultList.add(toUI(variableCorreo)));
        return resultList;
    }

    public void addVariableCorreo(Long variableId, UIEntity entity) {
        VariableCorreo variableCorreo = new VariableCorreo(new VariableCorreoPK(variableId, entity.get("correo")));
        variableCorreoDAO.insert(variableCorreo);
    }

    public void deleteVariableCorreo(Long variableId, String correo) {
        variableCorreoDAO.deleteVariableCorreo(variableId, correo);
    }

    public UIEntity toUI(VariableCorreo variableCorreo){
        UIEntity entity = new UIEntity();
        entity.put("variableId", variableCorreo.getId().getVariableId());
        entity.put("correo", variableCorreo.getId().getCorreo());
        return entity;
    }
}
