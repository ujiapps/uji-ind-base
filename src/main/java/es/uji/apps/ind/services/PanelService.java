package es.uji.apps.ind.services;

import es.uji.apps.ind.dao.PanelDAO;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.*;
import es.uji.apps.ind.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class PanelService {

    public final static Logger log = LoggerFactory.getLogger(PanelService.class);

    private PanelDAO panelDao;
    private static IndicadorService indicadorService;
    private AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService;

    @Autowired
    public PanelService(PanelDAO panelDao, IndicadorService indicadorService, AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService) {
        this.panelDao = panelDao;
        this.indicadorService = indicadorService;
        this.agrupadorIndicadoresPorTipoService = agrupadorIndicadoresPorTipoService;
    }

    public List<Panel> getPaneles() {
        return panelDao.get(Panel.class);
    }


    public Panel addPanel(Panel panel) {
        return panelDao.insert(panel);
    }

    public Panel updatePanel(Panel panel) {
        return panelDao.update(panel);
    }

    public void deletePanel(Long id) {
        panelDao.delete(Panel.class, id);
    }

    private List<IndicadorCalculoUI> agrupaIndicadores(String idioma, List<Indicador> indicadorList)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException {
        List<IndicadorCalculoUI> indicadoresAgrupados = new ArrayList<>();
        List<IndicadorPorTipo> agrupaciones = agrupadorIndicadoresPorTipoService.agrupaIndicadores(indicadorList);
        if (agrupaciones.size() > 1) {
            agrupaciones.forEach(agrupacion -> {
                agrupacion.getIndicadores().forEach(indicador -> {
                    IndicadorCalculoUI indicadorCalculoUI = new IndicadorCalculoUI();
                    try {
                        indicadorCalculoUI.setIndicadores(IndicadorUI.toUI(Collections.singletonList(indicador), idioma));
                        indicadorCalculoUI.setAnyos(indicadorService.getDistinctAnyosFromCache(indicador));
                        indicadorCalculoUI.setNivelAgregacion(indicadorService.getNivelAgregacion(indicador));
                        indicadorCalculoUI.setComponenteTemporal(indicadorService.getComponenteTemporal(indicador));
                        indicadorCalculoUI.setAgregaciones(indicadorService.getAgregacionesCache(Collections.singletonList(indicador), idioma));
                        indicadorCalculoUI.setValoresInteranuales(indicadorService.getValoresInteranualesCache(Collections.singletonList(indicador)));

                    } catch (ErrorEjecucionSQLException | ParseException | VariablesNoExistentesException e) {
                        log.error("Error al agrupar indicadores", e);
                    }
                    indicadoresAgrupados.add(indicadorCalculoUI);
                });
            });
        } else {
            agrupaciones.forEach(agrupacion -> {
                IndicadorCalculoUI indicadorCalculoUI = new IndicadorCalculoUI();
                List<Indicador> indicadores = agrupacion.getIndicadores();
                try {
                    indicadorCalculoUI.setIndicadores(IndicadorUI.toUI(indicadores,idioma));
                    indicadorCalculoUI.setDescripcion("");
                    indicadorCalculoUI.setAnyos(indicadorService.getDistinctAnyosFromListIndicadoresCache(indicadores));
                    indicadorCalculoUI.setNivelAgregacion(agrupacion.getNivelAgregacion());
                    indicadorCalculoUI.setComponenteTemporal(agrupacion.getComponenteTemporal());
                    indicadorCalculoUI.setAgregaciones(indicadorService.getAgregacionesCache(indicadores, idioma));
                    indicadorCalculoUI.setValoresInteranuales(indicadorService.getValoresInteranualesCache(indicadores));
                } catch (VariablesNoExistentesException | ErrorEjecucionSQLException | ParseException e) {
                    log.error("Error al agrupar indicadores", e);
                }
                indicadoresAgrupados.add(indicadorCalculoUI);
            });
        }
        return indicadoresAgrupados;
    }


    public PanelUI getPanel(Long panelId, String idioma) {
        Panel panel = panelDao.getPanel(panelId);
        PanelUI panelUI = new PanelUI();
        panelUI.setId(panel.getId());
        panelUI.setNombre(panel.getNombre());
        panelUI.setSecciones(panel.getSecciones().stream()
                .sorted(Comparator.comparing(Seccion::getOrden))
                .map(agrupaSecciones(idioma)).collect(Collectors.toList()));
        panelUI.setFiltroCompartido();

        if(panelUI.getFiltroCompartido()){
            IndicadorFiltroUI indicadorFiltroUI = new IndicadorFiltroUI();
            List<Agregacion> agregaciones = new ArrayList<>();
            List<ValorInteranual> valorInteranuales = new ArrayList<>();
            List<String> anyos = new ArrayList<>();
            List<IndicadorUI> indicadores = new ArrayList<>();
            panelUI.getSecciones().stream().flatMap(seccionUI -> seccionUI.getTiposIndicador().stream())
                    .forEach(ind -> {
                       agregaciones.addAll(ind.getAgregaciones());
                        anyos.addAll(ind.getAnyos());
                        indicadores.addAll(ind.getIndicadores());
                        indicadorFiltroUI.setComponenteTemporal(ind.getComponenteTemporal());
                        indicadorFiltroUI.setNivelAgregacion(ind.getNivelAgregacion());
                        valorInteranuales.addAll(ind.getValoresInteranuales());
                    });

            indicadorFiltroUI.setAgregaciones(agregaciones);
            indicadorFiltroUI.setAnyos(anyos);
            indicadorFiltroUI.setValoresInteranuales(valorInteranuales);
            indicadorFiltroUI.setIndicadores(indicadores);
            panelUI.setFiltrosPanel(indicadorFiltroUI);
        }
        return panelUI;

    }

    private Function<Seccion, SeccionUI> agrupaSecciones(String idioma) {
        return seccion -> {
            SeccionUI seccionUI = new SeccionUI();
            seccionUI.setId(seccion.getId());
            seccionUI.setNombre(seccion.getNombre());
            seccionUI.setOrden(seccion.getOrden());
            try {
                seccionUI.setTiposIndicador(agrupaIndicadores(idioma, seccion.getSeccionIndicadores()
                        .stream()
                        .map(SeccionIndicador::getIndicador)
                        .collect(Collectors.toList())));
            } catch (VariablesNoExistentesException | ErrorEjecucionSQLException | ParseException e) {
                log.error("Error al agrupar los indicadores", e);
            }
            seccionUI.setFiltroCompartido();
            return seccionUI;
        };
    }
}