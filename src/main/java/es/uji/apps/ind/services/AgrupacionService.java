package es.uji.apps.ind.services;

import es.uji.apps.ind.dao.AgrupacionDAO;
import es.uji.apps.ind.model.Agrupacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgrupacionService {

    private AgrupacionDAO agrupacionDAO;

    @Autowired
    public AgrupacionService(AgrupacionDAO agrupacionDAO) {
        this.agrupacionDAO = agrupacionDAO;
    }

    public Agrupacion getAgrupacion(Long id) {
        List<Agrupacion> agrupaciones = agrupacionDAO.get(Agrupacion.class, id);

        if (agrupaciones != null && !agrupaciones.isEmpty()) {
            return agrupaciones.get(0);
        }

        return null;
    }

    public Agrupacion addAgrupacion(Agrupacion agrupacion) {
        return agrupacionDAO.insert(agrupacion);
    }

    public Agrupacion updateAgrupacion(Agrupacion agrupacion) {
        return agrupacionDAO.update(agrupacion);
    }

    public void deleteAgrupacion(Long id) {
        agrupacionDAO.delete(Agrupacion.class, id);
    }

    public List<Agrupacion> getAllAgrupaciones() {
        return agrupacionDAO.get();
    }

    public void toggleActivarAgrupacion(Long id, Boolean activo) {
        agrupacionDAO.toggleActivarAgrupacion(id, activo);
    }

    public List<Agrupacion> getAgrupacionesActivas() {
        return agrupacionDAO.getAgrupacionesActivas();
    }
}