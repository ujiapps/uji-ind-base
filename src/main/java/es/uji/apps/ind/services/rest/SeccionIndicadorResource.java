package es.uji.apps.ind.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.services.SeccionIndicadorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

public class SeccionIndicadorResource extends CoreBaseService
{
    @PathParam("id")
    private Long seccionId;

    @InjectParam
    private SeccionIndicadorService seccionIndicadorService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIncidadores()
    {
        return seccionIndicadorService.getIndicadoresBySeccionId(seccionId).stream()
                .map(a ->{
                    UIEntity ui = UIEntity.toUI(a);
                    ui.put("indicadorNombre", a.getIndicador().getNombre() );
                    return ui;
                }).collect(Collectors.toList());
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addIndicador(UIEntity entity)
    {
        return UIEntity.toUI(seccionIndicadorService.insert(seccionId,entity.getLong("indicadorId"),entity.getLong("orden")));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateIndicadorUso(@PathParam("id") Long seccionIndicadorId, UIEntity entity)
    {
         seccionIndicadorService.update(seccionIndicadorId, entity.getLong("indicadorId"),entity.getLong("orden"));
        return Response.ok().build();

    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id)
    {
        seccionIndicadorService.delete(id);
        return Response.ok().build();
    }

}
