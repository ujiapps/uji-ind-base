package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.SeccionDAO;
import es.uji.apps.ind.model.Panel;
import es.uji.apps.ind.model.Seccion;

@Service
public class SeccionService
{
    private SeccionDAO seccionDAO;

    @Autowired
    public SeccionService(SeccionDAO seccionDAO)
    {
        this.seccionDAO = seccionDAO;
    }

    public List<Seccion> getSeccionesByPanelId(Long panelId)
    {
        return seccionDAO.getSeccionesByPanelId(panelId);
    }

    public Seccion insert(Long panelId, Seccion seccion)
    {
        Panel panel = new Panel();
        panel.setId(panelId);
        seccion.setPanel(panel);
        return seccionDAO.insert(seccion);
    }

    public Seccion update(Seccion seccion)
    {
        return seccionDAO.update(seccion);
    }

    public void delete(Long id){
        seccionDAO.delete(Seccion.class,id);
    }

    public Seccion updateSeccion(Seccion seccion)
    {
        seccionDAO.updateSeccion(seccion);
        return seccion;
    }
}
