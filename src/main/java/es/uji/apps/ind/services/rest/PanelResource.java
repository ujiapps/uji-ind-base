package es.uji.apps.ind.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.Panel;
import es.uji.apps.ind.model.enums.TipoPanel;
import es.uji.apps.ind.services.PanelService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

@Path("paneles")
public class PanelResource extends CoreBaseService
{
    @InjectParam
    private PanelService panelService;

    @Path("{id}/secciones")
    public SeccionResource getPlatformItem(
            @InjectParam SeccionResource seccionResource)
    {
        return seccionResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll()
    {
        return UIEntity.toUI(panelService.getPaneles());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity add(UIEntity entity)
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Panel panel =  new Panel(); //entity.toModel(Paneles.class);
        panel.setCreadorId(connectedUserId);
        panel.setNombre(entity.get("nombre"));
        panel.setTipo(TipoPanel.valueOf(entity.get("tipo")));

        return UIEntity.toUI(panelService.addPanel(panel));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity update(UIEntity entity)
    {
        Panel panel =  new Panel();
        panel.setId(entity.getLong("id"));
        panel.setNombre(entity.get("nombre"));
        panel.setCreadorId(entity.getLong("creadorId"));
        panel.setTipo(TipoPanel.valueOf(entity.get("tipo")));

        return UIEntity.toUI(panelService.updatePanel(panel));
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        panelService.deletePanel(id);
        return Response.noContent().build();
    }
}
