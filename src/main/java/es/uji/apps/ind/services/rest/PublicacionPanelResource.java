package es.uji.apps.ind.services.rest;

import java.text.ParseException;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.services.PanelService;
import es.uji.apps.ind.services.PublicacionService;
import es.uji.apps.ind.utils.AppUtils;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.web.template.Template;

public class PublicacionPanelResource extends CoreBaseService {
    @InjectParam
    private PanelService panelService;

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("{id}")
    public Template panelIndicadores(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @PathParam("id") Long panelId)
            throws ParseException {
        Boolean publico = !request.getRequestURI().contains("publicacion");
        Template template = AppUtils.buildPagina("/rest/publicacion/", idioma, publico, false);
        template.put("seccion", "ind/paneles");
        template.put("panel", panelService.getPanel(panelId,idioma));

        return template;
    }

}