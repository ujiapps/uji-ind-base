package es.uji.apps.ind.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.ServiciosAdministradoresDAO;
import es.uji.apps.ind.exceptions.AdministradorServicioException;
import es.uji.apps.ind.model.ServiciosAdministrador;

@Service
public class ServiciosAdministradoresService
{
    public final static Logger log = LoggerFactory.getLogger(ServiciosAdministradoresService.class);

    ServiciosAdministradoresDAO serviciosAdministradoresDAO;

    public ServiciosAdministradoresService(ServiciosAdministradoresDAO serviciosAdministradoresDAO)
    {
        this.serviciosAdministradoresDAO = serviciosAdministradoresDAO;
    }

    public List<ServiciosAdministrador> getServiciosAdministrador(String query)
    {
        return serviciosAdministradoresDAO.getServiciosAdministrador(query);
    }

    public void addAdministrador(Long personaId, Long servicio) throws AdministradorServicioException
    {
        try
        {
            serviciosAdministradoresDAO.insert(new ServiciosAdministrador(personaId, servicio));
        }
        catch (Exception e)
        {
            log.error("Registro duplicado");
            throw new AdministradorServicioException();
        }
    }

    public void deleteAdministrador(Long id)
    {
        serviciosAdministradoresDAO.delete(ServiciosAdministrador.class, id);
    }
}
