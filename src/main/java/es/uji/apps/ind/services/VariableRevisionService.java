package es.uji.apps.ind.services;

import es.uji.apps.ind.dao.VariableDAO;
import es.uji.apps.ind.dao.VariableRevisionDAO;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.model.Paginacion;
import es.uji.apps.ind.model.ResponsableVariable;
import es.uji.apps.ind.model.VariableRevision;
import es.uji.apps.ind.model.VariableValorRevision;
import es.uji.apps.ind.model.submodel.VariableRevisionModel;
import es.uji.commons.messaging.client.MessageNotSentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class VariableRevisionService {
    @Autowired
    private VariableRevisionDAO variableRevisionDAO;
    @Autowired
    private EnvioService envioService;
    @Autowired
    private VariableDAO variableDAO;

    public List<VariableRevisionModel> getVariablesRevisionByUser(Long userId, Paginacion paginacion, List<Map<String, String>> filtros) {
        return variableRevisionDAO.getVariablesRevisionByUser(userId, paginacion, filtros);
    }

    public List<VariableRevisionModel> getVariablesRevision(Paginacion paginacion, List<Map<String, String>> filtros) {
        return variableRevisionDAO.getVariablesRevision(paginacion, filtros);
    }

    public List<VariableValorRevision> getVariablesValorRevision(Long revisionId, Paginacion paginacion) {
        return variableRevisionDAO.getVariableValorRevision(revisionId, paginacion);
    }

    public void updateVariablesRevision(Long revisionId, String estado, String observaciones) throws VariableException {
        try {
            variableRevisionDAO.updateVariablesRevision(revisionId, estado, observaciones);
            //Notificar als usuaris
            if (estado == "N") {
                try {
                    VariableRevision revision = variableRevisionDAO.getVariableRevisionById(revisionId);
                    String variableCodigo = variableDAO.getCodigoVariable(revision.getVariableId());
                    List<ResponsableVariable> responsables = variableRevisionDAO.getResponsablesVariable(revision.getVariableId());
                    for (ResponsableVariable responsable : responsables) {
                            envioService.enviaCorreoDenegacion(responsable.getCorreo(), revisionId, observaciones, variableCodigo);
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        } catch (Exception e) {
            throw new VariableException();
        }
    }

    public VariableRevision addRevalidacion(VariableRevisionModel revision) {
        VariableRevision revalidacion = new VariableRevision();
        Date fechaActual = new Date();
        revalidacion.setFecha(fechaActual);
        Date fechaFin = new Date(fechaActual.getTime() + (1000L * 60L * 60L * 24L * 7L));
        revalidacion.setFechaFin(fechaFin);
        revalidacion.setEstadoRevision(null);
        revalidacion.setVariableId(revision.getVariableId());
        revalidacion.setNombre(revision.getNombre());
        revalidacion.setRevisionAnteriorId(revision.getRevisionAnteriorId());
        revalidacion.setObservaciones("");
        String variableCodigo = variableDAO.getCodigoVariable(revision.getVariableId());
        VariableRevision variableRevision = variableRevisionDAO.insert(revalidacion);
        //Notificar als usuaris
        notificaResponsablesVariable(revision.getVariableId(), revision.getRevisionAnteriorId(), variableCodigo);

        return variableRevision;
    }

    private void notificaResponsablesVariable(Long variableId, Long revisionAnteriorId, String variableCodigo) {
        List<ResponsableVariable> responsables = variableRevisionDAO.getResponsablesVariable(variableId);
        for (ResponsableVariable responsable : responsables) {
            try {
                envioService.enviaCorreoRevalidacion(responsable.getCorreo(), variableId, revisionAnteriorId, variableCodigo);
            } catch (MessageNotSentException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
