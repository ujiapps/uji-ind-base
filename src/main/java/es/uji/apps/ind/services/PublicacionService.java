package es.uji.apps.ind.services;

import es.uji.apps.ind.excelWriter.ExcelWriter;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.IndicadorNoEncontradoException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.model.*;
import es.uji.apps.ind.ui.*;
import es.uji.apps.ind.utils.AppUtils;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PublicacionService {
    public final static Logger log = LoggerFactory.getLogger(PublicacionService.class);

    private IndicadorService indicadorService;
    private UsoService usoService;
    private AgrupacionService agrupacionService;
    private AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService;
    private ValorInteranualService valorInteranualService;
    private VariableService variableService;
    private PanelService panelService;

    @Autowired
    public PublicacionService(IndicadorService indicadorService,
                              UsoService usoService,
                              AgrupacionService agrupacionService,
                              AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService,
                              ValorInteranualService valorInteranualService,
                              VariableService variableService, PanelService panelService) {
        this.indicadorService = indicadorService;
        this.usoService = usoService;
        this.agrupacionService = agrupacionService;
        this.agrupadorIndicadoresPorTipoService = agrupadorIndicadoresPorTipoService;
        this.valorInteranualService = valorInteranualService;
        this.variableService = variableService;
        this.panelService = panelService;
    }

    public Template buscadorTemplate(String idioma, String descripcion, Long uso, Long agrupacion, String agregacion, Boolean publico)
            throws ParseException, VariablesNoExistentesException {
        String url = "rest/publicacion";
        if (publico) {
            url = "rest/public";
        }
        Template template = AppUtils.buildPagina(url, idioma, publico);
        template.put("seccion", "ind/buscador");
        template.put("publico", publico);

        List<Uso> usoList = usoService.getUsosActivos();
        List<Agrupacion> agrupacionList = agrupacionService.getAgrupacionesActivas();
        template.put("usos", usoList);
        template.put("agrupaciones", agrupacionList);
        if (ParamUtils.isNotNull(descripcion) ||
                ParamUtils.isNotNull(uso) ||
                ParamUtils.isNotNull(agrupacion) ||
                ParamUtils.isNotNull(agregacion)) {
            List<Indicador> indicadorList = indicadorService.buscarIndicadores(descripcion, uso, agrupacion, agregacion, publico);
            template.put("indicadores", toUI(indicadorList, idioma));
            template.put("descripcion", descripcion);
            template.put("uso", uso);
            template.put("agrupacion", agrupacion);
            template.put("agregacion", agregacion);
            return template;
        }
        return template;
    }

    public List<IndicadorUI> toUI(List<Indicador> indicadorList, String idioma) throws VariablesNoExistentesException {
        List<IndicadorUI> list = new ArrayList<>();
        for (Indicador indicador : indicadorList) {
            list.add(toUI(indicador, idioma));
        }
        return list;
    }

    public IndicadorUI toUI(Indicador indicador, String idioma) throws VariablesNoExistentesException {
        IndicadorUI indicadorUI = new IndicadorUI();
        indicadorUI.setId(indicador.getId());
        indicadorUI.setPublico(indicador.getPublico());
        switch (idioma.toLowerCase()) {
            case "es":
                indicadorUI.setNombre(indicador.getNombreES() != null ? indicador.getNombreES() : indicador.getNombre());
                indicadorUI.setDenominacion(indicador.getDenominacionES() != null ? indicador.getDenominacionES() : indicador.getDenominacion());
                indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluarES() != null ? indicador.getConceptoAEvaluarES() : indicador.getConceptoAEvaluar());
                indicadorUI.setObservaciones(indicador.getObservacionesES());
                if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())){
                    indicadorUI.setIndicadorReferencia(indicador.getIndicadorReferencia().getNombreES());
                }
                break;
            case "en":
                indicadorUI.setNombre(indicador.getNombreEN() != null ? indicador.getNombreEN() : indicador.getNombre());
                indicadorUI.setDenominacion(indicador.getDenominacionEN() != null ? indicador.getDenominacionEN() : indicador.getDenominacion());
                indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluarEN() != null ? indicador.getConceptoAEvaluarEN() : indicador.getConceptoAEvaluar());
                indicadorUI.setObservaciones(indicador.getObservacionesEN());
                if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())){
                    indicadorUI.setIndicadorReferencia(indicador.getIndicadorReferencia().getNombreEN());
                }
                break;
            default:
                indicadorUI.setNombre(indicador.getNombre());
                indicadorUI.setDenominacion(indicador.getDenominacion());
                indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluar());
                indicadorUI.setObservaciones(indicador.getObservaciones());
                if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())){
                    indicadorUI.setIndicadorReferencia(indicador.getIndicadorReferencia().getNombre());
                }
        }
        indicadorUI.setCodigo(indicador.getCodigo());
        indicadorUI.setResponsable(indicador.getResponsable());
        indicadorUI.setFormula(indicador.getFormula());
        indicadorUI.setValorReferencia(indicador.getValorReferencia());
        indicadorUI.setUsos(AppUtils.joinUsos(indicador.getUsos()));
        indicadorUI.setAgrupaciones(AppUtils.joinAgrupaciones(indicador.getAgrupaciones()));
        indicadorUI.setAgregacion(indicadorService.getNivelAgregacion(indicador));
        indicadorUI.setPeriodo(indicadorService.getComponenteTemporal(indicador));
        indicadorUI.setUltimaActualizacion(indicadorService.getUltimaActulizacion(indicador.getId()));
        indicadorUI.setMarcas(AppUtils.marcasToUI(indicador.getMarcas()));
        indicadorUI.setActivo(indicador.getActivo());
        return indicadorUI;
    }

    public Template indicadorTemplate(Long id, String idioma, Boolean publico)
            throws ParseException, VariablesNoExistentesException, IndicadorNoEncontradoException, ErrorEjecucionSQLException {
        String url = "rest/publicacion/indicador/" + id;
        if (publico) {
            url = "rest/public/indicador/" + id;
        }
        Template template = AppUtils.buildPagina(url, idioma, publico);
        template.put("publico", publico);

        template.put("seccion", "ind/indicador");
        Indicador indicador = indicadorService.getIndicador(id, publico);
        List<Variable> variables = indicador.getVariables().stream().map(vi -> vi.getVariable()).collect(Collectors.toList());

        template.put("indicador", toUI(indicador, idioma));
        template.put("variables", VariablesUI.toUI(variables, idioma));
        template.put("anyos", indicadorService.getDistinctAnyosFromCache(indicador));
        template.put("nivelAgregacion", indicadorService.getNivelAgregacion(indicador));
        template.put("componenteTemporal", indicadorService.getComponenteTemporal(indicador));
        template.put("agregaciones", indicadorService.getAgregacionesCache(Collections.singletonList(indicador), idioma));
        template.put("valoresInteranuales", indicadorService.getValoresInteranualesCache(Collections.singletonList(indicador)));
        return template;
    }

    public Template calcularIndicadorTemplate(Long id, String idioma, Boolean publico)
            throws ParseException, VariablesNoExistentesException, ErrorEjecucionSQLException, IndicadorNoEncontradoException {
        String url = "rest/publicacion/" + id + "/calculo";
        if (publico) {
            url = "rest/public/" + id + "/calculo";
        }
        Template template = AppUtils.buildPagina(url, idioma, publico);

        template.put("seccion", "ind/calcular");
        Indicador indicador = indicadorService.getIndicador(id, publico);

        template.put("indicador", toUI(indicador, idioma));
        template.put("anyos", indicadorService.getDistinctAnyosFromCache(indicador));
        template.put("nivelAgregacion", indicadorService.getNivelAgregacion(indicador));
        template.put("componenteTemporal", indicadorService.getComponenteTemporal(indicador));
        template.put("agregaciones", indicadorService.getAgregacionesCache(Collections.singletonList(indicador), idioma));
        template.put("valoresInteranuales", indicadorService.getValoresInteranualesCache(Collections.singletonList(indicador)));

        return template;
    }

    public Template calcularIndicadoresTemplate(List<Long> indiadores, String idioma, Boolean publico)
            throws ParseException, VariablesNoExistentesException, ErrorEjecucionSQLException, IndicadorNoEncontradoException {
        String url = "/rest/publicacion/";
        if (publico) {
            url = "/rest/public/";
        }
        Template template = AppUtils.buildPagina(url, idioma, publico);
        template.put("publico", publico);

        template.put("seccion", "ind/indicadores");
        List<Indicador> indicadorList = indicadorService.getIndicadoresByIds(indiadores, publico);
        indicadorList.removeAll(Collections.singleton(null));
        template.put("indicadores", indiadores);
        template.put("indicadoresCalculo", agrupaIndicadores(idioma, indicadorList));
        template.put("comparar", false);

        return template;
    }

    private List<IndicadorCalculoUI> agrupaIndicadores(String idioma, List<Indicador> indicadorList)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException {

        List<IndicadorCalculoUI> indicadoresAgrupados = new ArrayList<>();

        for (IndicadorPorTipo indicadorPorTipo : agrupadorIndicadoresPorTipoService.agrupaIndicadores(indicadorList)) {
            String descripcion = "";
            IndicadorCalculoUI indicadorCalculoUI = new IndicadorCalculoUI();
            List<Indicador> indicadores = indicadorPorTipo.getIndicadores();
            indicadorCalculoUI.setIndicadores(toUI(indicadores, idioma));
            indicadorCalculoUI.setDescripcion(descripcion);
            indicadorCalculoUI.setAnyos(indicadorService.getDistinctAnyosFromListIndicadoresCache(indicadores));
            indicadorCalculoUI.setNivelAgregacion(indicadorPorTipo.getNivelAgregacion());
            indicadorCalculoUI.setComponenteTemporal(indicadorPorTipo.getComponenteTemporal());
            indicadorCalculoUI.setAgregaciones(indicadorService.getAgregacionesCache(indicadores, idioma));
            indicadorCalculoUI.setValoresInteranuales(indicadorService
                    .getValoresInteranualesCache(indicadores));

            indicadoresAgrupados.add(indicadorCalculoUI);
        }
        return indicadoresAgrupados;
    }

    public List<IndicadorResultadoUI> mostrarDatos(Long id, List<Long> agregacion, List<Integer> anyo, Long valorInteranual, List<Long> marca, String idioma, Boolean publico, Boolean pantalla)
            throws VariablesSinValorException, ErrorEjecucionSQLException, VariablesNoExistentesException, ParseException, IndicadorNoEncontradoException {
        Indicador indicador = indicadorService.getIndicador(id, publico);

        List<IndicadorMarcaResultadoVW> list = indicadorService
                .calculaIndicador(null, indicadorService.getIndicador(id, publico), agregacion, anyo, valorInteranual, "1", marca);

        return Collections.singletonList(toUI(null, indicador, idioma, list, agregacion, anyo, valorInteranual, marca, pantalla));
    }

    public List<IndicadorResultadoUI> mostrarDatos(List<Long> indicadores, List<Long> agregacion, List<Integer> anyo, Long valorInteranual, List<Long> marca, String idioma, Boolean publico, Boolean pantalla)
            throws VariablesSinValorException, ErrorEjecucionSQLException, VariablesNoExistentesException, ParseException, IndicadorNoEncontradoException {

        List<IndicadorResultadoUI> results = new ArrayList<>();

        for (Long indicadorId : indicadores) {

            Indicador indicador = indicadorService.getIndicador(indicadorId, publico);

            List<IndicadorMarcaResultadoVW> list = indicadorService
                    .calculaIndicador(null, indicadorService.getIndicador(indicadorId, publico), agregacion, anyo, valorInteranual, "1", marca);
            results.addAll(Collections.singletonList(toUI(null, indicador, idioma, list, agregacion, anyo, valorInteranual, marca, pantalla)));
        }
        return results;
    }

    private IndicadorResultadoUI toUI(Long userId, Indicador indicador, String idioma, List<IndicadorMarcaResultadoVW> list, List<Long> agregacion, List<Integer> anyo, Long valorInteranual, List<Long> marca, Boolean pantalla)
            throws VariablesNoExistentesException, ErrorEjecucionSQLException, ParseException, IndicadorNoEncontradoException, VariablesSinValorException {
        IndicadorResultadoUI indicadorResultadoUI = new IndicadorResultadoUI();
        indicadorResultadoUI.setIndicadorId(indicador.getId());
        indicadorResultadoUI.setTipoGrafico(indicador.getTipoGrafico());
        switch (idioma.toLowerCase()) {
            case "es":
                indicadorResultadoUI.setIndicadorNombre(indicador.getNombreES() != null ? indicador.getNombreEN() : indicador.getNombre());
                break;
            case "en":
                indicadorResultadoUI.setIndicadorNombre(indicador.getNombreEN() != null ? indicador.getNombreEN() : indicador.getNombre());
                break;
            default:
                indicadorResultadoUI.setIndicadorNombre(indicador.getNombre());

        }
        List<ResultadoUI> resultadosUI = new ArrayList<>();
        for (IndicadorMarcaResultadoVW indicadorMarcaResultado : list) {
            resultadosUI.add(creaResultadoIndicador(indicador, indicadorMarcaResultado, idioma));
        }

        if (pantalla) {
            if (ParamUtils.isNotNull(indicador.getValorReferencia())) {

                Integer minValor = resultadosUI.stream().min(Comparator.comparingInt(r -> r.getAnyo())).get().getAnyo();
                Integer maxValor = resultadosUI.stream().max(Comparator.comparingInt(r -> r.getAnyo())).get().getAnyo();

                while (minValor <= maxValor) {
                    resultadosUI.add(creaResultadoReferencia(indicador, null, BigDecimal.valueOf(ParamUtils.parseLong(indicador.getValorReferencia())), minValor, idioma));
                    minValor += 1;
                }
            }

            if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())) {
                Indicador indicadorReferencia = indicadorService.getIndicador(indicador.getIndicadorReferencia().getId(), false);
                List<IndicadorMarcaResultadoVW> valoresReferencia = indicadorService
                        .calculaIndicador(null, indicadorReferencia, new ArrayList<>(), new ArrayList<>(), null, "0", null);
                for (IndicadorMarcaResultadoVW valorReferencia : valoresReferencia) {
                    resultadosUI.add(creaResultadoReferencia(indicador, indicadorReferencia, valorReferencia.getValor(), valorReferencia.getAnyo(), idioma));
                }
            }
        }

        Comparator<ResultadoUI> comparator = Comparator.comparing(ResultadoUI::getAgregacion);
        comparator.thenComparing(ResultadoUI::getAnyo);
        resultadosUI.sort(comparator);
        indicadorResultadoUI.setResultados(resultadosUI);
        return indicadorResultadoUI;
    }

    private ResultadoUI creaResultadoIndicador(Indicador indicador, IndicadorMarcaResultadoVW indicadorMarcaResultado, String idioma) throws ParseException, VariablesNoExistentesException {

        String agregacionNombre = "";
        switch (idioma) {
            case "es":
                agregacionNombre = indicadorMarcaResultado.getNombreES();
                break;
            case "en":
                agregacionNombre = indicadorMarcaResultado.getNombreEN();
                break;
            default:
                agregacionNombre = indicadorMarcaResultado.getNombreCA();
        }
        return new ResultadoUI(indicadorMarcaResultado.getId(), indicadorMarcaResultado.getAgregacion(), agregacionNombre, indicadorMarcaResultado.getAnyo(),
                indicadorMarcaResultado.getValorInteranual(), valorInteranualService.get(indicadorMarcaResultado.getValorInteranual(), indicadorService.getComponenteTemporal(indicador)),
                indicadorMarcaResultado.getValor(), (ParamUtils.isNotNull(indicadorMarcaResultado.getExpresion()) ? indicadorMarcaResultado.getExpresion() : "-"));
    }

    private ResultadoUI creaResultadoReferencia(Indicador indicador, Indicador indicadorReferencia, BigDecimal valor, Integer anyo, String idioma) {


        String agregacionNombre = "";
        switch (idioma) {
            case "es":
                agregacionNombre = (ParamUtils.isNotNull(indicadorReferencia)) ? "Indicador de referencia: " + indicadorReferencia.getNombreES() : "Valor de referencia: " + valor;
                break;
            case "en":
                agregacionNombre = (ParamUtils.isNotNull(indicadorReferencia)) ? "Reference indicator: " + indicadorReferencia.getNombreEN() : "Reference value: " + valor;
                break;
            default:
                agregacionNombre = (ParamUtils.isNotNull(indicadorReferencia)) ? "Indicador de referència: " + indicadorReferencia.getNombre() : "Valor de referència: " + valor;
        }

        return new ResultadoUI(null, 1L, agregacionNombre, anyo,
                1L, "ANUAL", valor, "-");
    }

    public List<IndicadorResultadoUI> getCalculatedData(List<UIEntity> entities, String idioma, Boolean publico)
            throws VariablesSinValorException, ErrorEjecucionSQLException, VariablesNoExistentesException, ParseException, IndicadorNoEncontradoException {
        List<IndicadorResultadoUI> indicadorResultadoUIList = new ArrayList<>();
        for (UIEntity entity : entities) {
            Long id = ParamUtils.parseLong(entity.get("id"));
            String agregaciones = entity.get("agregacion");
            String anyos = entity.get("anyo");
            Long valorInteranual = AppUtils.toLong(AppUtils.normalizeId(entity.get("valorInteranual")));
            Indicador indicador = indicadorService.getIndicador(id, publico);
            List<String> marcas = entity.getArray("marca");
            List<Long> marcaList = new ArrayList<>();
            for (String marca : marcas) {
                marcaList.add(Long.parseLong(marca));
            }

            ArrayList<Long> agregacionList = new ArrayList<>();
            if (agregaciones != "") {
                String[] agregacionesSplit = agregaciones.split(",");
                for (String agregacion : agregacionesSplit) {
                    agregacionList.add(Long.parseLong(agregacion));
                }
            }

            ArrayList<Integer> anyoList = new ArrayList<>();
            if (anyos != "") {
                String[] anyosSplit = anyos.split(",");
                for (String anyo : anyosSplit) {
                    anyoList.add(Integer.parseInt(anyo));
                }
            }

            List<IndicadorMarcaResultadoVW> list = indicadorService
                    .calculaIndicador(null, indicador, agregacionList, anyoList, valorInteranual, "1", marcaList);
            IndicadorResultadoUI indicadorResultadoUI = toUI(null, indicador, idioma, list, agregacionList, anyoList, valorInteranual, marcaList, Boolean.TRUE);
            indicadorResultadoUIList.add(indicadorResultadoUI);

        }

        return indicadorResultadoUIList;
    }

    public Template mostrarVariableTemplate(String id, String idioma, Boolean publico) throws ParseException {
        String url = "rest/publicacion/variable/" + id;
        if (publico) {
            url = "rest/public/variable/" + id;
        }
        Template template = AppUtils.buildPagina(url, idioma, publico);
        template.put("publico", publico);

        template.put("seccion", "ind/variable");
        Variable variable = variableService.getVariableByCode(id);

        template.put("variable", variable);
        return template;
    }

    public ByteArrayInputStream descargarDatos(Long id, List<Long> agregacion, List<Integer> anyo, Long valorInteranual, List<Long> marca, String idioma, Boolean publico)
            throws VariablesSinValorException, ErrorEjecucionSQLException, VariablesNoExistentesException, IndicadorNoEncontradoException, ParseException, IOException {
        List<IndicadorResultadoUI> resultados = mostrarDatos(id, agregacion, anyo, valorInteranual, marca, idioma, publico, Boolean.FALSE);

        return ExcelWriter.indicadorResultadosExcel(resultados);
    }

    public ByteArrayInputStream descargarDatos(List<Long> indicadores, List<Long> agregacion, List<Integer> anyo, Long valorInteranual, List<Long> marca, String idioma, Boolean publico)
            throws VariablesSinValorException, ErrorEjecucionSQLException, VariablesNoExistentesException, IndicadorNoEncontradoException, ParseException, IOException {
        List<IndicadorResultadoUI> resultados = mostrarDatos(indicadores, agregacion, anyo, valorInteranual, marca, idioma, publico, Boolean.FALSE);

        return ExcelWriter.indicadorResultadosExcel(resultados);
    }

}