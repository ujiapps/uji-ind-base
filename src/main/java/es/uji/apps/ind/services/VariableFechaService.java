package es.uji.apps.ind.services;

import es.uji.apps.ind.dao.VariableFechaDAO;
import es.uji.apps.ind.exceptions.VariableFechaIncorrectaException;
import es.uji.apps.ind.model.VariableFecha;
import es.uji.apps.ind.model.VariableFechaPK;
import es.uji.commons.rest.UIEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class VariableFechaService {

    private VariableFechaDAO variableFechaDAO;

    @Autowired
    public VariableFechaService(VariableFechaDAO variableFechaDAO){
        this.variableFechaDAO = variableFechaDAO;
    }

    public List<UIEntity> getVariableFechas(Long variableId) {
        List<VariableFecha> fechas = variableFechaDAO.getVariableFechas(variableId);
        List<UIEntity> resultList = new ArrayList<>();
        fechas.forEach(variableFecha -> resultList.add(toUI(variableFecha)));
        return resultList;
    }

    public void addVariableFecha(VariableFecha variableFecha) throws VariableFechaIncorrectaException {
        try {
            variableFechaDAO.insert(variableFecha);
        } catch (Exception e) {
            throw new VariableFechaIncorrectaException("Error al insertar el registre. Per favor, revisa les dates i prova de nou.");
        }
    }

    public void deleteVariableFecha(UIEntity entity) {
        VariableFecha variableFecha = new VariableFecha();
        VariableFechaPK variableFechaPK = new VariableFechaPK();

        try {
            variableFechaPK.setVariableId(entity.getLong("variableId"));
            if (entity.get("fechaInicio") != null)
                variableFechaPK.setFechaInicio(new SimpleDateFormat("dd/MM/yyyy").parse(entity.get("fechaInicio")));

            if (entity.get("fechaFin") != null)
                variableFecha.setFechaFin(new SimpleDateFormat("dd/MM/yyyy").parse(entity.get("fechaFin")));
            variableFecha.setId(variableFechaPK);

            variableFechaDAO.deleteVariableFecha(variableFecha);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public UIEntity toUI(VariableFecha variableFecha) {
        UIEntity entity = new UIEntity();
        entity.put("variableId", variableFecha.getId().getVariableId());
        entity.put("fechaInicio", variableFecha.getId().getFechaInicio());
        entity.put("fechaFin", variableFecha.getFechaFin());
        return entity;
    }
}
