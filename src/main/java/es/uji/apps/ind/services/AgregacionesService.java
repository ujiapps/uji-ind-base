package es.uji.apps.ind.services;

import es.uji.apps.ind.dao.AgregacionesDAO;
import es.uji.apps.ind.model.*;
import es.uji.apps.ind.model.enums.NivelAgregacion;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AgregacionesService
{
    private ApaDAO apaDAO;
    private AgregacionesDAO agregacionesDAO;

    public static final String DEFAULT_LANG = "ca";

    @Autowired
    public AgregacionesService(AgregacionesDAO agregacionesDAO, ApaDAO apaDAO)
    {
        this.agregacionesDAO = agregacionesDAO;
        this.apaDAO = apaDAO;
    }

    public List<Agregacion> getCentros()
    {
        return getCentros(DEFAULT_LANG);
    }

    public List<Agregacion> getEstudios()
    {
        return getEstudios(DEFAULT_LANG);
    }

    public List<Agregacion> getDepartamentos()
    {
        return getDepartamentos(DEFAULT_LANG);
    }

    public List<Agregacion> getServicios(Long personaId)
    {
        return getServicios(DEFAULT_LANG, personaId);
    }

    public List<Agregacion> getInstitutos()
    {
        return getInstitutos(DEFAULT_LANG);
    }

    public List<Agregacion> getInstitucional()
    {
        return getInstitucional(DEFAULT_LANG);
    }

    public List<Agregacion> getCentros(String idioma)
    {
        return toAgregacionFromCentros(agregacionesDAO.getCentro(idioma), idioma);
    }

    public List<Agregacion> getEstudios(String idioma)
    {
        return toAgregacionFromEstudios(agregacionesDAO.getEstudios(idioma), idioma);
    }

    public List<Agregacion> getDepartamentos(String idioma)
    {
        return toAgregacionFromDepartamentos(agregacionesDAO.getDepartamentos(idioma), idioma);
    }

    public List<Agregacion> getServicios(String idioma,Long personaId)
    {
        if (apaDAO.hasPerfil("IND", "ADMIN", personaId))
        {
            return toAgregacionFromServicios(agregacionesDAO.getServicios(idioma), idioma);

        }else{
            return toAgregacionFromServicios(agregacionesDAO.getServicios(idioma,personaId), idioma);
        }
    }
    public List<Agregacion> getServicios(String idioma)
    {
        return toAgregacionFromServicios(agregacionesDAO.getServicios(idioma), idioma);
    }

    public List<Agregacion> getInstitutos(String idioma)
    {
        return toAgregacionFromInstitutos(agregacionesDAO.getInstitutos(idioma), idioma);
    }
    public List<Agregacion> getInstitucional(String idioma)
    {
        return toAgregacionFromInstitucional(agregacionesDAO.getInstitucional(), idioma);
    }

    public List<Agregacion> getFilteredList(String nivelAgregacion, List<Long> values, String idioma)
    {
        List<Agregacion> listaAgregaciones = new ArrayList<>();

        for (Agregacion agregacion : getList(nivelAgregacion, idioma))
        {
            if (values.contains(agregacion.getId()))
            {
                listaAgregaciones.add(agregacion);
            }
        }

        return listaAgregaciones;
    }

    public Agregacion getFilteredList(String nivelAgregacion, List<Long> values, String idioma,Long agregacion)
    {
        for (Agregacion agreg : getList(nivelAgregacion, idioma))
        {
            if (agreg.getId().equals(agregacion))
            {
               return agreg;
            }
        }
        return null;
    }

    public List<Agregacion> getList(String nivelAgregacion, String idioma)
    {
        List<Agregacion> listaAgregaciones = new ArrayList<>();

        if (nivelAgregacion.equals(NivelAgregacion.CENTRE.toString()))
        {
            listaAgregaciones = getCentros(idioma);
        }

        if (nivelAgregacion.equals(NivelAgregacion.DEPARTAMENT.toString()))
        {
            listaAgregaciones = getDepartamentos(idioma);
        }

        if (nivelAgregacion.equals(NivelAgregacion.ESTUDI.toString()))
        {
            listaAgregaciones = getEstudios(idioma);
        }

        if (nivelAgregacion.equals(NivelAgregacion.INSTITUCIONAL.toString()))
        {
            listaAgregaciones = getInstitucional(idioma);
        }

        if (nivelAgregacion.equals(NivelAgregacion.SERVEI.toString()))
        {
            listaAgregaciones = getServicios(idioma);
        }
        if (nivelAgregacion.equals(NivelAgregacion.INSTITUT.toString()))
        {
            listaAgregaciones = getInstitutos(idioma);
        }


        return listaAgregaciones;
    }

    private List<Agregacion> toAgregacionFromCentros(List<AgregacionCentro> lista, String idioma)
    {
        List<Agregacion> agregaciones = new ArrayList<>();

        for (AgregacionCentro agregacionCentro : lista)
        {
            if (idioma.equals("en"))
            {
                agregaciones.add(new Agregacion(agregacionCentro.getId(), agregacionCentro
                        .getNombreEN()));
            } else if (idioma.equals("es"))
            {
                agregaciones.add(new Agregacion(agregacionCentro.getId(), agregacionCentro
                        .getNombreES()));
            } else
            {
                agregaciones.add(new Agregacion(agregacionCentro.getId(), agregacionCentro
                        .getNombreCA()));
            }

        }

        return agregaciones;
    }

    private List<Agregacion> toAgregacionFromDepartamentos(List<AgregacionDepartamento> lista, String idioma)
    {
        List<Agregacion> agregaciones = new ArrayList<>();

        for (AgregacionDepartamento agregacionDepartamento : lista)
        {
            if (idioma.equals("en"))
            {
                agregaciones.add(new Agregacion(agregacionDepartamento.getId(), agregacionDepartamento
                        .getNombreEN()));
            } else if (idioma.equals("es"))
            {
                agregaciones.add(new Agregacion(agregacionDepartamento.getId(), agregacionDepartamento
                        .getNombreES()));
            } else
            {
                agregaciones.add(new Agregacion(agregacionDepartamento.getId(), agregacionDepartamento
                        .getNombreCA()));
            }
        }

        return agregaciones;
    }

    private List<Agregacion> toAgregacionFromServicios(List<AgregacionServicio> lista, String idioma)
    {
        List<Agregacion> agregaciones = new ArrayList<>();

        for (AgregacionServicio agregacionServicio : lista)
        {
            if (idioma.equals("en"))
            {
                agregaciones.add(new Agregacion(agregacionServicio.getId(), agregacionServicio
                        .getNombreEN()));
            } else if (idioma.equals("es"))
            {
                agregaciones.add(new Agregacion(agregacionServicio.getId(), agregacionServicio
                        .getNombreES()));
            } else
            {
                agregaciones.add(new Agregacion(agregacionServicio.getId(), agregacionServicio
                        .getNombreCA()));
            }
        }

        return agregaciones;
    }

    private List<Agregacion> toAgregacionFromEstudios(List<AgregacionEstudio> lista, String idioma)
    {
        List<Agregacion> agregaciones = new ArrayList<>();

        for (AgregacionEstudio agregacionEstudio : lista)
        {
            if (idioma.equals("en"))
            {
                agregaciones.add(new Agregacion(agregacionEstudio.getId(), agregacionEstudio
                        .getNombreEN()));
            } else if (idioma.equals("es"))
            {
                agregaciones.add(new Agregacion(agregacionEstudio.getId(), agregacionEstudio
                        .getNombreES()));
            } else
            {
                agregaciones.add(new Agregacion(agregacionEstudio.getId(), agregacionEstudio
                        .getNombreCA()));
            }
        }

        return agregaciones;
    }

    private List<Agregacion> toAgregacionFromInstitutos(List<AgregacionInstituto> lista, String idioma)
    {
        List<Agregacion> agregaciones = new ArrayList<>();

        for (AgregacionInstituto agregacionInstituto : lista)
        {
            if (idioma.equals("en"))
            {
                agregaciones.add(new Agregacion(agregacionInstituto.getId(), agregacionInstituto
                        .getNombreEN()));
            } else if (idioma.equals("es"))
            {
                agregaciones.add(new Agregacion(agregacionInstituto.getId(), agregacionInstituto
                        .getNombreES()));
            } else
            {
                agregaciones.add(new Agregacion(agregacionInstituto.getId(), agregacionInstituto
                        .getNombreCA()));
            }
        }

        return agregaciones;
    }

    private List<Agregacion> toAgregacionFromInstitucional(List<AgregacionInstitucional> lista, String idioma)
    {
        List<Agregacion> agregaciones = new ArrayList<>();

        for (AgregacionInstitucional agregacionInstitucional : lista)
        {
            if (idioma.equals("en"))
            {
                agregaciones.add(new Agregacion(agregacionInstitucional.getId(),
                        agregacionInstitucional.getNombreEN()));
            } else if (idioma.equals("es"))
            {
                agregaciones.add(new Agregacion(agregacionInstitucional.getId(),
                        agregacionInstitucional.getNombreES()));
            } else
            {
                agregaciones.add(new Agregacion(agregacionInstitucional.getId(),
                        agregacionInstitucional.getNombreCA()));
            }
        }

        return agregaciones;
    }
}
