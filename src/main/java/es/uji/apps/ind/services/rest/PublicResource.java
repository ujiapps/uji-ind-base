package es.uji.apps.ind.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.IndicadorNoEncontradoException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.services.PublicacionService;
import es.uji.apps.ind.ui.IndicadorResultadoUI;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.web.template.Template;

@Path("public")
public class PublicResource extends CoreBaseService
{
    private final static Boolean PUBLICO = true;

    @InjectParam
    private PublicacionService publicacionService;


    @Path("indicadores/comparar")
    public PublicacionCompararResource getPlatformItem(
            @InjectParam PublicacionCompararResource publicacionCompararResource)
    {
        return publicacionCompararResource;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarBuscador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @QueryParam("descripcion") String descripcion,
                                    @QueryParam("uso") Long uso,
                                    @QueryParam("agrupacion") Long agrupacion,
                                    @QueryParam("agregacion") String agregacion)
            throws VariablesNoExistentesException, ParseException
    {
        return publicacionService.buscadorTemplate(idioma, descripcion, uso, agrupacion, agregacion,PUBLICO);
    }

    @GET
    @Path("indicador/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarIndicador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                     @PathParam("id") Long id)
            throws VariablesNoExistentesException, ParseException, IndicadorNoEncontradoException, ErrorEjecucionSQLException {
        ParamUtils.checkNotNull(id);
        return publicacionService.indicadorTemplate(id, idioma,PUBLICO);
    }

    @GET
    @Path("indicador/{id}/calculo/")
    @Produces(MediaType.TEXT_HTML)
    public Template calcularIndicador(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                      @PathParam("id") Long id)
            throws VariablesNoExistentesException, ParseException, ErrorEjecucionSQLException, IndicadorNoEncontradoException
    {
        ParamUtils.checkNotNull(id);
        return publicacionService.calcularIndicadorTemplate(id, idioma,PUBLICO);
    }

    @GET
    @Path("indicadores")
    @Produces(MediaType.TEXT_HTML)
    public Template calcularIndicadores(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("indicadores") List<Long> indiadores)
            throws VariablesNoExistentesException, ParseException, ErrorEjecucionSQLException, IndicadorNoEncontradoException
    {
        return publicacionService.calcularIndicadoresTemplate(indiadores, idioma,PUBLICO);
    }

    @POST
    @Path("indicador/{id}/calculo/")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<IndicadorResultadoUI> mostrarDatos(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                                   @PathParam("id") Long id,
                                                   @FormParam("agregacion") List<Long> agregacion,
                                                   @FormParam("anyo") List<Integer> anyo,
                                                   @FormParam("valorInteranual") Long valorInteranual,
                                                   @FormParam("marca") List<Long> marca)
            throws ParseException, VariablesNoExistentesException, ErrorEjecucionSQLException, VariablesSinValorException, IndicadorNoEncontradoException
    {
        ParamUtils.checkNotNull(id);
        return publicacionService.mostrarDatos(id, agregacion, anyo, valorInteranual, marca, idioma,PUBLICO, Boolean.TRUE);
    }


    @POST
    @Path("indicadores/calculo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<IndicadorResultadoUI> getCalculatedData(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, List<UIEntity> entities)
            throws ErrorEvaluacionFormulaException, VariablesSinValorException,
            ErrorEjecucionSQLException, VariablesNoExistentesException, ParseException, IndicadorNoEncontradoException
    {
        return publicacionService.getCalculatedData(entities, idioma,PUBLICO);
    }


    @GET
    @Path("variable/{id}")
    @Produces(MediaType.TEXT_HTML)
    public Template mostrarVariable(@CookieParam("uji-lang") @DefaultValue("ca") String idioma,
                                    @PathParam("id") String id) throws VariablesNoExistentesException, ParseException
    {
        ParamUtils.checkNotNull(id);
        return publicacionService.mostrarVariableTemplate(id, idioma,PUBLICO);
    }
}
