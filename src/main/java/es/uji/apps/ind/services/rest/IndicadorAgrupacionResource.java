package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.services.AgrupacionService;
import es.uji.apps.ind.services.IndicadorAgrupacionService;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class IndicadorAgrupacionResource extends CoreBaseService
{
    @PathParam("id")
    private Long indicadorId;

    @InjectParam
    private IndicadorAgrupacionService indicadorAgrupacionService;

    @InjectParam
    private IndicadorService indicadorService;

    @InjectParam
    private AgrupacionService agrupacionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIndicadoresAgrupaciones()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (IndicadorAgrupacion indicadorAgrupacion : indicadorAgrupacionService
                .getByIndicadorId(indicadorId))
        {
            entities.add(buildEntity(indicadorAgrupacion));
        }

        return entities;
    }

    @DELETE
    @Path("{id}")
    public void deleteIndicadorAgrupacion(@PathParam("id") Long id)
    {
        indicadorAgrupacionService.delete(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addIndicadorAgrupacion(UIEntity entity)
    {
        String agrupacionId = entity.get("agrupacionId");

        if (agrupacionId == null)
        {
            return new UIEntity();
        }

        IndicadorAgrupacion indicadorAgrupacion = new IndicadorAgrupacion();

        indicadorAgrupacion.setIndicador(indicadorService.getIndicador(indicadorId));
        indicadorAgrupacion.setAgrupacion(agrupacionService.getAgrupacion(ParamUtils
                .parseLong(agrupacionId)));

        return buildEntity(indicadorAgrupacionService.insert(indicadorAgrupacion));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateIndicadorAgrupacion(@PathParam("id") Long id, UIEntity entity)
    {
        String agrupacionId = entity.get("agrupacionId");

        if (agrupacionId == null)
        {
            return new UIEntity();
        }

        IndicadorAgrupacion indicadorAgrupacion = indicadorAgrupacionService.get(id);

        indicadorAgrupacion.setAgrupacion(agrupacionService.getAgrupacion(ParamUtils
                .parseLong(agrupacionId)));

        return buildEntity(indicadorAgrupacionService.update(indicadorAgrupacion));
    }

    private UIEntity buildEntity(IndicadorAgrupacion indicadorAgrupacion)
    {
        UIEntity entity = new UIEntity();

        entity.put("id", indicadorAgrupacion.getId());
        entity.put("indicador", UIEntity.toUI(indicadorAgrupacion.getIndicador()));
        entity.put("agrupacion", UIEntity.toUI(indicadorAgrupacion.getAgrupacion()));

        return entity;
    }
}
