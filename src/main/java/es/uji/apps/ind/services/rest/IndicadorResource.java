package es.uji.apps.ind.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.exceptions.*;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.enums.TipoMarca;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("indicadores")
public class IndicadorResource extends CoreBaseService {
    public final static Logger log = LoggerFactory.getLogger(IndicadorResource.class);


    @InjectParam
    private IndicadorService indicadorService;

    @Path("{id}/usos")
    public IndicadorUsoResource getPlatformItem(
            @InjectParam IndicadorUsoResource indicadorUsosResource) {
        return indicadorUsosResource;
    }

    @Path("{id}/agrupaciones")
    public IndicadorAgrupacionResource getPlatformItem(
            @InjectParam IndicadorAgrupacionResource indicadorAgrupacionResource) {
        return indicadorAgrupacionResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getAll(@QueryParam("query") @DefaultValue("") String query)
            throws VariablesNoExistentesException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        return UIEntity.toUI(indicadorService.getIndicadores(connectedUserId, query));
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id) {

        Indicador indicador = indicadorService.getIndicador(id);
        UIEntity entity = UIEntity.toUI(indicador);
        entity.put("indicadorReferenciaNombre", (ParamUtils.isNotNull(indicador.getIndicadorReferencia()))?indicador.getIndicadorReferencia().getNombre():"");
        return entity;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/save-calculated-data")
    public ResponseMessage add(@PathParam("id") Long id, @FormParam("anyo") Integer anyo,
                               @FormParam("valorInteranual") Long valorInteranual, @FormParam("agregacion") Long agregacion,
                               @FormParam("marca") String marca)
            throws ErrorEjecucionSQLException, VariablesSinValorException {
        ParamUtils.checkNotNull(marca);
        Long userId = AccessManager.getConnectedUserId(request);
        indicadorService.guardaResultadosCalculoIndicador(userId, indicadorService.getIndicador(id),
                agregacion, anyo, valorInteranual, marca, TipoMarca.MARCA.toString());

        return new ResponseMessage(true);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage add(@FormParam("id") Long id, @FormParam("nombre") String nombre,
                               @FormParam("nombreES") String nombreES,
                               @FormParam("nombreEN") String nombreEN,
                               @FormParam("codigo") String codigo,
                               @FormParam("denominacion") String denominacion,
                               @FormParam("denominacionES") String denominacionES,
                               @FormParam("denominacionEN") String denominacionEN,
                               @FormParam("responsable") String responsable,
                               @FormParam("conceptoAEvaluar") String conceptoAEvaluar,
                               @FormParam("conceptoAEvaluarES") String conceptoAEvaluarES,
                               @FormParam("conceptoAEvaluarEN") String conceptoAEvaluarEN,
                               @FormParam("formula") String formula, @FormParam("observaciones") String observaciones,
                               @FormParam("observacionesES") String observacionesES,
                               @FormParam("observacionesEN") String observacionesEN,
                               @FormParam("tipoGrafico") String tipoGrafico,
                               @FormParam("valorReferencia") String valorReferencia,
                               @FormParam("indicadorReferenciaId") Long indicadorReferenciaId,
                               @FormParam("publico") String publico,
                               @FormParam("activo") @DefaultValue("1") String activo) throws VariableException,
            FormulaException {
        ParamUtils.checkNotNull(nombre, codigo, denominacion, responsable, conceptoAEvaluar,
                formula);

        Indicador indicador = new Indicador();

        if (id != null)
        {
            indicador = indicadorService.getIndicador(id);
        }

        indicador.setCodigo(codigo);
        indicador.setNombre(nombre);
        indicador.setNombreES(nombreES);
        indicador.setNombreEN(nombreEN);
        indicador.setDenominacion(denominacion);
        indicador.setDenominacionES(denominacionES);
        indicador.setDenominacionEN(denominacionEN);
        indicador.setResponsable(responsable);
        indicador.setConceptoAEvaluar(conceptoAEvaluar);
        indicador.setConceptoAEvaluarES(conceptoAEvaluarES);
        indicador.setConceptoAEvaluarEN(conceptoAEvaluarEN);
        indicador.setFormula(formula);
        indicador.setObservaciones(observaciones);
        indicador.setObservacionesES(observacionesES);
        indicador.setObservacionesEN(observacionesEN);
        indicador.setValorReferencia(valorReferencia);
        indicador.setTipoGrafico(tipoGrafico);
        indicador.setPublico(!publico.equals("0"));
        indicador.setActivo(activo.equals("1"));

        indicador.setIndicadorReferencia(null);
        if (ParamUtils.isNotNull(indicadorReferenciaId)) {
            Indicador indicadorReferencia = new Indicador();
            indicadorReferencia.setId(indicadorReferenciaId);
            indicador.setIndicadorReferencia(indicadorReferencia);
        }

        indicadorService.addOrUpdateIndicador(indicador);

        return new ResponseMessage(true);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id) {
        indicadorService.deleteIndicador(id);

        return Response.noContent().build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/duplicar")
    public ResponseMessage duplicate(@PathParam("id") Long id) throws VariableException,
            FormulaException, IndicadorNoEncontradoException {
        indicadorService.duplicarIndicador(id);

        return new ResponseMessage(true);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/activar")
    public ResponseMessage toggleActivarIndicador(@PathParam("id") Long id, UIEntity uiEntity) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        indicadorService.toggleActivarIndicador(id, uiEntity.getBoolean("activo"));
        return responseMessage;
    }
}