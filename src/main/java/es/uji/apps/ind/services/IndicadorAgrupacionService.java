package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.IndicadorAgrupacionDAO;
import es.uji.apps.ind.model.IndicadorAgrupacion;

@Service
public class IndicadorAgrupacionService
{
    private IndicadorAgrupacionDAO indicadorAgrupacionDAO;

    @Autowired
    public IndicadorAgrupacionService(IndicadorAgrupacionDAO indicadorAgrupacionDAO)
    {
        this.indicadorAgrupacionDAO = indicadorAgrupacionDAO;
    }

    public List<IndicadorAgrupacion> getByIndicadorId(Long indicadorId)
    {
        return indicadorAgrupacionDAO.getByIndicadorId(indicadorId);
    }

    public List<IndicadorAgrupacion> getByAgrupacionId(Long agrupacionId)
    {
        return indicadorAgrupacionDAO.getByAgrupacionId(agrupacionId);
    }

    public IndicadorAgrupacion get(Long id)
    {
        return indicadorAgrupacionDAO.get(IndicadorAgrupacion.class, id).get(0);
    }

    public void delete(Long id)
    {
        indicadorAgrupacionDAO.delete(IndicadorAgrupacion.class, id);
    }

    public IndicadorAgrupacion insert(IndicadorAgrupacion indicadorAgrupacion)
    {
        return indicadorAgrupacionDAO.insert(indicadorAgrupacion);
    }

    public IndicadorAgrupacion update(IndicadorAgrupacion indicadorAgrupacion)
    {
        return indicadorAgrupacionDAO.update(indicadorAgrupacion);
    }
}
