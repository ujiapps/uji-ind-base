package es.uji.apps.ind.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.VariableDAO;
import es.uji.apps.ind.excelReader.ValorExtractor;
import es.uji.apps.ind.excelReader.ValorExtractorFactory;
import es.uji.apps.ind.exceptions.SoloSeAdmitenFicherosExcelException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.enums.TipoFicheroExcel;

@Service
public class ImportDatosVariablesService
{
    private VariableDAO variableDAO;
    private ValorExtractorFactory valorExtractorFactory;

    @Autowired
    public ImportDatosVariablesService(VariableDAO variableDAO, ValorExtractorFactory valorExtractorFactory)
    {
        this.variableDAO = variableDAO;
        this.valorExtractorFactory = valorExtractorFactory;
    }

    public void insertFromExcel(InputStream file, Variable variable, String mimeType)
            throws IOException, SoloSeAdmitenFicherosExcelException
    {
        TipoFicheroExcel tipoFicheroExcel = getTipoFicheroExcel(mimeType);

        ValorExtractor extractor = valorExtractorFactory.getExtractor(tipoFicheroExcel);
        List<VariableValor> valores = extractor.extract(file);

        for (VariableValor valor : valores)
        {
            valor.setVariable(variable);
        }

        variable.setValores(new HashSet(valores));

        variableDAO.update(variable);
    }

    private TipoFicheroExcel getTipoFicheroExcel(String mimeType) throws SoloSeAdmitenFicherosExcelException
    {
        if ("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(mimeType))
        {
            return TipoFicheroExcel.XLSX;
        }

        if ("application/vnd.ms-excel".equals(mimeType))
        {
            return TipoFicheroExcel.XLS;
        }

        throw new SoloSeAdmitenFicherosExcelException();
    }
}
