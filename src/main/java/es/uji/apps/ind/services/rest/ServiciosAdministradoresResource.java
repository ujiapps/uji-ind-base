package es.uji.apps.ind.services.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.AdministradorServicioException;
import es.uji.apps.ind.services.ServiciosAdministradoresService;
import es.uji.commons.rest.UIEntity;

@Path("administradores")
public class ServiciosAdministradoresResource
{
    @InjectParam
    private ServiciosAdministradoresService serviciosAdministradoresService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getServiciosAdministrador(@QueryParam("query") String query)
    {
        return serviciosAdministradoresService.getServiciosAdministrador(query).stream().map(a ->{
            UIEntity uiEntity = new UIEntity();
            uiEntity.put("id", a.getId());
            uiEntity.put("personaId", a.getPersona().getPersonaId());
            uiEntity.put("nombreCompleto", a.getPersona().getNombreCompleto());
            uiEntity.put("mail", a.getPersona().getMail());
            uiEntity.put("servicioNombre", a.getServicio().getNombreCA());
            uiEntity.put("servicioId", a.getServicio().getId());
            return uiEntity;
        }).collect(Collectors.toList());
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(@FormParam("personaId") Long personaId, @FormParam("servicios") Long servicios)
            throws AdministradorServicioException
    {
        serviciosAdministradoresService.addAdministrador(personaId,servicios);
        return Response.ok().build();
    }
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long id)
    {
        serviciosAdministradoresService.deleteAdministrador(id);

        return Response.ok().build();
    }
}
