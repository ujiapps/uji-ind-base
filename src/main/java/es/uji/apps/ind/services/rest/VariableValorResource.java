package es.uji.apps.ind.services.rest;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.BodyPartEntity;
import com.sun.jersey.multipart.FormDataMultiPart;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.SoloSeAdmitenFicherosExcelException;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import es.uji.apps.ind.services.AgregacionesService;
import es.uji.apps.ind.services.ImportDatosVariablesService;
import es.uji.apps.ind.services.VariableService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;

public class VariableValorResource extends CoreBaseService
{
    @PathParam("id")
    private Long variableId;

    @InjectParam
    private VariableService variableService;

    @InjectParam
    private ImportDatosVariablesService importDatosVariablesService;

    @InjectParam
    private AgregacionesService agregacionesService;

    @Context
    ServletContext servletContext;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariablesValores() throws ErrorEjecucionSQLException
    {
        Long userId = AccessManager.getConnectedUserId(request);
        return UIEntity.toUI(variableService.getVariablesValorByVariableId(userId,variableId));
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addVariableValor(UIEntity entity) throws ParseException
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        Long agregacion = entity.getLong("agregacion");
        String anyo = entity.get("anyo");
        String valorInteranual = entity.get("valorInteranual");
        BigDecimal valor = new BigDecimal(entity.get("valor"));

        if (agregacion == null)
        {
            return new UIEntity();
        }

        ParamUtils.checkNotNull(agregacion, anyo, valorInteranual);
        Variable variable = variableService.getVariable(variableId);
        VariableValor variableValor = new VariableValor();
        variableValor.setVariable(variable);
        variableValor.setAgregacion(agregacion);
        variableValor.setAnyo(Integer.parseInt(anyo));
        if(variable.getComponenteTemporal().equals(ComponenteTemporal.DIARI.getValue())){
           Date fecha =  simpleDateFormat.parse(valorInteranual);
            SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyyMMddHHmmss");
            variableValor.setValorInteranual(Long.parseLong(formateadorFecha.format(fecha)));
        }else
        {
            variableValor.setValorInteranual(Long.parseLong(valorInteranual));
        }
        variableValor.setValor(valor);

        return UIEntity.toUI(variableService.insert(variableValor));
    }

    @POST
    @Path("excel")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public ResponseMessage addFromExcelFile(FormDataMultiPart multiPart)
            throws SoloSeAdmitenFicherosExcelException, IOException
    {
        for (BodyPart bodyPart : multiPart.getBodyParts())
        {
            String mimeType = bodyPart.getHeaders().getFirst("Content-Type");
            if (mimeType != null && !mimeType.isEmpty())
            {
                String fileName = "";
                String header = bodyPart.getHeaders().getFirst("Content-Disposition");
                Pattern fileNamePattern = Pattern.compile(".*filename=\"(.*)\"");
                Matcher m = fileNamePattern.matcher(header);
                if (m.matches())
                {
                    fileName = m.group(1);
                    fileName =  new String (fileName.getBytes ("iso-8859-1"), "UTF-8");

                }
                if (fileName.length() > 0 && fileName != null)
                {
                    Variable variable = variableService.getVariable(variableId);

                    BodyPartEntity bpe = (BodyPartEntity) bodyPart.getEntity();
                    InputStream documento = bpe.getInputStream();
                    importDatosVariablesService.insertFromExcel(documento, variable, mimeType);
                }
            }
        }
        return new ResponseMessage(true);
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateVariableValor(@PathParam("id") Long id, UIEntity entity) throws ParseException
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        Long agregacion = entity.getLong("agregacion");
        String anyo = entity.get("anyo");
        String valorInteranual = entity.get("valorInteranual");
        BigDecimal valor = new BigDecimal(entity.get("valor"));


        if (agregacion == null)
        {
            return new UIEntity();
        }

        ParamUtils.checkNotNull(anyo, valorInteranual, valor);

        Variable variable = variableService.getVariable(variableId);
        VariableValor variableValor = variableService.getVariableValor(id);
        variableValor.setAgregacion(agregacion);
        variableValor.setAnyo(Integer.parseInt(anyo));
        if(variable.getComponenteTemporal().equals(ComponenteTemporal.DIARI.getValue())){
            Date fecha =  simpleDateFormat.parse(valorInteranual);
            SimpleDateFormat formateadorFecha = new SimpleDateFormat("yyyyMMddHHmmss");
            variableValor.setValorInteranual(Long.parseLong(formateadorFecha.format(fecha)));
        }else
        {
            variableValor.setValorInteranual(Long.parseLong(valorInteranual));
        }
        variableValor.setValor(valor);

        return UIEntity.toUI(variableService.update(variableValor));
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity deleteVariableValor(@PathParam("id") Long id)
    {
        variableService.delete(id);
        return new UIEntity();
    }
}