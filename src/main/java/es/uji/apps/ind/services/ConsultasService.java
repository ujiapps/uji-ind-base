package es.uji.apps.ind.services;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.ConsultasDAO;
import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.model.ConsultaSQL;

@Service
public class ConsultasService
{
    private ConsultasDAO consultasDAO;

    @Autowired
    public ConsultasService(ConsultasDAO consultasDAO)
    {
        this.consultasDAO = consultasDAO;
    }


    public ConsultaSQL ejecutaSQl(Long userId, String sql) throws SQLException, ErrorEjecucionSQLException
    {
        return consultasDAO.ejecutaSQl(userId, sql);
    }
}
