package es.uji.apps.ind.services.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.IndicadorUso;
import es.uji.apps.ind.services.IndicadorService;
import es.uji.apps.ind.services.IndicadorUsoService;
import es.uji.apps.ind.services.UsoService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.UIEntity;

public class IndicadorUsoResource extends CoreBaseService
{
    @PathParam("id")
    private Long indicadorId;

    @InjectParam
    private IndicadorUsoService indicadorUsoService;

    @InjectParam
    private IndicadorService indicadorService;

    @InjectParam
    private UsoService usoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getIndicadoresUsos()
    {
        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (IndicadorUso indicadorUso : indicadorUsoService.getByIndicadorId(indicadorId))
        {
            entities.add(buildEntity(indicadorUso));
        }

        return entities;
    }

    @DELETE
    @Path("{id}")
    public void deleteIndicadorUso(@PathParam("id") Long id)
    {
        indicadorUsoService.delete(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addIndicadorUso(UIEntity entity)
    {
        String usoId = entity.get("usoId");

        if (usoId == null)
        {
            return new UIEntity();
        }

        IndicadorUso indicadorUso = new IndicadorUso();
        indicadorUso.setIndicador(indicadorService.getIndicador(indicadorId));
        indicadorUso.setUso(usoService.getUso(ParamUtils.parseLong(usoId)));

        return buildEntity(indicadorUsoService.insert(indicadorUso));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateIndicadorUso(@PathParam("id") Long id, UIEntity entity)
    {
        String usoId = entity.get("usoId");

        if (usoId == null)
        {
            return new UIEntity();
        }

        IndicadorUso indicadorUso = indicadorUsoService.get(id);
        indicadorUso.setUso(usoService.getUso(ParamUtils.parseLong(usoId)));

        return buildEntity(indicadorUsoService.update(indicadorUso));
    }

    private UIEntity buildEntity(IndicadorUso indicadorUso)
    {
        UIEntity entity = new UIEntity();

        entity.put("id", indicadorUso.getId());
        entity.put("indicador", UIEntity.toUI(indicadorUso.getIndicador()));
        entity.put("uso", UIEntity.toUI(indicadorUso.getUso()));

        return entity;
    }

}
