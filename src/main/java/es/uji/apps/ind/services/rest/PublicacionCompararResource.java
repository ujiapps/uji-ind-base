package es.uji.apps.ind.services.rest;

import java.text.ParseException;
import java.util.List;

import javax.ws.rs.CookieParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.exceptions.ErrorEjecucionSQLException;
import es.uji.apps.ind.exceptions.IndicadorNoEncontradoException;
import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.services.AgrupadorIndicadoresPorTipoService;
import es.uji.apps.ind.services.PublicacionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.web.template.Template;

public class PublicacionCompararResource extends CoreBaseService
{
    @InjectParam
    private PublicacionService publicacionService;

    @InjectParam
    private AgrupadorIndicadoresPorTipoService agrupadorIndicadoresPorTipoService;

    @GET
    @Path("valid")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage permitirComparar(@QueryParam("indicadores[]") List<Long> indicadores) throws VariablesNoExistentesException, ParseException, ErrorEjecucionSQLException
    {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        responseMessage.setSuccess(agrupadorIndicadoresPorTipoService.permitirComparar(indicadores));
        return responseMessage;
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Template calcularIndicadores(@CookieParam("uji-lang") @DefaultValue("ca") String idioma, @QueryParam("indicadores") List<Long> indiadores)
            throws VariablesNoExistentesException, ParseException, ErrorEjecucionSQLException, IndicadorNoEncontradoException
    {
        Boolean publico = !request.getRequestURI().contains("publicacion");
        Template template = publicacionService.calcularIndicadoresTemplate(indiadores,idioma,publico);
        template.put("comparar", true);
        return template;
    }

}
