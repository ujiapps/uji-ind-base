package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.ind.dao.SeccionIndicadorDAO;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.Seccion;
import es.uji.apps.ind.model.SeccionIndicador;

@Service
public class SeccionIndicadorService
{
    private SeccionIndicadorDAO seccionIndicadorDAO;

    @Autowired
    public SeccionIndicadorService(SeccionIndicadorDAO seccionIndicadorDAO)
    {
        this.seccionIndicadorDAO = seccionIndicadorDAO;
    }

    public List<SeccionIndicador> getIndicadoresBySeccionId(Long seccionId)
    {
        return seccionIndicadorDAO.getIndicadoresBySeccionId(seccionId);
    }


    @Transactional
    public SeccionIndicador insert(Long seccionId, Long indicadorId, Long orden)
    {
        SeccionIndicador seccionIndicador = new SeccionIndicador();
        Indicador indicador = new Indicador();
        indicador.setId(indicadorId);
        Seccion seccion = new Seccion();
        seccion.setId(seccionId);
        seccionIndicador.setIndicador(indicador);
        seccionIndicador.setSeccion(seccion);
        seccionIndicador.setOrden(orden);
        return seccionIndicadorDAO.insert(seccionIndicador);
    }

    public void update(Long seccionIndicadorId, Long indicadorId, Long orden)
    {
        seccionIndicadorDAO.update(seccionIndicadorId,indicadorId,orden);
    }

    public void delete(Long id)
    {
        seccionIndicadorDAO.delete(SeccionIndicador.class,id);
    }
}
