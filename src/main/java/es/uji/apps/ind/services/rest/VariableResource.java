package es.uji.apps.ind.services.rest;

import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.exceptions.*;
import es.uji.apps.ind.model.AgregacionServicio;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.enums.NivelAgregacion;
import es.uji.apps.ind.services.VariableService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Path("variables")
public class VariableResource extends CoreBaseService {
    @InjectParam
    private VariableService variableService;

    public final static Logger log = LoggerFactory.getLogger(VariableResource.class);

    @Path("{id}/usos")
    public IndicadorUsoResource getPlatformItem(
            @InjectParam IndicadorUsoResource indicadorUsosResource) {
        return indicadorUsosResource;
    }

    @Path("{id}/agrupaciones")
    public IndicadorAgrupacionResource getPlatformItem(
            @InjectParam IndicadorAgrupacionResource indicadorAgrupacionResource) {
        return indicadorAgrupacionResource;
    }

    @Path("{id}/valores")
    public VariableValorResource getPlatformItem(
            @InjectParam VariableValorResource variableValorResource) {
        return variableValorResource;
    }

    @Path("{id}/indicadores")
    public VariableIndicadorResource getPlatformItem(
            @InjectParam VariableIndicadorResource variableIndicadorResource) {
        return variableIndicadorResource;
    }

    @Path("{id}/correos")
    public VariableCorreoResource getPlatformItem(
            @InjectParam VariableCorreoResource variableCorreoResource
    ) {
        return variableCorreoResource;
    }


    @Path("{id}/fechas")
    public VariableFechaResource getPlatformItem(
            @InjectParam VariableFechaResource variableFechaResource
    ) {
        return variableFechaResource;
    }

    @Path("revision")
    public VariableRevisionResource getPlatformItem(
            @InjectParam VariableRevisionResource variableRevisionResource
    ) {
        return variableRevisionResource;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariables(@QueryParam("query") @DefaultValue("") String filter) {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<UIEntity> entities = new ArrayList<>();

        for (Variable variable : variableService.getVariables(filter, connectedUserId))
        {
            UIEntity entity = UIEntity.toUI(variable);

            entities.add(entity);
        }

        return entities;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity get(@PathParam("id") Long id) {
        return UIEntity.toUI(variableService.getVariable(id));
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage add(@FormParam("id") Long id, @FormParam("nombre") String nombre,
                               @FormParam("nombreES") String nombreES,
                               @FormParam("nombreEN") String nombreEN,
                               @FormParam("codigo") String codigo, @FormParam("dinamica") Integer dinamica,
                               @FormParam("sql") String sql, @FormParam("nivelAgregacion") String nivelAgregacion,
                               @FormParam("componenteTemporal") Integer componenteTemporal, @FormParam("servicioId") Long servicioId,
                               @FormParam("tipoAnualidad") String tipoAnualidad,
                               @FormParam("observaciones") String observaciones,
                               @FormParam("observacionesES") String observacionesES,
                               @FormParam("observacionesEN") String observacionesEN,
                               @FormParam("activa") Integer activa,
                               @FormParam("observacionesValidacion") String observacionesValidacion)
            throws VariableFormatoIncorrectoException, CodigoVariableRepetidoException,
            CodigoVariableEnUsoException, VariableSQLIncorrectoException, VariableEnUsoException {
        ParamUtils.checkNotNull(nombre, codigo, dinamica, nivelAgregacion, componenteTemporal, servicioId);

        Variable variable = new Variable();
        String codigoAnterior = null;
        Integer componenteTemporalAnterior = null;
        String nivelAgregacionAnterior = null;
        Boolean activoAnterior = null;

        if (id != null)
        {
            variable = variableService.getVariable(id);
            codigoAnterior = variable.getCodigo();
            componenteTemporalAnterior = variable.getComponenteTemporal();
            nivelAgregacionAnterior = variable.getNivelAgregacion();
            activoAnterior = variable.getActiva();
        }

        variable.setCodigo(codigo);
        variable.setNombre(nombre);
        variable.setNombreES(nombreES);
        variable.setNombreEN(nombreEN);
        variable.setDinamica(dinamica);
        variable.setNivelAgregacion(NivelAgregacion.valueOf(nivelAgregacion.toUpperCase())
                .toString());
        variable.setComponenteTemporal(componenteTemporal);
        variable.setObservaciones(observaciones);
        variable.setObservacionesES(observacionesES);
        variable.setObservacionesEN(observacionesEN);
        variable.setActiva(activa.equals(1) ? Boolean.TRUE : Boolean.FALSE);

        AgregacionServicio agregacionServicio = new AgregacionServicio();
        agregacionServicio.setId(servicioId);
        variable.setServicio(agregacionServicio);
        variable.setObservacionesValidacion(observacionesValidacion);


        if (variable.getDinamica().equals(1))
        {
            variable.setSql(sql);
        }
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);

        try
        {
            if (variable.getId() == null)
            {
                variableService.add(variable);
            } else
            {
                variableService.update(variable, codigoAnterior, componenteTemporalAnterior, nivelAgregacionAnterior, activoAnterior);
            }

        }
        catch (VariableEnUsoException e)
        {
            log.error("La variable s'està utilitzant pels indicadors", e.getMessage());
            responseMessage.setSuccess(false);
            responseMessage.setMessage(e.getMessage());
        }

        return responseMessage;
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage delete(@PathParam("id") Long id)
            throws CodigoVariableEnUsoException, VariableEnUsoException {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        try
        {
            variableService.deleteVariable(id);
        }
        catch (VariableEnUsoException e)
        {
            log.error("La variable s'està utilitzant pels indicadors", e.getMessage());
            responseMessage.setSuccess(false);
            responseMessage.setMessage(e.getMessage());
        }

        return responseMessage;

    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/duplicar")
    public ResponseMessage duplicate(@PathParam("id") Long id)
            throws CodigoVariableRepetidoException, VariableFormatoIncorrectoException,
            CodigoVariableEnUsoException {
        variableService.duplicarVariable(id);

        return new ResponseMessage(true);
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}/activar")
    public ResponseMessage toggleActivarVariable(@PathParam("id") Long id, UIEntity uiEntity) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setSuccess(true);
        try
        {
            variableService.toggleActivarVariable(id, uiEntity.getBoolean("activa"));
        }
        catch (VariableEnUsoException e)
        {
            log.error("La variable s'està utilitzant pels indicadors", e.getMessage());
            responseMessage.setSuccess(false);
            responseMessage.setMessage(e.getMessage());
        }

        return responseMessage;
    }
}