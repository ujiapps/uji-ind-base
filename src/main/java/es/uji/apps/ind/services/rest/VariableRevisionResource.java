package es.uji.apps.ind.services.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.core.InjectParam;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.model.Paginacion;
import es.uji.apps.ind.model.submodel.VariableRevisionModel;
import es.uji.apps.ind.services.VariableRevisionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.apps.ind.services.EnvioService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

public class VariableRevisionResource extends CoreBaseService {
    @InjectParam
    private VariableRevisionService variableRevisionService;
    @InjectParam
    private EnvioService envioService;

    @GET
    @Path("user")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getVariablesRevisionByUser(@QueryParam("start") @DefaultValue("0") Long start,
                                                      @QueryParam("limit") @DefaultValue("25") Long limit,
                                                      @QueryParam("sort") @DefaultValue("[]") String sortJson,
                                                      @QueryParam("filter") @DefaultValue("[]") String filter) {
        ResponseMessage responseMessage = new ResponseMessage(true);
        Long userId = AccessManager.getConnectedUserId(request);
        try {
            List<Map<String, String>> filtros = null;

            if (!filter.equals("[]")) {
                filtros = new ObjectMapper().readValue(filter, new TypeReference<List<Map<String, String>>>(){});
            }

            Paginacion paginacion = new Paginacion(start, limit);
            if (sortJson.length() > 2) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(sortJson, new TypeReference<List<Map<String, String>>>() {});
                paginacion.setOrdenarPor(sortList.get(0).get("property"));
                paginacion.setDireccion(sortList.get(0).get("direction"));
            }
            responseMessage.setTotalCount(paginacion.getTotalCount());
            responseMessage.setData(UIEntity.toUI(variableRevisionService.getVariablesRevisionByUser(userId, paginacion, filtros)));
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getVariablesRevision(@QueryParam("start") @DefaultValue("0") Long start,
                                                @QueryParam("limit") @DefaultValue("25") Long limit,
                                                @QueryParam("sort") @DefaultValue("[]") String sort,
                                                @QueryParam("filter") @DefaultValue("[]") String filter) {
        Paginacion paginacion;
        List<Map<String, String>> sorters;
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(null);

        try {

            List<Map<String, String>> filtros = null;

            if (!filter.equals("[]")) {
                filtros = new ObjectMapper().readValue(filter, new TypeReference<List<Map<String, String>>>(){});
            }

            if (!sort.equals("[]")) {
                sorters = new ObjectMapper().readValue(sort, new TypeReference<List<Map<String, String>>>() {});
                paginacion = new Paginacion(start, limit, sorters.get(0).get("property"), sorters.get(0).get("direction"));
            }
            else paginacion = new Paginacion(start, limit);

            responseMessage.setData(UIEntity.toUI(variableRevisionService.getVariablesRevision(paginacion, filtros)));
            responseMessage.setTotalCount(paginacion.getTotalCount());
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateVariablesRevision(@PathParam("id") Long revisionId, UIEntity entity) throws VariableException {
        String estado = entity.getBoolean("estado") ? "S": "N";
        String observaciones = entity.get("observaciones");
        variableRevisionService.updateVariablesRevision(revisionId, estado, observaciones);
        return Response.ok().build();
    }

    @GET
    @Path("{id}/valores")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseMessage getVariablesValorRevision(@PathParam("id") Long revisionId,
                                                     @QueryParam("start") @DefaultValue("0") Long start,
                                                     @QueryParam("limit") @DefaultValue("25") Long limit,
                                                     @QueryParam("sort") @DefaultValue("[]") String sort) {
        Paginacion paginacion;
        List<Map<String, String>> sorters;
        ResponseMessage responseMessage = new ResponseMessage(true);
        responseMessage.setData(null);
        try {
            if (!sort.equals("[]")) {
                sorters = new ObjectMapper().readValue(sort, new TypeReference<List<Map<String, String>>>() {});
                paginacion = new Paginacion(start, limit, sorters.get(0).get("property"), sorters.get(0).get("direction"));
            }
            else paginacion = new Paginacion(start, limit);

            responseMessage.setData(UIEntity.toUI(variableRevisionService.getVariablesValorRevision(revisionId, paginacion)));
            responseMessage.setTotalCount(paginacion.getTotalCount());
        } catch (Exception e) {
            responseMessage.setSuccess(false);
        }
        return responseMessage;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UIEntity addRevalidacion(UIEntity entity)
    {
        UIEntity entity1 = entity;
        VariableRevisionModel revalidacion = entity.getRelations().get("revision").get(0).toModel(VariableRevisionModel.class);

        variableRevisionService.addRevalidacion(revalidacion);
        return UIEntity.toUI(revalidacion);
    }
}
