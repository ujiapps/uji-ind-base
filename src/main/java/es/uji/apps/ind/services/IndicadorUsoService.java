package es.uji.apps.ind.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uji.apps.ind.dao.IndicadorUsoDAO;
import es.uji.apps.ind.model.IndicadorUso;

@Service
public class IndicadorUsoService
{
    private IndicadorUsoDAO indicadorUsoDAO;

    @Autowired
    public IndicadorUsoService(IndicadorUsoDAO indicadorUsoDAO)
    {
        this.indicadorUsoDAO = indicadorUsoDAO;
    }

    public List<IndicadorUso> getByIndicadorId(Long indicadorId)
    {
        return indicadorUsoDAO.getByIndicadorId(indicadorId);
    }

    public List<IndicadorUso> getByUsoId(Long usoId)
    {
        return indicadorUsoDAO.getByUsoId(usoId);
    }

    public IndicadorUso get(Long id)
    {
        return indicadorUsoDAO.get(IndicadorUso.class, id).get(0);
    }

    public void delete(Long id)
    {
        indicadorUsoDAO.delete(IndicadorUso.class, id);
    }

    public IndicadorUso insert(IndicadorUso indicadorUso)
    {
        return indicadorUsoDAO.insert(indicadorUso);
    }

    public IndicadorUso update(IndicadorUso indicadorUso)
    {
        return indicadorUsoDAO.update(indicadorUso);
    }
}
