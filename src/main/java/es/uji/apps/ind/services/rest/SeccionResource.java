package es.uji.apps.ind.services.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.core.InjectParam;

import es.uji.apps.ind.model.Seccion;
import es.uji.apps.ind.services.SeccionService;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

public class SeccionResource extends CoreBaseService
{
    @PathParam("id")
    private Long panelId;

    @InjectParam
    private SeccionService seccionService;

    @Path("{id}/indicadores")
    public SeccionIndicadorResource getPlatformItem(@InjectParam SeccionIndicadorResource seccionIndicadorResource)
    {
        return seccionIndicadorResource;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getSecciones()
    {
        return UIEntity.toUI(seccionService.getSeccionesByPanelId(panelId));
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity addSeccion(UIEntity entity)
    {
        Seccion seccion = entity.toModel(Seccion.class);

        return UIEntity.toUI(seccionService.insert(panelId,seccion));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity updateIndicadorUso(@PathParam("id") Long id, UIEntity entity)
    {
        Seccion seccion = entity.toModel(Seccion.class);
        return UIEntity.toUI(seccionService.updateSeccion(seccion));

    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id)
    {
        seccionService.delete(id);
    }

}
