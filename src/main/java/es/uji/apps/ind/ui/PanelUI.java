package es.uji.apps.ind.ui;

import java.util.List;
import java.util.stream.Collectors;

public class PanelUI {
    private Long id;
    private String nombre;
    private List<SeccionUI> secciones;
    private Boolean filtroCompartido;
    private IndicadorFiltroUI filtrosPanel;

    public PanelUI() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<SeccionUI> getSecciones() {
        return secciones;
    }

    public void setSecciones(List<SeccionUI> secciones) {
        this.secciones = secciones;
    }

    public void setFiltroCompartido() {
        this.filtroCompartido = this.getSecciones().stream()
                .flatMap(seccionUI -> seccionUI.getTiposIndicador().stream())
                .map(indicadorCalculoUI -> indicadorCalculoUI.getComponenteTemporal().concat(indicadorCalculoUI.getNivelAgregacion()))
                .distinct().collect(Collectors.toList()).size() <=1;
    }

    public Boolean getFiltroCompartido() {
        return filtroCompartido;
    }

    public Boolean isFiltroCompatido() {
        return filtroCompartido;
    }

    public void setFiltroCompartido(Boolean filtroCompartido) {
        this.filtroCompartido = filtroCompartido;
    }

    public IndicadorFiltroUI getFiltrosPanel() {
        return filtrosPanel;
    }

    public void setFiltrosPanel(IndicadorFiltroUI filtrosPanel) {
        this.filtrosPanel = filtrosPanel;
    }
}