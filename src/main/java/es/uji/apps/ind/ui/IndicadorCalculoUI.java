package es.uji.apps.ind.ui;

import java.util.List;

import es.uji.apps.ind.model.Agregacion;
import es.uji.apps.ind.model.ValorInteranual;

public class IndicadorCalculoUI
{

    private List<IndicadorUI> indicadores;
    private String descripcion;
    private List<String> anyos;
    private String nivelAgregacion;
    private String componenteTemporal;
    private List<Agregacion> agregaciones;
    private List<ValorInteranual> valoresInteranuales;

    public List<IndicadorUI> getIndicadores()
    {
        return indicadores;
    }

    public void setIndicadores(List<IndicadorUI> indicadores)
    {
        this.indicadores = indicadores;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public List<String> getAnyos()
    {
        return anyos;
    }

    public void setAnyos(List<String> anyos)
    {
        this.anyos = anyos;
    }

    public String getNivelAgregacion()
    {
        return nivelAgregacion;
    }

    public void setNivelAgregacion(String nivelAgregacion)
    {
        this.nivelAgregacion = nivelAgregacion;
    }

    public String getComponenteTemporal()
    {
        return componenteTemporal;
    }

    public void setComponenteTemporal(String componenteTemporal)
    {
        this.componenteTemporal = componenteTemporal;
    }

    public List<Agregacion> getAgregaciones()
    {
        return agregaciones;
    }

    public void setAgregaciones(List<Agregacion> agregaciones)
    {
        this.agregaciones = agregaciones;
    }

    public List<ValorInteranual> getValoresInteranuales()
    {
        return valoresInteranuales;
    }

    public void setValoresInteranuales(List<ValorInteranual> valoresInteranuales)
    {
        this.valoresInteranuales = valoresInteranuales;
    }
}