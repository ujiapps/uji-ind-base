package es.uji.apps.ind.ui;

import java.util.Date;
import java.util.List;
import java.util.Map;

import es.uji.apps.ind.model.Agregacion;
import es.uji.apps.ind.model.ValorInteranual;

public class IndicadorFiltroUI {
    private List<String> anyos;
    private String componenteTemporal;
    private List<IndicadorUI> indicadores;
    private List<Agregacion> agregaciones;
    private List<ValorInteranual> valoresInteranuales;
    private Date ultimaActualizacion;
    private Map<Long, String> marcas;
    private String nivelAgregacion;

    public IndicadorFiltroUI() {
    }

    public IndicadorFiltroUI(List<String> anyos, String componenteTemporal, List<Agregacion> agregaciones, List<ValorInteranual> valoresInteranuales, Date ultimaActualizacion, Map<Long, String> marcas, String nivelAgregacion) {
        this.anyos = anyos;
        this.componenteTemporal = componenteTemporal;
        this.agregaciones = agregaciones;
        this.valoresInteranuales = valoresInteranuales;
        this.ultimaActualizacion = ultimaActualizacion;
        this.marcas = marcas;
        this.nivelAgregacion = nivelAgregacion;
    }

    public List<String> getAnyos() {
        return anyos;
    }

    public void setAnyos(List<String> anyos) {
        this.anyos = anyos;
    }

    public String getComponenteTemporal() {
        return componenteTemporal;
    }

    public void setComponenteTemporal(String componenteTemporal) {
        this.componenteTemporal = componenteTemporal;
    }

    public List<Agregacion> getAgregaciones() {
        return agregaciones;
    }

    public void setAgregaciones(List<Agregacion> agregaciones) {
        this.agregaciones = agregaciones;
    }

    public List<ValorInteranual> getValoresInteranuales() {
        return valoresInteranuales;
    }

    public void setValoresInteranuales(List<ValorInteranual> valoresInteranuales) {
        this.valoresInteranuales = valoresInteranuales;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public Date getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public void setMarcas(Map<Long, String> marcas) {
        this.marcas = marcas;
    }

    public Map<Long, String> getMarcas() {
        return marcas;
    }

    public void setNivelAgregacion(String nivelAgregacion) {
        this.nivelAgregacion = nivelAgregacion;
    }

    public String getNivelAgregacion() {
        return nivelAgregacion;
    }

    public List<IndicadorUI> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(List<IndicadorUI> indicadores) {
        this.indicadores = indicadores;
    }
}