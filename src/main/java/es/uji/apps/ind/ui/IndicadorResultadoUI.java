package es.uji.apps.ind.ui;

import java.util.List;

public class IndicadorResultadoUI
{

    private Long indicadorId;
    private String indicadorNombre;
    private List<ResultadoUI> resultados;
    private String tipoGrafico;

    public Long getIndicadorId()
    {
        return indicadorId;
    }

    public void setIndicadorId(Long indicadorId)
    {
        this.indicadorId = indicadorId;
    }

    public List<ResultadoUI> getResultados()
    {
        return resultados;
    }

    public void setResultados(List<ResultadoUI> resultados)
    {
        this.resultados = resultados;
    }

    public String getTipoGrafico()
    {
        return tipoGrafico;
    }

    public void setTipoGrafico(String tipoGrafico)
    {
        this.tipoGrafico = tipoGrafico;
    }

    public String getIndicadorNombre()
    {
        return indicadorNombre;
    }

    public void setIndicadorNombre(String indicadorNombre)
    {
        this.indicadorNombre = indicadorNombre;
    }


    //    public static UIEntity toUI(IndicadorResultadoUI indicadorResultadoUI)
//    {
//        UIEntity uiEntity = new UIEntity();
//        uiEntity.put(indicadorResultadoUI.getIndicadorId());
//        return ;
//    }
}
