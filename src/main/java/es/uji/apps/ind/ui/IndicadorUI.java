package es.uji.apps.ind.ui;

import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.VariableValor;
import es.uji.apps.ind.utils.AppUtils;
import es.uji.commons.rest.ParamUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IndicadorUI
{
    private Long id;
    private String nombre;
    private String codigo;
    private String denominacion;
    private String responsable;
    private String formula;
    private String observaciones;
    private String conceptoAEvaluar;
    private String valorReferencia;
    private String indicadorReferencia;
    private String usos;
    private String agrupaciones;
    private String agregacion;
    private String periodo;
    private Date ultimaActualizacion;
    private List<VariableValor> variableValor;
    private Map<Long, String> marcas;
    private Boolean publico;
    private Boolean activo;


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getDenominacion()
    {
        return denominacion;
    }

    public void setDenominacion(String denominacion)
    {
        this.denominacion = denominacion;
    }

    public String getResponsable()
    {
        return responsable;
    }

    public void setResponsable(String responsable)
    {
        this.responsable = responsable;
    }

    public String getFormula()
    {
        return formula;
    }

    public void setFormula(String formula)
    {
        this.formula = formulaToHtml(formula);
    }

    public Boolean getPublico()
    {
        return publico;
    }

    public void setPublico(Boolean publico)
    {
        this.publico = publico;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String formulaToHtml(String formula)
    {
        Pattern pattern = Pattern.compile("[a-zA-Z][a-zA-Z0-9_]*");
        Matcher matcher = pattern.matcher(formula);
        StringBuilder builder = new StringBuilder();

        int i = 0;
        while (matcher.find())
        {
            String str;
            if(publico)
            {
                str = "<a href=\"/ind/rest/public/variable/" + matcher.group() + "\">" + matcher.group() + "</a>";

            }else{
                str = "<a href=\"/ind/rest/publicacion/variable/" + matcher.group() + "\">" + matcher.group() + "</a>";

            }

            builder.append(formula.substring(i, matcher.start()));
            builder.append(str);
            i = matcher.end();
        }
        builder.append(formula.substring(i, formula.length()));

        return builder.toString();
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public String getConceptoAEvaluar()
    {
        return conceptoAEvaluar;
    }

    public void setConceptoAEvaluar(String conceptoAEvaluar)
    {
        this.conceptoAEvaluar = conceptoAEvaluar;
    }

    public String getValorReferencia()
    {
        return valorReferencia;
    }

    public void setValorReferencia(String valorReferencia)
    {
        this.valorReferencia = valorReferencia;
    }

    public String getUsos()
    {
        return usos;
    }

    public void setUsos(String usos)
    {
        this.usos = usos;
    }

    public String getAgrupaciones()
    {
        return agrupaciones;
    }

    public void setAgrupaciones(String agrupaciones)
    {
        this.agrupaciones = agrupaciones;
    }

    public String getAgregacion()
    {
        return agregacion;
    }

    public void setAgregacion(String agregacion)
    {
        this.agregacion = agregacion;
    }

    public String getPeriodo()
    {
        return periodo;
    }

    public void setPeriodo(String periodo)
    {
        this.periodo = periodo;
    }

    public List<VariableValor> getVariableValor()
    {
        return variableValor;
    }

    public void setVariableValor(List<VariableValor> variableValor)
    {
        this.variableValor = variableValor;
    }

    public Date getUltimaActualizacion()
    {
        return ultimaActualizacion;
    }

    public void setUltimaActualizacion(Date ultimaActualizacion)
    {
        this.ultimaActualizacion = ultimaActualizacion;
    }

    public Map<Long, String> getMarcas()
    {
        return marcas;
    }

    public void setMarcas(Map<Long, String> marcas)
    {
        this.marcas = marcas;
    }

    public String getIndicadorReferencia() {
        return indicadorReferencia;
    }

    public void setIndicadorReferencia(String indicadorReferencia) {
        this.indicadorReferencia = indicadorReferencia;
    }

    public static List<IndicadorUI> toUI(List<Indicador> indicadorList, String idioma) throws VariablesNoExistentesException {
        List<IndicadorUI> list = new ArrayList<>();
        for (Indicador indicador : indicadorList) {
            list.add(toUI(indicador, idioma));
        }
        return list;
    }

    public static IndicadorUI toUI(Indicador indicador, String idioma) throws VariablesNoExistentesException {
        IndicadorUI indicadorUI = new IndicadorUI();
        indicadorUI.setId(indicador.getId());
        indicadorUI.setPublico(indicador.getPublico());

        switch (idioma.toLowerCase()) {
            case "es":
                indicadorUI.setNombre(indicador.getNombreES() != null ? indicador.getNombreES() : indicador.getNombre());
                indicadorUI.setDenominacion(indicador.getDenominacionES() != null ? indicador.getDenominacionES() : indicador.getDenominacion());
                indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluarES() != null ? indicador.getConceptoAEvaluarES() : indicador.getConceptoAEvaluar());
                indicadorUI.setObservaciones(indicador.getObservacionesES());
                if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())){
                    indicadorUI.setIndicadorReferencia(indicador.getIndicadorReferencia().getNombreES());
                }
                break;
            case "en":
                indicadorUI.setNombre(indicador.getNombreEN() != null ? indicador.getNombreEN() : indicador.getNombre());
                indicadorUI.setDenominacion(indicador.getDenominacionEN() != null ? indicador.getDenominacionEN() : indicador.getDenominacion());
                indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluarEN() != null ? indicador.getConceptoAEvaluarEN() : indicador.getConceptoAEvaluar());
                indicadorUI.setObservaciones(indicador.getObservacionesEN());
                if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())){
                    indicadorUI.setIndicadorReferencia(indicador.getIndicadorReferencia().getNombreEN());
                }
                break;
            default:
                indicadorUI.setNombre(indicador.getNombre());
                indicadorUI.setDenominacion(indicador.getDenominacion());
                indicadorUI.setConceptoAEvaluar(indicador.getConceptoAEvaluar());
                indicadorUI.setObservaciones(indicador.getObservaciones());
                if (ParamUtils.isNotNull(indicador.getIndicadorReferencia())){
                    indicadorUI.setIndicadorReferencia(indicador.getIndicadorReferencia().getNombre());
                }
        }
        indicadorUI.setCodigo(indicador.getCodigo());
        indicadorUI.setResponsable(indicador.getResponsable());
        indicadorUI.setFormula(indicador.getFormula());
        indicadorUI.setValorReferencia(indicador.getValorReferencia());
        indicadorUI.setMarcas(AppUtils.marcasToUI(indicador.getMarcas()));
        indicadorUI.setActivo(indicadorUI.getActivo());
//        indicadorUI.setUltimaActualizacion(indicadorService.getUltimaActulizacion(indicador.getId()));

        return indicadorUI;
    }
}