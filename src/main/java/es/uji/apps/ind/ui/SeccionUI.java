package es.uji.apps.ind.ui;

import java.util.List;

public class SeccionUI {
    private Long id;
    private String nombre;
    private Long orden;
    private List<IndicadorCalculoUI> tiposIndicador;
    private Boolean filtroCompartido;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getOrden() {
        return orden;
    }

    public void setOrden(Long orden) {
        this.orden = orden;
    }

    public List<IndicadorCalculoUI> getTiposIndicador() {
        return tiposIndicador;
    }

    public void setTiposIndicador(List<IndicadorCalculoUI> tiposIndicador) {
        this.tiposIndicador = tiposIndicador;
    }

    public Boolean getFiltroCompartido() {
        return filtroCompartido;
    }

    public void setFiltroCompartido() {
        if(this.getTiposIndicador().size()<=1){
            this.filtroCompartido=true;
        }else {
            this.filtroCompartido = false;
//                    this.getIndicadores().stream()
//                    .map(indicadorCalculoUI -> indicadorCalculoUI.getComponenteTemporal().concat(indicadorCalculoUI.getNivelAgregacion()))
//                    .distinct().limit(2).count() <= 1;
        }
    }

//    public void calculaTiposIndicadores() {
//        List<String> tiposCalculados = new ArrayList<>();
//        this.getIndicadores().stream().map(indicadorCalculoUI -> indicadorCalculoUI.getComponenteTemporal().concat(indicadorCalculoUI.getNivelAgregacion()))
//                .distinct().count()<= 1;
//        this.getIndicadores().forEach(indicadorCalculoUI -> {
//            if (tiposCalculados.isEmpty()) {
//                tiposCalculados.add(indicadorCalculoUI.getComponenteTemporal().concat(indicadorCalculoUI.getNivelAgregacion()));
//            } else if (!tiposCalculados.contains(indicadorCalculoUI.getComponenteTemporal().concat(indicadorCalculoUI.getNivelAgregacion()))) {
//                tiposCalculados.add(indicadorCalculoUI.getComponenteTemporal().concat(indicadorCalculoUI.getNivelAgregacion()));
//            }
//        });
//        this.setTiposIndicadores(tiposCalculados);
//    }
}