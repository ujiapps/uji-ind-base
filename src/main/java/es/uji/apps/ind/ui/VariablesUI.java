package es.uji.apps.ind.ui;

import es.uji.apps.ind.exceptions.VariablesNoExistentesException;
import es.uji.apps.ind.model.Variable;

import java.util.List;
import java.util.stream.Collectors;

public class VariablesUI {
    private Long id;
    private String codigo;
    private String nombre;

    public static List<VariablesUI> toUI(List<Variable> variablesList, String idioma) throws VariablesNoExistentesException {
        return variablesList.stream().map(variable -> toUI(variable, idioma)).collect(Collectors.toList());
    }

    public static VariablesUI toUI(Variable variable, String idioma) {
        VariablesUI variableUI = new VariablesUI();
        variableUI.setId(variable.getId());

        switch (idioma.toLowerCase()) {
            case "es":
                variableUI.setNombre(variable.getNombreES() != null ? variable.getNombreES() : variable.getNombre());
                break;
            case "en":
                variableUI.setNombre(variable.getNombreEN() != null ? variable.getNombreEN() : variable.getNombre());
                break;
            default:
                variableUI.setNombre(variable.getNombre());
        }
        variableUI.setCodigo(variable.getCodigo());

        return variableUI;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}