package es.uji.apps.ind.ui;

import java.math.BigDecimal;

public class ResultadoUI
{

    private Long id;
    private Long agregacion;
    private String agregacionNombre;
    private Integer anyo;
    private Long valorInteranual;
    private String valorInteranualtext;
    private BigDecimal valor;
    private String expresion;

    public ResultadoUI(Long id, Long agregacion, String agregacionNombre, Integer anyo, Long valorInteranual, String valorInteranualtext, BigDecimal valor, String expresion) {
        this.id = id;
        this.agregacion = agregacion;
        this.agregacionNombre = agregacionNombre;
        this.anyo = anyo;
        this.valorInteranual = valorInteranual;
        this.valorInteranualtext = valorInteranualtext;
        this.valor = valor;
        this.expresion = expresion;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getAgregacion()
    {
        return agregacion;
    }

    public void setAgregacion(Long agregacion)
    {
        this.agregacion = agregacion;
    }

    public String getAgregacionNombre()
    {
        return agregacionNombre;
    }

    public void setAgregacionNombre(String agregacionNombre)
    {
        this.agregacionNombre = agregacionNombre;
    }

    public Integer getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Integer anyo)
    {
        this.anyo = anyo;
    }

    public Long getValorInteranual()
    {
        return valorInteranual;
    }

    public void setValorInteranual(Long valorInteranual)
    {
        this.valorInteranual = valorInteranual;
    }

    public String getValorInteranualtext()
    {
        return valorInteranualtext;
    }

    public void setValorInteranualtext(String valorInteranualtext)
    {
        this.valorInteranualtext = valorInteranualtext;
    }

    public BigDecimal getValor()
    {
        return valor;
    }

    public void setValor(BigDecimal valor)
    {
        this.valor = valor;
    }

    public String getExpresion()
    {
        return expresion;
    }

    public void setExpresion(String expresion)
    {
        this.expresion = expresion;
    }
}
