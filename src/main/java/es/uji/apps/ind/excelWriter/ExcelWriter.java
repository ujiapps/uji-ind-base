package es.uji.apps.ind.excelWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import es.uji.apps.ind.ui.IndicadorResultadoUI;
import es.uji.apps.ind.ui.ResultadoUI;

public class ExcelWriter {
    public static ByteArrayInputStream indicadorResultadosExcel(List<IndicadorResultadoUI> indicadorResultados)
            throws IOException {
        String[] COLUMNs = {"Indicador", "Agregació", "Any", "Valor Interanual", "Valor", "Expressió"};
        try {
            String nameSheet = "Indicador";
            Workbook workbook = new XSSFWorkbook();
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int rowNum = 1;
            Sheet sheet = workbook.createSheet(); //workbook.createSheet("Indicador ".concat(String.valueOf(indicadorResultados.get(0).getIndicadorId())));
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
            }
            for (IndicadorResultadoUI indicadorResultadoUI : indicadorResultados) {
                nameSheet = nameSheet.concat(" " + indicadorResultadoUI.getIndicadorId());
                for (ResultadoUI resultado : indicadorResultadoUI.getResultados()) {
                    Row row = sheet.createRow(rowNum);
                    row.createCell(0).setCellValue(indicadorResultadoUI.getIndicadorNombre());
                    row.createCell(1).setCellValue(resultado.getAgregacionNombre());
                    row.createCell(2).setCellValue(resultado.getAnyo());
                    row.createCell(3).setCellValue(resultado.getValorInteranualtext());
                    if (resultado.getValor() != null) {
                        row.createCell(4).setCellValue(resultado.getValor().doubleValue());
                    }
                    row.createCell(5).setCellValue(resultado.getExpresion());
                    rowNum++;
                }
                for (int i = 0; i < COLUMNs.length; i++) {
                    sheet.autoSizeColumn(i);
                }

            }
            workbook.setSheetName(0, nameSheet);
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
