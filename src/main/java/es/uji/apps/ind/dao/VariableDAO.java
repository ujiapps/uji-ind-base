package es.uji.apps.ind.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import es.uji.apps.ind.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.xml.lookup.LookupItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class VariableDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {
    private final QVariable qVariable = QVariable.variable;
    private final QAgregacionServicio qAgregacionServicio = QAgregacionServicio.agregacionServicio;
    private final QPersona qPersona = QPersona.persona;
    private final QServiciosAdministrador qServiciosAdministrador = QServiciosAdministrador.serviciosAdministrador;

    public Variable getByCode(String variableCode) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariable).where(qVariable.codigo.eq(variableCode));

        return query.uniqueResult(qVariable);
    }

    public Variable getById(Long id) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariable).leftJoin(qVariable.valores).fetch().where(qVariable.id.eq(id));

        return query.uniqueResult(qVariable);
    }

    public List<Variable> getDynamicVariablesByCodes(List<String> variableCodes) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariable).where(qVariable.codigo.in(variableCodes).and(qVariable.dinamica.eq(1)));

        return query.list(qVariable);
    }

    public List<Variable> getVariablesByCodes(List<String> variableCodes) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariable).where(qVariable.codigo.in(variableCodes));

        return query.list(qVariable);
    }

    public List<Variable> getVariableByName(String filter) {
        return getVariableByName(filter, null);
    }

    public List<Variable> getVariableByName(String filter, Long connectedUserId) {
        JPAQuery query = new JPAQuery(entityManager);

        if (ParamUtils.isNotNull(connectedUserId))
        {
            query.from(qVariable)
                    .join(qVariable.servicio, qAgregacionServicio)
                    .join(qAgregacionServicio.serviciosAdministrador, qServiciosAdministrador)
                    .join(qServiciosAdministrador.persona, qPersona)
                    .where(qPersona.personaId.eq(connectedUserId).and(
                            qVariable.nombre.upper().like("%" + filter.toUpperCase() + "%")
                                    .or(qVariable.codigo.upper().like("%" + filter.toUpperCase() + "%"))));
        } else
        {
            query.from(qVariable).where(qVariable.nombre.upper().like("%" + filter.toUpperCase() + "%")
                    .or(qVariable.codigo.upper().like("%" + filter.toUpperCase() + "%")));
        }

        return query.list(QVariable.create(
                qVariable.id,
                qVariable.codigo,
                qVariable.nombre,
                qVariable.dinamica,
                qVariable.nivelAgregacion,
                qVariable.componenteTemporal,
                qVariable.servicio,
                qVariable.activa));
    }

    public List<Variable> getVariables(Long personaId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariable).join(qVariable.servicio, qAgregacionServicio);

        if (ParamUtils.isNotNull(personaId))
        {
            query.join(qAgregacionServicio.serviciosAdministrador, qServiciosAdministrador)
                    .join(qServiciosAdministrador.persona, qPersona);
            query.where(qPersona.personaId.eq(personaId));
        }

        return query.list(QVariable.create(
                qVariable.id,
                qVariable.codigo,
                qVariable.nombre,
                qVariable.dinamica,
                qVariable.nivelAgregacion,
                qVariable.componenteTemporal,
                qVariable.servicio,
                qVariable.activa));
    }

    @Transactional
    public void toggleActivarVariable(Long id, Boolean activar) {
        new JPAUpdateClause(entityManager, qVariable).where(qVariable.id.eq(id))
                .set(qVariable.activa, activar)
                .execute();
    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qVariable);

        if (!s.isEmpty()) {
            try {
                query.where(qVariable.id.eq(Long.parseLong(s))
                        .or(qVariable.codigo.containsIgnoreCase(StringUtils.limpiaAcentos(s))));
            } catch (NumberFormatException e) {
                query.where(qVariable.codigo.containsIgnoreCase(StringUtils.limpiaAcentos(s)));
            }
        }

        return query.list(qVariable)
                .stream()
                .map(variable -> {
                    LookupItem item = new LookupItem();
                    item.setId(variable.getId().toString());
                    item.setNombre(variable.getCodigo());
                    return item;
                }).collect(Collectors.toList());
    }

    public String getCodigoVariable(Long variableId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariable).where(qVariable.id.eq(variableId));

        return query.uniqueResult(qVariable.codigo);
    }
}