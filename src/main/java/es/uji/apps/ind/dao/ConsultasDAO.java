package es.uji.apps.ind.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.internal.SessionImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.uji.apps.ind.model.ConsultaSQL;
import es.uji.apps.ind.model.ConsultasLog;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class ConsultasDAO extends BaseDAODatabaseImpl
{

    @Transactional
    public ConsultaSQL ejecutaSQl(Long userId, String sql) throws SQLException
    {
        if(ParamUtils.isNotNull(userId))
        {
            insert(new ConsultasLog(userId, sql));
        }
        Connection conn = entityManager.unwrap(SessionImpl.class).connection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ResultSet rs = ps.executeQuery(sql);
        ResultSetMetaData mData = rs.getMetaData();
        Integer columnas = mData.getColumnCount();
        Set<String> info = new HashSet<>();
        List<Map<String, String>> rows = new ArrayList<>();
        for (int i = 1; i <= columnas; i++ ) {
            info.add(mData.getColumnName(i));
        }

        while (rs.next())
        {
            Map<String, String> row = new HashMap<>();
            for (int i = 1; i <= columnas; i++)
            {
                row.put(mData.getColumnName(i), rs.getString(i));
            }
            rows.add(row);

        }
        return new ConsultaSQL(info,rows);

    }
}
