package es.uji.apps.ind.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class PersonaDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem>
{

    public final static Logger log = LoggerFactory.getLogger(PersonaDAO.class);

    private QPersona qPersona = QPersona.persona;

    @Override
    public List<LookupItem> search(String s)
    {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(s);
        Long queryStringLong = -1L;

        try
        {
            queryStringLong = Long.parseLong(s);
        }
        catch (Exception e)
        {
            log.debug("NumberFormatException busqueda por string", e);
        }

        query.from(qPersona);

        if (ParamUtils.isNotNull(s))
        {
            query.where(qPersona.personaId.eq(queryStringLong)
                    .or(qPersona.nombreBusqueda.like('%' + cadenaLimpia.toUpperCase() + '%'))
                    .or(qPersona.mail.like('%' + cadenaLimpia + '%')));
        }
        return query.distinct().list(qPersona).stream()
                .map(persona -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(persona.getPersonaId()));
                    item.setNombre(persona.getNombreCompleto());
                    item.addExtraParam("user", persona.getMail());
                    return item;
                }).collect(Collectors.toList());
    }
}
