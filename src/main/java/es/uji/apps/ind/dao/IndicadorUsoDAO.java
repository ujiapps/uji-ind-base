package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.IndicadorUso;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QIndicadorUso;
import es.uji.apps.ind.model.QUso;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Component
public class IndicadorUsoDAO extends BaseDAODatabaseImpl
{
    private QIndicadorUso indicadorUso = QIndicadorUso.indicadorUso;
    private QUso uso = QUso.uso;
    private QIndicador indicador = QIndicador.indicador;

    public List<IndicadorUso> getByIndicadorId(Long indicadorId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicadorUso).join(indicadorUso.uso, uso).fetch()
                .where(indicadorUso.indicador.id.eq(indicadorId));

        return query.orderBy(uso.nombre.asc()).list(indicadorUso);
    }

    public List<IndicadorUso> getByUsoId(Long usoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicadorUso).join(indicadorUso.indicador, indicador).fetch()
                .leftJoin(indicador.variables).fetch().where(indicadorUso.uso.id.eq(usoId));

        return query.orderBy(indicador.nombre.asc()).distinct().list(indicadorUso);
    }
}
