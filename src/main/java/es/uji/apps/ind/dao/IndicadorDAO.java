package es.uji.apps.ind.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import es.uji.commons.db.LookupFilterDAO;
import es.uji.commons.sso.dao.ApaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.IndicadorMarcaResultadoVW;
import es.uji.apps.ind.model.QAgregacionServicio;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QIndicadorAgrupacion;
import es.uji.apps.ind.model.QIndicadorMarca;
import es.uji.apps.ind.model.QIndicadorMarcaResultado;
import es.uji.apps.ind.model.QIndicadorMarcaResultadoVW;
import es.uji.apps.ind.model.QIndicadorUso;
import es.uji.apps.ind.model.QPersona;
import es.uji.apps.ind.model.QServiciosAdministrador;
import es.uji.apps.ind.model.QVariable;
import es.uji.apps.ind.model.QVariableIndicador;
import es.uji.apps.ind.model.enums.TipoMarca;
import es.uji.apps.ind.utils.AppUtils;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class IndicadorDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem>, LookupFilterDAO<LookupItem> {
    private QIndicador indicador = QIndicador.indicador;
    private QVariableIndicador qVariableIndicador = QVariableIndicador.variableIndicador;
    private QVariable qVariable = QVariable.variable;
    private QAgregacionServicio qAgregacionServicio = QAgregacionServicio.agregacionServicio;
    private QServiciosAdministrador qServiciosAdministrador = QServiciosAdministrador.serviciosAdministrador;
    private QPersona qPersona = QPersona.persona;

    @Autowired
    private ApaDAO apaDAO;

    public Indicador getById(Long id, Boolean publico) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.usos).fetch()
                .leftJoin(indicador.agrupaciones).fetch()
                .leftJoin(indicador.variables).fetch()
                .leftJoin(indicador.marcas).fetch()
                .where(indicador.id.eq(id));
        if (publico) {
            query.where(indicador.publico.eq(publico));
        }
        return query.uniqueResult(indicador);
    }

    public List<Indicador> getIndicadoresByCode(String codigo) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador)
                .where(indicador.formula.like("%" + codigo + "%"));

        return query.list(indicador);
    }

    public List<Indicador> buscarIndicadores(String descripcion, Long uso, Long agrupacion, Boolean publico) {
        JPAQuery query = new JPAQuery(entityManager);
        QIndicadorUso qIndicadorUso = QIndicadorUso.indicadorUso;
        QIndicadorAgrupacion qIndicadorAgrupacion = QIndicadorAgrupacion.indicadorAgrupacion;

        query.from(indicador).leftJoin(indicador.usos, qIndicadorUso).fetch()
                .leftJoin(indicador.agrupaciones, qIndicadorAgrupacion).fetch()
                .leftJoin(indicador.marcas).fetch()
                .leftJoin(indicador.variables).fetch()
                .where(indicador.activo.isTrue());

        if (ParamUtils.isNotNull(descripcion)) {
            String s = StringUtils.limpiaAcentos(descripcion).toUpperCase();
            query.where(AppUtils.limpiaAcentos(indicador.nombre).like('%' + s + '%').or(indicador.codigo.upper().like('%' + s + '%')));
        }
        if (ParamUtils.isNotNull(uso)) {
            query.where(qIndicadorUso.uso.id.eq(uso));
        }
        if (ParamUtils.isNotNull(agrupacion)) {
            query.where(qIndicadorAgrupacion.agrupacion.id.eq(agrupacion));
        }
        if (publico) {
            query.where(indicador.publico.eq(publico));
        }

        return query.distinct().list(indicador);
    }

    public List<Indicador> getIndicadores(String query) {
        return getIndicadores(null, query);
    }

    public List<Indicador> getIndicadores(Long personaId, String queryString) {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(queryString);
        Long queryStringLong = -1L;

        try {
            queryStringLong = Long.parseLong(queryString);
        } catch (Exception e) {
        }

        query.from(indicador);
        if (ParamUtils.isNotNull(personaId)) {
            query.leftJoin(indicador.variables, qVariableIndicador)
                    .join(qVariableIndicador.variable, qVariable)
                    .join(qVariable.servicio, qAgregacionServicio)
                    .join(qAgregacionServicio.serviciosAdministrador, qServiciosAdministrador)
                    .join(qServiciosAdministrador.persona, qPersona)
                    .where(qPersona.personaId.eq(personaId));
        }

        if (ParamUtils.isNotNull(queryString)) {
            query.where(indicador.id.eq(queryStringLong)
                    .or(indicador.codigo.upper().like('%' + cadenaLimpia.toUpperCase() + '%')));
        }

        return query.list(QIndicador.create(
                indicador.id,
                indicador.nombre,
                indicador.codigo,
                indicador.responsable,
                indicador.formula,
                indicador.valorReferencia,
                indicador.tipoGrafico,
                indicador.publico,
                indicador.activo));
    }


    @Override
    public List<LookupItem> search(String s, Map<String, String> map) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador)
                .leftJoin(indicador.variables, qVariableIndicador)
                .join(qVariableIndicador.variable, qVariable)
                .where(qVariable.nivelAgregacion.eq("INSTITUCIONAL"));

        if (s != null && !s.isEmpty()) {
            query.where(indicador.nombre.containsIgnoreCase(s).or(indicador.codigo.containsIgnoreCase(s)));
        }

        Long personaId = ParamUtils.parseLong(map.get("userAuthenticatedId"));
        if (!apaDAO.hasPerfil("IND", "ADMIN", personaId)) {

            query.join(qVariable.servicio, qAgregacionServicio)
                    .join(qAgregacionServicio.serviciosAdministrador, qServiciosAdministrador)
                    .join(qServiciosAdministrador.persona, qPersona)
                    .where(qPersona.personaId.eq(personaId));
        }

        query.orderBy(indicador.codigo.asc(), indicador.nombre.asc());

        return query.distinct().list(QIndicador.create(
                indicador.id,
                indicador.nombre,
                indicador.codigo,
                indicador.responsable,
                indicador.formula,
                indicador.valorReferencia,
                indicador.tipoGrafico,
                indicador.publico,
                indicador.activo))
                .stream().map(ind -> {
                    LookupItem item = new LookupItem();
                    item.setId(String.valueOf(ind.getId()));
                    item.setNombre(ind.getNombre());
                    item.addExtraParam("codi", ind.getCodigo());

                    return item;
                }).collect(Collectors.toList());

    }

    public void borraCache(Long id) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qIndicadorMarca);

        deleteClause.where(qIndicadorMarca.indicador.id.eq(id).and(qIndicadorMarca.tipo.eq(TipoMarca.CACHE))).execute();
    }

    public List<IndicadorMarcaResultadoVW> getResultadosCache(Indicador indicador, List<Long> agregacion, List<Integer> anyo, Long valorInteranual, List<Long> marca) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultadoVW qIndicadorMarcaResultadoVW = QIndicadorMarcaResultadoVW.indicadorMarcaResultadoVW;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultadoVW).join(qIndicadorMarcaResultadoVW.marca, qIndicadorMarca).where(
                qIndicadorMarca.indicador.id.eq(indicador.getId()));
        if (marca != null && marca.size() != 0) {
            query.where(qIndicadorMarca.id.in(marca));
        } else {
            query.where(qIndicadorMarca.tipo.eq(TipoMarca.CACHE));
        }
        if (!agregacion.isEmpty()) {
            query.where(qIndicadorMarcaResultadoVW.agregacion.in(agregacion));
        }

        if (!anyo.isEmpty()) {
            query.where(qIndicadorMarcaResultadoVW.anyo.in(anyo));
        }
        if (ParamUtils.isNotNull(valorInteranual)) {
            query.where(qIndicadorMarcaResultadoVW.valorInteranual.eq(valorInteranual));
        }
        query.orderBy(qIndicadorMarcaResultadoVW.anyo.asc());
        return query.list(qIndicadorMarcaResultadoVW);
    }

    public List<Integer> getDistinctAnyosFromIndicadorCache(List<Indicador> indicador) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca, qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.in(indicador)));

        query.orderBy(qIndicadorMarcaResultado.anyo.asc());
        return query.distinct().list(qIndicadorMarcaResultado.anyo);

    }

    public List<Long> getDistinctAgregacionesIdCache(Indicador indicador) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca, qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.id.eq(indicador.getId())));


        return query.distinct().list(qIndicadorMarcaResultado.agregacion);
    }

    public List<Long> getDistinctAgregacionesIdCache(List<Indicador> indicadores) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca, qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.in(indicadores)));


        return query.distinct().list(qIndicadorMarcaResultado.agregacion);
    }

    public List<Long> getDistinctValoresInteranualesCache(List<Indicador> indicadores) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
        QIndicadorMarcaResultado qIndicadorMarcaResultado = QIndicadorMarcaResultado.indicadorMarcaResultado;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarcaResultado).join(qIndicadorMarcaResultado.marca, qIndicadorMarca).where(
                qIndicadorMarca.tipo.eq(TipoMarca.CACHE)
                        .and(qIndicadorMarca.indicador.in(indicadores)));


        return query.distinct().list(qIndicadorMarcaResultado.valorInteranual);
    }

    public Date getUltimaActulizacion(Long id) {
        QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;

        JPAQuery query = new JPAQuery(entityManager);

        query.from(qIndicadorMarca).where(qIndicadorMarca.indicador.id.eq(id).and(qIndicadorMarca.tipo.eq(TipoMarca.CACHE)));

        return query.uniqueResult(qIndicadorMarca.fecha);
    }

    public List<Indicador> getByIds(List<Long> indiadores) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.usos).fetch()
                .leftJoin(indicador.agrupaciones).fetch()
                .leftJoin(indicador.variables).fetch()
                .leftJoin(indicador.marcas).fetch()
                .where(indicador.id.in(indiadores));

        return query.list(indicador);
    }

    public List<Indicador> getByIds(List<Long> indiadores, Boolean publico) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicador).leftJoin(indicador.usos).fetch()
                .leftJoin(indicador.agrupaciones).fetch()
                .leftJoin(indicador.variables).fetch()
                .leftJoin(indicador.marcas).fetch()
                .where(indicador.id.in(indiadores));
        if (publico) {
            query.where(indicador.publico.eq(publico));
        }
        return query.list(indicador);
    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(s);
        Long queryStringLong = -1L;

        try {
            queryStringLong = Long.parseLong(s);
        } catch (Exception e) {
            e.printStackTrace();
        }

        query.from(indicador);

        if (ParamUtils.isNotNull(s)) {
            query.where(indicador.id.eq(queryStringLong)
                    .or(indicador.codigo.like('%' + cadenaLimpia + '%')));
        }

        return query.distinct().list(indicador).stream().map(ind -> {
            LookupItem item = new LookupItem();
            item.setId(String.valueOf(ind.getId()));
            item.setNombre(ind.getNombre());
            return item;
        }).collect(Collectors.toList());
    }

    public List<String> getIndicadoresByVariableId(Long variableId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariableIndicador)
                .join(qVariableIndicador.indicador, indicador)
                .where(qVariableIndicador.variable.id.eq(variableId));

        return query.list(indicador.codigo);

    }

    public List<Indicador> getAllIndicadoresByVariableId(Long variableId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariableIndicador)
                .join(qVariableIndicador.indicador, indicador)
                .where(qVariableIndicador.variable.id.eq(variableId));

        return query.list(indicador);

    }

    @Transactional
    public void toggleActivarIndicador(Long id, Boolean activar) {
        new JPAUpdateClause(entityManager, indicador).where(indicador.id.eq(id))
                .set(indicador.activo, activar)
                .execute();
    }
}