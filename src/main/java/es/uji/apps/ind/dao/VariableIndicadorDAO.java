package es.uji.apps.ind.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPADeleteClause;

import es.uji.apps.ind.model.QVariableIndicador;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class VariableIndicadorDAO extends BaseDAODatabaseImpl
{
    private QVariableIndicador qVariableIndicador = QVariableIndicador.variableIndicador;

    public void deleteByIndicadorId(Long indicadorId){

        JPADeleteClause delete = new JPADeleteClause(entityManager,qVariableIndicador);
        if(indicadorId!=null){
            delete.where(qVariableIndicador.indicador.id.eq(indicadorId)).execute();
        }
    }

}
