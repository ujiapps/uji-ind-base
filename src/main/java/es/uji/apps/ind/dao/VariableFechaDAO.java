package es.uji.apps.ind.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.ind.model.QVariableFecha;
import es.uji.apps.ind.model.VariableFecha;
import es.uji.apps.ind.model.VariableFechaPK;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class VariableFechaDAO extends BaseDAODatabaseImpl {

    private QVariableFecha qVariableFecha = QVariableFecha.variableFecha;

    public List<VariableFecha> getVariableFechas(Long variableId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariableFecha).where(qVariableFecha.id.variableId.eq(variableId)).orderBy(qVariableFecha.id.fechaInicio.desc());

        return query.list(qVariableFecha.id.variableId, qVariableFecha.id.fechaInicio, qVariableFecha.fechaFin).stream().map(tuple -> {
            VariableFecha variableFecha = new VariableFecha();
            VariableFechaPK variableFechaPK = new VariableFechaPK(
                    tuple.get(qVariableFecha.id.variableId),
                    tuple.get(qVariableFecha.id.fechaInicio)
            );
            variableFecha.setFechaFin(tuple.get(qVariableFecha.fechaFin));
            variableFecha.setId(variableFechaPK);
            return variableFecha;
        }).collect(Collectors.toList());
    }

    @Transactional
    public void deleteVariableFecha(VariableFecha variableFecha) {
        JPADeleteClause query = new JPADeleteClause(entityManager, qVariableFecha);
        query.where(qVariableFecha.id.variableId.eq(variableFecha.getId().getVariableId())
                .and(qVariableFecha.id.fechaInicio.eq(variableFecha.getId().getFechaInicio())));
        query.execute();
    }
}
