package es.uji.apps.ind.dao;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.ind.model.QVariableCorreo;
import es.uji.apps.ind.model.VariableCorreo;
import es.uji.apps.ind.model.VariableCorreoPK;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class VariableCorreoDAO extends BaseDAODatabaseImpl {

    private QVariableCorreo qVariableCorreo = QVariableCorreo.variableCorreo;

    public List<VariableCorreo> getVariablesCorreos(Long variableId) {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qVariableCorreo).where(qVariableCorreo.id.variableId.eq(variableId));

        return query.list(qVariableCorreo.id.variableId, qVariableCorreo.id.correo).stream().map(tuple -> {
            VariableCorreo variableCorreo = new VariableCorreo();
            VariableCorreoPK variableCorreoPK = new VariableCorreoPK(tuple.get(qVariableCorreo.id.variableId),tuple.get(qVariableCorreo.id.correo) );
            variableCorreo.setId(variableCorreoPK);
            return variableCorreo;
        }).collect(Collectors.toList());
    }

    @Transactional
    public void deleteVariableCorreo(Long variableId, String correo) {
        JPADeleteClause query = new JPADeleteClause(entityManager, qVariableCorreo);
        query.where(qVariableCorreo.id.variableId.eq(variableId).and(qVariableCorreo.id.correo.eq(correo)));
        query.execute();
    }
}
