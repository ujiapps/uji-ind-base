package es.uji.apps.ind.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.ind.model.QUso;
import es.uji.apps.ind.model.Uso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class UsoDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem> {
    private QUso qUso = QUso.uso;

    public List<Uso> get() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qUso).orderBy(qUso.nombre.asc());
        return query.list(qUso);

    }

    @Override
    public List<LookupItem> search(String s) {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(s);
        Long queryStringLong = -1L;

        try
        {
            queryStringLong = Long.parseLong(s);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        query.from(qUso);

        if (ParamUtils.isNotNull(s))
        {
            query.where(qUso.id.eq(queryStringLong)
                    .or(qUso.nombre.upper().like('%' + cadenaLimpia.toUpperCase() + '%')));
        }

        return query.distinct().list(qUso).stream().map(ind -> {
            LookupItem item = new LookupItem();
            item.setId(String.valueOf(ind.getId()));
            item.setNombre(ind.getNombre());
            return item;
        }).collect(Collectors.toList());
    }

    @Transactional
    public void toggleActivarUso(Long id, Boolean activo) {
        new JPAUpdateClause(entityManager, qUso).where(qUso.id.eq(id))
                .set(qUso.activo, activo)
                .execute();
    }

    public List<Uso> getUsosActivos() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qUso).where(qUso.activo.isTrue()).orderBy(qUso.nombre.asc());
        return query.list(qUso);
    }
}