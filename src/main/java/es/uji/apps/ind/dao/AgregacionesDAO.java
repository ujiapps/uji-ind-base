package es.uji.apps.ind.dao;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.AgregacionCentro;
import es.uji.apps.ind.model.AgregacionDepartamento;
import es.uji.apps.ind.model.AgregacionEstudio;
import es.uji.apps.ind.model.AgregacionInstitucional;
import es.uji.apps.ind.model.AgregacionInstituto;
import es.uji.apps.ind.model.AgregacionServicio;
import es.uji.apps.ind.model.QAgregacionCentro;
import es.uji.apps.ind.model.QAgregacionDepartamento;
import es.uji.apps.ind.model.QAgregacionEstudio;
import es.uji.apps.ind.model.QAgregacionInstituto;
import es.uji.apps.ind.model.QAgregacionServicio;
import es.uji.apps.ind.model.QPersona;
import es.uji.apps.ind.model.QServiciosAdministrador;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;

@Repository
public class AgregacionesDAO extends BaseDAODatabaseImpl
{
    public List<AgregacionInstitucional> getInstitucional()
    {
        AgregacionInstitucional agregacionInstitucional = new AgregacionInstitucional();

        agregacionInstitucional.setId(1L);
        agregacionInstitucional.setNombreCA("Institucional");
        agregacionInstitucional.setNombreES("Institucional");
        agregacionInstitucional.setNombreEN("Institucional");

        return Collections.singletonList(agregacionInstitucional);
    }


    public List<AgregacionCentro> getCentro(String idioma)
    {
        QAgregacionCentro qCentro = QAgregacionCentro.agregacionCentro;

        JPAQuery query = new JPAQuery(entityManager);
        query.from(qCentro);
        if (idioma.equals("ES"))
        {
            query.orderBy(qCentro.nombreES.asc());
        }
        else if (idioma.equals("EN"))
        {
            query.orderBy(qCentro.nombreEN.asc());

        }
        else
        {
            query.orderBy(qCentro.nombreCA.asc());
        }
        return query.list(qCentro);
    }

    public List<AgregacionDepartamento> getDepartamentos(String idioma)
    {
        QAgregacionDepartamento qDepartamento = QAgregacionDepartamento.agregacionDepartamento;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qDepartamento);
        if (idioma.equals("ES"))
        {
            query.orderBy(qDepartamento.nombreES.asc());
        }
        else if (idioma.equals("EN"))
        {
            query.orderBy(qDepartamento.nombreEN.asc());

        }
        else
        {
            query.orderBy(qDepartamento.nombreCA.asc());
        }
        return query.list(qDepartamento);
    }

    public List<AgregacionEstudio> getEstudios(String idioma)
    {
        QAgregacionEstudio qAgregacion = QAgregacionEstudio.agregacionEstudio;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAgregacion);
        if (idioma.equals("ES"))
        {
            query.orderBy(qAgregacion.nombreES.asc());
        }
        else if (idioma.equals("EN"))
        {
            query.orderBy(qAgregacion.nombreEN.asc());
        }
        else
        {
            query.orderBy(qAgregacion.nombreCA.asc());
        }
        return query.list(qAgregacion);
    }

    public List<AgregacionServicio> getServicios(String idioma)
    {
        return getServicios(idioma, null);
    }

    public List<AgregacionServicio> getServicios(String idioma, Long personaId)
    {
        QAgregacionServicio qServicio = QAgregacionServicio.agregacionServicio;
        QPersona qPersona = QPersona.persona;
        QServiciosAdministrador qServiciosAdministrador = QServiciosAdministrador.serviciosAdministrador;
        JPAQuery query = new JPAQuery(entityManager);
        if (ParamUtils.isNotNull(personaId))
        {
            query.from(qServicio)
                    .join(qServicio.serviciosAdministrador, qServiciosAdministrador)
                    .join(qServiciosAdministrador.persona,qPersona)
                    .where(qPersona.personaId.eq(personaId));
        }
        else
        {
            query.from(qServicio);
        }
        if (idioma.equals("ES"))
        {
            query.orderBy(qServicio.nombreES.asc());
        }
        else if (idioma.equals("EN"))
        {
            query.orderBy(qServicio.nombreEN.asc());
        }
        else
        {
            query.orderBy(qServicio.nombreCA.asc());
        }
        return query.list(qServicio);
    }

    public List<AgregacionInstituto> getInstitutos(String idioma)
    {
        QAgregacionInstituto qInstituto = QAgregacionInstituto.agregacionInstituto;
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qInstituto);
        if (idioma.equals("ES"))
        {
            query.orderBy(qInstituto.nombreES.asc());
        }
        else if (idioma.equals("EN"))
        {
            query.orderBy(qInstituto.nombreEN.asc());
        }
        else
        {
            query.orderBy(qInstituto.nombreCA.asc());
        }
        return query.list(qInstituto);
    }
}
