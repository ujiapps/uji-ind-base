package es.uji.apps.ind.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.ind.model.Agrupacion;
import es.uji.apps.ind.model.QAgrupacion;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;

@Repository
public class AgrupacionDAO extends BaseDAODatabaseImpl implements LookupDAO<LookupItem>
{
    private QAgrupacion qAgrupacion= QAgrupacion.agrupacion;
    public List<Agrupacion> get()
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAgrupacion).orderBy(qAgrupacion.nombre.asc());
        return query.list(qAgrupacion);
    }

    @Override
    public List<LookupItem> search(String s)
    {
        JPAQuery query = new JPAQuery(entityManager);

        String cadenaLimpia = StringUtils.limpiaAcentos(s);
        Long queryStringLong = -1L;

        try
        {
            queryStringLong = Long.parseLong(s);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        query.from(qAgrupacion);

        if (ParamUtils.isNotNull(s))
        {
            query.where(qAgrupacion.id.eq(queryStringLong)
                    .or(qAgrupacion.nombre.upper().like('%' + cadenaLimpia.toUpperCase() + '%')));
        }

        return query.distinct().list(qAgrupacion).stream().map(ind -> {
            LookupItem item = new LookupItem();
            item.setId(String.valueOf(ind.getId()));
            item.setNombre(ind.getNombre());
            return item;
        }).collect(Collectors.toList());
    }

    @Transactional
    public void toggleActivarAgrupacion(Long id, Boolean activo) {
        new JPAUpdateClause(entityManager, qAgrupacion).where(qAgrupacion.id.eq(id))
                .set(qAgrupacion.activo,activo)
                .execute();
    }

    public List<Agrupacion> getAgrupacionesActivas() {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qAgrupacion).where(qAgrupacion.activo.isTrue()).orderBy(qAgrupacion.nombre.asc());
        return query.list(qAgrupacion);
    }
}