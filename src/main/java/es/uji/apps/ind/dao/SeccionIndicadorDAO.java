package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.ind.model.Indicador;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QSeccionIndicador;
import es.uji.apps.ind.model.SeccionIndicador;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SeccionIndicadorDAO extends BaseDAODatabaseImpl
{
    private QSeccionIndicador qSeccionIndicador = QSeccionIndicador.seccionIndicador;
    private QIndicador qIndicador = QIndicador.indicador;

    public List<SeccionIndicador> getIndicadoresBySeccionId(Long seccionId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSeccionIndicador).join(qSeccionIndicador.indicador,qIndicador).where(qSeccionIndicador.seccion.id.eq(seccionId));
        return query.list(qSeccionIndicador);
    }

    @Transactional
    public void update(Long seccionIndicadorId, Long indicadorId, Long orden)
    {
        JPAUpdateClause updateClause = new JPAUpdateClause(entityManager,qSeccionIndicador);
        Indicador indicador = new Indicador();
        indicador.setId(indicadorId);
        updateClause.where(qSeccionIndicador.id.eq(seccionIndicadorId))
                .set(qSeccionIndicador.indicador,indicador)
                .set(qSeccionIndicador.orden,orden)
                .execute();
    }
}
