package es.uji.apps.ind.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.ind.model.IndicadorMarca;
import es.uji.apps.ind.model.Marca;
import es.uji.apps.ind.model.QIndicadorMarca;
import es.uji.apps.ind.model.QMarca;
import es.uji.apps.ind.model.enums.TipoMarca;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MarcaDAO extends BaseDAODatabaseImpl
{

    private QMarca qMarca = QMarca.marca1;
    private QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;

    public List<Marca> getMarcasByParam(String param)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if(param!=null){
            query.from(qMarca).where(qMarca.marca.like('%' + param + '%'));
            return query.list(qMarca);
        }else{
            return null;
        }

    }
    public List<IndicadorMarca> getIndicadoresMarcaByMarca(String param)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if (param != null) {
            query.from(qIndicadorMarca)
                    .leftJoin(qIndicadorMarca.indicador)
                    .where((qIndicadorMarca.marca.upper().like('%' + param.toUpperCase() + '%')
                            .or(qIndicadorMarca.indicador.codigo.like('%' + param + '%')))
                            .and(qIndicadorMarca.tipo.ne(TipoMarca.CACHE))
                            );
            return query.list(QIndicadorMarca.create(
                    qIndicadorMarca.id,
                    qIndicadorMarca.fecha,
                    qIndicadorMarca.marca,
                    qIndicadorMarca.indicador.id,
                    qIndicadorMarca.indicador.codigo,
                    qIndicadorMarca.indicador.nombre,
                    qIndicadorMarca.indicador.responsable,
                    qIndicadorMarca.indicador.formula,
                    qIndicadorMarca.indicador.valorReferencia
            ));
        }else{
            return null;
        }

    }

    public List<Marca> getMarcasByIndicador(Long marcaId, Long indicadorId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        if(marcaId!=null && indicadorId!=null){
            query.from(qMarca).where(qMarca.marcaId.eq(marcaId).and(qMarca.indicadorId.eq(indicadorId)));
            return query.list(qMarca);
        }else{
            return null;
        }
    }
}
