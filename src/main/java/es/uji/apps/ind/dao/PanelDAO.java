package es.uji.apps.ind.dao;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.Panel;
import es.uji.apps.ind.model.QAgregacionServicio;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QIndicadorMarca;
import es.uji.apps.ind.model.QPanel;
import es.uji.apps.ind.model.QSeccion;
import es.uji.apps.ind.model.QSeccionIndicador;
import es.uji.apps.ind.model.QVariable;
import es.uji.apps.ind.model.QVariableIndicador;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PanelDAO extends BaseDAODatabaseImpl
{
    private QPanel qPanel= QPanel.panel;
    private QSeccion qSeccion = QSeccion.seccion;
    private QSeccionIndicador qSeccionIndicador = QSeccionIndicador.seccionIndicador;
    private QIndicador qIndicador = QIndicador.indicador;
    private QVariableIndicador qVariableIndicador = QVariableIndicador.variableIndicador;
    private QIndicadorMarca qIndicadorMarca = QIndicadorMarca.indicadorMarca;
    private QVariable qVariable = QVariable.variable;
    private QAgregacionServicio qAgregacionServicio = QAgregacionServicio.agregacionServicio;
    public Panel getPanel(Long panelId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qPanel)
                .join(qPanel.secciones,qSeccion).fetch()
                .join(qSeccion.seccionIndicadores,qSeccionIndicador).fetch()
                .join(qSeccionIndicador.indicador,qIndicador).fetch()
                .join(qIndicador.variables, qVariableIndicador).fetch()
                .join(qVariableIndicador.variable,qVariable).fetch()
                .leftJoin(qVariable.servicio, qAgregacionServicio).fetch()
                .leftJoin(qIndicador.marcas, qIndicadorMarca).fetch()
                .where(qPanel.id.eq(panelId));

        query.orderBy(qSeccion.orden.asc(),qSeccionIndicador.orden.asc());

        return query.uniqueResult(qPanel);
    }
}