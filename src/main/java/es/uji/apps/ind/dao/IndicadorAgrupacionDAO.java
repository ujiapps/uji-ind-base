package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.IndicadorAgrupacion;
import es.uji.apps.ind.model.QAgrupacion;
import es.uji.apps.ind.model.QIndicador;
import es.uji.apps.ind.model.QIndicadorAgrupacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Component
public class IndicadorAgrupacionDAO extends BaseDAODatabaseImpl
{
    private QIndicadorAgrupacion indicadorAgrupacion = QIndicadorAgrupacion.indicadorAgrupacion;
    private QAgrupacion agrupacion = QAgrupacion.agrupacion;
    private QIndicador indicador = QIndicador.indicador;

    public List<IndicadorAgrupacion> getByIndicadorId(Long indicadorId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicadorAgrupacion).join(indicadorAgrupacion.agrupacion, agrupacion).fetch()
                .where(indicadorAgrupacion.indicador.id.eq(indicadorId));

        return query.orderBy(agrupacion.nombre.asc()).list(indicadorAgrupacion);
    }

    public List<IndicadorAgrupacion> getByAgrupacionId(Long agrupacionId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(indicadorAgrupacion).join(indicadorAgrupacion.indicador, indicador).fetch()
                .leftJoin(indicador.variables).fetch()
                .where(indicadorAgrupacion.agrupacion.id.eq(agrupacionId));

        return query.orderBy(indicador.nombre.asc()).distinct().list(indicadorAgrupacion);
    }
}
