package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;

import es.uji.apps.ind.model.QSeccion;
import es.uji.apps.ind.model.Seccion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class SeccionDAO extends BaseDAODatabaseImpl
{

    private QSeccion qSeccion = QSeccion.seccion;

    public List<Seccion> getSeccionesByPanelId(Long panelId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        query.from(qSeccion)
                .where(qSeccion.panel.id.eq(panelId));

        return query.list(qSeccion);
    }

    @Transactional
    public void updateSeccion(Seccion seccion)
    {
        new JPAUpdateClause(entityManager, qSeccion).where(qSeccion.id.eq(seccion.getId()))
                .set(qSeccion.nombre, seccion.getNombre())
                .set(qSeccion.orden, seccion.getOrden())
                .execute();

    }
}
