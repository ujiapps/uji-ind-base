package es.uji.apps.ind.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.jpa.impl.JPAUpdateClause;
import com.mysema.query.types.OrderSpecifier;
import com.mysema.query.types.Path;
import es.uji.apps.ind.model.*;
import es.uji.apps.ind.model.submodel.QVariableRevisionModel;
import es.uji.apps.ind.model.submodel.VariableRevisionModel;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.sql.DataSource;


import java.util.*;
import java.util.stream.Collectors;

@Repository
public class VariableRevisionDAO extends BaseDAODatabaseImpl {
    private final QVariableRevision qVariableRevision = QVariableRevision.variableRevision;
    private final QVariableValorRevision qVariableValorRevision = QVariableValorRevision.variableValorRevision;
    private final QVariable qVariable = QVariable.variable;
    private final QServiciosAdministrador qServiciosAdministrador = QServiciosAdministrador.serviciosAdministrador;
    private final QVariableIndicador qVariableIndicador = QVariableIndicador.variableIndicador;
    private final QIndicadorUso qIndicadorUso = QIndicadorUso.indicadorUso;
    private final Map<String, Path> relations = new HashMap<>();
    private final QResponsableVariable qResponsableVariable = QResponsableVariable.responsableVariable;

    public VariableRevisionDAO() {
        relations.put("nombre", qVariableRevision.nombre);
        relations.put("variableId", qVariableValorRevision.variableId);
        relations.put("variableCodigo", qVariable.codigo);
        relations.put("fechaIni", qVariableRevision.fecha);
        relations.put("fechaFin", qVariableRevision.fechaFin);
        relations.put("revisionAnteriorId", qVariableRevision.revisionAnteriorId);
        relations.put("estadoRevision", qVariableRevision.estadoRevision);
        relations.put("observaciones", qVariableRevision.observaciones);
        relations.put("anyo", qVariableValorRevision.anyo);
        relations.put("interanual", qVariableValorRevision.interanual);
        relations.put("mes", qVariableValorRevision.mes);
        relations.put("valor", qVariableValorRevision.valor);
        relations.put("agregacion", qVariableValorRevision.agregacion);
        relations.put("agregacionNombre", qVariableValorRevision.agregacionNombre);
        relations.put("interanualNombre", qVariableValorRevision.interanualNombre);

    }

    public List<VariableRevisionModel> getVariablesRevisionByUser(Long userId, Paginacion paginacion, List<Map<String, String>> filtros) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qVariableRevision)
                .join(qVariable).on(qVariable.id.eq(qVariableRevision.variableId))
                .join(qServiciosAdministrador).on(qServiciosAdministrador.servicio.id.eq(qVariable.servicio.id))
                .where(qServiciosAdministrador.persona.personaId.eq(userId));

        filterCondition(filtros, query);

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0)
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy((OrderSpecifier<?>) path.getClass().getMethod(paginacion.getDireccion().toLowerCase()).invoke(path));
                } catch (Exception e) {
                    query.orderBy(qVariableRevision.fecha.desc());
                }
            } else {
                query.orderBy(qVariableRevision.fecha.desc());
            }
        }
        return query.list(new QVariableRevisionModel(qVariableRevision, qVariable));
    }

    public List<VariableRevisionModel> getVariablesRevision(Paginacion paginacion, List<Map<String, String>> filtros) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qVariableRevision)
                .join(qVariable).on(qVariable.id.eq(qVariableRevision.variableId))
                .leftJoin(qVariableIndicador).on(qVariable.id.eq(qVariableIndicador.variable.id))
                .leftJoin(qIndicadorUso).on(qVariableIndicador.indicador.id.eq(qIndicadorUso.indicador.id));

        filterCondition(filtros, query);
        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }
            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception ignored) {
                }
            } else {
                query.orderBy(qVariableRevision.fecha.desc());
            }
        }
        List<VariableRevisionModel> result = query.list(new QVariableRevisionModel(qVariableRevision, qVariable)).stream().distinct().collect(Collectors.toList());
        paginacion.setTotalCount((long) result.size());

        return result;
    }

    private void filterCondition(List<Map<String, String>> filtros, JPAQuery query) {
        if (filtros != null) {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
            filtros.forEach(item -> {
                String property = item.get("property");
                String value = item.get("value");

                switch (property) {
                    case "usoId":
                        query.where(qIndicadorUso.uso.id.eq(Long.parseLong(value)));
                        break;
                    case "variableId":
                        query.where(qVariableRevision.variableId.eq(Long.parseLong(value)));
                        break;
                    case "fechaIni":
                        DateTime fechaIni = DateTime.parse(value, formatter);
                        query.where(qVariableRevision.fecha.goe(fechaIni.toDate()));
                        break;
                    case "fechaFin":
                        DateTime fechaFin = DateTime.parse(value, formatter);
                        DateTime fechaFin2 = fechaFin.plusDays(1);
                        query.where(qVariableRevision.fechaFin.lt(fechaFin2.toDate()));
                        break;
                    case "estado":
                        if (value.isEmpty()) break;
                        if (value.equals("P")) query.where(qVariableRevision.estadoRevision.isNull());
                        else query.where(qVariableRevision.estadoRevision.eq(value));
                        break;
                }
            });
        }
    }

    public VariableRevision getVariableRevisionById(Long revisionId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qVariableRevision)
                .where(qVariableRevision.id.eq(revisionId));

        return query.singleResult(qVariableRevision);
    }

    public List<VariableValorRevision> getVariableValorRevision(Long revisionId, Paginacion paginacion) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qVariableValorRevision)
                .where(qVariableValorRevision.revisionId.eq(revisionId));

        if (ParamUtils.isNotNull(paginacion)) {
            paginacion.setTotalCount(query.count());
            if (paginacion.getLimit() > 0) {
                query.offset(paginacion.getStart()).limit(paginacion.getLimit());
            }
            Path path = relations.get(paginacion.getOrdenarPor());
            if (path != null) {
                try {
                    query.orderBy(
                            (OrderSpecifier<?>)
                                    path
                                            .getClass()
                                            .getMethod(paginacion.getDireccion().toLowerCase())
                                            .invoke(path));
                } catch (Exception ignored) {
                }
            } else {
                query.orderBy(qVariableValorRevision.anyo.desc());
            }
        }
        paginacion.setTotalCount(query.count());
        return query.list(qVariableValorRevision);
    }

    @Transactional
    public void updateVariablesRevision(Long revisionId, String estadoRevision, String observaciones) {
        new JPAUpdateClause(entityManager, qVariableRevision)
                .set(qVariableRevision.estadoRevision, estadoRevision)
                .set(qVariableRevision.observaciones, observaciones)
                .where(qVariableRevision.id.eq(revisionId)).execute();
    }

    public List<ResponsableVariable> getResponsablesVariable(Long variableId) {
        JPAQuery query = new JPAQuery(entityManager)
                .from(qResponsableVariable)
                .where(qResponsableVariable.variableId.eq(variableId));

        return query.list(qResponsableVariable);
    }

}
