package es.uji.apps.ind.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ind.model.QServiciosAdministrador;
import es.uji.apps.ind.model.ServiciosAdministrador;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.StringUtils;

@Repository
public class ServiciosAdministradoresDAO extends BaseDAODatabaseImpl
{
    private QServiciosAdministrador qServiciosAdministrador = QServiciosAdministrador.serviciosAdministrador;

    public List<ServiciosAdministrador> getServiciosAdministrador(String queryString)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(qServiciosAdministrador);
        if (ParamUtils.isNotNull(queryString))
        {
            query.where(qServiciosAdministrador.persona.nombreBusqueda.contains(StringUtils.limpiaAcentos(queryString).toUpperCase())
                    .or(qServiciosAdministrador.persona.mail.contains(queryString))
                    .or(qServiciosAdministrador.servicio.nombreCA.contains(queryString)));
        }

        return query.list(qServiciosAdministrador);
    }

}
