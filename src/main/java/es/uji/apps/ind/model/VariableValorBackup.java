package es.uji.apps.ind.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IND_VARIABLES_VALORES_BACKUP")
public class VariableValorBackup {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "VARIABLE_ID")
    private Long variableId;
    @Column(name = "AGREGACION")
    private String agregacion;
    @Column(name = "ANYO")
    private Long anyo;
    @Column(name = "VALOR")
    private String valor;
    @Column(name = "VALOR_INTERANUAL")
    private Long valorInteranual;
    @Column(name = "MES")
    private Long mes;

    public VariableValorBackup() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }

    public String getAgregacion() {
        return agregacion;
    }

    public void setAgregacion(String agregacion) {
        this.agregacion = agregacion;
    }

    public Long getAnyo() {
        return anyo;
    }

    public void setAnyo(Long anyo) {
        this.anyo = anyo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Long getValorInteranual() {
        return valorInteranual;
    }

    public void setValorInteranual(Long valorInteranual) {
        this.valorInteranual = valorInteranual;
    }

    public Long getMes() {
        return mes;
    }

    public void setMes(Long mes) {
        this.mes = mes;
    }
}
