package es.uji.apps.ind.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "IND_SECCIONES")
public class Seccion
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    private Long orden;

    @ManyToOne
    @JoinColumn(name = "PANEL_ID")
    private Panel panel;

    @OneToMany(mappedBy = "seccion", cascade = CascadeType.ALL,orphanRemoval = true)
    private Set<SeccionIndicador> seccionIndicadores;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Panel getPanel()
    {
        return panel;
    }

    public void setPanel(Panel panel)
    {
        this.panel = panel;
    }

    public Set<SeccionIndicador> getSeccionIndicadores()
    {
        return seccionIndicadores;
    }

    public void setSeccionIndicadores(Set<SeccionIndicador> seccionIndicadores)
    {
        this.seccionIndicadores = seccionIndicadores;
    }
}