package es.uji.apps.ind.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class VariableValorForCalculus
{
    private Long agregacion;
    private Integer anyo;
    private Long valorInteranual;
    private Map<String, BigDecimal> valores = new HashMap<>();

    public VariableValorForCalculus(Long agregacion, Integer anyo, Long valorInteranual)
    {
        this.agregacion = agregacion;
        this.anyo = anyo;
        this.valorInteranual = valorInteranual;
    }

    public VariableValorForCalculus()
    {
    }

    public Long getAgregacion()
    {
        return agregacion;
    }

    public void setAgregacion(Long agregacion)
    {
        this.agregacion = agregacion;
    }

    public Integer getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Integer anyo)
    {
        this.anyo = anyo;
    }

    public Long getValorInteranual()
    {
        return valorInteranual;
    }

    public void setValorInteranual(Long valorInteranual)
    {
        this.valorInteranual = valorInteranual;
    }

    public Map<String, BigDecimal> getValores()
    {
        return valores;
    }

    public void setValores(Map<String, BigDecimal> valores)
    {
        this.valores = valores;
    }

    public Map<String, BigDecimal> addValue(String key, BigDecimal value)
    {

        BigDecimal existingValue = valores.get(key);
        value = value.add(new BigDecimal("0.001")).setScale(2, RoundingMode.HALF_UP);
        if (!valores.containsKey(key))
        {
            valores.put(key, value);
        }

        if (existingValue != null && !existingValue.equals(value))
        {
            valores.put(key, null);
        }

        return valores;
    }
}
