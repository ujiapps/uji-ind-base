package es.uji.apps.ind.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "IND_USOS")
public class Uso
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name= "ACTIVO")
    private Boolean activo;

    @OneToMany(mappedBy = "uso", cascade = CascadeType.ALL)
    private Set<IndicadorUso> indicadores;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean isActivo() {
        return  activo;
    }

    public Set<IndicadorUso> getIndicadores()
    {
        return indicadores;
    }

    public void setIndicadores(Set<IndicadorUso> usos)
    {
        this.indicadores = usos;
    }
}