package es.uji.apps.ind.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "IND_SERVICIOS_ADMINISTRADORES")
public class ServiciosAdministrador
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name="PER_ID")
    private Persona persona;

    @ManyToOne
    @JoinColumn(name = "SERVICIO_ID")
    private AgregacionServicio servicio;


    public ServiciosAdministrador()
    {
    }
    public ServiciosAdministrador(Long persona, Long servicio)
    {
        this.persona = new Persona(persona);
        this.servicio = new AgregacionServicio(servicio);
    }


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public AgregacionServicio getServicio()
    {
        return servicio;
    }

    public void setServicio(AgregacionServicio servicio)
    {
        this.servicio = servicio;
    }
}
