package es.uji.apps.ind.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConsultaSQL
{
    private Set<String> colInfo;
    private List<Map<String,String>> colData;

    public ConsultaSQL(Set<String> colInfo, List<Map<String, String>> colData)
    {
        this.colInfo = colInfo;
        this.colData = colData;
    }

    public Set<String> getColInfo()
    {
        return colInfo;
    }

    public void setColInfo(Set<String> colInfo)
    {
        this.colInfo = colInfo;
    }

    public List<Map<String, String>> getColData()
    {
        return colData;
    }

    public void setColData(List<Map<String, String>> colData)
    {
        this.colData = colData;
    }
}
