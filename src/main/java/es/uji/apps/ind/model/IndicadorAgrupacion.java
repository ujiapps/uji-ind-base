package es.uji.apps.ind.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "IND_INDICADORES_AGRUPACIONES")
public class IndicadorAgrupacion
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "AGRUPACION_ID")
    private Agrupacion agrupacion;

    @ManyToOne
    @JoinColumn(name = "INDICADOR_ID")
    private Indicador indicador;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Agrupacion getAgrupacion()
    {
        return agrupacion;
    }

    public void setAgrupacion(Agrupacion agrupacion)
    {
        this.agrupacion = agrupacion;
    }

    public Indicador getIndicador()
    {
        return indicador;
    }

    public void setIndicador(Indicador indicador)
    {
        this.indicador = indicador;
    }

}
