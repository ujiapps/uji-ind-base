package es.uji.apps.ind.model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Paginacion {
    private Long start;
    private Long limit;
    private String ordenarPor;
    private String direccion;
    private int totalCount;

    public Paginacion(Long start, Long limit)
    {
        this(start, limit, null, null);
    }

    public Paginacion(Long start, Long limit, String ordenarPor) throws IOException {
        this.start = start;
        this.limit = limit;
        if(ordenarPor.startsWith("[") && ordenarPor.endsWith("]")) {
            if (!ordenarPor.equals("[]")) {
                List<Map<String, String>> sortList = new ObjectMapper().readValue(ordenarPor, new TypeReference<List<Map<String, String>>>(){});
                this.setOrdenarPor(sortList.get(0).get("property"));
                this.setDireccion(sortList.get(0).get("direction"));
            }
        } else {
            this.setOrdenarPor(ordenarPor);
            this.setDireccion("ASC");
        }
    }
    public Paginacion(Long start, Long limit, String ordenarPor, String direccion) {
        this.start = start;
        this.limit = limit;
        this.ordenarPor = ordenarPor;
        this.direccion = direccion;
    }

    public Long getStart()
    {
        return start;
    }

    public void setStart(Long start)
    {
        this.start = start;
    }

    public Long getLimit()
    {
        return limit;
    }

    public void setLimit(Long limit)
    {
        this.limit = limit;
    }

    public String getOrdenarPor()
    {
        return ordenarPor;
    }

    public void setOrdenarPor(String ordenarPor)
    {
        this.ordenarPor = ordenarPor;
    }

    public String getDireccion()
    {
        return direccion;
    }

    public void setDireccion(String direccion)
    {
        this.direccion = direccion;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(Long totalCount)
    {
        this.totalCount = totalCount.intValue();
    }
}