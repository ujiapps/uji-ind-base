package es.uji.apps.ind.model;

import java.util.ArrayList;
import java.util.List;

public class IndicadorPorTipo
{
    private String nivelAgregacion;
    private String componenteTemporal;
    private List<Indicador> indicadores = new ArrayList<>();

    public IndicadorPorTipo(String nivelAgregacion, String componenteTemporal)
    {
        this.nivelAgregacion = nivelAgregacion;
        this.componenteTemporal = componenteTemporal;
    }

    public IndicadorPorTipo()
    {
    }

    public String getNivelAgregacion()
    {
        return nivelAgregacion;
    }

    public void setNivelAgregacion(String nivelAgregacion)
    {
        this.nivelAgregacion = nivelAgregacion;
    }

    public String getComponenteTemporal()
    {
        return componenteTemporal;
    }

    public void setComponenteTemporal(String componenteTemporal)
    {
        this.componenteTemporal = componenteTemporal;
    }

    public List<Indicador> getIndicadores()
    {
        return indicadores;
    }

    public void setIndicadores(List<Indicador> indicadores)
    {
        this.indicadores = indicadores;
    }

    public List<Indicador> addIndicador(Indicador newIndicador)
    {
        Boolean existe = false;

        for (Indicador indicador : indicadores)
        {
            if (indicador.getId() == newIndicador.getId())
            {
                existe = true;
            }
        }

        if (!existe)
        {
            indicadores.add(newIndicador);
        }

        return indicadores;
    }
}
