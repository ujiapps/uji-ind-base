package es.uji.apps.ind.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class VariableCorreoPK implements Serializable {
    @Column(name = "VARIABLE_ID" )
    private Long variableId;

    @Column(name = "CORREO" )
    private String correo;

    public VariableCorreoPK() {
    }

    public VariableCorreoPK(Long variableId, String correo) {
        this.variableId = variableId;
        this.correo = correo;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
}
