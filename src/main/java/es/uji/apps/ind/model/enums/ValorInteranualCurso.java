package es.uji.apps.ind.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.uji.apps.ind.model.ValorInteranual;

public enum ValorInteranualCurso
{
    CURSO(1);

    private long value;

    private ValorInteranualCurso(long value)
    {
        this.value = value;
    }

    private static HashMap<Long, ValorInteranualCurso> codeValueMap = new HashMap<>(7);

    static
    {
        for (ValorInteranualCurso componenteTemporal : ValorInteranualCurso.values())
        {
            codeValueMap.put(componenteTemporal.value, componenteTemporal);
        }
    }

    public static String get(long value)
    {
        return codeValueMap.get(value).name();
    }

    public static List<ValorInteranual> getValues(List<Long> values)
    {
        List<ValorInteranual> results = new ArrayList<>();

        for (ValorInteranualCurso componenteTemporal : ValorInteranualCurso.values())
        {
            if (values.contains(componenteTemporal.value))
            {
                results.add(new ValorInteranual(componenteTemporal.value,
                        componenteTemporal.toString().toLowerCase()));
            }
        }

        return results;
    }
}
