package es.uji.apps.ind.model.enums;

public enum TipoFicheroExcel
{
    XLS, XLSX;
}
