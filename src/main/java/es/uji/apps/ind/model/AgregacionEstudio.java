package es.uji.apps.ind.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IND_EXT_ESTUDIOS")
public class AgregacionEstudio
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    private String tipo;

    @Column(name = "CENTRO_ID")
    private Long centroId;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public String getTipo()
    {
        return tipo;
    }

    public void setTipo(String tipo)
    {
        this.tipo = tipo;
    }

    public Long getCentroId()
    {
        return centroId;
    }

    public void setCentroId(Long centroId)
    {
        this.centroId = centroId;
    }
}
