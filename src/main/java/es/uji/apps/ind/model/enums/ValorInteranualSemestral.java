package es.uji.apps.ind.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.uji.apps.ind.model.ValorInteranual;

public enum ValorInteranualSemestral
{
    PRIMER(1), SEGON(2);

    private long value;

    private ValorInteranualSemestral(long value)
    {
        this.value = value;
    }

    private static HashMap<Long, ValorInteranualSemestral> codeValueMap = new HashMap<>(7);

    static
    {
        for (ValorInteranualSemestral componenteTemporal : ValorInteranualSemestral.values())
        {
            codeValueMap.put(componenteTemporal.value, componenteTemporal);
        }
    }

    public static String get(long value)
    {
        return codeValueMap.get(value).name();
    }

    public static List<ValorInteranual> getValues(List<Long> values)
    {
        List<ValorInteranual> results = new ArrayList<>();

        for (ValorInteranualSemestral componenteTemporal : ValorInteranualSemestral.values())
        {
            if (values.contains(componenteTemporal.value))
            {
                results.add(new ValorInteranual(componenteTemporal.value,
                        componenteTemporal.toString().toLowerCase()));
            }
        }

        return results;
    }
}
