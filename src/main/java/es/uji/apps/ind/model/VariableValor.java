package es.uji.apps.ind.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.uji.apps.ind.model.enums.CodeError;

@Entity
@Table(name = "IND_VARIABLES_VALORES")
public class VariableValor
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "VARIABLE_ID")
    private Variable variable;

    private Long agregacion;
    private Integer anyo;

    @Column(name = "VALOR_INTERANUAL")
    private Long valorInteranual;
    private BigDecimal valor;

    @Transient
    private String expresion;

    @Transient
    private CodeError codeError;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Variable getVariable()
    {
        return variable;
    }

    public void setVariable(Variable variable)
    {
        this.variable = variable;
    }

    public Long getAgregacion()
    {
        return agregacion;
    }

    public void setAgregacion(Long agregacion)
    {
        this.agregacion = agregacion;
    }

    public Integer getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Integer anyo)
    {
        this.anyo = anyo;
    }

    public Long getValorInteranual()
    {
        return valorInteranual;
    }

    public void setValorInteranual(Long valorInteranual)
    {
        this.valorInteranual = valorInteranual;
    }


    public BigDecimal getValor()
    {
        return valor;
    }

    public void setValor(BigDecimal valor)
    {
        this.valor = valor;
    }


    public String getExpresion()
    {
        return expresion;
    }

    public void setExpresion(String expresion)
    {
        this.expresion = expresion;
    }

    public CodeError getCodeError()
    {
        return codeError;
    }

    public void setCodeError(CodeError codeError)
    {
        this.codeError = codeError;
    }
}
