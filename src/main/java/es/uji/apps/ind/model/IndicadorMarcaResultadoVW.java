package es.uji.apps.ind.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.uji.apps.ind.model.enums.CodeError;

@Entity
@Table(name = "IND_VW_INDICADORES_RESULTADOS")
public class IndicadorMarcaResultadoVW
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MARCA_ID")
    private IndicadorMarca marca;

    private Long agregacion;
    private Integer anyo;

    @Column(name = "VALOR_INTERANUAL")
    private Long valorInteranual;
    private BigDecimal valor;

    private String expresion;

    @Enumerated(EnumType.STRING)
    @Column(name = "CODE_ERROR")
    private CodeError codeError;

    @Column(name = "NOMBRE_CA")
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public IndicadorMarca getMarca()
    {
        return marca;
    }

    public void setMarca(IndicadorMarca marca)
    {
        this.marca = marca;
    }

    public Long getAgregacion()
    {
        return agregacion;
    }

    public void setAgregacion(Long agregacion)
    {
        this.agregacion = agregacion;
    }

    public Integer getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Integer anyo)
    {
        this.anyo = anyo;
    }

    public Long getValorInteranual()
    {
        return valorInteranual;
    }

    public void setValorInteranual(Long valorInteranual)
    {
        this.valorInteranual = valorInteranual;
    }

    public BigDecimal getValor()
    {
        BigDecimal valorScaled = (this.valor != null) ? this.valor.setScale(2, RoundingMode.HALF_UP) : this.valor;
        return valorScaled;
    }

    public void setValor(BigDecimal valor)
    {
        BigDecimal valorScaled = (valor != null) ? valor.setScale(2, RoundingMode.HALF_UP) : valor;
        this.valor = valorScaled;
    }

    public String getExpresion()
    {
        return expresion;
    }

    public void setExpresion(String expresion)
    {
        this.expresion = expresion;
    }

    public CodeError getCodeError()
    {
        return codeError;
    }

    public void setCodeError(CodeError codeError)
    {
        this.codeError = codeError;
    }

    public String getNombreCA() {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA) {
        this.nombreCA = nombreCA;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }
}
