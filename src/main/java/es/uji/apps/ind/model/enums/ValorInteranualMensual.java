package es.uji.apps.ind.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.uji.apps.ind.model.ValorInteranual;

public enum ValorInteranualMensual
{
    GENER(1), FEBRER(2), MARÇ(3), ABRIL(4), MAIG(5), JUNY(6), JULIOL(7), AGOST(8), SETEMBRE(9), OCTUBRE(
            10), NOVEMBRE(11), DECEMBRE(12);

    private long value;

    private ValorInteranualMensual(int value)
    {
        this.value = value;
    }

    private static HashMap<Long, ValorInteranualMensual> codeValueMap = new HashMap<>(7);

    static
    {
        for (ValorInteranualMensual componenteTemporal : ValorInteranualMensual.values())
        {
            codeValueMap.put(componenteTemporal.value, componenteTemporal);
        }
    }

    public static String get(long value)
    {
        return codeValueMap.get(value).name();
    }

    public static List<ValorInteranual> getValues(List<Long> values)
    {
        List<ValorInteranual> results = new ArrayList<>();

        for (ValorInteranualMensual componenteTemporal : ValorInteranualMensual.values())
        {
            if (values.contains(componenteTemporal.value))
            {
                results.add(new ValorInteranual(componenteTemporal.value,
                        componenteTemporal.toString().toLowerCase()));
            }
        }

        return results;
    }
}
