package es.uji.apps.ind.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "IND_SECCIONES_INDICADORES")
public class SeccionIndicador implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long orden;

    @ManyToOne
    @JoinColumn(name = "INDICADOR_ID")
    private Indicador indicador;

    @ManyToOne
    @JoinColumn(name = "SECCION_ID")
    private Seccion seccion;



    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getOrden()
    {
        return orden;
    }

    public void setOrden(Long orden)
    {
        this.orden = orden;
    }

    public Indicador getIndicador()
    {
        return indicador;
    }

    public void setIndicador(Indicador indicador)
    {
        this.indicador = indicador;
    }

    public Seccion getSeccion()
    {
        return seccion;
    }

    public void setSeccion(Seccion seccion)
    {
        this.seccion = seccion;
    }
}
