package es.uji.apps.ind.model;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Component
@Entity
@Table(name = "IND_VW_RESPONSABLES_VARIABLES")
public class ResponsableVariable {

    @Id
    @Column(name = "CORREO")
    private String correo;

    @Column(name = "VARIABLE_ID")
    private Long variableId;

    public ResponsableVariable() {
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }
}
