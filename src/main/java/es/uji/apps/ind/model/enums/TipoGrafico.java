package es.uji.apps.ind.model.enums;


public enum TipoGrafico
{
    COLUMNBASIC("COLUMNBASIC"),LINEBASIC("LINEBASIC") ;

    private String value;

    TipoGrafico(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }
}
