package es.uji.apps.ind.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "IND_VARIABLES_FECHAS")
public class VariableFecha {

    @EmbeddedId
    VariableFechaPK id;

    @Column(name = "FECHA_FIN")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;

    public VariableFecha(){}

    public VariableFecha(VariableFechaPK id) {
        this.id = id;
    }

    public VariableFechaPK getId() {
        return id;
    }

    public void setId(VariableFechaPK id) {
        this.id = id;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }
}
