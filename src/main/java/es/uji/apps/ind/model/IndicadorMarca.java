package es.uji.apps.ind.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.ind.model.enums.TipoMarca;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "IND_INDICADORES_MARCAS")
public class IndicadorMarca
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "INDICADOR_ID")
    private Indicador indicador;

    private String marca;

    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Enumerated(EnumType.STRING)
    private TipoMarca tipo;

    @OneToMany(mappedBy = "marca", cascade = CascadeType.ALL)
    private Set<IndicadorMarcaResultado> resultados;

    public IndicadorMarca()
    {
        this.setFecha(new Date());
    }

    @QueryProjection
    public IndicadorMarca(Long id, Date fecha, String marca, Long indicadorId,String codigo, String nombre, String responsable,String formula, String valorReferencia) {
        this.id = id;
        this.fecha= fecha;
        this.marca = marca;
        Indicador indicador = new Indicador();
        indicador.setId(indicadorId);
        indicador.setCodigo(codigo);
        indicador.setNombre(nombre);
        indicador.setResponsable(responsable);
        indicador.setFormula(formula);
        indicador.setValorReferencia(valorReferencia);
        this.indicador=indicador;

    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Indicador getIndicador()
    {
        return indicador;
    }

    public void setIndicador(Indicador indicador)
    {
        this.indicador = indicador;
    }

    public String getMarca()
    {
        return marca;
    }

    public void setMarca(String marca)
    {
        this.marca = marca;
    }

    public Date getFecha()
    {
        return fecha;
    }

    public void setFecha(Date fecha)
    {
        this.fecha = fecha;
    }

    public TipoMarca getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoMarca tipo)
    {
        this.tipo = tipo;
    }

    public Set<IndicadorMarcaResultado> getResultados()
    {
        return resultados;
    }

    public void setResultados(Set<IndicadorMarcaResultado> resultados)
    {
        this.resultados = resultados;
    }
}
