package es.uji.apps.ind.model.enums;

import java.util.HashMap;

public enum ComponenteTemporal
{
    ANY(1), CURS(2), SEMESTRE(3), TRIMESTRE(4), QUADRIMESTRE(5), MENSUAL(6), DIARI(7);

    private Integer value;

    private ComponenteTemporal(Integer value)
    {
        this.value = value;
    }

    private static HashMap<Integer, ComponenteTemporal> codeValueMap = new HashMap<>();

    static
    {
        for (ComponenteTemporal componenteTemporal : ComponenteTemporal.values())
        {
            codeValueMap.put(componenteTemporal.value, componenteTemporal);
        }
    }

    public static String get(Integer value)
    {
        return codeValueMap.get(value).name();
    }

    public Integer getValue()
    {
        return value;
    }

}
