package es.uji.apps.ind.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class VariableFechaPK implements Serializable {

    @Column(name = "VARIABLE_ID" )
    private Long variableId;

    @Column(name = "FECHA_INI")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;


    public VariableFechaPK() {
    }

    public VariableFechaPK(Long variableId, Date fechaInicio) {
        this.variableId = variableId;
        this.fechaInicio = fechaInicio;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

}
