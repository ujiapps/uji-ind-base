package es.uji.apps.ind.model;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "IND_VARIABLES_VALORES_REVISION")
public class VariableValorRevision {
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "VARIABLE_ID")
    private Long variableId;
    @Column(name = "ANYO")
    private Long anyo;
    @Column(name = "VALOR_INTERANUAL")
    private Long interanual;
    @Column(name = "MES")
    private Long mes;
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Column(name = "AGREGACION")
    private Long agregacion;
    @Column(name = "REVISION_ID")
    private Long revisionId;
    @Column(name = "AGREGACION_NOMBRE")
    private String agregacionNombre;
    @Column(name = "VALOR_INTERANUAL_NOMBRE")
    private String interanualNombre;

    public VariableValorRevision() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }

    public Long getAnyo() {
        return anyo;
    }

    public void setAnyo(Long anyo) {
        this.anyo = anyo;
    }

    public Long getInteranual() {
        return interanual;
    }

    public void setInteranual(Long interanual) {
        this.interanual = interanual;
    }

    public Long getMes() {
        return mes;
    }

    public void setMes(Long mes) {
        this.mes = mes;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Long getAgregacion() {
        return agregacion;
    }

    public void setAgregacion(Long agregacion) {
        this.agregacion = agregacion;
    }

    public Long getRevisionId() {
        return revisionId;
    }

    public void setRevisionId(Long revisionId) {
        this.revisionId = revisionId;
    }

    public String getAgregacionNombre() {
        return agregacionNombre;
    }

    public void setAgregacionNombre(String agregacionNombre) {
        this.agregacionNombre = agregacionNombre;
    }

    public String getInteranualNombre() {
        return interanualNombre;
    }

    public void setInteranualNombre(String interanualNombre) {
        this.interanualNombre = interanualNombre;
    }
}
