package es.uji.apps.ind.model.submodel;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.ind.model.Variable;
import es.uji.apps.ind.model.VariableRevision;

import java.util.Date;
import java.util.Objects;

public class VariableRevisionModel {
    private Long id;
    private String nombre;
    private Long variableId;
    private  String variableCodigo;
    private Date fechaIni;
    private Date fechaFin;
    private Long revisionAnteriorId;
    private String estadoRevision;
    private String observaciones;
    private String observacionesValidacion;

    public VariableRevisionModel() {
    }

    @QueryProjection
    public VariableRevisionModel(VariableRevision variableRevision, Variable variable) {
        this.id = variableRevision.getId();
        this.nombre = variableRevision.getNombre();
        this.variableId = variableRevision.getVariableId();
        this.variableCodigo = variable.getCodigo() + " - " + variable.getNombre();
        this.fechaIni = variableRevision.getFecha();
        this.fechaFin = variableRevision.getFechaFin();
        this.revisionAnteriorId = variableRevision.getRevisionAnteriorId();
        this.estadoRevision = variableRevision.getEstadoRevision();
        this.observaciones = variableRevision.getObservaciones();
        this.observacionesValidacion = variable.getObservacionesValidacion();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }

    public Date getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getRevisionAnteriorId() {
        return revisionAnteriorId;
    }

    public void setRevisionAnteriorId(Long revisionAnteriorId) {
        this.revisionAnteriorId = revisionAnteriorId;
    }

    public String  getEstadoRevision() {
        return estadoRevision;
    }

    public void setEstadoRevision(String estadoRevision) {
        this.estadoRevision = estadoRevision;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getVariableCodigo() {
        return variableCodigo;
    }

    public void setVariableCodigo(String variableCodigo) {
        this.variableCodigo = variableCodigo;
    }

    public String getObservacionesValidacion() {
        return observacionesValidacion;
    }

    public void setObservacionesValidacion(String observacionesValidacion) {
        this.observacionesValidacion = observacionesValidacion;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        VariableRevisionModel that = (VariableRevisionModel) obj;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
