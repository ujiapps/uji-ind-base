package es.uji.apps.ind.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import es.uji.apps.ind.model.ValorInteranual;

public enum ValorInteranualTrimestral
{
    PRIMER(1), SEGON(2), TERCER(3), QUART(4);

    private long value;

    private ValorInteranualTrimestral(long value)
    {
        this.value = value;
    }

    private static HashMap<Long, ValorInteranualTrimestral> codeValueMap = new HashMap<>(7);

    static
    {
        for (ValorInteranualTrimestral componenteTemporal : ValorInteranualTrimestral
                .values())
        {
            codeValueMap.put(componenteTemporal.value, componenteTemporal);
        }
    }

    public static String get(long value)
    {
        return codeValueMap.get(value).name();
    }

    public static List<ValorInteranual> getValues(List<Long> values)
    {
        List<ValorInteranual> results = new ArrayList<>();

        for (ValorInteranualTrimestral componenteTemporal : ValorInteranualTrimestral.values())
        {
            if (values.contains(componenteTemporal.value))
            {
                results.add(new ValorInteranual(componenteTemporal.value,
                        componenteTemporal.toString().toLowerCase()));
            }
        }

        return results;
    }
}
