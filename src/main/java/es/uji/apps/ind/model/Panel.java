package es.uji.apps.ind.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.uji.apps.ind.model.enums.TipoPanel;

@Entity
@Table(name = "IND_PANELES")
public class Panel
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;

    @Column(name = "CREADOR_ID")
    private Long creadorId;

    @Enumerated(EnumType.STRING)
    private TipoPanel tipo;


    @OneToMany(mappedBy = "panel", cascade = CascadeType.ALL,orphanRemoval = true)
    private Set<Seccion> secciones;


    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Long getCreadorId()
    {
        return creadorId;
    }

    public void setCreadorId(Long creadorId)
    {
        this.creadorId = creadorId;
    }

    public TipoPanel getTipo()
    {
        return tipo;
    }

    public void setTipo(TipoPanel tipo)
    {
        this.tipo = tipo;
    }

    public Set<Seccion> getSecciones()
    {
        return secciones;
    }

    public void setSecciones(Set<Seccion> secciones)
    {
        this.secciones = secciones;
    }
}