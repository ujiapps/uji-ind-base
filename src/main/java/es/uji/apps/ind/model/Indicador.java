package es.uji.apps.ind.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.ind.exceptions.ErrorEvaluacionFormulaException;
import es.uji.apps.ind.exceptions.FormulaException;
import es.uji.apps.ind.exceptions.VariableException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.formula.*;
import es.uji.apps.ind.model.enums.CodeError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
@Entity
@Table(name = "IND_INDICADORES")
public class Indicador implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nombre;
    @Column(name = "NOMBRE_ES")
    private String nombreES;
    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    private String codigo;

    @Lob
    private String denominacion;

    @Lob
    @Column(name="DENOMINACION_ES")
    private String denominacionES;
    @Lob
    @Column(name="DENOMINACION_EN")
    private String denominacionEN;
    private String responsable;
    private String formula;
    @Lob
    private String observaciones;

    @Lob
    @Column(name = "OBSERVACIONES_ES")
    private String observacionesES;
    @Lob
    @Column(name = "OBSERVACIONES_EN")
    private String observacionesEN;

    @Column(name = "CONCEPTO_A_EVALUAR")
    @Lob
    private String conceptoAEvaluar;

    @Column(name = "CONCEPTO_A_EVALUAR_ES")
    @Lob
    private String conceptoAEvaluarES;
    @Column(name = "CONCEPTO_A_EVALUAR_EN")
    @Lob
    private String conceptoAEvaluarEN;

    @Column(name = "VALOR_REFERENCIA")
    private String valorReferencia;

    @Column(name = "TIPO_GRAFICO")
    private String tipoGrafico;

    @Column(name= "PUBLICO")
    private Boolean publico;

    @Column(name= "ACTIVO")
    private Boolean activo;

    @ManyToOne
    @JoinColumn(name = "INDICADOR_REFERENCIA_ID")
    private Indicador indicadorReferencia;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorUso> usos;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorAgrupacion> agrupaciones;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<IndicadorMarca> marcas;

    @OneToMany(mappedBy = "indicador", cascade = CascadeType.ALL)
    private Set<VariableIndicador> variables;

    @OneToMany(mappedBy = "indicadorReferencia")
    private Set<Indicador> indicadores;

    @Transient
    private VariableChecker variableChecker;

    @Transient
    private VariableExtractor variableExtractor;

    @Transient
    private VariableReplacer variableReplacer;

    @Transient
    private FormulaEvaluator formulaEvaluator;

    @Transient
    private FormulaChecker formulaChecker;

    @Autowired
    public void setVariableReplacer(VariableReplacer variableReplacer)
    {
        this.variableReplacer = variableReplacer;
    }

    @Autowired
    public void setFormulaEvaluator(FormulaEvaluator formulaEvaluator)
    {
        this.formulaEvaluator = formulaEvaluator;
    }

    @Autowired
    public void setFormulaChecker(FormulaChecker formulaChecker)
    {
        this.formulaChecker = formulaChecker;
    }

    @Autowired
    public void setVariableChecker(VariableChecker variableChecker)
    {
        this.variableChecker = variableChecker;
    }

    @Autowired
    public void setVariableExtractor(VariableExtractor variableExtractor)
    {
        this.variableExtractor = variableExtractor;
    }

    public Indicador()
    {
    }

    @QueryProjection
    public Indicador(Long id, String nombre, String codigo, String responsable, String formula, String valorReferencia, String tipoGrafico, Boolean publico, Boolean activo) {
        this.id = id;
        this.nombre = nombre;
        this.codigo = codigo;
        this.responsable = responsable;
        this.formula = formula;
        this.valorReferencia = valorReferencia;
        this.tipoGrafico = tipoGrafico;
        this.publico = publico;
        this.activo= activo;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getDenominacion()
    {
        return denominacion;
    }

    public void setDenominacion(String denominacion)
    {
        this.denominacion = denominacion;
    }

    public String getResponsable()
    {
        return responsable;
    }

    public void setResponsable(String responsable)
    {
        this.responsable = responsable;
    }

    public String getConceptoAEvaluar()
    {
        return conceptoAEvaluar;
    }

    public void setConceptoAEvaluar(String conceptoAEvaluar)
    {
        this.conceptoAEvaluar = conceptoAEvaluar;
    }

    public String getFormula()
    {
        return formula;
    }

    public void setFormula(String formula)
    {
        this.formula = formula;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public Set<IndicadorUso> getUsos()
    {
        return usos;
    }

    public void setUsos(Set<IndicadorUso> usos)
    {
        this.usos = usos;
    }

    public Set<IndicadorAgrupacion> getAgrupaciones()
    {
        return agrupaciones;
    }

    public void setAgrupaciones(Set<IndicadorAgrupacion> agrupaciones)
    {
        this.agrupaciones = agrupaciones;
    }

    public Set<IndicadorMarca> getMarcas()
    {
        return marcas;
    }

    public void setMarcas(Set<IndicadorMarca> marcas)
    {
        this.marcas = marcas;
    }

    public Set<VariableIndicador> getVariables()
    {
        return variables;
    }

    public void setVariables(Set<VariableIndicador> variables)
    {
        this.variables = variables;
    }

    public String getValorReferencia()
    {
        return valorReferencia;
    }

    public String getTipoGrafico()
    {
        return tipoGrafico;
    }

    public void setTipoGrafico(String tipoGrafico)
    {
        this.tipoGrafico = tipoGrafico;
    }

    public void setValorReferencia(String valorReferencia)
    {
        this.valorReferencia = valorReferencia;
    }

    public Boolean getPublico()
    {
        return publico;
    }

    public void setPublico(Boolean publico)
    {
        this.publico = publico;
    }
    public Boolean isPublico()
    {
        return publico;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean isActivo() {
        return  activo;
    }

    private VariableExtractor getVariableExtractor()
    {
        if (variableExtractor == null)
        {
            variableExtractor = new VariableExtractor();
        }

        return variableExtractor;
    }

    private VariableReplacer getVariableReplacer()
    {
        if (variableReplacer == null)
        {
            variableReplacer = new VariableReplacer();
        }

        return variableReplacer;
    }

    private VariableChecker getVariableChecker()
    {
        if (variableChecker == null)
        {
            variableChecker = new VariableChecker(getVariableExtractor());
        }

        return variableChecker;
    }

    private FormulaEvaluator getFormulaEvaluator()
    {
        if (formulaEvaluator == null)
        {
            formulaEvaluator = new FormulaEvaluator();
        }

        return formulaEvaluator;
    }

    private FormulaChecker getFormulaChecker()
    {
        if (formulaChecker == null)
        {
            formulaChecker = new FormulaChecker(getVariableChecker(), getFormulaEvaluator(),
                    getVariableExtractor(), getVariableReplacer());
        }

        return formulaChecker;
    }

    public List<VariableValor> calcula(List<VariableValorForCalculus> valoresForCalculus) throws VariablesSinValorException
    {
        List<VariableValor> resultados = new ArrayList<>();

        for (VariableValorForCalculus valorForCalculus : valoresForCalculus)
        {
            VariableValor valor = new VariableValor();

            valor.setAnyo(valorForCalculus.getAnyo());
            valor.setValorInteranual(valorForCalculus.getValorInteranual());
            valor.setAgregacion(valorForCalculus.getAgregacion());
            String valorReemplazado = "";
            try
            {
                valorReemplazado = getVariableReplacer().replace(getFormula(),
                        valorForCalculus.getValores());
                if(valorReemplazado!=null)
                {
                    valor.setExpresion(valorReemplazado);
                    BigDecimal resultadoEvaluacion = getFormulaEvaluator().eval(valorReemplazado);
                    valor.setValor(resultadoEvaluacion);
                    valor.setCodeError(CodeError.OK);
                }
            }
            catch (VariablesSinValorException e)
            {
                valor.setExpresion(e.getMessage());
                valor.setCodeError(CodeError.VARIABLESINVALOR);

            }
            catch (ErrorEvaluacionFormulaException e){
                valor.setExpresion(valorReemplazado);
                valor.setCodeError(CodeError.ERROREXPRESION);
            }

            catch (Exception e)
            {
                valor.setExpresion(valorReemplazado);
                valor.setCodeError(CodeError.ERROREXPRESION);
            }

            resultados.add(valor);
        }

        return resultados;
    }

    public List<String> getVariablesFormula()
    {
        return getVariableExtractor().extract(getFormula());
    }

    public void compruebaFormula(List<Variable> variables) throws FormulaException,
            VariableException
    {
        getFormulaChecker().check(getFormula(), variables);
    }

    public Indicador duplicar(Indicador indicador)
    {
        indicador.setId(null);
        indicador.setVariables(null);
        indicador.setMarcas(null);

        for (IndicadorUso indicadorUso : indicador.getUsos())
        {
            indicadorUso.setId(null);
            indicadorUso.setIndicador(indicador);
        }

        for (IndicadorAgrupacion indicadorAgrupacion : indicador.getAgrupaciones())
        {
            indicadorAgrupacion.setId(null);
            indicadorAgrupacion.setIndicador(indicador);
        }

        return indicador;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Indicador indicador = (Indicador) o;
        return getId().equals(indicador.getId());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getId());
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }

    public String getDenominacionES() {
        return denominacionES;
    }

    public void setDenominacionES(String denominacionES) {
        this.denominacionES = denominacionES;
    }

    public String getDenominacionEN() {
        return denominacionEN;
    }

    public void setDenominacionEN(String denominacionEN) {
        this.denominacionEN = denominacionEN;
    }

    public String getObservacionesES() {
        return observacionesES;
    }

    public void setObservacionesES(String observacionesES) {
        this.observacionesES = observacionesES;
    }

    public String getObservacionesEN() {
        return observacionesEN;
    }

    public void setObservacionesEN(String observacionesEN) {
        this.observacionesEN = observacionesEN;
    }

    public String getConceptoAEvaluarES() {
        return conceptoAEvaluarES;
    }

    public void setConceptoAEvaluarES(String conceptoAEvaluarES) {
        this.conceptoAEvaluarES = conceptoAEvaluarES;
    }

    public String getConceptoAEvaluarEN() {
        return conceptoAEvaluarEN;
    }

    public void setConceptoAEvaluarEN(String conceptoAEvaluarEN) {
        this.conceptoAEvaluarEN = conceptoAEvaluarEN;
    }

    public Indicador getIndicadorReferencia() {
        return indicadorReferencia;
    }

    public void setIndicadorReferencia(Indicador indicadorReferencia) {
        this.indicadorReferencia = indicadorReferencia;
    }

    public Set<Indicador> getIndicadores() {
        return indicadores;
    }

    public void setIndicadores(Set<Indicador> indicadores) {
        this.indicadores = indicadores;
    }
}