package es.uji.apps.ind.model;

import es.uji.commons.db.hibernate.converters.BooleanToSiNoConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "IND_VARIABLES_REVISION")

public class VariableRevision implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "idSeq", sequenceName = "REVISION_VALOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idSeq")
    private Long id;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "VARIABLE_ID")
    private Long variableId;
    @Column(name = "FECHA_INICIO")
    private Date fecha;
    @Column(name = "FECHA_FIN")
    private Date fechaFin;
    @Column(name = "REVISION_ANTERIOR_ID")
    private Long revisionAnteriorId;
    @Column(name = "ESTADO_REVISION")
    private String estadoRevision;
    @Column(name = "OBSERVACIONES")
    private String observaciones;

    public VariableRevision() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getVariableId() {
        return variableId;
    }

    public void setVariableId(Long variableId) {
        this.variableId = variableId;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Long getRevisionAnteriorId() {
        return revisionAnteriorId;
    }

    public void setRevisionAnteriorId(Long revisionAnteriorId) {
        this.revisionAnteriorId = revisionAnteriorId;
    }

    public String  getEstadoRevision() {
        return estadoRevision;
    }

    public void setEstadoRevision(String estadoRevision) {
        this.estadoRevision = estadoRevision;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
}
