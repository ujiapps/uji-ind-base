package es.uji.apps.ind.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.uji.commons.rest.annotations.DataTag;

@Entity
@Table(name = "IND_EXT_SERVICIOS")
public class AgregacionServicio
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE_CA")
    @DataTag
    private String nombreCA;

    @Column(name = "NOMBRE_ES")
    @DataTag
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    @DataTag
    private String nombreEN;

    @OneToMany(mappedBy = "servicio")
    private Set<Variable> variables;

    @OneToMany(mappedBy = "servicio")
    private Set<ServiciosAdministrador> serviciosAdministrador;

    public AgregacionServicio()
    {
    }

    public AgregacionServicio(Long servicio)
    {
        this.id = servicio;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombreCA()
    {
        return nombreCA;
    }

    public void setNombreCA(String nombreCA)
    {
        this.nombreCA = nombreCA;
    }

    public String getNombreES()
    {
        return nombreES;
    }

    public void setNombreES(String nombreES)
    {
        this.nombreES = nombreES;
    }

    public String getNombreEN()
    {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN)
    {
        this.nombreEN = nombreEN;
    }

    public Set<Variable> getVariables()
    {
        return variables;
    }

    public void setVariables(Set<Variable> variables)
    {
        this.variables = variables;
    }

    public Set<ServiciosAdministrador> getServiciosAdministrador()
    {
        return serviciosAdministrador;
    }

    public void setServiciosAdministrador(Set<ServiciosAdministrador> serviciosAdministrador)
    {
        this.serviciosAdministrador = serviciosAdministrador;
    }
}
