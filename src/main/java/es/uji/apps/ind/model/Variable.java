package es.uji.apps.ind.model;

import com.mysema.query.annotations.QueryProjection;
import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;
import es.uji.apps.ind.exceptions.VariableSQLIncorrectoException;
import es.uji.apps.ind.formula.CodigoVariableChecker;
import es.uji.apps.ind.model.enums.ComponenteTemporal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Set;

@Component
@Entity
@Table(name = "IND_VARIABLES")
public class Variable
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String codigo;
    private String nombre;

    @Column(name = "NOMBRE_ES")
    private String nombreES;

    @Column(name = "NOMBRE_EN")
    private String nombreEN;

    private Integer dinamica;
    @Lob
    private String sql;
    @Lob
    private String observaciones;

    @Lob
    @Column(name = "OBSERVACIONES_ES")
    private String observacionesES;

    @Lob
    @Column(name = "OBSERVACIONES_EN")
    private String observacionesEN;


    @Column(name = "NIVEL_AGREGACION")
    private String nivelAgregacion;

    @Column(name = "COMPONENTE_TEMPORAL")
    private Integer componenteTemporal;

    @OneToMany(mappedBy = "variable")
    private Set<VariableIndicador> indicadores;

    @OneToMany(mappedBy = "variable", cascade = CascadeType.ALL)
    private Set<VariableValor> valores;

    @ManyToOne
    @JoinColumn(name = "SERVICIOS_ID")
    private AgregacionServicio servicio;

    @Column(name= "ACTIVA")
    private Boolean activa;

    @Lob
    @Column(name = "OBSERVACIONES_VALIDACION")
    private String observacionesValidacion;

    @Transient
    private CodigoVariableChecker codigoVariableChecker;

    public Variable()
    {
    }
    @QueryProjection
    public Variable(Long id, String codigo, String nombre, Integer dinamica, String nivelAgregacion, Integer componenteTemporal,  AgregacionServicio servicio,Boolean activa) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.dinamica = dinamica;
        this.nivelAgregacion = nivelAgregacion;
        this.componenteTemporal = componenteTemporal;
        this.servicio = servicio;
        this.activa= activa;
    }



    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql) throws VariableSQLIncorrectoException
    {
        if (sql.toUpperCase().contains("ORDER BY"))
        {
            throw new VariableSQLIncorrectoException("La consulta SQL no pot contenir expressions ORDER BY");
        }
        this.sql = sql;
    }

    public Set<VariableIndicador> getIndicadores()
    {
        return indicadores;
    }

    public void setIndicadores(Set<VariableIndicador> indicadores)
    {
        this.indicadores = indicadores;
    }

    public String getObservaciones()
    {
        return observaciones;
    }

    public void setObservaciones(String observaciones)
    {
        this.observaciones = observaciones;
    }

    public AgregacionServicio getServicio()
    {
        return servicio;
    }

    public void setServicio(AgregacionServicio servicio)
    {
        this.servicio = servicio;
    }

    public boolean mismoTipo(Variable variable)
    {
        if (getComponenteTemporal().equals(variable.getComponenteTemporal())
                && getNivelAgregacion().equals(variable.getNivelAgregacion()))
        {
            return true;
        }
        if (((getComponenteTemporal().equals(ComponenteTemporal.ANY.getValue()) || getComponenteTemporal().equals(ComponenteTemporal.CURS.getValue()))
                && (variable.getComponenteTemporal().equals(ComponenteTemporal.ANY.getValue()) || variable.getComponenteTemporal().equals(ComponenteTemporal.CURS.getValue())))
                && getNivelAgregacion().equals(variable.getNivelAgregacion()))
        {
            return true;
        }

        return false;
    }

    public String getNivelAgregacion()
    {
        if (this.nivelAgregacion != null)
        {
            this.nivelAgregacion = this.nivelAgregacion.toUpperCase();
        }

        return nivelAgregacion;
    }

    public void setNivelAgregacion(String nivelAgregacion)
    {
        this.nivelAgregacion = nivelAgregacion;
    }

    public Integer getComponenteTemporal()
    {
        return componenteTemporal;
    }

    public void setComponenteTemporal(Integer componenteTemporal)
    {
        this.componenteTemporal = componenteTemporal;
    }

    public boolean esDinamica()
    {
        return getDinamica() == 1;
    }

    public Integer getDinamica()
    {
        return dinamica;
    }

    public void setDinamica(Integer dinamica)
    {
        this.dinamica = dinamica;
    }

    public Variable duplicarVariable(Variable variable) throws VariableFormatoIncorrectoException
    {
        variable.setId(null);
        variable.setCodigo(variable.getCodigo() + "_nova");

        for (VariableValor variableValor : variable.getValores())
        {
            variableValor.setId(null);
            variableValor.setVariable(variable);
        }

        return variable;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo) throws VariableFormatoIncorrectoException
    {
        getCodigoVariableChecker().check(codigo);

        this.codigo = codigo;
    }

    private CodigoVariableChecker getCodigoVariableChecker()
    {
        if (codigoVariableChecker == null)
        {
            codigoVariableChecker = new CodigoVariableChecker();
        }

        return codigoVariableChecker;
    }
    public Boolean getActiva() {
        return activa;
    }

    public void setActiva(Boolean activa) {
        this.activa = activa;
    }

    public Boolean isActiva() {
        return  activa;
    }


    @Autowired
    public void setCodigoVariableChecker(CodigoVariableChecker codigoVariableChecker)
    {
        this.codigoVariableChecker = codigoVariableChecker;
    }

    public Set<VariableValor> getValores()
    {
        return valores;
    }

    public void setValores(Set<VariableValor> valores)
    {
        this.valores = valores;
    }

    public String getNombreES() {
        return nombreES;
    }

    public void setNombreES(String nombreES) {
        this.nombreES = nombreES;
    }

    public String getNombreEN() {
        return nombreEN;
    }

    public void setNombreEN(String nombreEN) {
        this.nombreEN = nombreEN;
    }

    public String getObservacionesES() {
        return observacionesES;
    }

    public void setObservacionesES(String observacionesES) {
        this.observacionesES = observacionesES;
    }

    public String getObservacionesEN() {
        return observacionesEN;
    }

    public void setObservacionesEN(String observacionesEN) {
        this.observacionesEN = observacionesEN;
    }

    public String getObservacionesValidacion() {
        return observacionesValidacion;
    }

    public void setObservacionesValidacion(String observacionesValidacion) {
        this.observacionesValidacion = observacionesValidacion;
    }
}