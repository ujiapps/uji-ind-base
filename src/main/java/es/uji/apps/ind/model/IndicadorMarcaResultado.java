package es.uji.apps.ind.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import es.uji.apps.ind.model.enums.CodeError;
import es.uji.commons.db.hibernate.converters.BooleanToSiNoConverter;

@Entity
@Table(name = "IND_INDICADORES_RESULTADOS")
public class IndicadorMarcaResultado
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MARCA_ID")
    private IndicadorMarca marca;

    private Long agregacion;
    private Integer anyo;

    @Column(name = "VALOR_INTERANUAL")
    private Long valorInteranual;
    private BigDecimal valor;

    private String expresion;

    @Enumerated(EnumType.STRING)
    @Column(name = "CODE_ERROR")
    private CodeError codeError;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public IndicadorMarca getMarca()
    {
        return marca;
    }

    public void setMarca(IndicadorMarca marca)
    {
        this.marca = marca;
    }

    public Long getAgregacion()
    {
        return agregacion;
    }

    public void setAgregacion(Long agregacion)
    {
        this.agregacion = agregacion;
    }

    public Integer getAnyo()
    {
        return anyo;
    }

    public void setAnyo(Integer anyo)
    {
        this.anyo = anyo;
    }

    public Long getValorInteranual()
    {
        return valorInteranual;
    }

    public void setValorInteranual(Long valorInteranual)
    {
        this.valorInteranual = valorInteranual;
    }

    public BigDecimal getValor()
    {
        return valor;
    }

    public void setValor(BigDecimal valor)
    {
        this.valor = valor;
    }

    public String getExpresion()
    {
        return expresion;
    }

    public void setExpresion(String expresion)
    {
        this.expresion = expresion;
    }

    public CodeError getCodeError()
    {
        return codeError;
    }

    public void setCodeError(CodeError codeError)
    {
        this.codeError = codeError;
    }
}