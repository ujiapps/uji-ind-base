package es.uji.apps.ind.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "IND_EXT_PERSONA")
public class Persona implements Serializable
{
    @Id
    @Column(name = "PER_ID")
    private Long personaId;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    @Column(name = "MAIL")
    private String mail;

    @Column(name = "NOMBRE_BUSQUEDA")
    private String nombreBusqueda;

    @OneToMany(mappedBy = "persona")
    private Set<ServiciosAdministrador> personaAdministradors;

    public Persona()
    {
    }

    public Persona(Long persona)
    {
        this.personaId = persona;
    }


    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getNombreCompleto()
    {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public String getMail()
    {
        return mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String getNombreBusqueda()
    {
        return nombreBusqueda;
    }

    public void setNombreBusqueda(String nombreBusqueda)
    {
        this.nombreBusqueda = nombreBusqueda;
    }

    public Set<ServiciosAdministrador> getPersonaAdministradors()
    {
        return personaAdministradors;
    }

    public void setPersonaAdministradors(Set<ServiciosAdministrador> personaAdministradors)
    {
        this.personaAdministradors = personaAdministradors;
    }
}