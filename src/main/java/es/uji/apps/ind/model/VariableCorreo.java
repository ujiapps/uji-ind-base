package es.uji.apps.ind.model;

import com.mysema.query.annotations.QueryProjection;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IND_VARIABLES_CORREOS")
public class VariableCorreo {

    @EmbeddedId
    VariableCorreoPK id;

    public VariableCorreo() {
    }

    public VariableCorreo(VariableCorreoPK id) {
        this.id = id;
    }

    public VariableCorreo(Long variableId, String correo){
        this.id = new VariableCorreoPK(variableId, correo);
    }

    public VariableCorreoPK getId() {
        return id;
    }

    public void setId(VariableCorreoPK id) {
        this.id = id;
    }
}
