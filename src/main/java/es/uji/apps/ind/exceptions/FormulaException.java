package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class FormulaException extends GeneralINDException
{
    public FormulaException()
    {
        super("S'ha produït un error en l'operació");
    }

    public FormulaException(String message)
    {
        super(message);
    }
}
