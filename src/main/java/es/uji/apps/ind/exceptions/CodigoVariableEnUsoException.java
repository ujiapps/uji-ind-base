package es.uji.apps.ind.exceptions;

import java.util.List;

@SuppressWarnings("serial")
public class CodigoVariableEnUsoException extends VariableException
{
    public CodigoVariableEnUsoException()
    {
        super("El codi de la variable ja s'està utilitzant pels indicadors ");
    }

    public CodigoVariableEnUsoException(String message)
    {
        super(message);
    }

    public CodigoVariableEnUsoException(List<String> indicadores)
    {
        super("El codi de la variable ja s'està utilitzant pels indicadors " + indicadores.toString());
    }
}
