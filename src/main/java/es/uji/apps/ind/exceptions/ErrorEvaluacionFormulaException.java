package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class ErrorEvaluacionFormulaException extends FormulaException
{
    public ErrorEvaluacionFormulaException()
    {
        super("S'ha produït una erra general evaluant la fòrmula");
    }

    public ErrorEvaluacionFormulaException(String message)
    {
        super(message);
    }

}
