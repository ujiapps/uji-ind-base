package es.uji.apps.ind.exceptions;

public class VariableSQLIncorrectoException extends VariableException
{
    public VariableSQLIncorrectoException()
    {
        super("El format del SQL de la variable es incorrecte.");
    }

    public VariableSQLIncorrectoException(String message)
    {
        super(message);
    }

}
