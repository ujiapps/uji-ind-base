package es.uji.apps.ind.exceptions;

public class VariableFechaIncorrectaException extends Exception{
    public VariableFechaIncorrectaException() {
    }

    public VariableFechaIncorrectaException(String message) {
        super(message);
    }

    public VariableFechaIncorrectaException(String message, Throwable cause) {
        super(message, cause);
    }

    public VariableFechaIncorrectaException(Throwable cause) {
        super(cause);
    }
}
