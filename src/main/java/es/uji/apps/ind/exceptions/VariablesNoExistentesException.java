package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class VariablesNoExistentesException extends VariableException
{
    public VariablesNoExistentesException()
    {
        super("Una o més variables de la fòrmula no estan definides");
    }
    
    public VariablesNoExistentesException(String message)
    {
        super(message);
    }
    
}
