package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class VariablesDiferentesTiposException extends VariableException
{
    public VariablesDiferentesTiposException()
    {
        super("Les variables de la fòrmula no coincideixen en component temporal o nivell d'agregació");
    }
    
    public VariablesDiferentesTiposException(String message)
    {
        super(message);
    }
    
}
