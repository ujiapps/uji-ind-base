package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class VariableException extends GeneralINDException
{
    public VariableException()
    {
        super("S'ha produït un error en l'operació");
    }

    public VariableException(String message)
    {
        super(message);
    }
}
