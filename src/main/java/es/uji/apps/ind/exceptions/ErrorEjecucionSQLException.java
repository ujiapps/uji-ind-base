package es.uji.apps.ind.exceptions;


@SuppressWarnings("serial")
public class ErrorEjecucionSQLException extends Exception
{
    private String sql;
    private String message;

    public ErrorEjecucionSQLException()
    {
        this.message = "La consulta SQL ha generat una errada";
        this.sql = "";
    }

    public ErrorEjecucionSQLException(String message, String sql)
    {
        this.message = message;
        this.sql = sql;
    }

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)
    {
        this.sql = sql;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

}
