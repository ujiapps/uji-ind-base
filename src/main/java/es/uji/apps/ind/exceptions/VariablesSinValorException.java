package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class VariablesSinValorException extends VariableException
{
    public VariablesSinValorException()
    {
        super("Una o més variables no tenen valor associat");
    }
    
    public VariablesSinValorException(String message)
    {
        super(message);
    }
    
}
