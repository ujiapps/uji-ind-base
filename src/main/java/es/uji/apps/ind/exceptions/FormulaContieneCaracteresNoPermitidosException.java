package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class FormulaContieneCaracteresNoPermitidosException extends FormulaException
{
    public FormulaContieneCaracteresNoPermitidosException()
    {
        super("La fòrmula conté caracters no permesos");
    }
    
    public FormulaContieneCaracteresNoPermitidosException(String message)
    {
        super(message);
    }
    
}
