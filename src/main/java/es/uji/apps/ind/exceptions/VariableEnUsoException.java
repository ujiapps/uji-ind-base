package es.uji.apps.ind.exceptions;

import java.util.List;

@SuppressWarnings("serial")
public class VariableEnUsoException extends Throwable {

    public VariableEnUsoException()
    {
        super("La variable s'està utilitzant pels indicadors ");
    }

    public VariableEnUsoException(String message)
    {
        super(message);
    }

    public VariableEnUsoException(List<String> indicadores)
    {
        super("La variable s'està utilitzant pels indicadors " + indicadores.toString());
    }
}