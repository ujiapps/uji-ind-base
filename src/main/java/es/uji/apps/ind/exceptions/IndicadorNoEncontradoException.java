package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class IndicadorNoEncontradoException extends GeneralINDException
{
    public IndicadorNoEncontradoException()
    {
        super("Indicador no encontrat");
    }

    public IndicadorNoEncontradoException(String message)
    {
        super(message);
    }
}
