package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class AdministradorServicioException extends GeneralINDException
{
    public AdministradorServicioException()
    {
        super("La persona ja està en uns dels serveis seleccionats");
    }

    public AdministradorServicioException(String message)
    {
        super(message);
    }
}
