package es.uji.apps.ind.exceptions;

@SuppressWarnings("serial")
public class SoloSeAdmitenFicherosExcelException extends GeneralINDException
{
    public SoloSeAdmitenFicherosExcelException()
    {
        super("Només s'admeten fitxers Excel amb extensions .xls o .xlsx");
    }
    
    public SoloSeAdmitenFicherosExcelException(String message)
    {
        super(message);
    }
}
