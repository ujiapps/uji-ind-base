$(document).ready(function () {


});

Grafico = function (indicadorId, tipoGrafico, varX, varY,indicadorNombre) {
    this.indicadorId = indicadorId;
    this.indicadorNombre=indicadorNombre;
    if (tipoGrafico=== "COLUMNBASIC") {
        this._tipoGrafico = 'column';
    } else if (tipoGrafico=== "LINEBASIC") {
        this._tipoGrafico = 'line';
    }else{
        return
    }
    this._varX = varX;
    this.varY = varY;
    var options = {
        chart: {
            renderTo: 'grafico-'+ this.indicadorId,
            type: this._tipoGrafico
        },
        title: {
            text: 'Indicador: ' + this.indicadorNombre + ' (Click per a ampliar)'
        },

        legend: {
            itemStyle: {
                fontSize:'10px !important',
                padding: 2,
                itemMarginTop: 5,
                itemMarginBottom: 5,
            },
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Valor'
            },
        },
        xAxis: {
            allowDecimals: false,
            title: {
                text: 'Any'
            }
        },

        tooltip: {
            valueDecimals: 2
        },


        series: this.varY
    };
    this._chart = new Highcharts.Chart(options);
    $('#grafico-'+this.indicadorId).show();
    $('#chart-container-'+this.indicadorId).unbind('mousedown');
    $('#chart-container-'+this.indicadorId).bind('mousedown', function () {
        var me = $(this);
        if ( me.hasClass( 'modal' ) ) {
            me.removeClass('modal');
            $('.grafico', me).attr('style','height:400px !important');
        } else {
            me.addClass('modal');
            $('.grafico', me).attr('style','height:90% !important');
        }
        $('.grafico', me).highcharts().reflow();
    });

}