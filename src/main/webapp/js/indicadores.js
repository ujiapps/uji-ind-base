$(document).ready(function () {

    $('input[name=mostrarButton]').click(function (event) {
        event.preventDefault();
        var indicadores = $(this).attr('data-value');
        var comparar = $(this).attr('data-comparar');
        if (indicadores) {
            actualizaDatos(indicadores, comparar);
        } else {
            actualizaDatos();
        }

    });

    function actualizaDatos(indicadores, comparar) {
        var data = [];
        var fields = [];
        if (!indicadores) {
            $('table tbody >tr').remove();
            fields = $('li');
        } else {
            fields = $('li[name=filtro' + indicadores + ']');
        }
        $.each(fields, function () {
            var me = $(this);
            var marca = "";
            $(this).find('table tbody >tr').remove();

            var agregaciones = [];
            $('form').find('div[name=agregacion]').find('input:checked').each(function (index) {
                agregaciones.push($(this).val());
            });

            var anyos = [];
            $('form').find('div[name=anyo]').find('input:checked').each(function (index) {
                anyos.push($(this).val());
            });

            var valorInteranual = $(this).find('select[name=valorInteranual] option:selected').val();

            $('.ind').addClass('loading');
            $('#spinner').show();
            $.each($(this).find('input[name=indicador]'), function () {
                var filtro = new Object();
                filtro.id = parseInt($(this).val());
                filtro.marca = [];
                marca = me.find('div[name=marca-' + filtro.id + ']');
                marca.find('input:checked').each(function (index) {
                    filtro.marca.push($(this).val());
                });
                if (!marca) {
                    filtro.marca = -1;
                }
                filtro.agregacion = agregaciones.join(",");
                filtro.anyo = anyos.join(",");
                filtro.valorInteranual = valorInteranual;

                data.push(filtro);
            });
        });
        if (comparar === "true") {
            getGraficoDatosComparar(data);
        } else {
            getGraficoDatos(data);
        }
    };


    function getGraficoDatos(data) {
        let noDatos = $('#nodatos').text();
        $.ajax({
            url: $("#calcularIndicadoresForm").attr('action'),
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({data: data}),
            success: function (res) {
                res.forEach(function (indicador) {
                    Grafico(indicador.indicadorId, indicador.tipoGrafico, indicador.resultados, 'Indicador: ' + indicador.indicadorNombre + ' ' +i18n.get_string('ampliar'));
                    indicador.resultados.forEach(function (item) {
                        var tr = $('<tr>').append(
                            $('<td>').text(item.agregacionNombre),
                            $('<td>').text(item.anyo),
                            $('<td>').text(item.valorInteranualtext),
                            (item.valor || item.valor === 0) ? $('<td>').text(parseFloat(item.valor).toFixed(2)) : $('<td class="alert label">').text(noDatos),
                            $('<td>').text(item.expresion)
                        );
                        $('#tabla-listado-' + indicador.indicadorId + '  > tbody').append(tr);
                        $('#tabla-listado-' + indicador.indicadorId).removeClass('hide');
                    });
                    $('#tabla-legend-' + indicador.indicadorId).removeClass('hide');
                    $('.descargarBoton').removeClass('hide');
                });
                $('.ind').removeClass('loading');
                $('#spinner').hide();
            },
            error: function () {
                $('.ind').removeClass('loading');
                $('#spinner').hide();
                alert("Error al obtindre resultats.");
            }
        });
    };

    function getGraficoDatosComparar(data) {
        let noDatos = $('#nodatos').text();
        $.ajax({
            url: $("#calcularIndicadoresForm").attr('action'),
            type: 'POST',
            contentType: "application/json",
            data: JSON.stringify({data: data}),
            success: function (res) {
                Grafico("comparar", "COMPARACION", res, "Comparacion");
                $.each(res, function (i, indicador) {
                    $.each(indicador.resultados, function (i, item) {
                        var tr = $('<tr>').append(
                            $('<td>').text(item.agregacionNombre),
                            $('<td>').text(item.anyo),
                            $('<td>').text(item.valorInteranualtext),
                            (item.valor|| item.valor===0) ? $('<td>').text(parseFloat(item.valor).toFixed(2)) : $('<td class="alert label">').text(noDatos),
                            $('<td>').text(item.expresion)
                        );
                        $('#tabla-listado-' + indicador.indicadorId + '  > tbody').append(tr);
                        $('#tabla-listado-' + indicador.indicadorId).removeClass('hide');

                    });
                    $('#tabla-legend-' + indicador.indicadorId).removeClass('hide');
                    $('.descargarBoton').removeClass('hide');
                    $('.indicadorRow').removeClass('hide');

                });
                $('.ind').removeClass('loading');
                $('#spinner').hide();
            },
            error: function () {
                $('.ind').removeClass('loading');
                $('#spinner').hide();
                alert("Error al obtindre resultats.");
            }
        });
    };


});


