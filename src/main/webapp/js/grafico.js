Grafico = (function (renderTo, tipoGrafico, datos, graficoNombre, modoGraficos) {
    _renderTo = renderTo;
    _graficoNombre = graficoNombre;
    _modo = 'NORMAL';
    if (tipoGrafico === "COLUMNBASIC") {
        _tipoGrafico = 'column';
    } else if (tipoGrafico === "LINEBASIC") {
        _tipoGrafico = 'line';
    } else if (tipoGrafico === "COMPARACION") {
        _tipoGrafico = 'line';
        _modo = 'COMPARACION';
    } else {
        return
    }
    _varY = [];
    _varX = [];
/*
    $('#chart-container-' + renderTo).unbind('mousedown');
    $('#chart-container-' + renderTo).bind('mousedown', function () {
        var me = $(this);
        if (me.hasClass('modal')) {
            me.removeClass('modal');
            $('.grafico', me).attr('style', 'height:400px !important');
        } else {
            me.addClass('modal');
            $('.grafico', me).attr('style', 'height:90% !important');
        }
        $('.grafico', me).highcharts().reflow();
    });
*/
    parseaDatos = function (resultados) {
        estudio = [];

        resultados.forEach(function (resultado) {
            _varX.push(resultado.anyo);
            if (estudio[resultado.agregacionNombre] === undefined) {
                estudio[resultado.agregacionNombre] = [];
            }
            if (resultado.valor || resultado.valor === 0) {
                estudio[resultado.agregacionNombre].push({
                    x: parseFloat(resultado.anyo),
                    y: parseFloat(resultado.valor),
                    inter: resultado.valorInteranualtext
                });
            }

        });
        for (let [agregacionNombre, datos] of Object.entries(estudio)) {
            _varY.push({name: agregacionNombre, data: datos});
        }
    };

    parseaDatosComparacion = function (indicadores) {
        let nombresIndicadores= [];
        indicadores.forEach(function (indicador) {
            estudio = [];
            indicador.resultados.forEach(function (resultado) {
                _varX.push(resultado.anyo);
                if (estudio[resultado.agregacionNombre] === undefined) {
                    estudio[resultado.agregacionNombre] = [];
                }
                if (resultado.valor || resultado.valor === 0) {
                    estudio[resultado.agregacionNombre].push({
                        x: parseFloat(resultado.anyo),
                        y: parseFloat(resultado.valor),
                        inter: resultado.valorInteranualtext
                    });
                }

            });
            nombresIndicadores.push(indicador.indicadorNombre);
            for (let [agregacionNombre, datos] of Object.entries(estudio)) {
                _varY.push({name: indicador.indicadorNombre, data: datos});
            }
        });
        _graficoNombre = nombresIndicadores.join(" - ").concat("<br /> (Click per a ampliar)");

    };

    renderGrafico = function () {
        options = {
            lang: {
                downloadCSV:"Desa en format CSV",
                downloadPNG:"Desa en formpat PNG",
                downloadJPEG:"Desa en format JPG",
                downloadPDF:"Desa en format PDF",
                downloadSVG:"Desa en format SVG",
                viewFullscreen:"Vore en pantalla completa",
                printChart: "Imprimir"
            },
            exporting: {
                chartOptions: { // specific options for the exported image
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    }
                },
                fallbackToExportServer: false
            },
            chart: {
                renderTo: 'grafico-' + _renderTo,
                type: _tipoGrafico
            },
            title: {
                text: "<br>",
                useHTML:true,
            },
            /*caption: {
                text: i18n.get_string('ampliar'),
                align: 'right'
            },*/
            legend: {
                itemStyle: {
                    fontSize: '10px !important',
                    padding: 2,
                    itemMarginTop: 5,
                    itemMarginBottom: 5,
                },
            },
            yAxis: {
                min: 0,
                title: {
                    text: i18n.get_string('valor')
                },
            },
            xAxis: {
                allowDecimals: false,
                title: {
                    text: i18n.get_string('any')
                }
            },

            tooltip: {
                valueDecimals: 2
            },
            credits: {
                enabled: false
            },
            series: _varY
        };
        chart = new Highcharts.Chart(options);
        $('#grafico-' + renderTo).show();
    }
    if (_modo === 'COMPARACION') {
        parseaDatosComparacion(datos);
    } else {
        parseaDatos(datos);
    }
    if (_varX.length > 0) {
        renderGrafico();
    }

});
