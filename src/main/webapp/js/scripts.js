$(document).ready(function () {

    $("#calcularForm").submit(function (event) {
        event.preventDefault();
        $('#spinner').show();
        var noDatos = $('#nodatos').text();
        $('#tabla-listado tbody >tr').remove();
        $.ajax({
            url: $("#calcularForm").attr('action'),
            type: 'POST',
            dataType: 'json',
            data: $('#calcularForm').serialize(),
            success: function (data) {
                $('#tabla-listado tbody >tr').remove();
                data.forEach(function (indicador) {

                    Grafico(indicador.indicadorId, indicador.tipoGrafico, indicador.resultados, 'Indicador: ' + indicador.indicadorNombre + ' ' + i18n.get_string('ampliar'));
                    indicador.resultados.forEach(function (item) {
                        if (item.id) {
                            var tr = $('<tr>').append(
                                $('<td>').text(item.agregacionNombre),
                                $('<td>').text(item.anyo),
                                $('<td>').text(item.valorInteranualtext),
                                (item.valor || item.valor === 0) ? $('<td>').text(parseFloat(item.valor).toFixed(2)) : $('<td class="alert label">').text(noDatos),
                                $('<td>').text(item.expresion)
                            );
                            $('#tabla-listado  > tbody').append(tr);
                            $('#tabla-listado').removeClass('hide');
                            $('#tabla-listado').css('width', '100%');
                        }
                    });

                    $('#tabla-legend-' + indicador.indicadorId).removeClass('hide');
                    $('.descargarBoton').removeClass('hide');
                    $('.bloque-descargar').removeClass('hide');
                });
                $('#spinner').hide();
            },
            error: function () {
                $('#spinner').hide();
                alert("Error al obtindre resultats.");
            }
        });
    });
    $('.marcas').find('.expand').click(function () {
        $(this).parent('.marcas').find('.listado').toggle();
    });

    $(".descargarBoton").click(function () {
        // var agregacion = $('form').find('select[name=agregacion] option:selected').val();
        var valorInteranual = $('form').find('select[name=valorInteranual] option:selected').val();
        var filtro = {};

        var anyos = [];
        $('form').find('div[name=anyo]').find('input:checked').each(function (index) {
            anyos.push($(this).val());
        });
        if (anyos.length > 0) {
            filtro.anyo = anyos.join('&anyo=');
        }

        filtro.id = ($(this).data('indicador')) ? parseInt($(this).data('indicador')) : null;
        var agregaciones = [];
        $('form').find('div[name=agregacion]').find('input:checked').each(function (index) {
            agregaciones.push($(this).val());
        });
        if (agregaciones.length > 0) {
            filtro.agregacion = agregaciones.join('&agregacion=');
        }

        let indicadores = $('[data-indicador]').map(function () {
            return $(this).data('indicador');
        }).get();
        filtro.indicador = indicadores.join('&indicador=');

        filtro.valorInteranual = valorInteranual != -1 ? valorInteranual : '';
        var marcares = [];
        $('form').find('div[name=marca-' + filtro.id + ']').find('input:checked').each(function (index) {
            marcares.push($(this).val());
        });
        if (marcares.length != 0) {
            filtro.marca = marcares.join('&marca=');
        }

        var s = Object.keys(filtro).map(function (key) {
            return key + '=' + filtro[key];
        }).join('&');
        if (filtro.id != null) {
            var url = '/ind/rest/publicacion/indicador/' + filtro.id + '/calculo/descargar/?' + s;
        } else {
            var url = '/ind/rest/publicacion/indicadores/calculo/descargar/?' + s;
        }
        $(this).attr('href', url);
    });

});