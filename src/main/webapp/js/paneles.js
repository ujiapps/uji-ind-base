const recalcular = function (datos) {
    $('#spinner').show();
    $.ajax({
        url: '/ind/rest/publicacion/indicadores/calculo',
        type: 'POST',
        contentType: "application/json",
        data: JSON.stringify({data: datos}),
        success: function (res) {
            res.forEach(function (indicador) {
                Grafico(indicador.indicadorId, indicador.tipoGrafico, indicador.resultados, 'Indicador: ' + indicador.indicadorNombre + ' ' +i18n.get_string('ampliar'));
            })
            $('.ind').removeClass('loading');
            $('#spinner').hide();
        },
        error: function () {
            $('.ind').removeClass('loading');
            $('#spinner').hide();
            alert("Error al obtindre resultats.");
        }
    });
}
window.onload = function () {
    $("[name=panelFiltroForm]").submit(function (event) {
        event.preventDefault();
        const datos = [];
        const agregacion = $(this).find('select[name=agregacion] option:selected').val();
        const anyo = $(this).find('select[name=anyo] option:selected').val();
        const valorInteranual=$(this).find('select[name=valorInteranual] option:selected').val();
        $(this).find("[name=indicador]").each(function( index, element ){
            datos.push({
                id: $(element).val(),
                agregacion: agregacion,
                anyo: anyo,
                valorInteranual:valorInteranual
            })
        });
        recalcular(datos);
    });
    let indicadores = [];
    $(".grafico").each(function () {
        return indicadores.push({"id": $(this).data('indicadorid'), "agregacion": "-1", "anyo": "-1", "valorInteranual": "-1"});
    });

    recalcular(indicadores);
};