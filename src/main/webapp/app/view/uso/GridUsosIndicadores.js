Ext.define('IND.view.uso.GridUsosIndicadores',
{
    extend: 'Ext.ux.uji.grid.Panel',
    alias: 'widget.gridUsosIndicadores',

    title: 'Indicadors',
    store: 'UsosIndicadores',

    columns: [
        {
            header: 'Id',
            dataIndex: 'id',
            flex: 5
        },
        {
            header: 'Id Uso',
            dataIndex: 'usoId',
            hidden: true,
            flex: 5
        },
        {
            xtype: 'combocolumn',
            header: 'Indicador',
            dataIndex: 'indicadorId',
            flex: 60,
            combo: {
                xtype: 'combobox',
                store: Ext.create('IND.store.Indicadores'),
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                triggerAction: 'all',
                editable: false
            }
        },
        {
            xtype: 'foreigncolumn',
            header: 'Codi',
            dataIndex: 'indicador',
            displayField: 'codigo',
            flex: 10
        },
        {
            xtype: 'foreigncolumn',
            header: 'Nom',
            dataIndex: 'indicador',
            displayField: 'nombre',
            flex: 30
        },
        {    xtype: 'foreigncolumn',
            header: 'Responsable',
            dataIndex: 'indicador',
            displayField: 'responsable',
            flex: 30
        },
        {    xtype: 'foreigncolumn',
            header: 'Fòrmula',
            dataIndex: 'indicador',
            displayField: 'formula',
            flex: 70
        },
        {    xtype: 'foreigncolumn',
            header: 'Valor referència',
            dataIndex: 'indicador',
            displayField: 'valorReferencia',
            flex: 10
        }
    ],

    getSelectedId: function ()
    {
        var selection = this.getSelectionModel().getSelection();
        return selection[0].get("id");
    },

    reloadData: function ()
    {
        this.getStore().load();
    },

    loadData: function (usoId)
    {
        var store = this.getStore();

        store.getProxy().url = '/ind/rest/usos/' + usoId + '/indicadores/';

        this.reloadData();
    },

    clearStore: function ()
    {
        var store = this.getStore();

        store.getProxy().url = '/ind/rest/usos/' + -1 + '/indicadores/';

        store.suspendAutoSync();
        store.removeAll();
        store.resumeAutoSync();
    }
});
