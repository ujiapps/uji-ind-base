Ext.define('IND.view.uso.PanelUsos',
{
    extend : 'Ext.Panel',
    alias : 'widget.panelUsos',

    title : 'Indicadors amb usos',

    requires : [ 'IND.view.uso.GridUsos', 'IND.view.uso.GridUsosIndicadores' ],

    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'gridUsos',
        flex : 1
    },
    {
        xtype : 'gridUsosIndicadores',
        flex : 1
    } ]
});
