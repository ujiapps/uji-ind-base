Ext.define('IND.view.variable.GridVariablesValores',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.gridVariablesValores',

    store : 'VariablesValores',
    title : 'Valors',
    selModel: {
        mode: 'MULTI'
    },

    tbar: [ '-',
        {
            xtype: 'filefield',
            buttonOnly: true,
            buttonText: 'Excel',
            action: 'upload',
            name: 'fileExcel',
            buttonConfig: {
                iconCls: 'table-refresh'
            }
        }, '-',
        {
            xtype: 'button',
            text: 'Mostrar dades',
            action: 'view',
            iconCls: 'database-go'
        } ],

    columns: [
        {
            header: 'Id',
            dataIndex: 'id',
            flex: 5,
            hidden: true
        },
        {
            xtype: 'combocolumn',
            header: 'Agregació',
            dataIndex: 'agregacion',
            flex: 30,
            combo: {
                xtype: 'combobox',
                store: Ext.create('IND.store.Agregaciones'),
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                triggerAction: 'all',
                editable: false
            }
        },
        {
            header: 'Any',
            dataIndex: 'anyo',
            flex: 30,
            editor: {
                xtype: 'numberfield',
                allowBlank: false,
                decimalPrecision: 0,
                step: 1,
                minValue: 0
            }
        },
        {
            xtype: 'combocolumn',
            header: 'Valor Interanual',
            dataIndex: 'valorInteranual',
            flex: 30,
            combo: {
                xtype: 'combobox',
                store: Ext.create('IND.store.ValoresInteranuales'),
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                triggerAction: 'all',
                queryMode: 'local',
                editable: false

            }
        },
        {
            header: 'Valor',
            dataIndex: 'valor',
            flex: 30,
            renderer: Ext.util.Format.numberRenderer('0.00'),
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        }
    ],

    reconfigureColumns: function(tipo){
        var ref = this;
         if(tipo=='DIARI'){
            var col = [
                 {
                     header: 'Id',
                     dataIndex: 'id',
                     flex: 5,
                     hidden: true,
                     editor:{
                       xtype:'textfield'
                     }
                 },
                 {
                     xtype: 'combocolumn',
                     header: 'Agregació',
                     dataIndex: 'agregacion',
                     flex: 30,
                     combo: {
                         xtype: 'combobox',
                         store: Ext.create('IND.store.Agregaciones'),
                         displayField: 'nombre',
                         valueField: 'id',
                         allowBlank: false,
                         triggerAction: 'all',
                         editable: false
                     }
                 },
                 {
                     header: 'Any',
                     dataIndex: 'anyo',
                     flex: 30,
                     editor: {
                         xtype: 'numberfield',
                         allowBlank: false,
                         decimalPrecision: 0,
                         step: 1,
                         minValue: 0
                     }
                 },
                 {
                     header: 'Valor Interanual',
                     dataIndex: 'valorInteranual',
                     flex: 30,
                     xtype: 'datecolumn',
                     format:'d/m/Y H:i:s',
                     editor:{
                         xtype:'datefield',
                         format:'d/m/Y H:i:s',
                         allowBlank: false,
                         editable: true
                     }
                 },
                 {
                     header: 'Valor',
                     dataIndex: 'valor',
                     flex: 30,
                     renderer: Ext.util.Format.numberRenderer('0.00'),
                     editor: {
                         xtype: 'textfield',
                         allowBlank: false
                     }
                 }
             ];
             Ext.suspendLayouts();
             this.reconfigure(Ext.create('IND.store.VariablesValoresFecha'),col);
             Ext.resumeLayouts(true);
         }else{
             var col = [
                 {
                     header: 'Id',
                     dataIndex: 'id',
                     flex: 5,
                     hidden: true,
                     editor:{
                         xtype:'textfield'
                     }
                 },
                 {
                     xtype: 'combocolumn',
                     header: 'Agregació',
                     dataIndex: 'agregacion',
                     flex: 30,
                     combo: {
                         xtype: 'combobox',
                         store: Ext.create('IND.store.Agregaciones'),
                         displayField: 'nombre',
                         valueField: 'id',
                         allowBlank: false,
                         triggerAction: 'all',
                         editable: false
                     }
                 },
                 {
                     header: 'Any',
                     dataIndex: 'anyo',
                     flex: 30,
                     editor: {
                         xtype: 'numberfield',
                         allowBlank: false,
                         decimalPrecision: 0,
                         step: 1,
                         minValue: 0
                     }
                 },
                 {
                     xtype: 'combocolumn',
                     header: 'Valor Interanual',
                     dataIndex: 'valorInteranual',
                     flex: 30,
                     combo: {
                         xtype: 'combobox',
                         store: Ext.create('IND.store.ValoresInteranuales'),
                         displayField: 'nombre',
                         valueField: 'id',
                         allowBlank: false,
                         triggerAction: 'all',
                         queryMode: 'local',
                         editable: false

                     }
                 },
                 {
                     header: 'Valor',
                     dataIndex: 'valor',
                     flex: 30,
                     renderer: Ext.util.Format.numberRenderer('0.00'),
                     editor: {
                         xtype: 'numberfield',
                         allowBlank: false
                     }
                 }
             ];
             Ext.suspendLayouts();
             this.reconfigure(Ext.create('IND.store.VariablesValores'),col);
             Ext.resumeLayouts(true);
         }
    },
    getSelectedId : function()
    {
        var selection = this.getSelectionModel().getSelection();
        return selection[0].get("id");
    },

    reloadData : function()
    {
        this.getStore().load(
        {
            scope : this,
            callback : function(records, operation, success)
            {
                if (!success)
                {
                    this.clearStore();
                }
            }
        });
    },

    loadData : function(variableId, nivelAgregacion, componenteTemporal)
    {
        if (variableId && nivelAgregacion && componenteTemporal)
        {
            var ref = this;
            this.loadComboValorInteranual(componenteTemporal, function()
            {
                ref.setUrl(variableId);
                ref.loadComboAgregacion(nivelAgregacion, ref.reloadData);
            });
        }
    },

    loadComboValorInteranual : function(componenteTemporal, callback)
    {
        if (componenteTemporal.nombre == 'DIARI')
        {
            this.reconfigureColumns(componenteTemporal.nombre);
        } else
        {
            this.reconfigureColumns(componenteTemporal.nombre);

            var store = Ext.create(componenteTemporal.store);
            var combo = this.down('combocolumn[dataIndex=valorInteranual]').getEditor();
            combo.getStore().loadData(store.getRange(), false);
        }
        callback();
    },

    loadComboAgregacion : function(nivelAgregacion, callback)
    {
        this.setUrlComboAgregacion(nivelAgregacion);
        this.down('combocolumn[dataIndex=agregacion]').getEditor().getStore().load(
        {
            callback : callback,
            scope : this
        });
    },

    setUrlComboAgregacion : function(nivelAgregacion)
    {
        this.down('combocolumn[dataIndex=agregacion]').getEditor().getStore().getProxy().url = '/ind/rest/agregaciones/' + nivelAgregacion.servicio;
    },

    setUrl : function(variableId)
    {
        if (variableId)
        {
            this.getStore().getProxy().url = '/ind/rest/variables/' + variableId + '/valores/';
        }
    },

    clearStore : function()
    {
        var store = this.getStore();
        store.suspendAutoSync();
        store.removeAll();
        store.resumeAutoSync();
    },

    disableDefaultButtons : function(action)
    {
        this.down('button[action=add]').setDisabled(action);
        this.down('button[action=remove]').setDisabled(action);
        this.down('filefield').setDisabled(action);
    },

    disableColumns : function(action)
    {
        this.columns.forEach(function(column)
        {
            column.setDisabled(action);
        })
    },

    disableColumnsAndDefaultButtons : function(action)
    {
        this.disableDefaultButtons(action);
        this.columns.forEach(function(column)
        {
            column.setDisabled(action);
        })
    }
});