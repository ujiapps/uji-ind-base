Ext.define('IND.view.variable.WindowVariable',
{
    extend : 'Ext.window.Window',
    alias : 'widget.windowVariables',

    title : 'Gestió de Variables',
    reference : 'windowVariables',
    width : 500,
    height : 620,
    frame : false,
    bodyStyle : 'padding:1em; background-color:white;',
    modal : true,
    closable : false,
    layout : 'fit',

    items : [
    {
        xtype : 'form',
        border : false,
        url : '/ind/rest/variables',
        layout :
        {
            type : 'form',
            align : 'stretch'
        },
        items : [
        {
            xtype : 'hidden',
            name : 'id'
        },
        {
            xtype : 'textfield',
            name : 'codigo',
            fieldLabel : 'Codi',
            allowBlank : false,
            maskRe : /[a-zA-Z0-9_]/
        },
        {
            xtype : 'textfield',
            name : 'nombre',
            fieldLabel : 'Nom',
            allowBlank : false
        },
        {
            xtype : 'combo',
            name : 'nivelAgregacion',
            store : 'NivelesAgregaciones',
            queryMode : 'local',
            displayField : 'nombre',
            editable : false,
            valueField : 'nombre',
            fieldLabel : 'Agregació',
            allowBlank : false
        },
        {
            xtype : 'combo',
            name : 'servicioId',
            store : 'Servicios',
            displayField : 'nombre',
            editable : false,
            valueField : 'id',
            fieldLabel : 'Servei Propietari',
            allowBlank : false
        },
        {
            xtype : 'checkbox',
            name : 'dinamica',
            inputValue : '1',
            uncheckedValue : '0',
            fieldLabel : 'Dinàmica',
            allowBlank : false
        },
        {
            xtype : 'textarea',
            name : 'sql',
            fieldLabel : 'Sql',
            allowBlank : false,
            disabled : true,
            rows : 18
        },
        {
            xtype : 'combo',
            name : 'componenteTemporal',
            store : 'ComponentesTemporales',
            queryMode : 'local',
            displayField : 'nombre',
            editable : false,
            valueField : 'id',
            fieldLabel : 'C. temporal',
            allowBlank : false,
            triggerAction : 'all'
        },
        {
            xtype : 'textarea',
            name : 'observaciones',
            fieldLabel : 'Observacions',
            rows : 6
        } ]
    } ],

    buttons : [
    {
        text : 'Guardar consulta',
        action : 'save'
    },
    {
        text : 'Cancelar',
        action : 'close'
    } ],

    clearForm : function()
    {
        this.down('form').getForm().reset();
    },

    clearAndHide : function()
    {
        this.clearForm();
        this.hide();
    },

    save : function(callback)
    {
        var form = this.down('form').getForm();
        var ref = this;

        if (form.isValid())
        {
            form.submit(
            {
                success : function(form, action)
                {
                    callback();
                }
            });
        }
    },

    load : function(id)
    {
        var form = this.down('form').getForm();

        form.load(
        {
            url : form.url + '/' + id,
            method : 'GET'
        });
    }
});
