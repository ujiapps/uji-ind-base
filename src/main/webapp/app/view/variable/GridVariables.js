Ext.define('IND.view.variable.GridVariables',
{
    extend: 'Ext.ux.uji.grid.Panel',
    alias: 'widget.gridVariables',

    store: 'Variables',
    title: 'Variables',

    allowEdit: false,
    handlerAddButton: null,
    showSearchField: true,

    tbar: [
        {
            xtype: 'button',
            text: 'Duplicar',
            action: 'duplicate',
            iconCls: 'application-double'
        }],

    columns: [
        {
            header: 'Id',
            dataIndex: 'id',
            flex: 5
        },
        {
            header: 'Codi',
            dataIndex: 'codigo',
            flex: 10
        },
        {
            header: 'Nom',
            dataIndex: 'nombre',
            flex: 30
        },
        {
            header: 'Agregació',
            dataIndex: 'nivelAgregacion',
            flex: 15
        },
        {
            header: 'Dinàmica',
            xtype: 'booleancolumn',
            trueText: 'Sí',
            falseText: 'No',
            dataIndex: 'dinamica'
        },
        {
            xtype: 'combocolumn',
            header: 'Component temporal',
            dataIndex: 'componenteTemporal',
            flex: 20,
            combo: {
                xtype: 'combobox',
                store: Ext.create('IND.store.ComponentesTemporales'),
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                triggerAction: 'all',
                editable: false
            }
        },  {
            header : 'Servei Propietari',
            dataIndex : 'servicioId',
            renderer : function(value, metaData, record)
            {
                return record.get("_servicio").nombreCA;
            },
            editable : false,
            allowBlank : false,
            flex : 30
        },
        {
            dataIndex: 'observaciones',
            width: 16,
            headers: false,
            aling: 'center',
            menuDisabled: true,
            renderer: function (value, metaData)
            {
                if (value)
                {
                    metaData.tdCls = 'table-multiple';
                }
            }
        }],

    getSelectedId: function ()
    {
        var selection = this.getSelectionModel().getSelection();

        if (selection.length > 0)
        {
            return selection[0].get("id");
        }
    },

    getSelectedRow: function ()
    {
        var selection = this.getSelectionModel().getSelection();

        if (selection.length > 0)
        {
            return selection[0];
        }
    },

    reloadData: function ()
    {
        this.getStore().load();
    },

    duplicateRecord: function ()
    {
        var id = this.getSelectedId();
        var ref = this;

        if (!id)
        {
            return;
        }

        var url = this.getStore().getProxy().url + '/' + this.getSelectedId() + '/duplicar';

        Ext.Ajax.request(
        {
            url: url,
            method: 'POST',
            success: function ()
            {
                ref.reloadData();
            }
        });
    }
});