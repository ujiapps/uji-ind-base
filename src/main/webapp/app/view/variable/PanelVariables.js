Ext.define('IND.view.variable.PanelVariables',
{
    extend : 'Ext.Panel',
    alias : 'widget.panelVariables',

    title : 'Variables',

    requires : [ 'IND.view.variable.GridVariables', 'IND.view.variable.GridVariablesValores' ],

    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'gridVariables',
        flex : 1
    },
    {
        xtype : 'gridVariablesValores',
        flex : 1
    } ]
});