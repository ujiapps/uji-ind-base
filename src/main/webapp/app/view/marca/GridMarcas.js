Ext.define('IND.view.marca.GridMarcas',
{
    extend: 'Ext.ux.uji.grid.Panel',
    alias: 'widget.gridMarcas',

    title: 'Indicadors - Valors',
    store: 'Marcas',

    allowEdit: false,
    handlerAddButton: null,
    showAddButton : false,
    showRemoveButton : false,
    showReloadButton : false,
    tbar: [ '-',
        {
            xtype: 'button',
            text: 'Descarregar',
            disabled:true,
            action: 'download',
            iconCls: 'arrow-down'
        }],

    columns: [
        {
            header: 'Agregacio',
            dataIndex: 'nombreAgregacion',
            flex: 10
        },
        {
            header: 'Any',
            dataIndex: 'resultadoAnyo',
            width: 45,
        },
        {
            header: 'Interanual',
            dataIndex: 'nombreComponenteTemporal',
            flex: 20
        },
        {
            header: 'Valor',
            dataIndex: 'resultadoValor',
            renderer : function(value,metaData)
            {
                if(value){
                    return Ext.util.Format.number(value, '0.00');
                }
                else if (metaData.record.data.codeError=='VARIABLESINVALOR') {
                    metaData.style = "color:red;";
                    return 'Variables sense valors'
                }else if (metaData.record.data.codeError=='ERROREXPRESION'){
                    metaData.style = "color:red;";
                    return 'Error en avaluar expressió'
                }
            },
            flex: 10
        },
        {
            header: 'Expresion',
            dataIndex: 'expresion',
            flex: 20
        },

    ],

    getSelectedId: function ()
    {
        var selection = this.getSelectionModel().getSelection();
        return selection[0].get("id");
    },

    reloadData: function ()
    {
        this.getStore().load();
    },
    loadData: function (marcaId,indicadorId)
    {
        var store = this.getStore();
        store.getProxy().url = '/ind/rest/marcas/' + marcaId + '/indicador/'+indicadorId;
        this.reloadData();
        this.disableDefaultButtons(false);
    },
    clearStore : function()
    {
        var store = this.getStore();

        store.getProxy().url = '/ind/rest/marcas/' + -1 + '/indicador/-1';

        store.suspendAutoSync();
        store.removeAll();
        store.resumeAutoSync();
        this.disableDefaultButtons(true);
    },

    disableDefaultButtons : function(action)
    {
        this.down('button[action=download]').setDisabled(action);
    },
});
