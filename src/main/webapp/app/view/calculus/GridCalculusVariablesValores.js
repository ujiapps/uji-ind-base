Ext.define('IND.view.calculus.GridCalculusVariablesValores',
{
    extend : 'Ext.grid.Panel',
    alias : 'widget.gridCalculusVariablesValores',

    store : 'CalculusVariablesValores',
    title : 'Valors',

    features : [
    {
        ftype : 'grouping',
        groupHeaderTpl : [ ' {name:this.indicadorNombre}',
        {
            indicadorNombre : function(value)
            {
                var set = this.owner.grid.up("window").getSetByIndicadorId(value);
                var result;

                Ext.each(set.indicadores, function(indicador)
                {
                    if (indicador.id == value)
                    {
                        result = indicador.nombre;
                    }
                });

                return result;
            }
        } ]
    } ],

    initComponent : function()
    {
        this.columns = [
        {
            header : 'indicadorId',
            dataIndex : 'indicadorId',
            flex : 30,
            hidden : true
        },
        {
            header : 'Agregació',
            dataIndex : 'agregacion',
            flex : 30,
            renderer : function(value, record)
            {
                var indicadorId = record.record.data.indicadorId;
                var set = this.up("window").getSetByIndicadorId(indicadorId);
                var comboStore = set.down("combobox[name=agregacion]").getStore();

                return comboStore.getAt(comboStore.find("id", value)).get("nombre");
            }
        },
        {
            header : 'Any',
            dataIndex : 'anyo',
            width:45
        },
        {
            header : 'Valor Interanual',
            dataIndex : 'valorInteranual',
            flex : 20,
            renderer : function(value, record)
            {
                var indicadorId = record.record.data.indicadorId;
                var set = this.up("window").getSetByIndicadorId(indicadorId);
                var comboStore = set.down("combobox[name=valorInteranual]").getStore();
                var val = comboStore.getAt(comboStore.find("id", value));
                if(val){
                    return val.get("nombre");
                }else{
                    var myDate = Ext.Date.parse(value, 'YmdHis');
                    return Ext.Date.format(myDate, 'd/m/Y h:i:s');
                }
            }
        },
        {
            header : 'Valor',
            dataIndex : 'valor',
            renderer : function(value,metaData)
            {
               if(value){
                return Ext.util.Format.number(value, '0.00');
               }
               else if (metaData.record.data.codeError=="VARIABLESINVALOR") {
                   metaData.style = "color:red;";
                   return 'Variables sense valors'
               }else if (metaData.record.data.codeError=="ERROREXPRESION"){
                   metaData.style = "color:red;";
                   return 'Error en avaluar expressió'
               }
            }
            ,
            flex : 30
        },{
                header : 'Expresion',
                dataIndex : 'expresion',
                flex : 40
            } ];

        this.tbar = [ new Ext.form.TextField(
        {
            width : '80%',
            name : 'marca',
            disabled : true,
            allowBlank : false
        }),
        {
            text : 'Desar dades',
            action : 'save-calculated',
            disabled : true
        } ];

        this.callParent();
    },

    load : function(list, callback)
    {
        var store = this.getStore();
        this.clear();
        Ext.Ajax.timeout=120000;

        Ext.Ajax.request(
        {
            url : '/ind/rest/calculus/calculated-data/',
            jsonData :
            {
                data : list
            },
            method : 'POST',
            success : function(r)
            {
                var success= Ext.JSON.decode(r.responseText).success;
                if(success) {
                    var results = Ext.JSON.decode(r.responseText).data;
                    store.loadData(results);
                    callback(true);
                }else{
                    var object = Ext.JSON.decode(r.responseText).data;
                    callback(false,object);
                }
            }
        });
    },

    save : function(list, marca, callback)
    {
        var store = this.getStore();

        Ext.Ajax.request(
        {
            url : '/ind/rest/calculus/save-calculated-data/',
            jsonData :
            {
                data :
                {
                    marca : marca,
                    list : list
                }
            },
            method : 'POST',
            success : function(r)
            {
                callback();
            }
        });
    },

    clear : function()
    {
        this.getStore().removeAll();
    }
});