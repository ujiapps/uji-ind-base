Ext.define('IND.view.indicador.WindowIndicador',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.windowIndicadores',

        title: 'Gestió de indicadors',
        reference: 'windowIndicadores',
        width: 500,
        height: 485,
        frame: false,
        bodyStyle: 'padding:1em; background-color:white;',
        modal: true,
        closable: false,
        layout: 'fit',

        items: [
            {
                xtype: 'form',
                border: false,
                url: '/ind/rest/indicadores',
                layout: {
                    type: 'form',
                    align: 'stretch'
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'id'
                    },
                    {
                        xtype: 'textfield',
                        name: 'codigo',
                        fieldLabel: 'Codi',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'nombre',
                        fieldLabel: 'Nom',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'responsable',
                        fieldLabel: 'Responsable',
                        allowBlank: false
                    },
                    {
                        xtype: 'textfield',
                        name: 'formula',
                        fieldLabel: 'Fòrmula',
                        allowBlank: false,
                        maskRe: /[a-zA-Z0-9_.\*\+\-/\(\) ]/
                    },
                    {
                        xtype: 'textfield',
                        name: 'valorReferencia',
                        fieldLabel: 'Valor referència'
                    },
                    {
                        xtype: 'textarea',
                        name: 'denominacion',
                        fieldLabel: 'Denominació',
                        rows: 3,
                        allowBlank: false
                    },
                    {
                        xtype: 'textarea',
                        name: 'conceptoAEvaluar',
                        fieldLabel: 'Concepte a avaluar',
                        rows: 3,
                        allowBlank: false
                    }, {
                        xtype: 'combo',
                        store: 'TipoGrafico',
                        fieldLabel: 'Tipus gràfic',
                        displayField: 'nombre',
                        valueField: 'id',
                        name: 'tipoGrafico',
                        typeAhead: false,
                        allowBlank: true,
                    },
                    {
                        xtype : 'checkbox',
                        name : 'publico',
                        inputValue: '1',
                        uncheckedValue: '0',
                        labelWidth : 80,
                        fieldLabel : 'Publico'

                    },
                    {
                        xtype: 'textarea',
                        name: 'observaciones',
                        fieldLabel: 'Observacions',
                        rows: 6
                    }]
            }],

        buttons: [
            {
                text: 'Guardar consulta',
                action: 'save'
            },
            {
                text: 'Cancelar',
                action: 'close'
            }],

        clearForm: function () {
            this.down('form').getForm().reset();
        },

        clearAndHide: function () {
            this.clearForm();
            this.hide();
        },

        save: function (callback) {
            var form = this.down('form').getForm();
            var ref = this;

            if (form.isValid()) {
                form.submit(
                    {
                        success: function (form, action) {
                            callback();
                        }
                    });
            }
        },

        load: function (id) {
            var form = this.down('form').getForm();

            form.load(
                {
                    url: form.url + '/' + id,
                    method: 'GET'
                });
        }
    });