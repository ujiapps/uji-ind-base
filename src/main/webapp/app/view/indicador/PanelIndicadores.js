Ext.define('IND.view.indicador.PanelIndicadores',
{
    extend : 'Ext.Panel',
    alias : 'widget.panelIndicadores',

    title : 'Indicadors',

    requires : [ 'IND.view.indicador.GridIndicadores', 'IND.view.indicador.GridIndicadoresUsos', 'IND.view.indicador.GridIndicadoresAgrupaciones' ],

    closable : true,

    layout :
    {
        type : 'vbox',
        align : 'stretch'
    },

    items : [
    {
        xtype : 'gridIndicadores',
        flex : 1
    },
    {
        xtype : 'panel',
        flex: 1,
        layout :
        {
            type : 'hbox',
            align : 'stretch'
        },
        items : [
        {
            xtype : 'gridIndicadoresUsos',
            flex : 1
        },

        {
            xtype : 'gridIndicadoresAgrupaciones',
            flex : 1
        } ]
    } ]
});