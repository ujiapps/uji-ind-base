Ext.define('IND.view.indicador.GridIndicadores',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.gridIndicadores',

    requires: ['IND.store.TipoGrafico'],
    store : 'Indicadores',
    title : 'Indicadors',

    allowEdit : false,
    handlerAddButton : null,
    showSearchField: true,

    tbar : [
    {
        xtype : 'button',
        text : 'Duplicar',
        action : 'duplicate',
        iconCls : 'application-double'
    }, '-',
    {
        xtype : 'button',
        text : 'Calcular',
        action : 'calculate',
        iconCls : 'calculator'
    } ],

    columns : [
    {
        header : 'Id',
        dataIndex : 'id',
        flex : 10
    },
    {
        header : 'Codi',
        dataIndex : 'codigo',
        flex : 10
    },
    {
        header : 'Nom',
        dataIndex : 'nombre',
        flex : 30
    },
    {
        header : 'Responsable',
        dataIndex : 'responsable',
        flex : 30
    },
    {
        header : 'Fòrmula',
        dataIndex : 'formula',
        flex : 70
    },
    {
        header : 'Valor referència',
        dataIndex : 'valorReferencia',
        flex : 10
    }, {
            xtype: 'combocolumn',
            header: 'Tipus Grafic',
            dataIndex: 'tipoGrafico',
            flex: 20,
            combo: {
                xtype: 'combobox',
                store: Ext.create('IND.store.TipoGrafico'),
                displayField: 'nombre',
                valueField: 'id',
                allowBlank: false,
                triggerAction: 'all',
                editable: false
            }
        },{
            text : 'Publico',
            dataIndex : 'publico',
            xtype : 'booleancolumn',
            trueText : 'Sí',
            falseText : 'No',
            flex : 1
        }
        ],

    getSelectedId : function()
    {
        var selection = this.getSelectionModel().getSelection();

        if (selection.length > 0)
        {
            return selection[0].get("id");
        }
    },

    getSelectedRow : function()
    {
        var selection = this.getSelectionModel().getSelection();

        if (selection.length > 0)
        {
            return selection[0];
        }
    },

    reloadData : function()
    {
        this.getStore().load();
    },

    duplicateRecord : function()
    {
        var id = this.getSelectedId();
        var ref = this;

        if (!id)
        {
            return;
        }

        var url = this.getStore().getProxy().url + '/' + this.getSelectedId() + '/duplicar';

        Ext.Ajax.request(
        {
            url : url,
            method : 'POST',
            success : function()
            {
                ref.reloadData();
            }
        });
    }
});