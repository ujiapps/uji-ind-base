Ext.define('IND.view.indicador.GridIndicadoresAgrupaciones',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.gridIndicadoresAgrupaciones',

    title : 'Agrupacions',
    store : 'IndicadoresAgrupaciones',

    columns : [
    {
        header : 'Id',
        dataIndex : 'id',
        flex : 5
    },
    {
        header : 'Id Indicador',
        dataIndex : 'indicadorId',
        hidden : true,
        flex : 5
    },
    {
        xtype : 'combocolumn',
        header : 'Agrupació',
        dataIndex : 'agrupacionId',
        flex : 60,
        combo :
        {
            xtype : 'combobox',
            store : Ext.create('IND.store.Agrupaciones'),
            displayField : 'nombre',
            valueField : 'id',
            allowBlank : false,
            triggerAction : 'all',
            editable : false
        }
    } ],

    getSelectedId : function()
    {
        var selection = this.getSelectionModel().getSelection();
        return selection[0].get("id");
    },

    reloadData : function()
    {
        this.getStore().load();
    },

    loadData : function(indicadorId)
    {
        var store = this.getStore();

        store.getProxy().url = '/ind/rest/indicadores/' + indicadorId + '/agrupaciones/';

        this.reloadData();
    },

    clearStore : function()
    {
        var store = this.getStore();

        store.getProxy().url = '/ind/rest/indicadores/' + -1 + '/agrupaciones/';

        store.suspendAutoSync();
        store.removeAll();
        store.resumeAutoSync();
    }
});
