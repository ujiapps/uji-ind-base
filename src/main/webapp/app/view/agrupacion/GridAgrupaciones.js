Ext.define('IND.view.agrupacion.GridAgrupaciones',
{
    extend : 'Ext.ux.uji.grid.Panel',
    alias : 'widget.gridAgrupaciones',

    store : 'Agrupaciones',

    title : 'Agrupaciones',
    closable : true,

    tbar : [ '-',
    {
        xtype : 'button',
        text : 'Calcular',
        action : 'calculate',
        iconCls : 'calculator'
    } ],

    columns : [
    {
        header : 'Codi',
        dataIndex : 'id',
        flex : 5
    },
    {
        header : 'Nom',
        dataIndex : 'nombre',
        flex : 60,
        editor :
        {
            xtype : 'textfield',
            allowBlank : false
        }
    } ],

    getSelectedId : function()
    {
        var selection = this.getSelectionModel().getSelection();
        if (selection.length > 0)
        {
            return selection[0].get("id");
        }
    },

    getSelectedRow : function()
    {
        var selection = this.getSelectionModel().getSelection();

        if (selection.length > 0)
        {
            return selection[0];
        }
    },

    reloadData : function()
    {
        this.getStore().load();
    }
});
