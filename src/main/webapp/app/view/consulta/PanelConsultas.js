Ext.define('IND.view.consulta.PanelConsultas',
    {
        extend: 'Ext.Panel',
        alias: 'widget.panelConsultas',

        title: 'Consultas',
        requires : [ 'IND.view.consulta.FormConsultas', 'IND.view.consulta.GridConsultasValores' ],


        closable: true,

        layout: 'border',

        items: [
            {
                xtype: 'formConsultas',
                region: 'center',
                flex: 1
            },
            {
                xtype: 'gridConsultasValores',
                flex: 1,
                region: 'south'
            }
        ]
    });