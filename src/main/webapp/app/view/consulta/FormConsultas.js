Ext.define('IND.view.consulta.FormConsultas',
{
    extend : 'Ext.form.Panel',
    alias : 'widget.formConsultas',

    title : 'Consulta SQL',
    autoWidth: true,
    layout   : 'fit',
    border : false,
    bodyPadding: '5px 5px',
    url : '/ind/rest/consultas',
    items: [ {
        xtype: 'textarea',
        name:'sql',
        emptyText:'SQL',
        anchor:'100%'
    }],
    buttons : [
        {
            text : 'Executar consulta',
            action : 'exec'
        }
       ],

    clearForm : function()
    {
        this.down('form').getForm().reset();
    }

});