Ext.define('IND.view.consulta.GridConsultasValores',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.gridConsultasValores',

        store: 'Consultas',
        title: 'Valors',
        columns: [
            {
                header: 'Valor',
                dataIndex: 'valor',
                flex: 30,
            }
        ],
        tbar: ['-',
            {
                xtype: 'button',
                text: 'Download',
                action: 'download',
                disabled: true,
                iconCls: 'arrow-down',
            }],
        initComponent: function () {
            this.callParent(arguments);
        },

        getSelectedId: function () {
            var selection = this.getSelectionModel().getSelection();
            return selection[0].get("id");
        },

        reconfigureColumns: function (resultado) {
            var ref = this;
            var cols = [];
            Ext.each(resultado.colInfo, function (nombre) {

                var col =
                {
                    header: nombre,
                    dataIndex: nombre,
                    flex: 1,
                    hidden: false,
                };
                cols.push(col);
            });
            Ext.define('resultadoSqlModel', {
                extend: 'Ext.data.Model',
                fields: resultado.colInfo
            });
            var store = Ext.create('Ext.data.Store', {
                model: 'resultadoSqlModel',
                data: resultado.colData
            });
            Ext.suspendLayouts();
            this.reconfigure(store, cols);
            Ext.resumeLayouts(true);
        },

        disableButtons : function(action)
        {
            this.down('button[action=download]').setDisabled(action);
        },

        onExportToExcel: function () {

            var me              = this,
                csvContent      = '',
                noCsvSupport     = ( 'download' in document.createElement('a') ) ? false : true,
                sdelimiter      = noCsvSupport ? "<td>"   : "",
                edelimiter      = noCsvSupport ? "</td>"  : ",",
                snewLine        = noCsvSupport ? "<tr>"   : "",
                enewLine        = noCsvSupport ? "</tr>"  : "\r\n",
                printableValue  = '';

            csvContent += snewLine;

            var selectedRecords = this.getStore().data.items;
            Ext.each(Object.keys(selectedRecords[0].data), function (key) {
                csvContent += sdelimiter + key + edelimiter;
            });

            csvContent += enewLine;
            for (var i = 0; i < selectedRecords.length; i++) {
                csvContent += snewLine;

                Ext.each(Object.keys(selectedRecords[i].data), function (key) {
                    printableValue = selectedRecords[i].data[key];
                    printableValue = String(printableValue).replace(/,/g, "");
                    printableValue = String(printableValue).replace(/(\r\n|\n|\r)/gm, "");
                    csvContent += sdelimiter + printableValue + edelimiter;
                });
                csvContent += enewLine;
            }
            if('download' in document.createElement('a')){
                var link = document.createElement("a");
                link.setAttribute("href", "data:text/csv;charset=utf-8," + encodeURI(csvContent));
                link.setAttribute("download", "consultaSQL.csv");
                link.click();
            } else {
                var newWin = open('windowName',"_blank");
                newWin.document.write('<table border=1>' + csvContent + '</table>');
            }
        }
    });