Ext.define('IND.model.VariableValorFecha',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'agregacion',
        type : 'int',
        useNull : true
    },
    {
        name : 'anyo',
        type : 'int',
        useNull : true
    },
    {
        name : 'valorInteranual',
        type: 'date',
        dateFormat: 'YmdHis',
        useNull : true
    }, 'valor' ],

    validations : [
    {
        type : 'length',
        field : 'anyo',
        min : 1
    },
    {
        type : 'length',
        field : 'valorInteranual',
        min : 1
    },
    {
        type : 'length',
        field : 'agregacion',
        min : 1
    },
    {
        type : 'length',
        field : 'valor',
        min : 1
    } ]
});
