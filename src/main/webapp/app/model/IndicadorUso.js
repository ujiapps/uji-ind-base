Ext.define('IND.model.IndicadorUso',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'uso',
        foreign : true,
        useNull : true
    },
    {
        name : 'indicador',
        foreign : true,
        useNull : true
    } ],

    validations : [
    {
        type : 'length',
        field : 'usoId',
        min : 1
    } ]
});
