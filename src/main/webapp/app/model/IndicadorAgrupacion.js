Ext.define('IND.model.IndicadorAgrupacion',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'agrupacion',
        foreign : true,
        useNull : true
    },
    {
        name : 'indicador',
        foreign : true,
        useNull : true
    } ],

    validations : [
    {
        type : 'length',
        field : 'agrupacionId',
        min : 1
    } ]
});
