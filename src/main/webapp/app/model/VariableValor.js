Ext.define('IND.model.VariableValor',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'agregacion',
        type : 'int',
        useNull : true
    },
    {
        name : 'anyo',
        type : 'int',
        useNull : true
    },
    {
        name : 'valorInteranual',
        useNull : true
    }, {name:'valor', type : 'float'} ],

    validations : [
    {
        type : 'length',
        field : 'anyo',
        min : 1
    },
    {
        type : 'length',
        field : 'valorInteranual',
        min : 1
    },
    {
        type : 'length',
        field : 'agregacion',
        min : 1
    },
    {
        type : 'length',
        field : 'valor',
        min : 1
    } ]
});
