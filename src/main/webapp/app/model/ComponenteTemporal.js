Ext.define('IND.model.ComponenteTemporal',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ],

    validations : [
    {
        type : 'length',
        field : 'nombre',
        min : 1
    } ]
});
