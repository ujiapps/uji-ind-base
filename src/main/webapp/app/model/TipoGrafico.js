Ext.define('IND.model.TipoGrafico',
{
    extend : 'Ext.data.Model',

    fields : [ 'id', 'nombre' ],

    validations : [
    {
        type : 'length',
        field : 'nombre',
        min : 1
    } ]
});
