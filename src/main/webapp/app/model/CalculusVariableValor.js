Ext.define('IND.model.CalculusVariableValor',
    {
        extend: 'Ext.data.Model',

        fields: ['indicadorId', 'indicadorNombre', 'agregacion', 'agregacionNombre', 'anyo', 'valorInteranual', 'valor', 'expresion','codeError']
    });
