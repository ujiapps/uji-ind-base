Ext.define('IND.model.AgrupacionIndicador',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'agrupacion',
        foreign : true,
        useNull : true
    },
    {
        name : 'indicador',
        foreign : true,
        useNull : true
    } ],

    validations : [
    {
        type : 'length',
        field : 'indicadorId',
        min : 1
    } ]
});
