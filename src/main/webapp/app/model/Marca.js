Ext.define('IND.model.Marca',
{
    extend : 'Ext.data.Model',
    fields : [
    'indicadorId','indicadorNombre','indicadorCodigo', 'marcaId',
        {
            name: 'marcaFecha',
            type: 'date',
            dateFormat: 'd/m/Y H:i:s'
        }, 'marca', 'resultadoId', 'resultadoAnyo', 'resultadoInteranual',
        'resultadoValor', 'resultadoAgregacion', 'nombreAgregacion','nombreComponenteTemporal','expresion']

});
