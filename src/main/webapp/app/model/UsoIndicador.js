Ext.define('IND.model.UsoIndicador',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    },
    {
        name : 'uso',
        foreign : true,
        useNull : true
    },
    {
        name : 'indicador',
        foreign : true,
        useNull : true
    } ],

    validations : [
    {
        type : 'length',
        field : 'indicadorId',
        min : 1
    } ]
});
