Ext.define('IND.model.Indicador',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    }, 'nombre', 'codigo', 'tipoGrafico','denominacion', 'responsable', 'conceptoAEvaluar', 'formula', 'observaciones', 'valorReferencia', 'nivelAgregacion', 'componenteTemporal', 'publico' ],

    validations : [
    {
        type : 'length',
        field : 'nombre',
        min : 1
    },
    {
        type : 'length',
        field : 'codigo',
        min : 1
    },
    {
        type : 'length',
        field : 'formula',
        min : 1
    },
    {
        type : 'length',
        field : 'denominacion',
        min : 1
    },
    {
        type : 'length',
        field : 'responsable',
        min : 1
    }, ,
    {
        type : 'length',
        field : 'conceptoAEvaludar',
        min : 1
    } ]
});
