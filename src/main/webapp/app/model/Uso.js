Ext.define('IND.model.Uso',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    }, 'nombre' ],

    validations : [
    {
        type : 'length',
        field : 'nombre',
        min : 1
    } ]
});
