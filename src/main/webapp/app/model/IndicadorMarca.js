Ext.define('IND.model.IndicadorMarca',
{
    extend: 'Ext.data.Model',
    fields: [
        {
            name: 'id',
            type: 'int',
            useNull: true
        },
        {
            name: 'fecha',
            type: 'date',
            dateFormat: 'd/m/Y H:i:s'
        },
         'marca',
        {
            name: 'indicador',
            foreign: true,
            useNull: true
        }
    ]

});
