Ext.define('IND.model.Consulta',
{
    extend : 'Ext.data.Model',

    fields : [
    {
        name : 'id',
        type : 'int',
        useNull : true
    }, 'valor' ],

    validations : [
    {
        type : 'length',
        field : 'valor',
        min : 1
    } ]
});
