Ext.define('IND.controller.ControllerAgrupaciones',
{
    extend : 'Ext.app.Controller',

    stores : [ 'Agrupaciones', 'AgrupacionesIndicadores' ],
    model : [ 'Agrupacion', 'AgrupacionIndicador' ],

    refs : [
    {
        selector : 'gridAgrupaciones',
        ref : 'gridAgrupaciones'
    },
    {
        selector : 'gridAgrupacionesIndicadores',
        ref : 'gridAgrupacionesIndicadores'
    } ],

    init : function()
    {
        this.control(
        {
            'gridAgrupaciones' :
            {
                select : this.onSelectRow,
                beforerender : this.clearDetailStores
            },

            'gridAgrupaciones button[action=add]' :
            {
                click : this.clearDetailStores
            },

            'gridAgrupaciones button[action=calculate]' :
            {
                click : this.onShowWindowCalculus
            }
        })

        this.windowCalculus = Ext.create('IND.view.calculus.WindowCalculus');
    },

    onSelectRow : function(ref, record, index, eOpts)
    {
        var agrupacionId = record.data.id;

        if (agrupacionId)
        {
            this.getGridAgrupacionesIndicadores().loadData(agrupacionId);
        }
    },

    clearDetailStores : function()
    {
        this.getGridAgrupacionesIndicadores().clearStore();
    },

    onShowWindowCalculus : function()
    {
        var agrupacionId = this.getGridAgrupaciones().getSelectedId();

        if (agrupacionId)
        {
            this.windowCalculus.getIndicadorAndValuesForCombosFromAgrupacionId(agrupacionId);
            this.windowCalculus.show();
        }
    }
});