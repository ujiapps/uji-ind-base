Ext.define('IND.controller.ControllerWindowCalculus',
{
    extend : 'Ext.app.Controller',

    init : function()
    {
        this.control(
        {
            'windowCalculus button[action=close]' :
            {
                click : this.onWindowCalculusClose
            },

            'windowCalculus button[action=calculate]' :
            {
                click : this.onGetCalculusResult
            },
            'windowCalculus button[action=cache]' :
            {
                click : this.onGetCacheResult
            },

            'windowCalculus button[action=save-calculated]' :
            {
                click : this.onSaveCalculusResult
            },

            'windowCalculus combobox' :
            {
                change : this.onChangeAnyCombobox
            },

            'windowCalculus button[action=return]' :
            {
                click : this.onReturnToFormOnCardLayout
            }
        });
    },

    onWindowCalculusClose : function(button)
    {
        button.up('window').clearAndHide();
    },
    onGetCacheResult : function(button)
    {
        button.up('window').showResults(1);
    },

    onGetCalculusResult : function(button)
    {
        button.up('window').showResults(0);
    },

    onSaveCalculusResult : function(button)
    {
        button.up('window').saveResults()
    },

    onChangeAnyCombobox : function(button)
    {
        button.up('window').disableSaveComponents(true);
    },

    onReturnToFormOnCardLayout : function(button)
    {
        button.up('window').showForm();
    }
});
