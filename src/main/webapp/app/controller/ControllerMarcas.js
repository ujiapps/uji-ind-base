Ext.define('IND.controller.ControllerMarcas',
{
    extend : 'Ext.app.Controller',

    stores : [ 'Marcas','IndicadoresMarcas'  ],
    model : [ 'Marca'  ],

    refs : [
        {
            selector: 'gridIndicadoresMarcas',
            ref: 'gridIndicadoresMarcas'
        },{
            selector: 'gridMarcas',
            ref: 'gridMarcas'
        }

     ],

    init : function()
    {
        this.control(
        {
            'gridIndicadoresMarcas':{
                select : this.onSelectRow
            },
            'gridIndicadoresMarcas button[action=remove]':{
                click : this.removeButton
            },
            'gridMarcas button[action=download]': {
                click: this.descargarCSV
            }
        });

    },
    onSelectRow : function(ref, record, index, eOpts)
    {
        var marcaId = record.data.id;
        var indicadorId = record.data.indicadorId;
        if (marcaId && indicadorId)
        {
            this.getGridMarcas().loadData(marcaId,indicadorId);
        }
    },

    removeButton: function() {
        var me = this;
        var ref = this.getGridIndicadoresMarcas();
        var records = ref.getSelectionModel().getSelection();

        if (records.length > 0) {
            Ext.Msg.confirm('Esborrasdasdar', 'Esteu segur/a de voler esborrar el registre ?', function(btn, text) {
                if (btn == 'yes') {
                    var rowEditor = ref.getPlugin();

                    rowEditor.cancelEdit();
                    ref.store.remove(records);
                    me.getGridMarcas().clearStore();
                }
            });
        }
    },

    descargarCSV: function() {
        var record = this.getGridIndicadoresMarcas().getSelectedRow();
        var marcaId = record.data.id;
        var indicadorId = record.data.indicadorId;
        ;
        window.open('/ind/rest/marcas/' + marcaId + '/indicador/'+indicadorId+'/csv');
    },

});