Ext.define('IND.controller.ControllerDashboard',
{
    extend : 'Ext.app.Controller',

    views : [ 'dashboard.PanelDashboard' ],

    init : function()
    {
        this.control(
        {
            'viewport > treepanel' :
            {
                itemclick : this.onTreeItemSelected
            }
        });
    },

    onTreeItemSelected : function(view, node, item, index, event, opts)
    {
        view.up("viewport").addNewTab(node.data.id);
    }
});