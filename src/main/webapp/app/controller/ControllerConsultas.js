Ext.define('IND.controller.ControllerConsultas',
    {
        extend: 'Ext.app.Controller',

        stores: ['Consultas'],
        model: ['Consulta'],

        refs: [
            {
                selector: 'gridConsultasValores',
                ref: 'gridConsultasValores'
            }, {
                selector: 'formConsultas',
                ref: 'formConsultas'
            }],

        init: function () {
            this.control(
                {

                    'formConsultas button[action=exec]': {
                        click: this.executeSql
                    },
                    'gridConsultasValores button[action=download]':{
                        click: this.onClickDownload
                    }
                });

        },
        executeSql: function () {
            var ref = this;

            var sql = ref.getFormConsultas().getForm().getValues();
            var grid = ref.getGridConsultasValores();
            if(sql != "" || sql != null ) {
                Ext.Ajax.request({
                    url: '/ind/rest/consultas/',
                    method: 'POST',
                    params: {
                        sql: sql
                    },
                    success: function (r) {
                        grid.reconfigureColumns(Ext.decode(r.responseText));
                        grid.disableButtons(false);
                    }
                });
            }
        },
        onClickDownload: function () {
            var ref = this;
            ref.getGridConsultasValores().onExportToExcel();
        }

    });