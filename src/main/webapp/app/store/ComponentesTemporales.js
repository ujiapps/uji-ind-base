Ext.define('IND.store.ComponentesTemporales',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ComponenteTemporal',

    autoLoad : true,

    data :
    {
        componenteTemporal : [
        {
            id : '1',
            nombre : 'ANY',
            store : 'IND.store.ValoresInteranualesAnual'
        },
        {
            id : '2',
            nombre : 'CURS',
            store : 'IND.store.ValoresInteranualesCurso'
        },
        {
            id : '3',
            nombre : 'SEMESTRE',
            store : 'IND.store.ValoresInteranualesSemestre'
        },
        {
            id : '4',
            nombre : 'TRIMESTRE',
            store : 'IND.store.ValoresInteranualesTrimestre'
        },
        {
            id : '5',
            nombre : 'QUADRIMESTRE',
            store : 'IND.store.ValoresInteranualesCuatrimestre'
        },
        {
            id : '6',
            nombre : 'MENSUAL',
            store : 'IND.store.ValoresInteranualesMensual'
        },
        {
            id : '7',
            nombre : 'DIARI'
        }]
    },
    proxy :
    {
        type : 'memory',
        reader :
        {
            type : 'json',
            root : 'componenteTemporal'
        }
    }
});