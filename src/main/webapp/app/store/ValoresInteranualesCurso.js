Ext.define('IND.store.ValoresInteranualesCurso',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ValorInteranual',

    autoLoad : true,
    autoSync : false,

    data :
    {
        valoresInteranuales : [
        {
            id : 1,
            nombre : 'Curs'
        } ]
    },

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'valoresInteranuales'
        }
    }
});