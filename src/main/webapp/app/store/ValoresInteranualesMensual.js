Ext.define('IND.store.ValoresInteranualesMensual',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ValorInteranual',

    autoLoad : true,
    autoSync : false,

    data :
    {
        valoresInteranuales : [
        {
            id : 1,
            nombre : 'Gener'
        },
        {
            id : 2,
            nombre : 'Febrer'
        },
        {
            id : 3,
            nombre : 'Març'
        },
        {
            id : 4,
            nombre : 'Abril'
        },
        {
            id : 5,
            nombre : 'Maig'
        },
        {
            id : 6,
            nombre : 'Juny'
        },
        {
            id : 7,
            nombre : 'Juliol'
        },
        {
            id : 8,
            nombre : 'Agost'
        },
        {
            id : 9,
            nombre : 'Setembre'
        },
        {
            id : 10,
            nombre : 'Octubre'
        },
        {
            id : 11,
            nombre : 'Novembre'
        },
        {
            id : 12,
            nombre : 'Decembre'
        } ]
    },

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'valoresInteranuales'
        }
    }
});