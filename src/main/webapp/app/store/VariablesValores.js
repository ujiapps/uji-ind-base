Ext.define('IND.store.VariablesValores',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.VariableValor',

    autoLoad : false,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/variables',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});