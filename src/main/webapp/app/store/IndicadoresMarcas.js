Ext.define('IND.store.IndicadoresMarcas',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.IndicadorMarca',

    autoLoad : false,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/marcas',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    },
    groupers: [

        {
            property: 'fecha',
            direction: 'ASC'
        } ]
});