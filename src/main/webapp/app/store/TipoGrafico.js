Ext.define('IND.store.TipoGrafico',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.TipoGrafico',

    autoLoad : true,

    data :
    {
        tipoGrafico : [
        {
            id : 'COLUMNBASIC',
            nombre : 'Basic Column',
        },
        {
            id : 'LINEBASIC',
            nombre : 'Basic Line',
        }]
    },
    proxy :
    {
        type : 'memory',
        reader :
        {
            type : 'json',
            root : 'tipoGrafico'
        }
    }
});