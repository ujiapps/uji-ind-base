Ext.define('IND.store.ValoresInteranualesCuatrimestre',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ValorInteranual',

    autoLoad : false,
    autoSync : false,

    data :
    {
        valoresInteranuales : [
        {
            id : 1,
            nombre : 'Primer'
        },
        {
            id : 2,
            nombre : 'Segon'
        },
        {
            id : 3,
            nombre : 'Tercer'
        } ]
    },

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'valoresInteranuales'
        }
    }
});