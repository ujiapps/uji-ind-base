Ext.define('IND.store.ValoresInteranualesSemestre',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ValorInteranual',

    autoLoad : true,
    autoSync : false,

    data :
    {
        valoresInteranuales : [
        {
            id : 1,
            nombre : 'Primer'
        },
        {
            id : 2,
            nombre : 'Segon'
        } ]
    },

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'valoresInteranuales'
        }
    }
});