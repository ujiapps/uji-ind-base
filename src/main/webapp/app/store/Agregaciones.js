Ext.define('IND.store.Agregaciones',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.Agregacion',

    autoLoad : false,
    autoSync: true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/agregaciones',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});