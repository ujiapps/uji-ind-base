Ext.define('IND.store.UsosIndicadores',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.UsoIndicador',

    autoLoad : false,
    autoSync : true,

    proxy :
    {
        type : 'rest',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});