Ext.define('IND.store.Indicadores',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.Indicador',

    autoLoad : true,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/indicadores',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});