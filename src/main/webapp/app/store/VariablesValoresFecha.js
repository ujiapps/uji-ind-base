Ext.define('IND.store.VariablesValoresFecha',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.VariableValorFecha',

    autoLoad : false,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/variables',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});