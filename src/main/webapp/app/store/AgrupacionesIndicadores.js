Ext.define('IND.store.AgrupacionesIndicadores',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.AgrupacionIndicador',

    autoLoad : false,
    autoSync : true,

    proxy :
    {
        type : 'rest',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});