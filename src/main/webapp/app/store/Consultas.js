Ext.define('IND.store.Consultas',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.Consulta',

    autoLoad : false,
    autoSync : false,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/consulta',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});