Ext.define('IND.store.Usos',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.Uso',

    autoLoad : true,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/usos',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});