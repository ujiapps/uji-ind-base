Ext.define('IND.store.ValoresInteranuales',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ValorInteranual',

    autoLoad : true,
    autoSync : false,

    data: [],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'data'
        }
    },

    listeners :
    {
        load : function(store)
        {
            store.insert(0, new store.model(
            {
                id : -1,
                nombre : 'Tots'
            }));
        }
    }
});