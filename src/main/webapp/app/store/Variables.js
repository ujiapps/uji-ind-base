Ext.define('IND.store.Variables',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.Variable',

    autoLoad : true,
    autoSync : true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/variables',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});