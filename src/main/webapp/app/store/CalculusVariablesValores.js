Ext.define('IND.store.CalculusVariablesValores',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.CalculusVariableValor',

    autoLoad : false,
    autoSync : false,

    groupField: 'indicadorId',

    proxy :
    {
        type : 'rest',
        url : '',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        }
    }
});