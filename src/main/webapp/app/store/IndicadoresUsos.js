Ext.define('IND.store.IndicadoresUsos',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.IndicadorUso',

    autoLoad : false,
    autoSync : true,

    proxy :
    {
        type : 'rest',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});