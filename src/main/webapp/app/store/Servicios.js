Ext.define('IND.store.Servicios',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.Agregacion',

    autoLoad : true,
    autoSync: true,

    proxy :
    {
        type : 'rest',
        url : '/ind/rest/agregaciones/servicios',

        reader :
        {
            type : 'json',
            successProperty : 'success',
            root : 'data'
        },

        writer :
        {
            type : 'json'
        }
    }
});