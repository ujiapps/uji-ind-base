Ext.define('IND.store.ValoresInteranualesTrimestre',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ValorInteranual',

    autoLoad : true,
    autoSync : false,

    data :
    {
        valoresInteranuales : [
        {
            id : 1,
            nombre : 'Primer'
        },
        {
            id : 2,
            nombre : 'Segon'
        },
        {
            id : 3,
            nombre : 'Tercer'
        },
        {
            id : 4,
            nombre : 'Quart'
        } ]
    },

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'valoresInteranuales'
        }
    }
});