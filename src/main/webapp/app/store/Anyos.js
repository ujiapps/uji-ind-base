Ext.define('IND.store.Anyos',
{
    extend : 'Ext.data.Store',
    model : 'IND.model.ComponenteTemporal',

    autoLoad : false,
    autoSync : false,

    data: [],

    proxy :
    {
        type : 'memory',

        reader :
        {
            type : 'json',
            root : 'data'
        }
    },

    listeners :
    {
        load : function(store)
        {
            store.insert(0, new store.model(
            {
                id : -1,
                nombre : 'Tots'
            }));
        }
    }
});