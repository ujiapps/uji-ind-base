Ext.define('IND.Util',
{
    singleton : true,

    getPlural : function(nombre)
    {
        if (this.isVowel(nombre.slice(-1)))
        {
            return nombre + 's';
        }

        return nombre
    },

    isVowel : function(char)
    {
        return [ 'a', 'e', 'i', 'o', 'u' ].indexOf(char) !== -1
    },

    findByKey : function(array, key, value)
    {
       for (var i = 0; i < array.length ; i++)
       {
           if (array[i][key] == value)
            {
                return array[i];
            }
       }
    },

    toList : function(object)
    {
        if (Object.prototype.toString.call(object) == '[object Array]')
        {
            return object;
        }

        return [object];
    },

    capitaliseFirstLetter: function(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

});
