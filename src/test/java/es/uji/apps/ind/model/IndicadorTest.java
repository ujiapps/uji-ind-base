package es.uji.apps.ind.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;
import es.uji.apps.ind.exceptions.VariablesSinValorException;
import es.uji.apps.ind.formula.VariableValorForCalculusConversor;
import es.uji.apps.ind.model.enums.NivelAgregacion;

import static org.junit.Assert.assertTrue;

public class IndicadorTest
{
    Variable variable1, variable2;
    VariableValorForCalculusConversor variableValorForCalculusConversor = new VariableValorForCalculusConversor();
    List<VariableValorForCalculus> valoresForCalculus = new ArrayList<>();

    @Test
    public void debenCalcularseCorrectamenteLosResultados() throws VariableFormatoIncorrectoException, VariablesSinValorException
    {
        variable1 = buildVariable("A1");
        variable2 = buildVariable("A2");

        addVariableValorToVariable(variable1, buildVariableValor(1L, 1, 1L, new BigDecimal("1.1")));
        addVariableValorToVariable(variable1, buildVariableValor(1L, 2, 3L, new BigDecimal("1")));

        addVariableValorToVariable(variable2, buildVariableValor(1L, 1, 1L, new BigDecimal("1")));
        addVariableValorToVariable(variable2, buildVariableValor(1L, 2, 3L, new BigDecimal("2")));
        addVariableValorToVariable(variable2, buildVariableValor(1L, 3, 1L, new BigDecimal("1")));

        Indicador indicador = new Indicador();
        indicador.setFormula("A1 + A2");

        valoresForCalculus = variableValorForCalculusConversor.add(valoresForCalculus,
                new ArrayList<>(variable1.getValores()), variable1);

        valoresForCalculus = variableValorForCalculusConversor.add(valoresForCalculus,
                new ArrayList<>(variable2.getValores()), variable2);

        List<VariableValor> resultado = indicador.calcula(valoresForCalculus);

        assertTrue(resultado.size() == 3);
        assertTrue(get(resultado, 1L, 1, 1L).getValor().equals(new BigDecimal("2.1")));
        assertTrue(get(resultado, 1L, 2, 3L).getValor().equals(new BigDecimal("3.0")));
        assertTrue(get(resultado, 1L, 3, 1L).getValor() == null);
    }

    private VariableValor get(List<VariableValor> valores, Long agregacion, Integer anyo,
            Long mes)
    {
        for (VariableValor valor : valores)
        {
            if (valor.getAgregacion().equals(agregacion) && valor.getAnyo().equals(anyo)
                    && valor.getValorInteranual().equals(mes))
            {
                return valor;
            }
        }

        return null;
    }

    private Variable buildVariable(String codigo) throws VariableFormatoIncorrectoException
    {
        Variable variable = new Variable();

        variable.setComponenteTemporal(1);
        variable.setNivelAgregacion(NivelAgregacion.CENTRE.toString());
        variable.setCodigo(codigo);

        return variable;
    }

    private VariableValor buildVariableValor(Long agregacion, Integer anyo, Long mes,
            BigDecimal valor)
    {
        VariableValor variableValor = new VariableValor();

        variableValor.setAgregacion(agregacion);
        variableValor.setAnyo(anyo);
        variableValor.setValorInteranual(mes);
        variableValor.setValor(valor);

        return variableValor;
    }

    private void addVariableValorToVariable(Variable variable, VariableValor variableValor)
    {
        Set<VariableValor> valores = variable.getValores();

        if (valores == null)
        {
            valores = new HashSet<>();
        }

        valores.add(variableValor);
        variable.setValores(valores);
        variableValor.setVariable(variable);
    }
}
