package es.uji.apps.ind.formula;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VariableExtractorTest
{
    VariableExtractor variableExtractor = new VariableExtractor();

    @Test
    public void elExtractorDeVariablesDeberiaExtraerTodasLasVariablesDeLaExpresion()
    {
        String expresion = "E2 + ae12 * (7 / base2) + e22";

        List<String> variables = variableExtractor.extract(expresion);

        assertEquals(4, variables.size());

        assertEquals("E2", variables.get(0));
        assertEquals("ae12", variables.get(1));
        assertEquals("base2", variables.get(2));
        assertEquals("e22", variables.get(3));
    }
}
