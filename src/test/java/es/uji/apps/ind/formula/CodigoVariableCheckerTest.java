package es.uji.apps.ind.formula;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.uji.apps.ind.exceptions.VariableFormatoIncorrectoException;

public class CodigoVariableCheckerTest
{
    private CodigoVariableChecker codigoVariableChecker;

    public CodigoVariableCheckerTest()
    {
        codigoVariableChecker = new CodigoVariableChecker();
    }

    @Test(expected = VariableFormatoIncorrectoException.class)
    public void siElFormatoDelCodigoDeLaVariableEsIncorrectoSeDebeLanzarUnaExcepcion()
            throws VariableFormatoIncorrectoException
    {
        codigoVariableChecker.check("1a");
    }

    @Test
    public void formulaConCaracteresPermitidos() throws VariableFormatoIncorrectoException
    {
        codigoVariableChecker.check("a123");
    }
}
