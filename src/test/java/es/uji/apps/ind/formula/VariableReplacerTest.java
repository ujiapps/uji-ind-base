package es.uji.apps.ind.formula;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import es.uji.apps.ind.exceptions.VariablesSinValorException;

import static org.junit.Assert.assertEquals;

public class VariableReplacerTest
{
    VariableReplacer variableReplacer = new VariableReplacer();

    @Test
    public void elExtractorDeVariablesDeberiaExtraerTodasLasVariablesDeLaExpresion()

    {
        String expresion = "e1 + ae12 * (7 / base2_2) + e1";

        Map<String, BigDecimal> valores = new HashMap<>();
        valores.put("e1", new BigDecimal("1"));
        valores.put("ae12", new BigDecimal("2"));
        valores.put("base2_2", new BigDecimal("3.3"));

        try
        {
            Assert.assertEquals("1 + 2 * (7 / 3.3) + 1",
                    variableReplacer.replace(expresion, valores));
        }
        catch (VariablesSinValorException e)
        {
            Assert.fail();
        }
    }

    @Test(expected=VariablesSinValorException.class)
    public void debeDevolverVacioSiUnaVariableNoTieneValor()throws VariablesSinValorException
    {
        String expresion = "e1 + ae12 * (7 / base2_2) + e1";

        Map<String, BigDecimal> valores = new HashMap<>();
        valores.put("e1", new BigDecimal("1"));
        valores.put("ae12", new BigDecimal("2"));

            Assert.assertNull(variableReplacer.replace(expresion, valores));
    }

    @Test(expected=VariablesSinValorException.class)
    public void debeDevolverVacioSiUnaVariableTieneValorANulo() throws VariablesSinValorException
    {
        String expresion = "e1 + ae12 * (7 / base2_2) + e1";

        Map<String, BigDecimal> valores = new HashMap<>();
        valores.put("e1", new BigDecimal("1"));
        valores.put("ae12", new BigDecimal("2"));
        valores.put("base2_2", null);

        Assert.assertNull(variableReplacer.replace(expresion, valores));
    }
}
