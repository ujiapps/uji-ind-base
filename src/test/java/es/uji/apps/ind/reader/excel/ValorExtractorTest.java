package es.uji.apps.ind.reader.excel;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import es.uji.apps.ind.excelReader.ValorExtractor;
import es.uji.apps.ind.excelReader.ValorExtractorFromXLS;
import es.uji.apps.ind.excelReader.ValorExtractorFromXLSX;
import es.uji.apps.ind.model.VariableValor;

public class ValorExtractorTest
{
    @Test
    public void debeExtraerLosDatosCorrectamenteEnUnFicheroXLS() throws IOException
    {
        ValorExtractor valorExtractor = new ValorExtractorFromXLS();
        
        List<VariableValor> valores = valorExtractor.extract(new FileInputStream(
                "src/test/resources/VariablesValores.xls"));

        Assert.assertTrue(valores.size() == 4);

        Assert.assertEquals(new Long(1), valores.get(0).getAgregacion());
        Assert.assertTrue(2001 == valores.get(0).getAnyo());
        Assert.assertTrue(1 == valores.get(0).getValorInteranual());
        Assert.assertEquals(new BigDecimal("10.0"), valores.get(0).getValor());

        Assert.assertEquals(new Long(3), valores.get(2).getAgregacion());
        Assert.assertTrue(2003 == valores.get(2).getAnyo());
        Assert.assertTrue(3 == valores.get(2).getValorInteranual());
        Assert.assertEquals(new BigDecimal("30.2"), valores.get(2).getValor());

        Assert.assertEquals(new Long(4), valores.get(3).getAgregacion());
        Assert.assertTrue(2004 == valores.get(3).getAnyo());
        Assert.assertTrue(4 == valores.get(3).getValorInteranual());
        Assert.assertEquals(new BigDecimal("40.2"), valores.get(3).getValor());
    }

    @Test
    public void debeExtraerLosDatosCorrectamenteEnUnFicheroXLSX() throws IOException
    {
        ValorExtractor valorExtractor = new ValorExtractorFromXLSX();
        
        List<VariableValor> valores = valorExtractor.extract(new FileInputStream(
                "src/test/resources/VariablesValores.xlsx"));

        Assert.assertTrue(valores.size() == 3);

        Assert.assertEquals(new Long(1), valores.get(0).getAgregacion());
        Assert.assertTrue(2001 == valores.get(0).getAnyo());
        Assert.assertTrue(1 == valores.get(0).getValorInteranual());
        Assert.assertEquals(new BigDecimal("10.0"), valores.get(0).getValor());
    }
}
